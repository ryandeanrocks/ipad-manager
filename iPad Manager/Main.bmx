'This BMX file was edited with BLIde ( http://www.blide.org )
If dm = False
	If prompt("Password: ", 0, 1, true) <> password
		Notify "You must enter the correct password for " + ProgramInfo + " to work. Please ask IT for assistance.", 1
		End
	End If
EndIf
Local mainWin:MainWindow = New MainWindow
mainWin._init()

While True
	WaitEvent()
	If mainWin._update(CurrentEvent) Return
	If debugWin.handle <> Null
		debugWin._update()
	EndIf
	For Local i:iPad = EachIn ipadList
		i._update()
	Next
	For Local s:Student = EachIn studentList
		s._update()
	Next
	For Local e:Note = EachIn noteWinList
		e._update()
	Next
	
Wend

mainWin._end()
debugWin._end()
End

Function prompt:String(line:String, charLimit = 0, supress = 0, password = False)
	Local pwin:Window = New Window
	pwin._new(0, 0, 215, 65, line, winStyleDefault - WINDOW_STATUS - WINDOW_MENU)
	Local TF:TextField = newTextField(10, 10, 150, 20, pwin, password)
	Local ab:Button = newButton("Go", 165, 10, 40, 20, pwin)
	pwin._show()
	ActivateGadget(TF.handle)
	While True
		WaitEvent()
		Select EventSource()
			Case TF.handle
				If charLimit <> 0
					If Len(GadgetText(TF.handle)) > charLimit
						SetGadgetText(TF.handle, Left(GadgetText(TF.handle), charLimit))
					End If
				End If
			Case ab.handle
				Local conf:Int
				If quickMode Or supress
					conf = 1
				Else
					conf = Confirm("Are you sure? Remember that there is no way to undo what you are about to do.")
				End If
				If conf
					Local rline:String = GadgetText(tf.handle)
					pwin._hide()
					FreeGadget(ab.handle)
					FreeGadget(TF.handle)
					FreeGadget(pwin.handle)
					TF = Null
					ab = Null
					pwin = Null
					Return rline
				End If
			Case pwin.handle
				If EventID() = EVENT_WINDOWCLOSE
					pwin._hide()
					FreeGadget(ab.handle)
					FreeGadget(TF.handle)
					FreeGadget(pwin.handle)
					TF = Null
					ab = Null
					pwin = Null
					Return ""
				End If
		End Select
	WEnd
End Function
Function convertFromCSV:TList (line:String)
	Local lineArray:TList = New TList
	Local word:String
	Local letter:String
	Local i:Int = Len(line)
	Repeat
		letter = Left(line, 1)
		If letter = ","
			If word = "eoe"
				Exit
			End If
			ListAddLast(lineArray, word)
			word = ""
		EndIf
		If letter <> ","
			word = word + letter
		EndIf
		i = i - 1
		line = Right(line, i)
	Until line = ""
	Return lineArray
End Function

Function convertFromSSV:TList (line:String)
	Local lineArray:TList = New TList
	Local word:String
	Local letter:String
	Local i:Int = Len(line)
	Repeat
		letter = Left(line, 1)
		If letter = ";"
			ListAddLast(lineArray, word)
			word = ""
		EndIf
		If letter <> ";"
			word = word + letter
		EndIf
		i = i - 1
		line = Right(line, i)
	Until line = ""
	If word = "eon"
		Return lineArray
	End If
	dlog "The note data is corrupt [" + lineArray.ToString() + "]", 2
	Return lineArray
End Function

Function resetFile:TStream(file:TStream, filen:String)
	CloseFile(file)
	file = OpenFile(filen)
	Return file
End Function
