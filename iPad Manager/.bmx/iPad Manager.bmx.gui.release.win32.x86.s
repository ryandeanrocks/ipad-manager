	format	MS COFF
	extrn	___bb_appstub_appstub
	extrn	___bb_audio_audio
	extrn	___bb_basic_basic
	extrn	___bb_blitz_blitz
	extrn	___bb_bmploader_bmploader
	extrn	___bb_d3d7max2d_d3d7max2d
	extrn	___bb_d3d9max2d_d3d9max2d
	extrn	___bb_data_data
	extrn	___bb_directsoundaudio_directsoundaudio
	extrn	___bb_drivers_drivers
	extrn	___bb_eventqueue_eventqueue
	extrn	___bb_freeaudioaudio_freeaudioaudio
	extrn	___bb_freejoy_freejoy
	extrn	___bb_freeprocess_freeprocess
	extrn	___bb_freetypefont_freetypefont
	extrn	___bb_glew_glew
	extrn	___bb_gnet_gnet
	extrn	___bb_jpgloader_jpgloader
	extrn	___bb_macos_macos
	extrn	___bb_maxlua_maxlua
	extrn	___bb_maxutil_maxutil
	extrn	___bb_oggloader_oggloader
	extrn	___bb_openalaudio_openalaudio
	extrn	___bb_pngloader_pngloader
	extrn	___bb_retro_retro
	extrn	___bb_tgaloader_tgaloader
	extrn	___bb_threads_threads
	extrn	___bb_timer_timer
	extrn	___bb_wavloader_wavloader
	extrn	_bbEmptyString
	extrn	_bbEnd
	extrn	_bbGCFree
	extrn	_bbNullObject
	extrn	_bbObjectClass
	extrn	_bbObjectCompare
	extrn	_bbObjectCtor
	extrn	_bbObjectDowncast
	extrn	_bbObjectFree
	extrn	_bbObjectNew
	extrn	_bbObjectRegisterType
	extrn	_bbObjectReserved
	extrn	_bbObjectSendMessage
	extrn	_bbObjectToString
	extrn	_bbStringClass
	extrn	_bbStringCompare
	extrn	_bbStringConcat
	extrn	_bbStringFromFloat
	extrn	_bbStringFromInt
	extrn	_bbStringToInt
	extrn	_brl_eventqueue_CurrentEvent
	extrn	_brl_eventqueue_EventID
	extrn	_brl_eventqueue_EventSource
	extrn	_brl_eventqueue_WaitEvent
	extrn	_brl_filesystem_CloseFile
	extrn	_brl_filesystem_OpenFile
	extrn	_brl_filesystem_WriteFile
	extrn	_brl_linkedlist_ListAddLast
	extrn	_brl_linkedlist_ListContains
	extrn	_brl_linkedlist_ListRemove
	extrn	_brl_linkedlist_TList
	extrn	_brl_retro_Left
	extrn	_brl_retro_Right
	extrn	_brl_stream_Eof
	extrn	_brl_stream_ReadLine
	extrn	_brl_stream_WriteLine
	extrn	_brl_system_Confirm
	extrn	_brl_system_CurrentDate
	extrn	_brl_system_CurrentTime
	extrn	_brl_system_Notify
	extrn	_maxgui_maxgui_ActivateGadget
	extrn	_maxgui_maxgui_ActiveGadget
	extrn	_maxgui_maxgui_AddGadgetItem
	extrn	_maxgui_maxgui_ButtonState
	extrn	_maxgui_maxgui_CreateButton
	extrn	_maxgui_maxgui_CreateLabel
	extrn	_maxgui_maxgui_CreateListBox
	extrn	_maxgui_maxgui_CreateMenu
	extrn	_maxgui_maxgui_CreateTextArea
	extrn	_maxgui_maxgui_CreateTextField
	extrn	_maxgui_maxgui_CreateWindow
	extrn	_maxgui_maxgui_FreeGadget
	extrn	_maxgui_maxgui_GadgetItemText
	extrn	_maxgui_maxgui_GadgetText
	extrn	_maxgui_maxgui_GadgetWidth
	extrn	_maxgui_maxgui_HideGadget
	extrn	_maxgui_maxgui_ModifyGadgetItem
	extrn	_maxgui_maxgui_PopupWindowMenu
	extrn	_maxgui_maxgui_RemoveGadgetItem
	extrn	_maxgui_maxgui_SelectedGadgetItem
	extrn	_maxgui_maxgui_SetButtonState
	extrn	_maxgui_maxgui_SetGadgetFilter
	extrn	_maxgui_maxgui_SetGadgetText
	extrn	_maxgui_maxgui_ShowGadget
	extrn	_maxgui_maxgui_TGadget
	extrn	_maxgui_maxgui_TextAreaText
	extrn	_maxgui_maxgui_TextFieldText
	extrn	_maxgui_maxgui_UpdateWindowMenu
	extrn	_maxgui_maxgui_WindowMenu
	public	__bb_Button_Delete
	public	__bb_Button_New
	public	__bb_Button__cstate
	public	__bb_Button__init
	public	__bb_Button__state
	public	__bb_ComboBox_Delete
	public	__bb_ComboBox_New
	public	__bb_ListBox_Delete
	public	__bb_ListBox_New
	public	__bb_ListBox__add
	public	__bb_ListBox__getText
	public	__bb_ListBox__init
	public	__bb_ListBox__modify
	public	__bb_ListBox__remove
	public	__bb_MainWindow_Delete
	public	__bb_MainWindow_New
	public	__bb_MainWindow__end
	public	__bb_MainWindow__init
	public	__bb_MainWindow__populate
	public	__bb_MainWindow__update
	public	__bb_Note_Delete
	public	__bb_Note_New
	public	__bb_Note__hide
	public	__bb_Note__init
	public	__bb_Note__save
	public	__bb_Note__typeByInt
	public	__bb_Note__update
	public	__bb_Student_Delete
	public	__bb_Student_New
	public	__bb_Student__addNote
	public	__bb_Student__build
	public	__bb_Student__hide
	public	__bb_Student__init
	public	__bb_Student__noteBySummary
	public	__bb_Student__save
	public	__bb_Student__show
	public	__bb_Student__update
	public	__bb_TextArea_Delete
	public	__bb_TextArea_New
	public	__bb_TextArea__clear
	public	__bb_TextArea__getText
	public	__bb_TextArea__init
	public	__bb_TextField_Delete
	public	__bb_TextField_New
	public	__bb_TextField__clear
	public	__bb_TextField__getText
	public	__bb_TextField__init
	public	__bb_Window_Delete
	public	__bb_Window_New
	public	__bb_Window__hide
	public	__bb_Window__new
	public	__bb_Window__show
	public	__bb_Window__update
	public	__bb_iPad_Delete
	public	__bb_iPad_New
	public	__bb_iPad__addNote
	public	__bb_iPad__build
	public	__bb_iPad__hide
	public	__bb_iPad__init
	public	__bb_iPad__noteBySummary
	public	__bb_iPad__save
	public	__bb_iPad__show
	public	__bb_iPad__update
	public	__bb_logWin_Delete
	public	__bb_logWin_New
	public	__bb_logWin__end
	public	__bb_logWin__init
	public	__bb_logWin__new
	public	__bb_logWin__populate
	public	__bb_logWin__update
	public	__bb_main
	public	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Application
	public	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete
	public	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New
	public	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Resources
	public	__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete
	public	__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New
	public	__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_Delete
	public	__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_New
	public	_bb_Button
	public	_bb_ComboBox
	public	_bb_ListBox
	public	_bb_MainWindow
	public	_bb_Note
	public	_bb_Student
	public	_bb_TextArea
	public	_bb_TextField
	public	_bb_Window
	public	_bb_convertFromCSV
	public	_bb_convertFromSSV
	public	_bb_dataDir
	public	_bb_debugWin
	public	_bb_dlog
	public	_bb_findByBC
	public	_bb_findByFirst
	public	_bb_findByLast
	public	_bb_findBySN
	public	_bb_iPad
	public	_bb_iPaddb
	public	_bb_insertIpadEntry
	public	_bb_ipadList
	public	_bb_ipaddbn
	public	_bb_logWin
	public	_bb_newBlankIpadBC
	public	_bb_newBlankIpadSN
	public	_bb_newBlankStudent
	public	_bb_newButton
	public	_bb_newIpad
	public	_bb_newListBox
	public	_bb_newNote
	public	_bb_newStudent
	public	_bb_newTextArea
	public	_bb_newTextField
	public	_bb_noComma
	public	_bb_noteWinList
	public	_bb_prompt
	public	_bb_quickMode
	public	_bb_readNote
	public	_bb_resetFile
	public	_bb_studdb
	public	_bb_studdbn
	public	_bb_studentList
	public	_bb_winList
	section	"code" code
__bb_main:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	cmp	dword [_863],0
	je	_864
	mov	eax,0
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_864:
	mov	dword [_863],1
	call	___bb_blitz_blitz
	call	___bb_drivers_drivers
	call	___bb_appstub_appstub
	call	___bb_audio_audio
	call	___bb_basic_basic
	call	___bb_bmploader_bmploader
	call	___bb_d3d7max2d_d3d7max2d
	call	___bb_d3d9max2d_d3d9max2d
	call	___bb_data_data
	call	___bb_directsoundaudio_directsoundaudio
	call	___bb_eventqueue_eventqueue
	call	___bb_freeaudioaudio_freeaudioaudio
	call	___bb_freetypefont_freetypefont
	call	___bb_gnet_gnet
	call	___bb_jpgloader_jpgloader
	call	___bb_maxlua_maxlua
	call	___bb_maxutil_maxutil
	call	___bb_oggloader_oggloader
	call	___bb_openalaudio_openalaudio
	call	___bb_pngloader_pngloader
	call	___bb_retro_retro
	call	___bb_tgaloader_tgaloader
	call	___bb_threads_threads
	call	___bb_timer_timer
	call	___bb_wavloader_wavloader
	call	___bb_freejoy_freejoy
	call	___bb_freeprocess_freeprocess
	call	___bb_glew_glew
	call	___bb_macos_macos
	push	_28
	call	_bbObjectRegisterType
	add	esp,4
	push	_29
	call	_bbObjectRegisterType
	add	esp,4
	push	_35
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Window
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Button
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_TextField
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_ComboBox
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_ListBox
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_iPad
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Note
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Student
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_logWin
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_MainWindow
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_TextArea
	call	_bbObjectRegisterType
	add	esp,4
	mov	eax,dword [_800]
	and	eax,1
	cmp	eax,0
	jne	_801
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_winList],eax
	or	dword [_800],1
_801:
	mov	eax,dword [_800]
	and	eax,2
	cmp	eax,0
	jne	_803
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_ipadList],eax
	or	dword [_800],2
_803:
	mov	eax,dword [_800]
	and	eax,4
	cmp	eax,0
	jne	_805
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_studentList],eax
	or	dword [_800],4
_805:
	mov	eax,dword [_800]
	and	eax,8
	cmp	eax,0
	jne	_807
	push	_bb_logWin
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_debugWin],eax
	or	dword [_800],8
_807:
	mov	eax,dword [_bb_debugWin]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	push	0
	push	_809
	call	_bb_dlog
	add	esp,8
	mov	eax,dword [_800]
	and	eax,16
	cmp	eax,0
	jne	_811
	push	_39
	push	dword [_bb_dataDir]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_ipaddbn],eax
	or	dword [_800],16
_811:
	mov	eax,dword [_800]
	and	eax,32
	cmp	eax,0
	jne	_813
	push	1
	push	1
	push	dword [_bb_ipaddbn]
	call	_brl_filesystem_OpenFile
	add	esp,12
	inc	dword [eax+4]
	mov	dword [_bb_iPaddb],eax
	or	dword [_800],32
_813:
	cmp	dword [_bb_iPaddb],_bbNullObject
	jne	_814
	push	dword [_bb_ipaddbn]
	call	_brl_filesystem_WriteFile
	add	esp,4
	push	1
	push	1
	push	dword [_bb_ipaddbn]
	call	_brl_filesystem_OpenFile
	add	esp,12
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_818
	push	eax
	call	_bbGCFree
	add	esp,4
_818:
	mov	dword [_bb_iPaddb],ebx
_814:
	mov	eax,dword [_800]
	and	eax,64
	cmp	eax,0
	jne	_820
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_noteWinList],eax
	or	dword [_800],64
_820:
	mov	eax,dword [_800]
	and	eax,128
	cmp	eax,0
	jne	_822
	push	_169
	push	dword [_bb_dataDir]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_studdbn],eax
	or	dword [_800],128
_822:
	mov	eax,dword [_800]
	and	eax,256
	cmp	eax,0
	jne	_824
	push	1
	push	1
	push	dword [_bb_studdbn]
	call	_brl_filesystem_OpenFile
	add	esp,12
	inc	dword [eax+4]
	mov	dword [_bb_studdb],eax
	or	dword [_800],256
_824:
	cmp	dword [_bb_studdb],_bbNullObject
	jne	_825
	push	dword [_bb_studdbn]
	call	_brl_filesystem_WriteFile
	add	esp,4
	push	1
	push	1
	push	dword [_bb_studdbn]
	call	_brl_filesystem_OpenFile
	add	esp,12
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_studdb]
	dec	dword [eax+4]
	jnz	_829
	push	eax
	call	_bbGCFree
	add	esp,4
_829:
	mov	dword [_bb_studdb],ebx
_825:
	xor	eax,eax
	cmp	eax,0
	je	_830
	push	_36
	push	1
	push	1
	push	0
	push	_238
	call	_bb_prompt
	add	esp,16
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_831
	push	1
	push	_833
	call	_brl_system_Notify
	add	esp,8
	call	_bbEnd
_831:
_830:
	push	_bb_MainWindow
	call	_bbObjectNew
	add	esp,4
	mov	edi,eax
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	jmp	_241
_243:
	call	_brl_eventqueue_WaitEvent
	mov	eax,edi
	push	dword [_brl_eventqueue_CurrentEvent]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,8
	cmp	eax,0
	je	_837
	mov	eax,0
	jmp	_433
_837:
	mov	eax,dword [_bb_debugWin]
	cmp	dword [eax+8],_bbNullObject
	je	_838
	mov	eax,dword [_bb_debugWin]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
_838:
	mov	esi,dword [_bb_ipadList]
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_244
_246:
	mov	eax,ebx
	push	_bb_iPad
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_244
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+72]
	add	esp,4
_244:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_246
_245:
	mov	esi,dword [_bb_studentList]
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_247
_249:
	mov	eax,ebx
	push	_bb_Student
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_247
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+72]
	add	esp,4
_247:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_249
_248:
	mov	esi,dword [_bb_noteWinList]
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_250
_252:
	mov	eax,ebx
	push	_bb_Note
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_250
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
_250:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_252
_251:
_241:
	mov	eax,1
	cmp	eax,0
	jne	_243
_242:
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
	mov	eax,dword [_bb_debugWin]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+64]
	add	esp,4
	call	_bbEnd
	mov	eax,0
	jmp	_433
_433:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_28
	mov	eax,0
	jmp	_436
_436:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_Delete:
	push	ebp
	mov	ebp,esp
_439:
	mov	eax,0
	jmp	_865
_865:
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_29
	mov	eax,0
	jmp	_442
_442:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete:
	push	ebp
	mov	ebp,esp
_445:
	mov	eax,0
	jmp	_866
_866:
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_35
	mov	eax,0
	jmp	_448
_448:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete:
	push	ebp
	mov	ebp,esp
_451:
	mov	eax,0
	jmp	_867
_867:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Window_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_Window
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+12],eax
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [ebx+16],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+20],eax
	mov	eax,0
	jmp	_454
_454:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Window_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_457:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_874
	push	eax
	call	_bbGCFree
	add	esp,4
_874:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_876
	push	eax
	call	_bbGCFree
	add	esp,4
_876:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_878
	push	eax
	call	_bbGCFree
	add	esp,4
_878:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_880
	push	eax
	call	_bbGCFree
	add	esp,4
_880:
	mov	eax,0
	jmp	_872
_872:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Window__new:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	esi,dword [ebp+12]
	mov	ecx,dword [ebp+16]
	mov	edx,dword [ebp+20]
	mov	eax,dword [ebp+24]
	mov	edi,dword [ebp+28]
	push	dword [ebp+32]
	push	_bbNullObject
	push	eax
	push	edx
	push	ecx
	push	esi
	push	edi
	call	_maxgui_maxgui_CreateWindow
	add	esp,28
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_884
	push	eax
	call	_bbGCFree
	add	esp,4
_884:
	mov	dword [ebx+8],esi
	push	ebx
	push	dword [_bb_winList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	dword [ebx+8]
	call	_maxgui_maxgui_WindowMenu
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_888
	push	eax
	call	_bbGCFree
	add	esp,4
_888:
	mov	dword [ebx+20],esi
	mov	eax,0
	jmp	_466
_466:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Window__show:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_ShowGadget
	add	esp,4
	mov	eax,0
	jmp	_469
_469:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Window__hide:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	mov	eax,0
	jmp	_472
_472:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Window__update:
	push	ebp
	mov	ebp,esp
	mov	eax,0
	jmp	_475
_475:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Button_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_Button
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	dword [ebx+12],0
	mov	eax,0
	jmp	_478
_478:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Button_Delete:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_481:
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_892
	push	eax
	call	_bbGCFree
	add	esp,4
_892:
	mov	eax,0
	jmp	_890
_890:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Button__init:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	ecx,dword [ebp+16]
	mov	edx,dword [ebp+20]
	mov	eax,dword [ebp+24]
	mov	edi,dword [ebp+28]
	mov	esi,dword [ebp+36]
	push	esi
	mov	esi,dword [ebp+32]
	push	dword [esi+8]
	push	edi
	push	eax
	push	edx
	push	ecx
	push	dword [ebp+12]
	call	_maxgui_maxgui_CreateButton
	add	esp,28
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_896
	push	eax
	call	_bbGCFree
	add	esp,4
_896:
	mov	dword [ebx+8],esi
	push	dword [ebx+8]
	mov	eax,dword [ebp+32]
	push	dword [eax+16]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	eax,0
	jmp	_491
_491:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Button__state:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	mov	eax,dword [ebx+12]
	cmp	eax,0
	sete	al
	movzx	eax,al
	mov	dword [ebx+12],eax
	push	dword [ebx+12]
	push	dword [ebx+8]
	call	_maxgui_maxgui_SetButtonState
	add	esp,8
	push	dword [ebx+8]
	call	_maxgui_maxgui_ButtonState
	add	esp,4
	jmp	_494
_494:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Button__cstate:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+12]
	jmp	_497
_497:
	mov	esp,ebp
	pop	ebp
	ret
_bb_newButton:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+12]
	mov	edi,dword [ebp+16]
	push	_bb_Button
	call	_bbObjectNew
	add	esp,4
	mov	ebx,eax
	mov	eax,ebx
	push	dword [ebp+32]
	push	dword [ebp+28]
	push	dword [ebp+24]
	push	dword [ebp+20]
	push	edi
	push	esi
	push	dword [ebp+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,32
	mov	eax,ebx
	jmp	_506
_506:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextField_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_TextField
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	eax,0
	jmp	_509
_509:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextField_Delete:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_512:
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_902
	push	eax
	call	_bbGCFree
	add	esp,4
_902:
	mov	eax,0
	jmp	_900
_900:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextField__init:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	ecx,dword [ebp+12]
	mov	edx,dword [ebp+16]
	mov	eax,dword [ebp+20]
	mov	edi,dword [ebp+24]
	mov	esi,dword [ebp+32]
	push	esi
	mov	esi,dword [ebp+28]
	push	dword [esi+8]
	push	edi
	push	eax
	push	edx
	push	ecx
	call	_maxgui_maxgui_CreateTextField
	add	esp,24
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_906
	push	eax
	call	_bbGCFree
	add	esp,4
_906:
	mov	dword [ebx+8],esi
	push	dword [ebx+8]
	mov	eax,dword [ebp+28]
	push	dword [eax+16]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	eax,0
	jmp	_521
_521:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextField__getText:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_TextFieldText
	add	esp,4
	jmp	_524
_524:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextField__clear:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	_1
	push	dword [eax+8]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	mov	eax,0
	jmp	_527
_527:
	mov	esp,ebp
	pop	ebp
	ret
_bb_newTextField:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+12]
	mov	edi,dword [ebp+16]
	push	_bb_TextField
	call	_bbObjectNew
	add	esp,4
	mov	ebx,eax
	mov	eax,ebx
	push	dword [ebp+28]
	push	dword [ebp+24]
	push	dword [ebp+20]
	push	edi
	push	esi
	push	dword [ebp+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,28
	mov	eax,ebx
	jmp	_535
_535:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ComboBox_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_ComboBox
	mov	eax,0
	jmp	_538
_538:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ComboBox_Delete:
	push	ebp
	mov	ebp,esp
_541:
	mov	eax,0
	jmp	_909
_909:
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_ListBox
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	eax,0
	jmp	_544
_544:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox_Delete:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_547:
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_913
	push	eax
	call	_bbGCFree
	add	esp,4
_913:
	mov	eax,0
	jmp	_911
_911:
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox__init:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	ecx,dword [ebp+12]
	mov	edx,dword [ebp+16]
	mov	eax,dword [ebp+20]
	mov	edi,dword [ebp+24]
	mov	esi,dword [ebp+32]
	push	esi
	mov	esi,dword [ebp+28]
	push	dword [esi+8]
	push	edi
	push	eax
	push	edx
	push	ecx
	call	_maxgui_maxgui_CreateListBox
	add	esp,24
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_917
	push	eax
	call	_bbGCFree
	add	esp,4
_917:
	mov	dword [ebx+8],esi
	push	dword [ebx+8]
	mov	eax,dword [ebp+28]
	push	dword [eax+16]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	eax,0
	jmp	_556
_556:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox__add:
	push	ebp
	mov	ebp,esp
	mov	ecx,dword [ebp+8]
	mov	edx,dword [ebp+12]
	mov	eax,dword [ebp+16]
	push	_bbNullObject
	push	_1
	push	-1
	push	eax
	push	edx
	push	dword [ecx+8]
	call	_maxgui_maxgui_AddGadgetItem
	add	esp,24
	mov	eax,0
	jmp	_561
_561:
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox__modify:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	mov	eax,dword [ebp+12]
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	eax
	push	dword [ebx+8]
	call	_maxgui_maxgui_SelectedGadgetItem
	add	esp,4
	push	eax
	push	dword [ebx+8]
	call	_maxgui_maxgui_ModifyGadgetItem
	add	esp,28
	mov	eax,0
	jmp	_565
_565:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox__remove:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	dword [ebx+8]
	call	_maxgui_maxgui_SelectedGadgetItem
	add	esp,4
	push	eax
	push	dword [ebx+8]
	call	_maxgui_maxgui_RemoveGadgetItem
	add	esp,8
	mov	eax,0
	jmp	_568
_568:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox__getText:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	dword [ebx+8]
	call	_maxgui_maxgui_SelectedGadgetItem
	add	esp,4
	push	eax
	push	dword [ebx+8]
	call	_maxgui_maxgui_GadgetItemText
	add	esp,8
	jmp	_571
_571:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newListBox:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+12]
	mov	edi,dword [ebp+16]
	push	_bb_ListBox
	call	_bbObjectNew
	add	esp,4
	mov	ebx,eax
	mov	eax,ebx
	push	dword [ebp+28]
	push	dword [ebp+24]
	push	dword [ebp+20]
	push	edi
	push	esi
	push	dword [ebp+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,28
	mov	eax,ebx
	jmp	_579
_579:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_iPad
	push	_bb_Window
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	dword [ebx+12],0
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+16],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+20],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+24],eax
	mov	dword [ebx+28],0
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+32],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+36],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+40],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+44],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+48],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+52],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+56],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+60],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+64],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+68],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+72],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+76],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+80],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+84],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+88],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+92],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+96],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+100],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+104],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+108],eax
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [ebx+112],eax
	mov	eax,0
	jmp	_582
_582:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_585:
	mov	eax,dword [ebx+112]
	dec	dword [eax+4]
	jnz	_947
	push	eax
	call	_bbGCFree
	add	esp,4
_947:
	mov	eax,dword [ebx+108]
	dec	dword [eax+4]
	jnz	_949
	push	eax
	call	_bbGCFree
	add	esp,4
_949:
	mov	eax,dword [ebx+104]
	dec	dword [eax+4]
	jnz	_951
	push	eax
	call	_bbGCFree
	add	esp,4
_951:
	mov	eax,dword [ebx+100]
	dec	dword [eax+4]
	jnz	_953
	push	eax
	call	_bbGCFree
	add	esp,4
_953:
	mov	eax,dword [ebx+96]
	dec	dword [eax+4]
	jnz	_955
	push	eax
	call	_bbGCFree
	add	esp,4
_955:
	mov	eax,dword [ebx+92]
	dec	dword [eax+4]
	jnz	_957
	push	eax
	call	_bbGCFree
	add	esp,4
_957:
	mov	eax,dword [ebx+88]
	dec	dword [eax+4]
	jnz	_959
	push	eax
	call	_bbGCFree
	add	esp,4
_959:
	mov	eax,dword [ebx+84]
	dec	dword [eax+4]
	jnz	_961
	push	eax
	call	_bbGCFree
	add	esp,4
_961:
	mov	eax,dword [ebx+80]
	dec	dword [eax+4]
	jnz	_963
	push	eax
	call	_bbGCFree
	add	esp,4
_963:
	mov	eax,dword [ebx+76]
	dec	dword [eax+4]
	jnz	_965
	push	eax
	call	_bbGCFree
	add	esp,4
_965:
	mov	eax,dword [ebx+72]
	dec	dword [eax+4]
	jnz	_967
	push	eax
	call	_bbGCFree
	add	esp,4
_967:
	mov	eax,dword [ebx+68]
	dec	dword [eax+4]
	jnz	_969
	push	eax
	call	_bbGCFree
	add	esp,4
_969:
	mov	eax,dword [ebx+64]
	dec	dword [eax+4]
	jnz	_971
	push	eax
	call	_bbGCFree
	add	esp,4
_971:
	mov	eax,dword [ebx+60]
	dec	dword [eax+4]
	jnz	_973
	push	eax
	call	_bbGCFree
	add	esp,4
_973:
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_975
	push	eax
	call	_bbGCFree
	add	esp,4
_975:
	mov	eax,dword [ebx+52]
	dec	dword [eax+4]
	jnz	_977
	push	eax
	call	_bbGCFree
	add	esp,4
_977:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_979
	push	eax
	call	_bbGCFree
	add	esp,4
_979:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_981
	push	eax
	call	_bbGCFree
	add	esp,4
_981:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_983
	push	eax
	call	_bbGCFree
	add	esp,4
_983:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_985
	push	eax
	call	_bbGCFree
	add	esp,4
_985:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_987
	push	eax
	call	_bbGCFree
	add	esp,4
_987:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_989
	push	eax
	call	_bbGCFree
	add	esp,4
_989:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_991
	push	eax
	call	_bbGCFree
	add	esp,4
_991:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_993
	push	eax
	call	_bbGCFree
	add	esp,4
_993:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_995
	push	eax
	call	_bbGCFree
	add	esp,4
_995:
	mov	eax,0
	jmp	_945
_945:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__init:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	ebx,dword [ebp+8]
	mov	eax,dword [ebx+8]
	push	21
	push	_40
	push	300
	push	400
	push	0
	push	0
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,28
	push	ebx
	push	dword [_bb_ipadList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	0
	push	dword [ebx+8]
	push	125
	push	375
	push	125
	push	10
	call	_bb_newListBox
	add	esp,24
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+96]
	dec	dword [eax+4]
	jnz	_1000
	push	eax
	call	_bbGCFree
	add	esp,4
_1000:
	mov	dword [ebx+96],esi
	push	0
	push	0
	mov	eax,dword [ebx+8]
	push	dword [eax+20]
	push	0
	push	_41
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_1004
	push	eax
	call	_bbGCFree
	add	esp,4
_1004:
	mov	dword [ebx+32],esi
	push	0
	push	0
	push	dword [ebx+32]
	push	0
	push	_42
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_1008
	push	eax
	call	_bbGCFree
	add	esp,4
_1008:
	mov	dword [ebx+44],esi
	push	0
	push	0
	push	dword [ebx+32]
	push	0
	push	_43
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_1012
	push	eax
	call	_bbGCFree
	add	esp,4
_1012:
	mov	dword [ebx+48],esi
	push	0
	push	0
	mov	eax,dword [ebx+8]
	push	dword [eax+20]
	push	0
	push	_44
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_1016
	push	eax
	call	_bbGCFree
	add	esp,4
_1016:
	mov	dword [ebx+40],esi
	push	0
	push	0
	push	dword [ebx+40]
	push	0
	push	_45
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+60]
	dec	dword [eax+4]
	jnz	_1020
	push	eax
	call	_bbGCFree
	add	esp,4
_1020:
	mov	dword [ebx+60],esi
	push	0
	push	0
	push	dword [ebx+40]
	push	0
	push	_1
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	push	0
	push	0
	push	dword [ebx+40]
	push	0
	push	_46
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+52]
	dec	dword [eax+4]
	jnz	_1024
	push	eax
	call	_bbGCFree
	add	esp,4
_1024:
	mov	dword [ebx+52],esi
	push	0
	push	0
	push	dword [ebx+40]
	push	0
	push	_47
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_1028
	push	eax
	call	_bbGCFree
	add	esp,4
_1028:
	mov	dword [ebx+56],esi
	push	0
	push	0
	push	dword [ebx+40]
	push	0
	push	_48
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+64]
	dec	dword [eax+4]
	jnz	_1032
	push	eax
	call	_bbGCFree
	add	esp,4
_1032:
	mov	dword [ebx+64],esi
	push	0
	push	0
	push	dword [ebx+40]
	push	0
	push	_49
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+68]
	dec	dword [eax+4]
	jnz	_1036
	push	eax
	call	_bbGCFree
	add	esp,4
_1036:
	mov	dword [ebx+68],esi
	push	0
	push	0
	push	_bbNullObject
	push	0
	push	_50
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+80]
	dec	dword [eax+4]
	jnz	_1040
	push	eax
	call	_bbGCFree
	add	esp,4
_1040:
	mov	dword [ebx+80],esi
	push	0
	push	0
	push	dword [ebx+80]
	push	0
	push	_41
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+72]
	dec	dword [eax+4]
	jnz	_1044
	push	eax
	call	_bbGCFree
	add	esp,4
_1044:
	mov	dword [ebx+72],esi
	push	0
	push	0
	push	dword [ebx+80]
	push	0
	push	_51
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+76]
	dec	dword [eax+4]
	jnz	_1048
	push	eax
	call	_bbGCFree
	add	esp,4
_1048:
	mov	dword [ebx+76],esi
	push	0
	push	0
	push	dword [ebx+76]
	push	0
	push	_52
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+84]
	dec	dword [eax+4]
	jnz	_1052
	push	eax
	call	_bbGCFree
	add	esp,4
_1052:
	mov	dword [ebx+84],esi
	push	0
	push	0
	push	dword [ebx+76]
	push	0
	push	_44
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+88]
	dec	dword [eax+4]
	jnz	_1056
	push	eax
	call	_bbGCFree
	add	esp,4
_1056:
	mov	dword [ebx+88],esi
	push	0
	push	0
	push	dword [ebx+76]
	push	0
	push	_53
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+92]
	dec	dword [eax+4]
	jnz	_1060
	push	eax
	call	_bbGCFree
	add	esp,4
_1060:
	mov	dword [ebx+92],esi
	mov	eax,dword [ebx+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	mov	eax,0
	jmp	_588
_588:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__show:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	mov	eax,0
	jmp	_591
_591:
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__build:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+8]
	mov	esi,dword [edi+112]
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_54
_56:
	mov	eax,ebx
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_54
	push	_bb_Note
	call	_bbObjectNew
	add	esp,4
	push	dword [eax+24]
	push	dword [edi+112]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
_54:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_56
_55:
	push	0
	mov	eax,dword [edi+8]
	push	dword [eax+8]
	push	15
	push	75
	push	10
	push	10
	push	dword [edi+16]
	push	_57
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+100]
	dec	dword [eax+4]
	jnz	_1072
	push	eax
	call	_bbGCFree
	add	esp,4
_1072:
	mov	dword [edi+100],ebx
	push	0
	mov	eax,dword [edi+8]
	push	dword [eax+8]
	push	15
	push	125
	push	25
	push	10
	push	dword [edi+20]
	push	_58
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+104]
	dec	dword [eax+4]
	jnz	_1076
	push	eax
	call	_bbGCFree
	add	esp,4
_1076:
	mov	dword [edi+104],ebx
	push	0
	mov	eax,dword [edi+8]
	push	dword [eax+8]
	push	15
	push	150
	push	40
	push	10
	push	dword [edi+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+108]
	dec	dword [eax+4]
	jnz	_1080
	push	eax
	call	_bbGCFree
	add	esp,4
_1080:
	mov	dword [edi+108],ebx
	mov	ebx,dword [edi+24]
	push	_60
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1083
	push	_61
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1084
	push	_62
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1085
	push	_65
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1086
	push	_66
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1087
	push	_67
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1088
	push	_68
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1089
	jmp	_1082
_1083:
	push	_45
	push	dword [edi+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	jmp	_1082
_1084:
	push	_45
	push	dword [edi+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	jmp	_1082
_1085:
	push	_63
	push	dword [edi+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	0
	push	0
	mov	eax,dword [edi+8]
	push	dword [eax+20]
	push	0
	push	_64
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+36]
	dec	dword [eax+4]
	jnz	_1093
	push	eax
	call	_bbGCFree
	add	esp,4
_1093:
	mov	dword [edi+36],ebx
	jmp	_1082
_1086:
	push	_63
	push	dword [edi+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	jmp	_1082
_1087:
	push	_63
	push	dword [edi+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	jmp	_1082
_1088:
	push	_63
	push	dword [edi+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	jmp	_1082
_1089:
	push	_63
	push	dword [edi+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	jmp	_1082
_1082:
	mov	eax,dword [edi+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	mov	eax,0
	jmp	_594
_594:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__hide:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	cmp	dword [_bb_quickMode],0
	je	_1094
	mov	eax,dword [ebp+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+64]
	add	esp,4
	jmp	_1096
_1094:
	push	0
	push	_69
	call	_brl_system_Confirm
	add	esp,8
	cmp	eax,0
	je	_1097
	mov	eax,dword [ebp+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+64]
	add	esp,4
_1097:
_1096:
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	mov	ebx,dword [eax+16]
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_70
_72:
	mov	eax,edi
	push	_maxgui_maxgui_TGadget
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	je	_70
	push	esi
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	push	esi
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
_70:
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_72
_71:
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	mov	ebx,_bbNullObject
	inc	dword [ebx+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_1109
	push	eax
	call	_bbGCFree
	add	esp,4
_1109:
	mov	eax,dword [ebp+8]
	mov	dword [eax+8],ebx
	push	dword [ebp+8]
	push	dword [_bb_ipadList]
	call	_brl_linkedlist_ListRemove
	add	esp,8
	mov	eax,0
	jmp	_597
_597:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__save:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	push	_73
	mov	eax,dword [ebp+8]
	push	dword [eax+24]
	push	_73
	mov	eax,dword [ebp+8]
	push	dword [eax+20]
	push	_73
	mov	eax,dword [ebp+8]
	push	dword [eax+16]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	esi,eax
	mov	eax,dword [ebp+8]
	mov	edi,dword [eax+112]
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-4],eax
	jmp	_74
_76:
	mov	eax,dword [ebp-4]
	push	_bb_Note
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	je	_74
	push	_78
	push	_77
	push	dword [ebx+32]
	push	_77
	push	dword [ebx+28]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_77
	push	dword [ebx+36]
	push	_77
	push	dword [ebx+24]
	push	esi
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	esi,eax
_74:
	mov	eax,dword [ebp-4]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_76
_75:
	push	_79
	push	esi
	call	_bbStringConcat
	add	esp,8
	mov	esi,eax
	push	esi
	push	dword [ebp+8]
	call	_bb_insertIpadEntry
	add	esp,8
	mov	eax,0
	jmp	_600
_600:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__addNote:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+12]
	push	eax
	call	_bb_convertFromSSV
	add	esp,4
	mov	dword [ebp-4],1
	mov	ebx,_bbNullObject
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp-8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-12],eax
	jmp	_80
_82:
	mov	eax,dword [ebp-12]
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	edi,eax
	cmp	edi,_bbNullObject
	je	_80
	cmp	dword [ebp-4],1
	jne	_1126
	call	_bb_newNote
	mov	ebx,eax
	push	ebx
	mov	eax,dword [ebp+8]
	push	dword [eax+112]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	eax,edi
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_1130
	push	eax
	call	_bbGCFree
	add	esp,4
_1130:
	mov	dword [ebx+24],esi
_1126:
	cmp	dword [ebp-4],2
	jne	_1131
	mov	eax,edi
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_1135
	push	eax
	call	_bbGCFree
	add	esp,4
_1135:
	mov	dword [ebx+36],esi
	mov	eax,dword [ebp+8]
	mov	esi,dword [eax+96]
	push	1
	push	dword [ebx+24]
	push	_32
	push	dword [ebx+36]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+52]
	add	esp,12
_1131:
	cmp	dword [ebp-4],3
	jne	_1137
	push	edi
	call	_bbStringToInt
	add	esp,4
	mov	dword [ebx+28],eax
_1137:
	cmp	dword [ebp-4],4
	jne	_1138
	mov	esi,edi
	inc	dword [esi+4]
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_1142
	push	eax
	call	_bbGCFree
	add	esp,4
_1142:
	mov	dword [ebx+32],esi
_1138:
	add	dword [ebp-4],1
_80:
	mov	eax,dword [ebp-12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_82
_81:
	mov	eax,ebx
	jmp	_604
_604:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__update:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	call	_brl_eventqueue_EventSource
	mov	edx,dword [ebp+8]
	cmp	eax,dword [edx+44]
	je	_1145
	mov	edx,dword [ebp+8]
	cmp	eax,dword [edx+48]
	je	_1146
	mov	edx,dword [ebp+8]
	cmp	eax,dword [edx+36]
	je	_1147
	mov	edx,dword [ebp+8]
	cmp	eax,dword [edx+52]
	je	_1148
	mov	edx,dword [ebp+8]
	cmp	eax,dword [edx+56]
	je	_1149
	mov	edx,dword [ebp+8]
	cmp	eax,dword [edx+60]
	je	_1150
	mov	edx,dword [ebp+8]
	cmp	eax,dword [edx+64]
	je	_1151
	mov	edx,dword [ebp+8]
	cmp	eax,dword [edx+68]
	je	_1152
	mov	edx,dword [ebp+8]
	mov	edx,dword [edx+96]
	cmp	eax,dword [edx+8]
	je	_1153
	mov	edx,dword [ebp+8]
	cmp	eax,dword [edx+72]
	je	_1154
	mov	edx,dword [ebp+8]
	cmp	eax,dword [edx+84]
	je	_1155
	mov	edx,dword [ebp+8]
	cmp	eax,dword [edx+88]
	je	_1156
	mov	edx,dword [ebp+8]
	cmp	eax,dword [edx+92]
	je	_1157
	mov	edx,dword [ebp+8]
	mov	edx,dword [edx+8]
	cmp	eax,dword [edx+8]
	je	_1158
	jmp	_1144
_1145:
	cmp	dword [_bb_quickMode],0
	je	_1160
	mov	eax,1
	jmp	_1161
_1160:
	push	0
	push	_85
	push	_37
	push	_84
	mov	eax,dword [ebp+8]
	push	dword [eax+12]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_83
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_system_Confirm
	add	esp,8
_1161:
	cmp	eax,0
	je	_1162
	push	0
	push	0
	push	4
	push	_86
	call	_bb_prompt
	add	esp,16
	mov	esi,eax
	push	_1
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1164
	mov	ebx,dword [ebp+8]
	push	_90
	push	esi
	push	_89
	mov	eax,dword [ebp+8]
	push	dword [eax+16]
	push	_88
	call	_brl_system_CurrentDate
	push	eax
	push	_87
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	inc	dword [esi+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+16]
	dec	dword [eax+4]
	jnz	_1169
	push	eax
	call	_bbGCFree
	add	esp,4
_1169:
	mov	eax,dword [ebp+8]
	mov	dword [eax+16],esi
	mov	eax,dword [ebp+8]
	push	dword [eax+16]
	push	_57
	call	_bbStringConcat
	add	esp,8
	push	eax
	mov	eax,dword [ebp+8]
	push	dword [eax+100]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
_1164:
_1162:
	jmp	_1144
_1146:
	cmp	dword [_bb_quickMode],0
	je	_1171
	mov	eax,1
	jmp	_1172
_1171:
	push	0
	push	_92
	push	_37
	push	_84
	mov	eax,dword [ebp+8]
	push	dword [eax+12]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_91
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_system_Confirm
	add	esp,8
_1172:
	cmp	eax,0
	je	_1173
	push	0
	push	0
	push	12
	push	_93
	call	_bb_prompt
	add	esp,16
	mov	esi,eax
	push	_1
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1175
	mov	ebx,dword [ebp+8]
	push	_90
	push	esi
	push	_89
	mov	eax,dword [ebp+8]
	push	dword [eax+20]
	push	_95
	call	_brl_system_CurrentDate
	push	eax
	push	_94
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	inc	dword [esi+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+20]
	dec	dword [eax+4]
	jnz	_1180
	push	eax
	call	_bbGCFree
	add	esp,4
_1180:
	mov	eax,dword [ebp+8]
	mov	dword [eax+20],esi
	mov	eax,dword [ebp+8]
	push	dword [eax+20]
	push	_58
	call	_bbStringConcat
	add	esp,8
	push	eax
	mov	eax,dword [ebp+8]
	push	dword [eax+104]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
_1175:
_1173:
	jmp	_1144
_1147:
	mov	eax,dword [ebp+8]
	cmp	dword [eax+36],_bbNullObject
	je	_1181
	push	0
	push	_96
	call	_bb_dlog
	add	esp,8
_1181:
	jmp	_1144
_1148:
	mov	ebx,dword [ebp+8]
	push	_100
	mov	eax,dword [ebp+8]
	push	dword [eax+24]
	push	_99
	call	_brl_system_CurrentTime
	push	eax
	push	_98
	call	_brl_system_CurrentDate
	push	eax
	push	_97
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	mov	ebx,eax
	mov	esi,_66
	inc	dword [esi+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+24]
	dec	dword [eax+4]
	jnz	_1187
	push	eax
	call	_bbGCFree
	add	esp,4
_1187:
	mov	eax,dword [ebp+8]
	mov	dword [eax+24],esi
	mov	eax,dword [ebp+8]
	push	dword [eax+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	mov	eax,dword [ebp+8]
	push	dword [eax+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	mov	eax,dword [ebx+16]
	push	dword [eax+8]
	call	_maxgui_maxgui_ActivateGadget
	add	esp,4
	jmp	_1144
_1149:
	push	0
	push	1
	push	0
	push	_101
	call	_bb_prompt
	add	esp,16
	mov	ebx,dword [ebp+8]
	push	_90
	mov	edx,dword [ebp+8]
	push	dword [edx+24]
	push	_105
	push	eax
	push	_104
	call	_brl_system_CurrentTime
	push	eax
	push	_103
	call	_brl_system_CurrentDate
	push	eax
	push	_102
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	mov	ebx,_65
	inc	dword [ebx+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+24]
	dec	dword [eax+4]
	jnz	_1194
	push	eax
	call	_bbGCFree
	add	esp,4
_1194:
	mov	eax,dword [ebp+8]
	mov	dword [eax+24],ebx
	mov	eax,dword [ebp+8]
	push	dword [eax+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	mov	eax,dword [ebp+8]
	push	dword [eax+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	jmp	_1144
_1150:
	push	_45
	mov	eax,dword [ebp+8]
	push	dword [eax+60]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_1195
	mov	ebx,dword [ebp+8]
	push	_90
	mov	eax,dword [ebp+8]
	push	dword [eax+24]
	push	_108
	call	_brl_system_CurrentTime
	push	eax
	push	_107
	call	_brl_system_CurrentDate
	push	eax
	push	_106
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	mov	ebx,_62
	inc	dword [ebx+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+24]
	dec	dword [eax+4]
	jnz	_1200
	push	eax
	call	_bbGCFree
	add	esp,4
_1200:
	mov	eax,dword [ebp+8]
	mov	dword [eax+24],ebx
	mov	eax,dword [ebp+8]
	push	dword [eax+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	mov	eax,dword [ebp+8]
	push	dword [eax+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_63
	mov	eax,dword [ebp+8]
	push	dword [eax+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	0
	push	0
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	dword [eax+20]
	push	0
	push	_64
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+36]
	dec	dword [eax+4]
	jnz	_1204
	push	eax
	call	_bbGCFree
	add	esp,4
_1204:
	mov	eax,dword [ebp+8]
	mov	dword [eax+36],ebx
	jmp	_1205
_1195:
	mov	ebx,dword [ebp+8]
	push	_90
	mov	eax,dword [ebp+8]
	push	dword [eax+24]
	push	_111
	call	_brl_system_CurrentTime
	push	eax
	push	_110
	call	_brl_system_CurrentDate
	push	eax
	push	_109
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	mov	ebx,_61
	inc	dword [ebx+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+24]
	dec	dword [eax+4]
	jnz	_1210
	push	eax
	call	_bbGCFree
	add	esp,4
_1210:
	mov	eax,dword [ebp+8]
	mov	dword [eax+24],ebx
	mov	eax,dword [ebp+8]
	push	dword [eax+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	mov	eax,dword [ebp+8]
	push	dword [eax+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_45
	mov	eax,dword [ebp+8]
	push	dword [eax+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	mov	eax,dword [ebp+8]
	push	dword [eax+36]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
_1205:
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	jmp	_1144
_1151:
	mov	ebx,dword [ebp+8]
	push	_90
	mov	eax,dword [ebp+8]
	push	dword [eax+24]
	push	_114
	call	_brl_system_CurrentTime
	push	eax
	push	_113
	call	_brl_system_CurrentDate
	push	eax
	push	_112
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	mov	ebx,_67
	inc	dword [ebx+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+24]
	dec	dword [eax+4]
	jnz	_1215
	push	eax
	call	_bbGCFree
	add	esp,4
_1215:
	mov	eax,dword [ebp+8]
	mov	dword [eax+24],ebx
	mov	eax,dword [ebp+8]
	push	dword [eax+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	mov	eax,dword [ebp+8]
	push	dword [eax+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	jmp	_1144
_1152:
	push	0
	push	1
	push	0
	push	_115
	call	_bb_prompt
	add	esp,16
	mov	ebx,dword [ebp+8]
	push	_119
	push	eax
	push	_118
	mov	eax,dword [ebp+8]
	push	dword [eax+24]
	push	_114
	call	_brl_system_CurrentTime
	push	eax
	push	_117
	call	_brl_system_CurrentDate
	push	eax
	push	_116
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	mov	ebx,_68
	inc	dword [ebx+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+24]
	dec	dword [eax+4]
	jnz	_1221
	push	eax
	call	_bbGCFree
	add	esp,4
_1221:
	mov	eax,dword [ebp+8]
	mov	dword [eax+24],ebx
	mov	eax,dword [ebp+8]
	push	dword [eax+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	mov	eax,dword [ebp+8]
	push	dword [eax+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	jmp	_1144
_1153:
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_1224
	cmp	eax,8196
	je	_1225
	jmp	_1223
_1224:
	mov	ebx,dword [ebp+8]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+96]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+64]
	add	esp,4
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+76]
	add	esp,8
	push	eax
	call	_bb_readNote
	add	esp,4
	jmp	_1223
_1225:
	push	_bbNullObject
	mov	eax,dword [ebp+8]
	push	dword [eax+80]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_PopupWindowMenu
	add	esp,12
	jmp	_1223
_1223:
	jmp	_1144
_1154:
	call	_maxgui_maxgui_ActiveGadget
	mov	edx,dword [ebp+8]
	mov	edx,dword [edx+96]
	cmp	eax,dword [edx+8]
	jne	_1228
	mov	ebx,dword [ebp+8]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+96]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+64]
	add	esp,4
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+76]
	add	esp,8
	push	eax
	call	_bb_readNote
	add	esp,4
_1228:
	jmp	_1144
_1155:
	mov	ebx,dword [ebp+8]
	push	_121
	call	_brl_system_CurrentDate
	push	eax
	push	_120
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	jmp	_1144
_1156:
	mov	ebx,dword [ebp+8]
	push	_122
	call	_brl_system_CurrentDate
	push	eax
	push	_120
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	jmp	_1144
_1157:
	mov	ebx,dword [ebp+8]
	push	_123
	call	_brl_system_CurrentDate
	push	eax
	push	_120
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	jmp	_1144
_1158:
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	je	_1242
	jmp	_1241
_1242:
	mov	eax,dword [ebp+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
	jmp	_1241
_1241:
	jmp	_1144
_1144:
	mov	eax,dword [ebp+8]
	mov	edi,dword [eax+112]
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-4],eax
	jmp	_124
_126:
	mov	eax,dword [ebp-4]
	push	_bb_Note
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	je	_124
	cmp	ebx,_bbNullObject
	je	_1250
	call	_brl_eventqueue_EventSource
	mov	edx,dword [ebx+8]
	cmp	eax,dword [edx+8]
	je	_1253
	jmp	_1252
_1253:
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	je	_1256
	jmp	_1255
_1256:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
	cmp	eax,0
	je	_1258
	mov	eax,dword [ebx+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_1263
	push	eax
	call	_bbGCFree
	add	esp,4
_1263:
	mov	dword [ebx+24],esi
	mov	eax,dword [ebx+16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_1268
	push	eax
	call	_bbGCFree
	add	esp,4
_1268:
	mov	dword [ebx+32],esi
	mov	eax,dword [ebp+8]
	mov	esi,dword [eax+96]
	push	dword [ebx+24]
	push	_32
	push	dword [ebx+36]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+56]
	add	esp,8
_1258:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+64]
	add	esp,4
	jmp	_1255
_1255:
	jmp	_1252
_1252:
_1250:
_124:
	mov	eax,dword [ebp-4]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_126
_125:
	mov	eax,0
	jmp	_607
_607:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__noteBySummary:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	ebx,dword [eax+112]
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_127
_129:
	mov	eax,edi
	push	_bb_Note
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	je	_127
	push	dword [ebp+12]
	push	dword [esi+24]
	push	_32
	push	dword [esi+36]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_1277
	mov	eax,esi
	jmp	_611
_1277:
_127:
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_129
_128:
	mov	eax,_bbNullObject
	jmp	_611
_611:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_insertIpadEntry:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	mov	ebx,eax
_132:
	push	dword [_bb_iPaddb]
	call	_brl_stream_ReadLine
	add	esp,4
	push	eax
	push	ebx
	call	_brl_linkedlist_ListAddLast
	add	esp,8
_130:
	push	dword [_bb_iPaddb]
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_132
_131:
	push	dword [_bb_iPaddb]
	call	_brl_filesystem_CloseFile
	add	esp,4
	push	1
	push	1
	push	dword [_bb_ipaddbn]
	call	_brl_filesystem_OpenFile
	add	esp,12
	mov	esi,eax
	inc	dword [esi+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_1282
	push	eax
	call	_bbGCFree
	add	esp,4
_1282:
	mov	dword [_bb_iPaddb],esi
	mov	esi,1
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_133
_135:
	mov	eax,edi
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_133
	mov	edx,dword [ebp+8]
	cmp	esi,dword [edx+12]
	jne	_1290
	push	dword [ebp+12]
	push	dword [_bb_iPaddb]
	call	_brl_stream_WriteLine
	add	esp,8
	jmp	_1291
_1290:
	push	eax
	push	dword [_bb_iPaddb]
	call	_brl_stream_WriteLine
	add	esp,8
_1291:
	add	esi,1
_133:
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_135
_134:
	mov	eax,dword [ebp+8]
	cmp	dword [eax+12],-1
	jne	_1292
	push	dword [ebp+12]
	push	dword [_bb_iPaddb]
	call	_brl_stream_WriteLine
	add	esp,8
_1292:
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_1296
	push	eax
	call	_bbGCFree
	add	esp,4
_1296:
	mov	dword [_bb_iPaddb],ebx
	mov	eax,0
	jmp	_615
_615:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newIpad:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	_bb_iPad
	call	_bbObjectNew
	add	esp,4
	mov	ebx,eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	mov	eax,ebx
	jmp	_617
_617:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newBlankIpadBC:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	esi,dword [ebp+8]
	call	_bb_newIpad
	mov	ebx,eax
	mov	dword [ebx+12],-1
	inc	dword [esi+4]
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_1303
	push	eax
	call	_bbGCFree
	add	esp,4
_1303:
	mov	dword [ebx+16],esi
	mov	esi,_136
	inc	dword [esi+4]
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_1307
	push	eax
	call	_bbGCFree
	add	esp,4
_1307:
	mov	dword [ebx+20],esi
	mov	esi,_60
	inc	dword [esi+4]
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_1311
	push	eax
	call	_bbGCFree
	add	esp,4
_1311:
	mov	dword [ebx+24],esi
	push	_119
	call	_brl_system_CurrentTime
	push	eax
	push	_139
	call	_brl_system_CurrentDate
	push	eax
	push	_138
	call	_brl_system_CurrentDate
	push	eax
	push	_137
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	mov	eax,_bbNullObject
	jmp	_620
_620:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newBlankIpadSN:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+8]
	call	_bb_newIpad
	mov	esi,eax
	mov	dword [esi+12],-1
	mov	eax,_140
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_1318
	push	eax
	call	_bbGCFree
	add	esp,4
_1318:
	mov	dword [esi+16],ebx
	mov	ebx,edi
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_1322
	push	eax
	call	_bbGCFree
	add	esp,4
_1322:
	mov	dword [esi+20],ebx
	mov	ebx,_60
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_1326
	push	eax
	call	_bbGCFree
	add	esp,4
_1326:
	mov	dword [esi+24],ebx
	mov	ebx,esi
	push	_119
	call	_brl_system_CurrentTime
	push	eax
	push	_139
	call	_brl_system_CurrentDate
	push	eax
	push	_138
	call	_brl_system_CurrentDate
	push	eax
	push	_137
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,_bbNullObject
	jmp	_623
_623:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_findBySN:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [_bb_ipadList]
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_141
_143:
	push	_bb_iPad
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_141
	push	dword [ebp+8]
	push	dword [eax+20]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_1335
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_1339
	push	eax
	call	_bbGCFree
	add	esp,4
_1339:
	mov	dword [_bb_iPaddb],ebx
	push	0
	push	_144
	call	_brl_system_Notify
	add	esp,8
	mov	eax,_bbNullObject
	jmp	_626
_1335:
_141:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_143
_142:
	mov	edi,dword [_bb_iPaddb]
	mov	ebx,1
_147:
	push	edi
	call	_brl_stream_ReadLine
	add	esp,4
	push	eax
	call	_bb_convertFromCSV
	add	esp,4
	mov	esi,eax
	push	dword [ebp+8]
	push	esi
	call	_brl_linkedlist_ListContains
	add	esp,8
	cmp	eax,0
	je	_1344
	mov	dword [ebp-4],1
	call	_bb_newIpad
	mov	edi,eax
	mov	dword [edi+12],ebx
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-8],eax
	jmp	_148
_150:
	mov	eax,dword [ebp-8]
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_148
	cmp	dword [ebp-4],1
	jne	_1353
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [edi+16]
	dec	dword [eax+4]
	jnz	_1357
	push	eax
	call	_bbGCFree
	add	esp,4
_1357:
	mov	dword [edi+16],ebx
	jmp	_1358
_1353:
	cmp	dword [ebp-4],2
	jne	_1359
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [edi+20]
	dec	dword [eax+4]
	jnz	_1363
	push	eax
	call	_bbGCFree
	add	esp,4
_1363:
	mov	dword [edi+20],ebx
	jmp	_1364
_1359:
	cmp	dword [ebp-4],3
	jne	_1365
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [edi+24]
	dec	dword [eax+4]
	jnz	_1369
	push	eax
	call	_bbGCFree
	add	esp,4
_1369:
	mov	dword [edi+24],ebx
	jmp	_1370
_1365:
	mov	edx,edi
	push	eax
	push	edx
	mov	eax,dword [edx]
	call	dword [eax+68]
	add	esp,8
_1370:
_1364:
_1358:
	add	dword [ebp-4],1
_148:
	mov	eax,dword [ebp-8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_150
_149:
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_1375
	push	eax
	call	_bbGCFree
	add	esp,4
_1375:
	mov	dword [_bb_iPaddb],ebx
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,edi
	jmp	_626
_1344:
	add	ebx,1
_145:
	push	edi
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_147
_146:
	cmp	dword [_bb_quickMode],0
	je	_1377
	push	dword [ebp+8]
	call	_bb_newBlankIpadSN
	add	esp,4
	jmp	_1378
_1377:
	push	0
	push	_152
	push	dword [ebp+8]
	push	_151
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_system_Confirm
	add	esp,8
	cmp	eax,0
	je	_1379
	push	dword [ebp+8]
	call	_bb_newBlankIpadSN
	add	esp,4
_1379:
_1378:
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_1383
	push	eax
	call	_bbGCFree
	add	esp,4
_1383:
	mov	dword [_bb_iPaddb],ebx
	mov	eax,_bbNullObject
	jmp	_626
_626:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_findByBC:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [_bb_ipadList]
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_153
_155:
	push	_bb_iPad
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_153
	push	dword [ebp+8]
	push	dword [eax+16]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_1390
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_1394
	push	eax
	call	_bbGCFree
	add	esp,4
_1394:
	mov	dword [_bb_iPaddb],ebx
	push	0
	push	_144
	call	_brl_system_Notify
	add	esp,8
	mov	eax,_bbNullObject
	jmp	_629
_1390:
_153:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_155
_154:
	mov	edi,dword [_bb_iPaddb]
	mov	ebx,1
_158:
	push	edi
	call	_brl_stream_ReadLine
	add	esp,4
	push	eax
	call	_bb_convertFromCSV
	add	esp,4
	mov	esi,eax
	push	dword [ebp+8]
	push	esi
	call	_brl_linkedlist_ListContains
	add	esp,8
	cmp	eax,0
	je	_1399
	mov	dword [ebp-4],1
	call	_bb_newIpad
	mov	edi,eax
	mov	dword [edi+12],ebx
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-8],eax
	jmp	_159
_161:
	mov	eax,dword [ebp-8]
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_159
	cmp	dword [ebp-4],1
	jne	_1408
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [edi+16]
	dec	dword [eax+4]
	jnz	_1412
	push	eax
	call	_bbGCFree
	add	esp,4
_1412:
	mov	dword [edi+16],ebx
	jmp	_1413
_1408:
	cmp	dword [ebp-4],2
	jne	_1414
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [edi+20]
	dec	dword [eax+4]
	jnz	_1418
	push	eax
	call	_bbGCFree
	add	esp,4
_1418:
	mov	dword [edi+20],ebx
	jmp	_1419
_1414:
	cmp	dword [ebp-4],3
	jne	_1420
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [edi+24]
	dec	dword [eax+4]
	jnz	_1424
	push	eax
	call	_bbGCFree
	add	esp,4
_1424:
	mov	dword [edi+24],ebx
	jmp	_1425
_1420:
	mov	edx,edi
	push	eax
	push	edx
	mov	eax,dword [edx]
	call	dword [eax+68]
	add	esp,8
_1425:
_1419:
_1413:
	add	dword [ebp-4],1
_159:
	mov	eax,dword [ebp-8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_161
_160:
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_1430
	push	eax
	call	_bbGCFree
	add	esp,4
_1430:
	mov	dword [_bb_iPaddb],ebx
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,edi
	jmp	_629
_1399:
	add	ebx,1
_156:
	push	edi
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_158
_157:
	cmp	dword [_bb_quickMode],0
	je	_1432
	push	dword [ebp+8]
	call	_bb_newBlankIpadBC
	add	esp,4
	jmp	_1433
_1432:
	push	0
	push	_152
	push	dword [ebp+8]
	push	_162
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_system_Confirm
	add	esp,8
	cmp	eax,0
	je	_1434
	push	dword [ebp+8]
	call	_bb_newBlankIpadBC
	add	esp,4
_1434:
_1433:
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_1438
	push	eax
	call	_bbGCFree
	add	esp,4
_1438:
	mov	dword [_bb_iPaddb],ebx
	mov	eax,_bbNullObject
	jmp	_629
_629:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_Note
	push	_bb_Window
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+12],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+16],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+20],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+24],eax
	mov	dword [ebx+28],0
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+32],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+36],eax
	mov	eax,0
	jmp	_632
_632:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_635:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_1448
	push	eax
	call	_bbGCFree
	add	esp,4
_1448:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_1450
	push	eax
	call	_bbGCFree
	add	esp,4
_1450:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_1452
	push	eax
	call	_bbGCFree
	add	esp,4
_1452:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_1454
	push	eax
	call	_bbGCFree
	add	esp,4
_1454:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_1456
	push	eax
	call	_bbGCFree
	add	esp,4
_1456:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_1458
	push	eax
	call	_bbGCFree
	add	esp,4
_1458:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_1460
	push	eax
	call	_bbGCFree
	add	esp,4
_1460:
	mov	eax,0
	jmp	_1446
_1446:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note__init:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	ebx,dword [ebp+8]
	mov	esi,dword [ebx+8]
	push	21
	push	dword [ebx+36]
	push	_163
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	300
	push	400
	push	0
	push	450
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+48]
	add	esp,28
	push	ebx
	push	dword [_bb_noteWinList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	0
	push	dword [ebx+8]
	push	20
	push	150
	push	10
	push	10
	call	_bb_newTextField
	add	esp,24
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_1465
	push	eax
	call	_bbGCFree
	add	esp,4
_1465:
	mov	dword [ebx+12],esi
	push	_bbNullObject
	push	_bb_noComma
	mov	eax,dword [ebx+12]
	push	dword [eax+8]
	call	_maxgui_maxgui_SetGadgetFilter
	add	esp,12
	push	0
	push	dword [ebx+8]
	push	215
	push	375
	push	35
	push	10
	call	_bb_newTextArea
	add	esp,24
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_1469
	push	eax
	call	_bbGCFree
	add	esp,4
_1469:
	mov	dword [ebx+16],esi
	push	_bbNullObject
	push	_bb_noComma
	mov	eax,dword [ebx+16]
	push	dword [eax+8]
	call	_maxgui_maxgui_SetGadgetFilter
	add	esp,12
	push	0
	mov	eax,dword [ebx+8]
	push	dword [eax+8]
	push	20
	push	150
	push	10
	push	200
	push	dword [ebx+28]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,8
	push	eax
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_1474
	push	eax
	call	_bbGCFree
	add	esp,4
_1474:
	mov	dword [ebx+20],esi
	push	dword [ebx+24]
	mov	eax,dword [ebx+12]
	push	dword [eax+8]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	dword [ebx+32]
	mov	eax,dword [ebx+16]
	push	dword [eax+8]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	mov	eax,0
	jmp	_638
_638:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note__typeByInt:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+12]
	cmp	eax,0
	je	_1477
	cmp	eax,1
	je	_1478
	mov	eax,_53
	jmp	_642
_1477:
	mov	eax,_52
	jmp	_642
_1478:
	mov	eax,_44
	jmp	_642
_642:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note__update:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	call	_brl_eventqueue_EventSource
	mov	edx,dword [ebx+8]
	cmp	eax,dword [edx+8]
	je	_1481
	mov	edx,dword [ebx+12]
	cmp	eax,dword [edx+8]
	je	_1482
	jmp	_1480
_1481:
	jmp	_1480
_1482:
	jmp	_1480
_1480:
	mov	eax,0
	jmp	_645
_645:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note__save:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,0
	cmp	dword [_bb_quickMode],0
	je	_1484
	mov	ebx,1
	jmp	_1485
_1484:
	push	0
	push	_164
	call	_brl_system_Confirm
	add	esp,8
	cmp	eax,0
	je	_1486
	mov	ebx,1
_1486:
_1485:
	mov	eax,ebx
	jmp	_648
_648:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note__hide:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,0
	jmp	_651
_651:
	mov	esp,ebp
	pop	ebp
	ret
_bb_noComma:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	cmp	dword [eax+8],515
	jne	_1488
	mov	eax,dword [eax+16]
	cmp	eax,44
	je	_1491
	cmp	eax,59
	je	_1492
	cmp	eax,13
	je	_1493
	jmp	_1490
_1491:
	push	1
	push	_165
	call	_bb_dlog
	add	esp,8
	mov	eax,0
	jmp	_655
_1492:
	push	1
	push	_166
	call	_bb_dlog
	add	esp,8
	mov	eax,0
	jmp	_655
_1493:
	push	2
	push	_167
	call	_bb_dlog
	add	esp,8
	push	1
	push	_168
	call	_brl_system_Notify
	add	esp,8
	mov	eax,0
	jmp	_655
_1490:
_1488:
	mov	eax,1
	jmp	_655
_655:
	mov	esp,ebp
	pop	ebp
	ret
_bb_readNote:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	mov	edx,dword [eax+8]
	cmp	dword [edx+8],_bbNullObject
	je	_1494
	mov	eax,dword [eax+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	jmp	_1496
_1494:
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
_1496:
	mov	eax,0
	jmp	_658
_658:
	mov	esp,ebp
	pop	ebp
	ret
_bb_newNote:
	push	ebp
	mov	ebp,esp
	push	_bb_Note
	call	_bbObjectNew
	add	esp,4
	jmp	_660
_660:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_Student
	push	_bb_Window
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	dword [ebx+12],0
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+16],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+20],eax
	fldz
	fstp	dword [ebx+24]
	mov	dword [ebx+28],0
	mov	dword [ebx+32],0
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+36],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+40],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+44],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+48],eax
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [ebx+52],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+56],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+60],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+64],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+68],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+72],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+76],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+80],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+84],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+88],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+92],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+96],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+100],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+104],eax
	mov	eax,0
	jmp	_663
_663:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_666:
	mov	eax,dword [ebx+104]
	dec	dword [eax+4]
	jnz	_1522
	push	eax
	call	_bbGCFree
	add	esp,4
_1522:
	mov	eax,dword [ebx+100]
	dec	dword [eax+4]
	jnz	_1524
	push	eax
	call	_bbGCFree
	add	esp,4
_1524:
	mov	eax,dword [ebx+96]
	dec	dword [eax+4]
	jnz	_1526
	push	eax
	call	_bbGCFree
	add	esp,4
_1526:
	mov	eax,dword [ebx+92]
	dec	dword [eax+4]
	jnz	_1528
	push	eax
	call	_bbGCFree
	add	esp,4
_1528:
	mov	eax,dword [ebx+88]
	dec	dword [eax+4]
	jnz	_1530
	push	eax
	call	_bbGCFree
	add	esp,4
_1530:
	mov	eax,dword [ebx+84]
	dec	dword [eax+4]
	jnz	_1532
	push	eax
	call	_bbGCFree
	add	esp,4
_1532:
	mov	eax,dword [ebx+80]
	dec	dword [eax+4]
	jnz	_1534
	push	eax
	call	_bbGCFree
	add	esp,4
_1534:
	mov	eax,dword [ebx+76]
	dec	dword [eax+4]
	jnz	_1536
	push	eax
	call	_bbGCFree
	add	esp,4
_1536:
	mov	eax,dword [ebx+72]
	dec	dword [eax+4]
	jnz	_1538
	push	eax
	call	_bbGCFree
	add	esp,4
_1538:
	mov	eax,dword [ebx+68]
	dec	dword [eax+4]
	jnz	_1540
	push	eax
	call	_bbGCFree
	add	esp,4
_1540:
	mov	eax,dword [ebx+64]
	dec	dword [eax+4]
	jnz	_1542
	push	eax
	call	_bbGCFree
	add	esp,4
_1542:
	mov	eax,dword [ebx+60]
	dec	dword [eax+4]
	jnz	_1544
	push	eax
	call	_bbGCFree
	add	esp,4
_1544:
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_1546
	push	eax
	call	_bbGCFree
	add	esp,4
_1546:
	mov	eax,dword [ebx+52]
	dec	dword [eax+4]
	jnz	_1548
	push	eax
	call	_bbGCFree
	add	esp,4
_1548:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_1550
	push	eax
	call	_bbGCFree
	add	esp,4
_1550:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_1552
	push	eax
	call	_bbGCFree
	add	esp,4
_1552:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_1554
	push	eax
	call	_bbGCFree
	add	esp,4
_1554:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_1556
	push	eax
	call	_bbGCFree
	add	esp,4
_1556:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_1558
	push	eax
	call	_bbGCFree
	add	esp,4
_1558:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_1560
	push	eax
	call	_bbGCFree
	add	esp,4
_1560:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_1562
	push	eax
	call	_bbGCFree
	add	esp,4
_1562:
	mov	eax,0
	jmp	_1520
_1520:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__init:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	eax,dword [ebx+8]
	push	21
	push	_170
	push	300
	push	400
	push	400
	push	10
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,28
	push	ebx
	push	dword [_bb_studentList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	0
	push	dword [ebx+8]
	push	125
	push	375
	push	125
	push	10
	call	_bb_newListBox
	add	esp,24
	mov	esi,eax
	inc	dword [esi+4]
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_1567
	push	eax
	call	_bbGCFree
	add	esp,4
_1567:
	mov	dword [ebx+36],esi
	push	0
	push	0
	mov	eax,dword [ebx+8]
	push	dword [eax+20]
	push	0
	push	_41
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	edi,eax
	push	0
	push	0
	push	edi
	push	0
	push	_171
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+60]
	dec	dword [eax+4]
	jnz	_1572
	push	eax
	call	_bbGCFree
	add	esp,4
_1572:
	mov	dword [ebx+60],esi
	push	0
	push	0
	push	edi
	push	0
	push	_172
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+64]
	dec	dword [eax+4]
	jnz	_1576
	push	eax
	call	_bbGCFree
	add	esp,4
_1576:
	mov	dword [ebx+64],esi
	push	0
	push	0
	push	edi
	push	0
	push	_173
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	esi,eax
	inc	dword [esi+4]
	mov	eax,dword [ebx+68]
	dec	dword [eax+4]
	jnz	_1580
	push	eax
	call	_bbGCFree
	add	esp,4
_1580:
	mov	dword [ebx+68],esi
	push	0
	push	0
	mov	eax,dword [ebx+8]
	push	dword [eax+20]
	push	0
	push	_44
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	dword [ebp-4],eax
	push	0
	push	0
	push	dword [ebp-4]
	push	0
	push	_174
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	esi,eax
	inc	dword [esi+4]
	mov	eax,dword [ebx+72]
	dec	dword [eax+4]
	jnz	_1585
	push	eax
	call	_bbGCFree
	add	esp,4
_1585:
	mov	dword [ebx+72],esi
	push	0
	push	0
	push	dword [ebp-4]
	push	0
	push	_1
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	push	0
	push	0
	push	dword [ebp-4]
	push	0
	push	_175
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	edi,eax
	push	0
	push	0
	push	edi
	push	0
	push	_1589
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+76]
	dec	dword [eax+4]
	jnz	_1593
	push	eax
	call	_bbGCFree
	add	esp,4
_1593:
	mov	dword [ebx+76],esi
	push	0
	push	0
	push	edi
	push	0
	push	_1595
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+80]
	dec	dword [eax+4]
	jnz	_1599
	push	eax
	call	_bbGCFree
	add	esp,4
_1599:
	mov	dword [ebx+80],esi
	push	0
	push	0
	push	edi
	push	0
	push	_1602
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+84]
	dec	dword [eax+4]
	jnz	_1606
	push	eax
	call	_bbGCFree
	add	esp,4
_1606:
	mov	dword [ebx+84],esi
	push	0
	push	0
	push	edi
	push	0
	push	_180
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	esi,eax
	inc	dword [esi+4]
	mov	eax,dword [ebx+88]
	dec	dword [eax+4]
	jnz	_1610
	push	eax
	call	_bbGCFree
	add	esp,4
_1610:
	mov	dword [ebx+88],esi
	push	0
	push	0
	push	dword [ebp-4]
	push	0
	push	_181
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	esi,eax
	inc	dword [esi+4]
	mov	eax,dword [ebx+92]
	dec	dword [eax+4]
	jnz	_1614
	push	eax
	call	_bbGCFree
	add	esp,4
_1614:
	mov	dword [ebx+92],esi
	push	0
	push	0
	push	dword [ebp-4]
	push	0
	push	_182
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	esi,eax
	inc	dword [esi+4]
	mov	eax,dword [ebx+96]
	dec	dword [eax+4]
	jnz	_1618
	push	eax
	call	_bbGCFree
	add	esp,4
_1618:
	mov	dword [ebx+96],esi
	push	0
	push	0
	push	dword [ebp-4]
	push	0
	push	_183
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	esi,eax
	inc	dword [esi+4]
	mov	eax,dword [ebx+100]
	dec	dword [eax+4]
	jnz	_1622
	push	eax
	call	_bbGCFree
	add	esp,4
_1622:
	mov	dword [ebx+100],esi
	push	0
	push	0
	mov	eax,dword [ebx+8]
	push	dword [eax+20]
	push	0
	push	_184
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	esi,eax
	inc	dword [esi+4]
	mov	eax,dword [ebx+104]
	dec	dword [eax+4]
	jnz	_1626
	push	eax
	call	_bbGCFree
	add	esp,4
_1626:
	mov	dword [ebx+104],esi
	mov	eax,dword [ebx+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	mov	eax,0
	jmp	_669
_669:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__show:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	mov	eax,0
	jmp	_672
_672:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__hide:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	cmp	dword [_bb_quickMode],0
	je	_1628
	mov	eax,dword [ebp+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+64]
	add	esp,4
	jmp	_1630
_1628:
	push	0
	push	_69
	call	_brl_system_Confirm
	add	esp,8
	cmp	eax,0
	je	_1631
	mov	eax,dword [ebp+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+64]
	add	esp,4
_1631:
_1630:
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	mov	ebx,dword [eax+16]
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_185
_187:
	mov	eax,edi
	push	_maxgui_maxgui_TGadget
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	je	_185
	push	esi
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	push	esi
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
_185:
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_187
_186:
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	mov	ebx,_bbNullObject
	inc	dword [ebx+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_1643
	push	eax
	call	_bbGCFree
	add	esp,4
_1643:
	mov	eax,dword [ebp+8]
	mov	dword [eax+8],ebx
	push	dword [ebp+8]
	push	dword [_bb_studentList]
	call	_brl_linkedlist_ListRemove
	add	esp,8
	mov	eax,0
	jmp	_675
_675:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__build:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+8]
	mov	esi,dword [edi+52]
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_188
_190:
	mov	eax,ebx
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_188
	push	_bb_Note
	call	_bbObjectNew
	add	esp,4
	push	dword [eax+24]
	push	dword [edi+52]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
_188:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_190
_189:
	push	0
	mov	eax,dword [edi+8]
	push	dword [eax+8]
	push	20
	push	150
	push	0
	push	0
	push	dword [edi+16]
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+40]
	dec	dword [eax+4]
	jnz	_1654
	push	eax
	call	_bbGCFree
	add	esp,4
_1654:
	mov	dword [edi+40],ebx
	push	0
	mov	eax,dword [edi+8]
	push	dword [eax+8]
	push	20
	push	150
	push	0
	push	0
	push	dword [edi+20]
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+44]
	dec	dword [eax+4]
	jnz	_1658
	push	eax
	call	_bbGCFree
	add	esp,4
_1658:
	mov	dword [edi+44],ebx
	push	0
	mov	eax,dword [edi+8]
	push	dword [eax+8]
	push	20
	push	150
	push	0
	push	0
	push	dword [edi+24]
	call	_bbStringFromFloat
	add	esp,4
	push	eax
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+48]
	dec	dword [eax+4]
	jnz	_1662
	push	eax
	call	_bbGCFree
	add	esp,4
_1662:
	mov	dword [edi+48],ebx
	cmp	dword [edi+56],_bbNullObject
	jne	_1663
	jmp	_1664
_1663:
	push	_191
	push	dword [edi+72]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
_1664:
	mov	eax,dword [edi+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	mov	eax,0
	jmp	_678
_678:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__save:
	push	ebp
	mov	ebp,esp
	push	0
	push	_192
	call	_bb_dlog
	add	esp,8
	mov	eax,0
	jmp	_681
_681:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__addNote:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+12]
	push	eax
	call	_bb_convertFromSSV
	add	esp,4
	mov	dword [ebp-4],1
	mov	ebx,_bbNullObject
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp-8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-12],eax
	jmp	_193
_195:
	mov	eax,dword [ebp-12]
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	edi,eax
	cmp	edi,_bbNullObject
	je	_193
	cmp	dword [ebp-4],1
	jne	_1674
	call	_bb_newNote
	mov	ebx,eax
	push	ebx
	mov	eax,dword [ebp+8]
	push	dword [eax+52]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	eax,edi
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_1678
	push	eax
	call	_bbGCFree
	add	esp,4
_1678:
	mov	dword [ebx+24],esi
_1674:
	cmp	dword [ebp-4],2
	jne	_1679
	mov	eax,edi
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_1683
	push	eax
	call	_bbGCFree
	add	esp,4
_1683:
	mov	dword [ebx+36],esi
	mov	eax,dword [ebp+8]
	mov	esi,dword [eax+36]
	push	1
	push	dword [ebx+24]
	push	_32
	push	dword [ebx+36]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+52]
	add	esp,12
_1679:
	cmp	dword [ebp-4],3
	jne	_1685
	push	edi
	call	_bbStringToInt
	add	esp,4
	mov	dword [ebx+28],eax
_1685:
	cmp	dword [ebp-4],4
	jne	_1686
	mov	esi,edi
	inc	dword [esi+4]
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_1690
	push	eax
	call	_bbGCFree
	add	esp,4
_1690:
	mov	dword [ebx+32],esi
_1686:
	add	dword [ebp-4],1
_193:
	mov	eax,dword [ebp-12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_195
_194:
	mov	eax,ebx
	jmp	_685
_685:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__update:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	call	_brl_eventqueue_EventSource
	mov	edx,dword [ebp+8]
	mov	edx,dword [edx+36]
	cmp	eax,dword [edx+8]
	je	_1693
	mov	edx,dword [ebp+8]
	mov	edx,dword [edx+8]
	cmp	eax,dword [edx+8]
	je	_1694
	jmp	_1692
_1693:
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_1697
	cmp	eax,8196
	je	_1698
	jmp	_1696
_1697:
	mov	ebx,dword [ebp+8]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+36]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+64]
	add	esp,4
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+76]
	add	esp,8
	push	eax
	call	_bb_readNote
	add	esp,4
	jmp	_1696
_1698:
	jmp	_1696
_1696:
	jmp	_1692
_1694:
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	je	_1703
	jmp	_1702
_1703:
	mov	eax,dword [ebp+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	jmp	_1702
_1702:
	jmp	_1692
_1692:
	mov	eax,dword [ebp+8]
	mov	edi,dword [eax+52]
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-4],eax
	jmp	_196
_198:
	mov	eax,dword [ebp-4]
	push	_bb_Note
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	je	_196
	cmp	ebx,_bbNullObject
	je	_1711
	call	_brl_eventqueue_EventSource
	mov	edx,dword [ebx+8]
	cmp	eax,dword [edx+8]
	je	_1714
	jmp	_1713
_1714:
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	je	_1717
	jmp	_1716
_1717:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
	cmp	eax,0
	je	_1719
	mov	eax,dword [ebx+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_1724
	push	eax
	call	_bbGCFree
	add	esp,4
_1724:
	mov	dword [ebx+24],esi
	mov	eax,dword [ebx+16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_1729
	push	eax
	call	_bbGCFree
	add	esp,4
_1729:
	mov	dword [ebx+32],esi
	mov	eax,dword [ebp+8]
	mov	esi,dword [eax+36]
	push	dword [ebx+24]
	push	_32
	push	dword [ebx+36]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+56]
	add	esp,8
_1719:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+64]
	add	esp,4
	jmp	_1716
_1716:
	jmp	_1713
_1713:
_1711:
_196:
	mov	eax,dword [ebp-4]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_198
_197:
	mov	eax,0
	jmp	_688
_688:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__noteBySummary:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	ebx,dword [eax+52]
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_199
_201:
	mov	eax,edi
	push	_bb_Note
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	je	_199
	push	dword [ebp+12]
	push	dword [esi+24]
	push	_32
	push	dword [esi+36]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_1738
	mov	eax,esi
	jmp	_692
_1738:
_199:
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_201
_200:
	mov	eax,_bbNullObject
	jmp	_692
_692:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newStudent:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	_bb_Student
	call	_bbObjectNew
	add	esp,4
	mov	ebx,eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	mov	eax,ebx
	jmp	_694
_694:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newBlankStudent:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	mov	edi,dword [ebp+12]
	push	_bb_Student
	call	_bbObjectNew
	add	esp,4
	mov	ebx,eax
	mov	dword [ebx+12],-1
	mov	eax,esi
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_1745
	push	eax
	call	_bbGCFree
	add	esp,4
_1745:
	mov	dword [ebx+16],esi
	mov	esi,edi
	inc	dword [esi+4]
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_1749
	push	eax
	call	_bbGCFree
	add	esp,4
_1749:
	mov	dword [ebx+20],esi
	fldz
	fstp	dword [ebx+24]
	mov	dword [ebx+32],9
	push	_119
	call	_brl_system_CurrentTime
	push	eax
	push	_139
	call	_brl_system_CurrentDate
	push	eax
	push	_203
	call	_brl_system_CurrentDate
	push	eax
	push	_202
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
	mov	eax,ebx
	jmp	_698
_698:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_findByFirst:
	push	ebp
	mov	ebp,esp
	call	_bb_newStudent
	jmp	_701
_701:
	mov	esp,ebp
	pop	ebp
	ret
_bb_findByLast:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	0
	push	eax
	push	_204
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	mov	eax,_bbNullObject
	jmp	_704
_704:
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_logWin
	push	_bb_Window
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+12],eax
	mov	eax,0
	jmp	_707
_707:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_710:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_1757
	push	eax
	call	_bbGCFree
	add	esp,4
_1757:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_1759
	push	eax
	call	_bbGCFree
	add	esp,4
_1759:
	mov	eax,0
	jmp	_1755
_1755:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin__init:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	mov	eax,dword [ebx+8]
	push	17
	push	_205
	push	180
	push	800
	push	800
	push	240
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,28
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	xor	eax,eax
	cmp	eax,0
	je	_1762
	mov	eax,dword [ebx+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
_1762:
	mov	eax,0
	jmp	_713
_713:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin__populate:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	ebx,dword [ebp+8]
	push	0
	push	dword [ebx+8]
	push	140
	push	775
	push	10
	push	10
	call	_bb_newListBox
	add	esp,24
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_1767
	push	eax
	call	_bbGCFree
	add	esp,4
_1767:
	mov	dword [ebx+12],esi
	push	0
	push	_1769
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,12
	mov	eax,0
	jmp	_716
_716:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin__new:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ecx,dword [ebp+8]
	mov	edx,dword [ebp+12]
	mov	eax,dword [ebp+16]
	cmp	eax,0
	je	_1772
	cmp	eax,1
	je	_1773
	cmp	eax,2
	je	_1774
	jmp	_1771
_1772:
	mov	ebx,dword [ecx+12]
	push	1
	push	edx
	push	_207
	call	_brl_system_CurrentTime
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,12
	jmp	_1771
_1773:
	mov	ebx,dword [ecx+12]
	push	1
	push	edx
	push	_208
	call	_brl_system_CurrentTime
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,12
	jmp	_1771
_1774:
	mov	ebx,dword [ecx+12]
	push	1
	push	edx
	push	_209
	call	_brl_system_CurrentTime
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,12
	jmp	_1771
_1771:
	mov	eax,0
	jmp	_721
_721:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin__update:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	call	_brl_eventqueue_EventSource
	mov	edx,dword [ebx+8]
	cmp	eax,dword [edx+8]
	jne	_1778
	push	0
	push	_210
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,12
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	je	_1782
	jmp	_1781
_1782:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+64]
	add	esp,4
	jmp	_1781
_1781:
	mov	eax,0
	jmp	_724
_1778:
	mov	eax,0
	jmp	_724
_724:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin__end:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	push	0
	push	_211
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,12
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	mov	ebx,dword [eax+16]
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_212
_214:
	mov	eax,edi
	push	_maxgui_maxgui_TGadget
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	je	_212
	push	esi
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	push	esi
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
_212:
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_214
_213:
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	mov	ebx,_bbNullObject
	inc	dword [ebx+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_1794
	push	eax
	call	_bbGCFree
	add	esp,4
_1794:
	mov	eax,dword [ebp+8]
	mov	dword [eax+8],ebx
	push	dword [ebp+8]
	push	dword [_bb_winList]
	call	_brl_linkedlist_ListRemove
	add	esp,8
	mov	eax,0
	jmp	_727
_727:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_dlog:
	push	ebp
	mov	ebp,esp
	mov	ecx,dword [ebp+8]
	mov	edx,dword [ebp+12]
	mov	eax,dword [_bb_debugWin]
	push	edx
	push	ecx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,12
	mov	eax,0
	jmp	_731
_731:
	mov	esp,ebp
	pop	ebp
	ret
__bb_MainWindow_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_MainWindow
	push	_bb_Window
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+12],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+16],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+20],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+24],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+28],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+32],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+36],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+40],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+44],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+48],eax
	mov	eax,0
	jmp	_734
_734:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_MainWindow_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_737:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_1809
	push	eax
	call	_bbGCFree
	add	esp,4
_1809:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_1811
	push	eax
	call	_bbGCFree
	add	esp,4
_1811:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_1813
	push	eax
	call	_bbGCFree
	add	esp,4
_1813:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_1815
	push	eax
	call	_bbGCFree
	add	esp,4
_1815:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_1817
	push	eax
	call	_bbGCFree
	add	esp,4
_1817:
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_1819
	push	eax
	call	_bbGCFree
	add	esp,4
_1819:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_1821
	push	eax
	call	_bbGCFree
	add	esp,4
_1821:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_1823
	push	eax
	call	_bbGCFree
	add	esp,4
_1823:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_1825
	push	eax
	call	_bbGCFree
	add	esp,4
_1825:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_1827
	push	eax
	call	_bbGCFree
	add	esp,4
_1827:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_1829
	push	eax
	call	_bbGCFree
	add	esp,4
_1829:
	mov	eax,0
	jmp	_1807
_1807:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_MainWindow__init:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	mov	eax,dword [ebx+8]
	push	589
	push	_276
	push	300
	push	400
	push	0
	push	0
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,28
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	mov	eax,dword [ebx+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	mov	eax,0
	jmp	_740
_740:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_MainWindow__populate:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	ebx,dword [ebp+8]
	push	0
	push	dword [ebx+8]
	push	20
	push	150
	push	50
	mov	eax,dword [ebx+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	sub	eax,75
	push	eax
	call	_bb_newTextField
	add	esp,24
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_1836
	push	eax
	call	_bbGCFree
	add	esp,4
_1836:
	mov	dword [ebx+12],esi
	push	2
	push	dword [ebx+8]
	push	15
	push	75
	push	85
	mov	eax,dword [ebx+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	sub	eax,75
	push	eax
	push	_42
	call	_bb_newButton
	add	esp,28
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_1840
	push	eax
	call	_bbGCFree
	add	esp,4
_1840:
	mov	dword [ebx+16],esi
	mov	eax,dword [ebx+16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	2
	push	dword [ebx+8]
	push	15
	push	75
	push	105
	mov	eax,dword [ebx+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	sub	eax,75
	push	eax
	push	_43
	call	_bb_newButton
	add	esp,28
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_1845
	push	eax
	call	_bbGCFree
	add	esp,4
_1845:
	mov	dword [ebx+20],esi
	push	2
	push	dword [ebx+8]
	push	15
	push	75
	push	85
	mov	eax,dword [ebx+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	push	eax
	push	_215
	call	_bb_newButton
	add	esp,28
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_1849
	push	eax
	call	_bbGCFree
	add	esp,4
_1849:
	mov	dword [ebx+24],esi
	push	2
	push	dword [ebx+8]
	push	15
	push	75
	push	105
	mov	eax,dword [ebx+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	push	eax
	push	_216
	call	_bb_newButton
	add	esp,28
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_1853
	push	eax
	call	_bbGCFree
	add	esp,4
_1853:
	mov	dword [ebx+28],esi
	push	8
	push	dword [ebx+8]
	push	40
	push	75
	push	150
	mov	eax,dword [ebx+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	sub	eax,37
	push	eax
	push	_217
	call	_bb_newButton
	add	esp,28
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_1857
	push	eax
	call	_bbGCFree
	add	esp,4
_1857:
	mov	dword [ebx+32],esi
	push	0
	push	0
	mov	eax,dword [ebx+8]
	push	dword [eax+20]
	push	0
	push	_218
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_1861
	push	eax
	call	_bbGCFree
	add	esp,4
_1861:
	mov	dword [ebx+36],esi
	push	0
	push	0
	push	dword [ebx+36]
	push	0
	push	_219
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_1865
	push	eax
	call	_bbGCFree
	add	esp,4
_1865:
	mov	dword [ebx+48],esi
	push	0
	push	0
	mov	eax,dword [ebx+8]
	push	dword [eax+20]
	push	0
	push	_220
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_1869
	push	eax
	call	_bbGCFree
	add	esp,4
_1869:
	mov	dword [ebx+44],esi
	push	0
	push	0
	mov	eax,dword [ebx+8]
	push	dword [eax+20]
	push	0
	push	_221
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_1873
	push	eax
	call	_bbGCFree
	add	esp,4
_1873:
	mov	dword [ebx+40],esi
	mov	eax,dword [ebx+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	mov	eax,dword [ebx+12]
	push	dword [eax+8]
	call	_maxgui_maxgui_ActivateGadget
	add	esp,4
	mov	eax,0
	jmp	_743
_743:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_MainWindow__update:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	call	_brl_eventqueue_EventSource
	cmp	eax,dword [esi+48]
	je	_1876
	cmp	eax,dword [esi+40]
	je	_1877
	cmp	eax,dword [esi+44]
	je	_1878
	mov	edx,dword [esi+12]
	cmp	eax,dword [edx+8]
	je	_1879
	mov	edx,dword [esi+32]
	cmp	eax,dword [edx+8]
	je	_1880
	mov	edx,dword [esi+16]
	cmp	eax,dword [edx+8]
	je	_1881
	mov	edx,dword [esi+20]
	cmp	eax,dword [edx+8]
	je	_1882
	mov	edx,dword [esi+24]
	cmp	eax,dword [edx+8]
	je	_1883
	mov	edx,dword [esi+28]
	cmp	eax,dword [edx+8]
	je	_1884
	jmp	_1875
_1876:
	cmp	dword [_bb_quickMode],0
	je	_1885
	mov	dword [_bb_quickMode],0
	push	_219
	push	dword [esi+48]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	jmp	_1886
_1885:
	mov	dword [_bb_quickMode],1
	push	_222
	push	dword [esi+48]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
_1886:
	mov	eax,dword [esi+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	jmp	_1875
_1877:
	push	0
	push	_1887
	call	_brl_system_Notify
	add	esp,8
	mov	eax,0
	jmp	_747
_1878:
	push	0
	push	_224
	call	_bb_dlog
	add	esp,8
	mov	eax,0
	jmp	_747
_1879:
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_1890
	jmp	_1889
_1890:
	mov	eax,dword [esi+12]
	push	dword [eax+8]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	mov	ebx,eax
	mov	eax,dword [esi+20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_1893
	cmp	dword [ebx+8],12
	jne	_1894
	push	0
	push	ebx
	push	_225
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	push	ebx
	call	_bb_findBySN
	add	esp,4
	cmp	eax,_bbNullObject
	je	_1896
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1896:
	mov	eax,dword [esi+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,0
	jmp	_747
_1894:
_1893:
	mov	eax,dword [esi+16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_1900
	push	1
	push	ebx
	call	_brl_retro_Left
	add	esp,8
	push	_265
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_1902
	cmp	dword [ebx+8],5
	jne	_1903
	push	4
	push	ebx
	call	_brl_retro_Right
	add	esp,8
	mov	ebx,eax
	push	0
	push	ebx
	push	_226
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	push	ebx
	call	_bb_findByBC
	add	esp,4
	cmp	eax,_bbNullObject
	je	_1905
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1905:
	mov	eax,dword [esi+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,0
	jmp	_747
_1903:
	jmp	_1908
_1902:
	cmp	dword [ebx+8],4
	jne	_1909
	push	0
	push	ebx
	push	_226
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	push	ebx
	call	_bb_findByBC
	add	esp,4
	cmp	eax,_bbNullObject
	je	_1911
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1911:
	mov	eax,dword [esi+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,0
	jmp	_747
_1909:
_1908:
_1900:
	mov	eax,0
	jmp	_747
_1889:
	jmp	_1875
_1880:
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_1916
	jmp	_1915
_1916:
	mov	eax,dword [esi+12]
	push	dword [eax+8]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	mov	ebx,eax
	mov	eax,dword [esi+24]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_1919
	push	0
	push	ebx
	push	_227
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	push	_228
	push	ebx
	call	_bb_newBlankStudent
	add	esp,8
	mov	eax,dword [esi+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,0
	jmp	_747
_1919:
	mov	eax,dword [esi+28]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_1923
	push	0
	push	ebx
	push	_229
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	push	ebx
	push	_228
	call	_bb_newBlankStudent
	add	esp,8
	mov	eax,dword [esi+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,0
	jmp	_747
_1923:
	jmp	_1915
_1915:
	jmp	_1875
_1881:
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_1928
	jmp	_1927
_1928:
	mov	eax,dword [esi+16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	mov	eax,dword [esi+16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_1931
	mov	eax,dword [esi+20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_1933
	mov	eax,dword [esi+20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1933:
	mov	eax,dword [esi+24]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_1936
	mov	eax,dword [esi+24]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1936:
	mov	eax,dword [esi+28]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_1939
	mov	eax,dword [esi+28]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1939:
_1931:
	jmp	_1927
_1927:
	jmp	_1875
_1882:
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_1943
	jmp	_1942
_1943:
	mov	eax,dword [esi+20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	mov	eax,dword [esi+20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_1946
	mov	eax,dword [esi+16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_1948
	mov	eax,dword [esi+16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1948:
	mov	eax,dword [esi+24]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_1951
	mov	eax,dword [esi+24]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1951:
	mov	eax,dword [esi+28]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_1954
	mov	eax,dword [esi+28]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1954:
_1946:
	jmp	_1942
_1942:
	jmp	_1875
_1883:
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_1958
	jmp	_1957
_1958:
	mov	eax,dword [esi+24]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	mov	eax,dword [esi+24]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_1961
	mov	eax,dword [esi+20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_1963
	mov	eax,dword [esi+20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1963:
	mov	eax,dword [esi+16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_1966
	mov	eax,dword [esi+16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1966:
	mov	eax,dword [esi+28]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_1969
	mov	eax,dword [esi+28]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1969:
_1961:
	jmp	_1957
_1957:
	jmp	_1875
_1884:
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_1973
	jmp	_1972
_1973:
	mov	eax,dword [esi+28]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	mov	eax,dword [esi+28]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_1976
	mov	eax,dword [esi+20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_1978
	mov	eax,dword [esi+20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1978:
	mov	eax,dword [esi+24]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_1981
	mov	eax,dword [esi+24]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1981:
	mov	eax,dword [esi+16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_1984
	mov	eax,dword [esi+16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_1984:
_1976:
	jmp	_1972
_1972:
	jmp	_1875
_1875:
	call	_brl_eventqueue_EventSource
	mov	edx,dword [esi+8]
	cmp	eax,dword [edx+8]
	jne	_1986
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	je	_1989
	jmp	_1988
_1989:
	mov	eax,1
	jmp	_747
_1988:
	mov	eax,0
	jmp	_747
_1986:
	mov	eax,dword [esi+8]
	mov	ebx,dword [eax+16]
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_230
_232:
	mov	eax,edi
	push	_maxgui_maxgui_TGadget
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	je	_230
	call	_brl_eventqueue_EventSource
	cmp	eax,esi
	jne	_1996
	call	_brl_eventqueue_EventID
	cmp	eax,8200
	je	_1999
	cmp	eax,8193
	je	_2000
	call	_brl_eventqueue_EventSource
	mov	ebx,eax
	push	0
	call	_brl_eventqueue_EventID
	push	eax
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_234
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+24]
	add	esp,4
	push	eax
	push	_233
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	jmp	_1998
_1999:
	jmp	_1998
_2000:
	jmp	_1998
_1998:
	mov	eax,0
	jmp	_747
_1996:
_230:
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_232
_231:
	mov	eax,0
	jmp	_747
_747:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_MainWindow__end:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	mov	ebx,dword [eax+16]
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_235
_237:
	mov	eax,edi
	push	_maxgui_maxgui_TGadget
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	je	_235
	push	esi
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	push	esi
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
_235:
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_237
_236:
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	push	dword [eax+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	mov	ebx,_bbNullObject
	inc	dword [ebx+4]
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_2011
	push	eax
	call	_bbGCFree
	add	esp,4
_2011:
	mov	eax,dword [ebp+8]
	mov	dword [eax+8],ebx
	push	dword [ebp+8]
	push	dword [_bb_winList]
	call	_brl_linkedlist_ListRemove
	add	esp,8
	mov	eax,0
	jmp	_750
_750:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_prompt:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	mov	edi,dword [ebp+12]
	mov	ebx,dword [ebp+20]
	push	_bb_Window
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp-4]
	push	577
	push	esi
	push	65
	push	215
	push	0
	push	0
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,28
	push	ebx
	push	dword [ebp-4]
	push	20
	push	150
	push	10
	push	10
	call	_bb_newTextField
	add	esp,24
	mov	ebx,eax
	push	8
	push	dword [ebp-4]
	push	20
	push	40
	push	10
	push	165
	push	_217
	call	_bb_newButton
	add	esp,28
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp-4]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	dword [ebx+8]
	call	_maxgui_maxgui_ActivateGadget
	add	esp,4
	jmp	_253
_255:
	call	_brl_eventqueue_WaitEvent
	call	_brl_eventqueue_EventSource
	cmp	eax,dword [ebx+8]
	je	_2019
	mov	edx,dword [ebp-8]
	cmp	eax,dword [edx+8]
	je	_2020
	mov	edx,dword [ebp-4]
	cmp	eax,dword [edx+8]
	je	_2021
	jmp	_2018
_2019:
	cmp	edi,0
	je	_2022
	push	dword [ebx+8]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	cmp	dword [eax+8],edi
	jle	_2023
	push	edi
	push	dword [ebx+8]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	push	eax
	call	_brl_retro_Left
	add	esp,8
	push	eax
	push	dword [ebx+8]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
_2023:
_2022:
	jmp	_2018
_2020:
	mov	eax,dword [_bb_quickMode]
	cmp	eax,0
	jne	_2025
	mov	eax,dword [ebp+16]
_2025:
	cmp	eax,0
	je	_2027
	mov	eax,1
	jmp	_2028
_2027:
	push	0
	push	_256
	call	_brl_system_Confirm
	add	esp,8
_2028:
	cmp	eax,0
	je	_2029
	push	dword [ebx+8]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	mov	esi,eax
	mov	eax,dword [ebp-4]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,dword [ebp-8]
	push	dword [eax+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	push	dword [ebx+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	mov	eax,dword [ebp-4]
	push	dword [eax+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	jmp	_756
_2029:
	jmp	_2018
_2021:
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	jne	_2032
	mov	eax,dword [ebp-4]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+56]
	add	esp,4
	mov	eax,dword [ebp-8]
	push	dword [eax+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	push	dword [ebx+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	mov	eax,dword [ebp-4]
	push	dword [eax+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	mov	esi,_1
	jmp	_756
_2032:
	jmp	_2018
_2018:
_253:
	mov	eax,1
	cmp	eax,0
	jne	_255
_254:
	mov	esi,_bbEmptyString
	jmp	_756
_756:
	mov	eax,esi
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_convertFromCSV:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+8]
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-8],eax
	mov	esi,_bbEmptyString
	mov	eax,dword [edi+8]
	mov	dword [ebp-4],eax
_259:
	push	1
	push	edi
	call	_brl_retro_Left
	add	esp,8
	mov	ebx,eax
	push	_73
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_2038
	push	_79
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_2039
	jmp	_258
_2039:
	push	esi
	push	dword [ebp-8]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	esi,_1
_2038:
	push	_73
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_2040
	push	ebx
	push	esi
	call	_bbStringConcat
	add	esp,8
	mov	esi,eax
_2040:
	sub	dword [ebp-4],1
	push	dword [ebp-4]
	push	edi
	call	_brl_retro_Right
	add	esp,8
	mov	edi,eax
_257:
	push	_1
	push	edi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_259
_258:
	mov	eax,dword [ebp-8]
	jmp	_759
_759:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_convertFromSSV:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-8],eax
	mov	edi,_bbEmptyString
	mov	eax,dword [esi+8]
	mov	dword [ebp-4],eax
_262:
	push	1
	push	esi
	call	_brl_retro_Left
	add	esp,8
	mov	ebx,eax
	push	_77
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_2045
	push	edi
	push	dword [ebp-8]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	edi,_1
_2045:
	push	_77
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_2046
	push	ebx
	push	edi
	call	_bbStringConcat
	add	esp,8
	mov	edi,eax
_2046:
	sub	dword [ebp-4],1
	push	dword [ebp-4]
	push	esi
	call	_brl_retro_Right
	add	esp,8
	mov	esi,eax
_260:
	push	_1
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_262
_261:
	push	_263
	push	edi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_2047
	mov	eax,dword [ebp-8]
	jmp	_762
_2047:
	mov	eax,dword [ebp-8]
	push	2
	push	_177
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+24]
	add	esp,4
	push	eax
	push	_264
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	mov	eax,dword [ebp-8]
	jmp	_762
_762:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_resetFile:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	eax,dword [ebp+8]
	mov	ebx,dword [ebp+12]
	push	eax
	call	_brl_filesystem_CloseFile
	add	esp,4
	push	1
	push	1
	push	ebx
	call	_brl_filesystem_OpenFile
	add	esp,12
	jmp	_766
_766:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextArea_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_TextArea
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	eax,0
	jmp	_769
_769:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextArea_Delete:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_772:
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_2052
	push	eax
	call	_bbGCFree
	add	esp,4
_2052:
	mov	eax,0
	jmp	_2050
_2050:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextArea__init:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	ebx,dword [ebp+8]
	mov	esi,dword [ebp+16]
	mov	edi,dword [ebp+20]
	mov	ecx,dword [ebp+32]
	mov	edx,dword [ebp+36]
	mov	eax,0
	cmp	ecx,0
	je	_2054
	mov	eax,2
_2054:
	cmp	edx,0
	je	_2055
	mov	eax,1
_2055:
	push	eax
	mov	eax,dword [ebp+28]
	push	dword [eax+8]
	push	dword [ebp+24]
	push	edi
	push	esi
	push	dword [ebp+12]
	call	_maxgui_maxgui_CreateTextArea
	add	esp,24
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_2059
	push	eax
	call	_bbGCFree
	add	esp,4
_2059:
	mov	dword [ebx+8],esi
	push	dword [ebx+8]
	mov	eax,dword [ebp+28]
	push	dword [eax+16]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	eax,0
	jmp	_782
_782:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextArea__getText:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	1
	push	-1
	push	0
	push	dword [eax+8]
	call	_maxgui_maxgui_TextAreaText
	add	esp,16
	jmp	_785
_785:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextArea__clear:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	_1
	push	dword [eax+8]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	mov	eax,0
	jmp	_788
_788:
	mov	esp,ebp
	pop	ebp
	ret
_bb_newTextArea:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+12]
	mov	edi,dword [ebp+16]
	push	_bb_TextArea
	call	_bbObjectNew
	add	esp,4
	mov	ebx,eax
	mov	eax,ebx
	push	1
	push	dword [ebp+28]
	push	dword [ebp+24]
	push	dword [ebp+20]
	push	edi
	push	esi
	push	dword [ebp+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,32
	mov	eax,ebx
	jmp	_796
_796:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_863:
	dd	0
_278:
	db	"z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0",0
_279:
	db	"New",0
_280:
	db	"()i",0
_281:
	db	"Delete",0
	align	4
_277:
	dd	2
	dd	_278
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	0
	align	4
_28:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_277
	dd	8
	dd	__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_New
	dd	__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_283:
	db	"z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea",0
_284:
	db	"Name",0
_285:
	db	"$",0
	align	4
_286:
	dd	_bbStringClass
	dd	2147483646
	dd	12
	dw	105,80,97,100,32,77,97,110,97,103,101,114
_287:
	db	"MajorVersion",0
_288:
	db	"i",0
	align	4
_289:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	48
_290:
	db	"MinorVersion",0
_291:
	db	"Revision",0
	align	4
_292:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	49
_293:
	db	"VersionString",0
	align	4
_294:
	dd	_bbStringClass
	dd	2147483646
	dd	5
	dw	48,46,48,46,49
_295:
	db	"AssemblyInfo",0
	align	4
_296:
	dd	_bbStringClass
	dd	2147483646
	dd	18
	dw	105,80,97,100,32,77,97,110,97,103,101,114,32,48,46,48
	dw	46,49
_297:
	db	"Platform",0
	align	4
_298:
	dd	_bbStringClass
	dd	2147483646
	dd	5
	dw	87,105,110,51,50
_299:
	db	"Architecture",0
	align	4
_300:
	dd	_bbStringClass
	dd	2147483646
	dd	3
	dw	120,56,54
_301:
	db	"DebugOn",0
	align	4
_282:
	dd	2
	dd	_283
	dd	1
	dd	_284
	dd	_285
	dd	_286
	dd	1
	dd	_287
	dd	_288
	dd	_289
	dd	1
	dd	_290
	dd	_288
	dd	_289
	dd	1
	dd	_291
	dd	_288
	dd	_292
	dd	1
	dd	_293
	dd	_285
	dd	_294
	dd	1
	dd	_295
	dd	_285
	dd	_296
	dd	1
	dd	_297
	dd	_285
	dd	_298
	dd	1
	dd	_299
	dd	_285
	dd	_300
	dd	1
	dd	_301
	dd	_288
	dd	_289
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	0
	align	4
_29:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_282
	dd	8
	dd	__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New
	dd	__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	align	4
__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Application:
	dd	_bbNullObject
	align	4
__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Resources:
	dd	_bbNullObject
_303:
	db	"z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea",0
	align	4
_302:
	dd	2
	dd	_303
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	0
	align	4
_35:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_302
	dd	8
	dd	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New
	dd	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_305:
	db	"Window",0
_306:
	db	"handle",0
_307:
	db	":TGadget",0
_308:
	db	"id",0
_309:
	db	"gadgetList",0
_310:
	db	":TList",0
_311:
	db	"menu",0
_312:
	db	"_new",0
_313:
	db	"(i,i,i,i,$,i)i",0
_314:
	db	"_show",0
_315:
	db	"_hide",0
_316:
	db	"_update",0
	align	4
_304:
	dd	2
	dd	_305
	dd	3
	dd	_306
	dd	_307
	dd	8
	dd	3
	dd	_308
	dd	_285
	dd	12
	dd	3
	dd	_309
	dd	_310
	dd	16
	dd	3
	dd	_311
	dd	_307
	dd	20
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	6
	dd	_312
	dd	_313
	dd	48
	dd	6
	dd	_314
	dd	_280
	dd	52
	dd	6
	dd	_315
	dd	_280
	dd	56
	dd	6
	dd	_316
	dd	_280
	dd	60
	dd	0
	align	4
_bb_Window:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_304
	dd	24
	dd	__bb_Window_New
	dd	__bb_Window_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_Window__new
	dd	__bb_Window__show
	dd	__bb_Window__hide
	dd	__bb_Window__update
_318:
	db	"Button",0
_319:
	db	"state",0
_320:
	db	"_init",0
_321:
	db	"($,i,i,i,i,:Window,i)i",0
_322:
	db	"_state",0
_323:
	db	"_cstate",0
	align	4
_317:
	dd	2
	dd	_318
	dd	3
	dd	_306
	dd	_307
	dd	8
	dd	3
	dd	_319
	dd	_288
	dd	12
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	6
	dd	_320
	dd	_321
	dd	48
	dd	6
	dd	_322
	dd	_280
	dd	52
	dd	6
	dd	_323
	dd	_280
	dd	56
	dd	0
	align	4
_bb_Button:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_317
	dd	16
	dd	__bb_Button_New
	dd	__bb_Button_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_Button__init
	dd	__bb_Button__state
	dd	__bb_Button__cstate
_325:
	db	"TextField",0
_326:
	db	"(i,i,i,i,:Window,i)i",0
_327:
	db	"_getText",0
_328:
	db	"()$",0
_329:
	db	"_clear",0
	align	4
_324:
	dd	2
	dd	_325
	dd	3
	dd	_306
	dd	_307
	dd	8
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	6
	dd	_320
	dd	_326
	dd	48
	dd	6
	dd	_327
	dd	_328
	dd	52
	dd	6
	dd	_329
	dd	_280
	dd	56
	dd	0
	align	4
_bb_TextField:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_324
	dd	12
	dd	__bb_TextField_New
	dd	__bb_TextField_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_TextField__init
	dd	__bb_TextField__getText
	dd	__bb_TextField__clear
_331:
	db	"ComboBox",0
	align	4
_330:
	dd	2
	dd	_331
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	0
	align	4
_bb_ComboBox:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_330
	dd	8
	dd	__bb_ComboBox_New
	dd	__bb_ComboBox_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_333:
	db	"ListBox",0
_334:
	db	"_add",0
_335:
	db	"($,i)i",0
_336:
	db	"_modify",0
_337:
	db	"($)i",0
_338:
	db	"_remove",0
	align	4
_332:
	dd	2
	dd	_333
	dd	3
	dd	_306
	dd	_307
	dd	8
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	6
	dd	_320
	dd	_326
	dd	48
	dd	6
	dd	_334
	dd	_335
	dd	52
	dd	6
	dd	_336
	dd	_337
	dd	56
	dd	6
	dd	_338
	dd	_280
	dd	60
	dd	6
	dd	_327
	dd	_328
	dd	64
	dd	0
	align	4
_bb_ListBox:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_332
	dd	12
	dd	__bb_ListBox_New
	dd	__bb_ListBox_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_ListBox__init
	dd	__bb_ListBox__add
	dd	__bb_ListBox__modify
	dd	__bb_ListBox__remove
	dd	__bb_ListBox__getText
_340:
	db	"iPad",0
_341:
	db	":Window",0
_342:
	db	"pos",0
_343:
	db	"bc",0
_344:
	db	"sn",0
_345:
	db	"ownerPos",0
_346:
	db	"iEditMenu",0
_347:
	db	"ownerMenu",0
_348:
	db	"actionMenu",0
_349:
	db	"bcEdit",0
_350:
	db	"snEdit",0
_351:
	db	"damageAction",0
_352:
	db	"repairAction",0
_353:
	db	"coutAction",0
_354:
	db	"lostAction",0
_355:
	db	"stolenAction",0
_356:
	db	"nEditMenu",0
_357:
	db	"newMenu",0
_358:
	db	"contextMenu",0
_359:
	db	"infNew",0
_360:
	db	"actNew",0
_361:
	db	"misNew",0
_362:
	db	"noteList",0
_363:
	db	":ListBox",0
_364:
	db	"bcLabel",0
_365:
	db	"snLabel",0
_366:
	db	"stLabel",0
_367:
	db	"noteTypeList",0
_368:
	db	"_build",0
_369:
	db	"_save",0
_370:
	db	"_addNote",0
_371:
	db	"($):Note",0
_372:
	db	"_noteBySummary",0
	align	4
_339:
	dd	2
	dd	_340
	dd	3
	dd	_306
	dd	_341
	dd	8
	dd	3
	dd	_342
	dd	_288
	dd	12
	dd	3
	dd	_343
	dd	_285
	dd	16
	dd	3
	dd	_344
	dd	_285
	dd	20
	dd	3
	dd	_319
	dd	_285
	dd	24
	dd	3
	dd	_345
	dd	_288
	dd	28
	dd	3
	dd	_346
	dd	_307
	dd	32
	dd	3
	dd	_347
	dd	_307
	dd	36
	dd	3
	dd	_348
	dd	_307
	dd	40
	dd	3
	dd	_349
	dd	_307
	dd	44
	dd	3
	dd	_350
	dd	_307
	dd	48
	dd	3
	dd	_351
	dd	_307
	dd	52
	dd	3
	dd	_352
	dd	_307
	dd	56
	dd	3
	dd	_353
	dd	_307
	dd	60
	dd	3
	dd	_354
	dd	_307
	dd	64
	dd	3
	dd	_355
	dd	_307
	dd	68
	dd	3
	dd	_356
	dd	_307
	dd	72
	dd	3
	dd	_357
	dd	_307
	dd	76
	dd	3
	dd	_358
	dd	_307
	dd	80
	dd	3
	dd	_359
	dd	_307
	dd	84
	dd	3
	dd	_360
	dd	_307
	dd	88
	dd	3
	dd	_361
	dd	_307
	dd	92
	dd	3
	dd	_362
	dd	_363
	dd	96
	dd	3
	dd	_364
	dd	_307
	dd	100
	dd	3
	dd	_365
	dd	_307
	dd	104
	dd	3
	dd	_366
	dd	_307
	dd	108
	dd	3
	dd	_367
	dd	_310
	dd	112
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	6
	dd	_320
	dd	_280
	dd	48
	dd	6
	dd	_314
	dd	_280
	dd	52
	dd	6
	dd	_368
	dd	_280
	dd	56
	dd	6
	dd	_315
	dd	_280
	dd	60
	dd	6
	dd	_369
	dd	_280
	dd	64
	dd	6
	dd	_370
	dd	_371
	dd	68
	dd	6
	dd	_316
	dd	_280
	dd	72
	dd	6
	dd	_372
	dd	_371
	dd	76
	dd	0
	align	4
_bb_iPad:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_339
	dd	116
	dd	__bb_iPad_New
	dd	__bb_iPad_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_iPad__init
	dd	__bb_iPad__show
	dd	__bb_iPad__build
	dd	__bb_iPad__hide
	dd	__bb_iPad__save
	dd	__bb_iPad__addNote
	dd	__bb_iPad__update
	dd	__bb_iPad__noteBySummary
_374:
	db	"Note",0
_375:
	db	"sumField",0
_376:
	db	":TextField",0
_377:
	db	"detField",0
_378:
	db	":TextArea",0
_379:
	db	"NTypeLabel",0
_380:
	db	"summary",0
_381:
	db	"nType",0
_382:
	db	"details",0
_383:
	db	"date",0
_384:
	db	"_typeByInt",0
_385:
	db	"(i)$",0
	align	4
_373:
	dd	2
	dd	_374
	dd	3
	dd	_306
	dd	_341
	dd	8
	dd	3
	dd	_375
	dd	_376
	dd	12
	dd	3
	dd	_377
	dd	_378
	dd	16
	dd	3
	dd	_379
	dd	_307
	dd	20
	dd	3
	dd	_380
	dd	_285
	dd	24
	dd	3
	dd	_381
	dd	_288
	dd	28
	dd	3
	dd	_382
	dd	_285
	dd	32
	dd	3
	dd	_383
	dd	_285
	dd	36
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	6
	dd	_320
	dd	_280
	dd	48
	dd	6
	dd	_384
	dd	_385
	dd	52
	dd	6
	dd	_316
	dd	_280
	dd	56
	dd	6
	dd	_369
	dd	_280
	dd	60
	dd	6
	dd	_315
	dd	_280
	dd	64
	dd	0
	align	4
_bb_Note:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_373
	dd	40
	dd	__bb_Note_New
	dd	__bb_Note_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_Note__init
	dd	__bb_Note__typeByInt
	dd	__bb_Note__update
	dd	__bb_Note__save
	dd	__bb_Note__hide
_387:
	db	"Student",0
_388:
	db	"first",0
_389:
	db	"last",0
_390:
	db	"balance",0
_391:
	db	"f",0
_392:
	db	"ipadPos",0
_393:
	db	"grade",0
_394:
	db	"fnLabel",0
_395:
	db	"lnLabel",0
_396:
	db	"balLabel",0
_397:
	db	"ipad",0
_398:
	db	":Ipad",0
_399:
	db	"fnEdit",0
_400:
	db	"lnEdit",0
_401:
	db	"gEdit",0
_402:
	db	"ipadAction",0
_403:
	db	"chargerFee",0
_404:
	db	"cableFee",0
_405:
	db	"caseFee",0
_406:
	db	"extraFee",0
_407:
	db	"lostIpad",0
_408:
	db	"stolenIpad",0
_409:
	db	"damagedIpad",0
_410:
	db	"iPadMenu",0
	align	4
_386:
	dd	2
	dd	_387
	dd	3
	dd	_306
	dd	_341
	dd	8
	dd	3
	dd	_342
	dd	_288
	dd	12
	dd	3
	dd	_388
	dd	_285
	dd	16
	dd	3
	dd	_389
	dd	_285
	dd	20
	dd	3
	dd	_390
	dd	_391
	dd	24
	dd	3
	dd	_392
	dd	_288
	dd	28
	dd	3
	dd	_393
	dd	_288
	dd	32
	dd	3
	dd	_362
	dd	_363
	dd	36
	dd	3
	dd	_394
	dd	_307
	dd	40
	dd	3
	dd	_395
	dd	_307
	dd	44
	dd	3
	dd	_396
	dd	_307
	dd	48
	dd	3
	dd	_367
	dd	_310
	dd	52
	dd	3
	dd	_397
	dd	_398
	dd	56
	dd	3
	dd	_399
	dd	_307
	dd	60
	dd	3
	dd	_400
	dd	_307
	dd	64
	dd	3
	dd	_401
	dd	_307
	dd	68
	dd	3
	dd	_402
	dd	_307
	dd	72
	dd	3
	dd	_403
	dd	_307
	dd	76
	dd	3
	dd	_404
	dd	_307
	dd	80
	dd	3
	dd	_405
	dd	_307
	dd	84
	dd	3
	dd	_406
	dd	_307
	dd	88
	dd	3
	dd	_407
	dd	_307
	dd	92
	dd	3
	dd	_408
	dd	_307
	dd	96
	dd	3
	dd	_409
	dd	_307
	dd	100
	dd	3
	dd	_410
	dd	_307
	dd	104
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	6
	dd	_320
	dd	_280
	dd	48
	dd	6
	dd	_314
	dd	_280
	dd	52
	dd	6
	dd	_315
	dd	_280
	dd	56
	dd	6
	dd	_368
	dd	_280
	dd	60
	dd	6
	dd	_369
	dd	_280
	dd	64
	dd	6
	dd	_370
	dd	_371
	dd	68
	dd	6
	dd	_316
	dd	_280
	dd	72
	dd	6
	dd	_372
	dd	_371
	dd	76
	dd	0
	align	4
_bb_Student:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_386
	dd	108
	dd	__bb_Student_New
	dd	__bb_Student_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_Student__init
	dd	__bb_Student__show
	dd	__bb_Student__hide
	dd	__bb_Student__build
	dd	__bb_Student__save
	dd	__bb_Student__addNote
	dd	__bb_Student__update
	dd	__bb_Student__noteBySummary
_412:
	db	"logWin",0
_413:
	db	"entryList",0
_414:
	db	"_populate",0
_415:
	db	"_end",0
	align	4
_411:
	dd	2
	dd	_412
	dd	3
	dd	_306
	dd	_341
	dd	8
	dd	3
	dd	_413
	dd	_363
	dd	12
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	6
	dd	_320
	dd	_280
	dd	48
	dd	6
	dd	_414
	dd	_280
	dd	52
	dd	6
	dd	_312
	dd	_335
	dd	56
	dd	6
	dd	_316
	dd	_280
	dd	60
	dd	6
	dd	_415
	dd	_280
	dd	64
	dd	0
	align	4
_bb_logWin:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_411
	dd	16
	dd	__bb_logWin_New
	dd	__bb_logWin_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_logWin__init
	dd	__bb_logWin__populate
	dd	__bb_logWin__new
	dd	__bb_logWin__update
	dd	__bb_logWin__end
_417:
	db	"MainWindow",0
_418:
	db	"snTF",0
_419:
	db	"bcBox",0
_420:
	db	":Button",0
_421:
	db	"snBox",0
_422:
	db	"sfnBox",0
_423:
	db	"slnBox",0
_424:
	db	"goBut",0
_425:
	db	"modeMenu",0
_426:
	db	"aboutMenu",0
_427:
	db	"helpMenu",0
_428:
	db	"qmFile",0
_429:
	db	"(:TEvent)i",0
	align	4
_416:
	dd	2
	dd	_417
	dd	3
	dd	_306
	dd	_341
	dd	8
	dd	3
	dd	_418
	dd	_376
	dd	12
	dd	3
	dd	_419
	dd	_420
	dd	16
	dd	3
	dd	_421
	dd	_420
	dd	20
	dd	3
	dd	_422
	dd	_420
	dd	24
	dd	3
	dd	_423
	dd	_420
	dd	28
	dd	3
	dd	_424
	dd	_420
	dd	32
	dd	3
	dd	_425
	dd	_307
	dd	36
	dd	3
	dd	_426
	dd	_307
	dd	40
	dd	3
	dd	_427
	dd	_307
	dd	44
	dd	3
	dd	_428
	dd	_307
	dd	48
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	6
	dd	_320
	dd	_280
	dd	48
	dd	6
	dd	_414
	dd	_280
	dd	52
	dd	6
	dd	_316
	dd	_429
	dd	56
	dd	6
	dd	_415
	dd	_280
	dd	60
	dd	0
	align	4
_bb_MainWindow:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_416
	dd	52
	dd	__bb_MainWindow_New
	dd	__bb_MainWindow_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_MainWindow__init
	dd	__bb_MainWindow__populate
	dd	__bb_MainWindow__update
	dd	__bb_MainWindow__end
_431:
	db	"TextArea",0
_432:
	db	"(i,i,i,i,:Window,i,i)i",0
	align	4
_430:
	dd	2
	dd	_431
	dd	3
	dd	_306
	dd	_307
	dd	8
	dd	6
	dd	_279
	dd	_280
	dd	16
	dd	6
	dd	_281
	dd	_280
	dd	20
	dd	6
	dd	_320
	dd	_432
	dd	48
	dd	6
	dd	_327
	dd	_328
	dd	52
	dd	6
	dd	_329
	dd	_280
	dd	56
	dd	0
	align	4
_bb_TextArea:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_430
	dd	12
	dd	__bb_TextArea_New
	dd	__bb_TextArea_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_TextArea__init
	dd	__bb_TextArea__getText
	dd	__bb_TextArea__clear
	align	4
_798:
	dd	_bbNullObject
	align	4
_800:
	dd	0
	align	4
_bb_winList:
	dd	_bbNullObject
	align	4
_bb_ipadList:
	dd	_bbNullObject
	align	4
_bb_studentList:
	dd	_bbNullObject
	align	4
_bb_quickMode:
	dd	0
	align	4
_bb_debugWin:
	dd	_bbNullObject
	align	4
_1:
	dd	_bbStringClass
	dd	2147483647
	dd	0
	align	4
_bb_dataDir:
	dd	_1
	align	4
_809:
	dd	_bbStringClass
	dd	2147483647
	dd	24
	dw	89,111,117,32,97,114,101,32,114,117,110,110,105,110,103,32
	dw	111,110,32,87,105,110,51,50
	align	4
_39:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	105,112,97,100,100,98,46,99,115,118
	align	4
_bb_ipaddbn:
	dd	_bbEmptyString
	align	4
_bb_iPaddb:
	dd	_bbNullObject
	align	4
_bb_noteWinList:
	dd	_bbNullObject
	align	4
_169:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	115,116,117,100,100,98,46,99,115,118
	align	4
_bb_studdbn:
	dd	_bbEmptyString
	align	4
_bb_studdb:
	dd	_bbNullObject
	align	4
_36:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	52,51,50,49,120,103,51,116
	align	4
_238:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	80,97,115,115,119,111,114,100,58,32
	align	4
_833:
	dd	_bbStringClass
	dd	2147483647
	dd	97
	dw	89,111,117,32,109,117,115,116,32,101,110,116,101,114,32,116
	dw	104,101,32,99,111,114,114,101,99,116,32,112,97,115,115,119
	dw	111,114,100,32,102,111,114,32,105,80,97,100,32,77,97,110
	dw	97,103,101,114,32,48,46,48,46,49,32,116,111,32,119,111
	dw	114,107,46,32,80,108,101,97,115,101,32,97,115,107,32,73
	dw	84,32,102,111,114,32,97,115,115,105,115,116,97,110,99,101
	dw	46
	align	4
_40:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	83,116,117,100,101,110,116,32,105,80,97,100
	align	4
_41:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	69,100,105,116
	align	4
_42:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	66,97,114,99,111,100,101
	align	4
_43:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	83,101,114,105,97,108
	align	4
_44:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	65,99,116,105,111,110
	align	4
_45:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	67,104,101,99,107,32,79,117,116
	align	4
_46:
	dd	_bbStringClass
	dd	2147483647
	dd	13
	dw	82,101,112,111,114,116,32,68,97,109,97,103,101
	align	4
_47:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	83,101,110,100,32,102,111,114,32,82,101,112,97,105,114
	align	4
_48:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	77,97,114,107,32,97,115,32,76,111,115,116
	align	4
_49:
	dd	_bbStringClass
	dd	2147483647
	dd	14
	dw	77,97,114,107,32,97,115,32,83,116,111,108,101,110
	align	4
_50:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	99,111,110,116,101,120,116,77,101,110,117
	align	4
_51:
	dd	_bbStringClass
	dd	2147483647
	dd	3
	dw	78,101,119
	align	4
_52:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	73,110,102,111,114,109,97,116,105,111,110
	align	4
_53:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	79,116,104,101,114
	align	4
_57:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	66,97,114,99,111,100,101,58,32
	align	4
_58:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	83,101,114,105,97,108,58,32
	align	4
_59:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	83,116,97,116,101,58,32
	align	4
_60:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	82,101,103,105,115,116,101,114,101,100
	align	4
_61:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	67,104,101,99,107,101,100,32,73,110
	align	4
_62:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	67,104,101,99,107,101,100,32,79,117,116
	align	4
_65:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	83,101,110,116,32,102,111,114,32,82,101,112,97,105,114
	align	4
_66:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	68,97,109,97,103,101,100
	align	4
_67:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	76,111,115,116
	align	4
_68:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	83,116,111,108,101,110
	align	4
_63:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	67,104,101,99,107,32,73,110
	align	4
_64:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	79,119,110,101,114
	align	4
_69:
	dd	_bbStringClass
	dd	2147483647
	dd	58
	dw	87,111,117,108,100,32,121,111,117,32,108,105,107,101,32,116
	dw	111,32,119,114,105,116,101,32,121,111,117,114,32,99,104,97
	dw	110,103,101,115,32,98,97,99,107,32,116,111,32,116,104,101
	dw	32,100,97,116,97,98,97,115,101,63
	align	4
_73:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	44
	align	4
_78:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	101,111,110,44
	align	4
_77:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	59
	align	4
_79:
	dd	_bbStringClass
	dd	2147483647
	dd	3
	dw	101,111,101
	align	4
_32:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	32
	align	4
_85:
	dd	_bbStringClass
	dd	2147483647
	dd	188
	dw	84,104,105,115,32,119,105,108,108,32,99,104,97,110,103,101
	dw	32,116,104,101,32,73,68,32,105,110,32,116,104,101,32,100
	dw	97,116,97,98,97,115,101,32,97,110,100,32,121,111,117,32
	dw	119,105,108,108,32,110,111,32,108,111,110,103,101,114,32,102
	dw	105,110,100,32,116,104,101,32,105,80,97,100,32,119,105,116
	dw	104,32,116,104,101,32,111,108,100,32,66,97,114,99,111,100
	dw	101,46,32,65,108,115,111,32,105,102,32,121,111,117,32,112
	dw	114,111,118,105,100,101,32,97,110,32,97,108,114,101,97,100
	dw	121,32,117,115,101,100,32,98,97,114,99,111,100,101,32,116
	dw	104,105,115,32,101,110,116,114,121,32,119,105,108,108,32,111
	dw	118,101,114,119,114,105,116,101,32,116,104,101,32,112,114,101
	dw	118,105,111,117,115,32,101,110,116,114,121,46
	align	4
_37:
	dd	_bbStringClass
	dd	2147483647
	dd	147
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32
	align	4
_84:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	63
	align	4
_83:
	dd	_bbStringClass
	dd	2147483647
	dd	58
	dw	65,114,101,32,121,111,117,32,115,117,114,101,32,116,104,97
	dw	116,32,121,111,117,32,119,97,110,116,32,116,111,32,99,104
	dw	97,110,103,101,32,116,104,101,32,66,97,114,99,111,100,101
	dw	32,102,111,114,32,105,80,97,100,32
	align	4
_86:
	dd	_bbStringClass
	dd	2147483647
	dd	24
	dw	80,108,101,97,115,101,32,101,110,116,101,114,32,110,101,119
	dw	32,66,97,114,99,111,100,101
	align	4
_90:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	93,59,101,111,110
	align	4
_89:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	93,32,116,111,32,91
	align	4
_88:
	dd	_bbStringClass
	dd	2147483647
	dd	34
	dw	93,59,49,59,84,104,101,32,98,97,114,99,111,100,101,32
	dw	119,97,115,32,99,104,97,110,103,101,100,32,102,114,111,109
	dw	32,91
	align	4
_87:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	67,104,97,110,103,101,100,32,66,97,114,99,111,100,101,59
	dw	91
	align	4
_92:
	dd	_bbStringClass
	dd	2147483647
	dd	204
	dw	84,104,105,115,32,105,115,32,100,97,110,103,101,114,111,117
	dw	115,32,97,110,100,32,119,105,108,108,32,99,104,97,110,103
	dw	101,32,116,104,101,32,73,68,32,105,110,32,116,104,101,32
	dw	100,97,116,97,98,97,115,101,32,97,110,100,32,121,111,117
	dw	32,119,105,108,108,32,110,111,32,108,111,110,103,101,114,32
	dw	102,105,110,100,32,116,104,101,32,105,80,97,100,32,119,105
	dw	116,104,32,116,104,101,32,111,108,100,32,115,101,114,105,97
	dw	108,46,32,65,108,115,111,32,105,102,32,121,111,117,32,112
	dw	114,111,118,105,100,101,32,97,110,32,97,108,114,101,97,100
	dw	121,32,117,115,101,100,32,98,97,114,99,111,100,101,32,116
	dw	104,105,115,32,101,110,116,114,121,32,119,105,108,108,32,111
	dw	118,101,114,119,114,105,116,101,32,116,104,101,32,112,114,101
	dw	118,105,111,117,115,32,101,110,116,114,121,46
	align	4
_91:
	dd	_bbStringClass
	dd	2147483647
	dd	57
	dw	65,114,101,32,121,111,117,32,115,117,114,101,32,116,104,97
	dw	116,32,121,111,117,32,119,97,110,116,32,116,111,32,99,104
	dw	97,110,103,101,32,116,104,101,32,83,101,114,105,97,108,32
	dw	102,111,114,32,105,80,97,100,32
	align	4
_93:
	dd	_bbStringClass
	dd	2147483647
	dd	23
	dw	80,108,101,97,115,101,32,101,110,116,101,114,32,110,101,119
	dw	32,83,101,114,105,97,108
	align	4
_95:
	dd	_bbStringClass
	dd	2147483647
	dd	33
	dw	93,59,49,59,84,104,101,32,115,101,114,105,97,108,32,119
	dw	97,115,32,99,104,97,110,103,101,100,32,102,114,111,109,32
	dw	91
	align	4
_94:
	dd	_bbStringClass
	dd	2147483647
	dd	16
	dw	67,104,97,110,103,101,100,32,83,101,114,105,97,108,59,91
	align	4
_96:
	dd	_bbStringClass
	dd	2147483647
	dd	22
	dw	83,72,79,87,32,79,87,78,69,82,32,65,67,84,73,79
	dw	78,32,72,69,82,69
	align	4
_100:
	dd	_bbStringClass
	dd	2147483647
	dd	61
	dw	93,46,32,80,108,101,97,115,101,32,97,100,100,32,97,110
	dw	121,32,101,120,116,114,97,32,110,111,116,101,115,32,100,101
	dw	115,99,114,105,98,105,110,103,32,116,104,101,32,100,97,109
	dw	97,103,101,32,104,101,114,101,46,59,101,111,110
	align	4
_99:
	dd	_bbStringClass
	dd	2147483647
	dd	80
	dw	46,32,84,104,101,32,111,119,110,101,114,32,114,101,115,112
	dw	111,110,115,105,98,108,101,32,119,97,115,32,91,73,78,83
	dw	69,82,84,32,83,84,85,68,69,78,84,32,78,65,77,69
	dw	32,72,69,82,69,93,46,32,84,104,101,32,112,114,101,118
	dw	105,111,117,115,32,115,116,97,116,101,32,119,97,115,32,91
	align	4
_98:
	dd	_bbStringClass
	dd	2147483647
	dd	43
	dw	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	dw	32,114,101,112,111,114,116,101,100,32,100,97,109,97,103,101
	dw	100,32,116,111,32,117,115,32,97,116,32
	align	4
_97:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	105,80,97,100,32,119,97,115,32,68,97,109,97,103,101,100
	dw	59,91
	align	4
_101:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	65,112,112,108,101,32,80,79,32,78,117,109,98,101,114
	align	4
_105:
	dd	_bbStringClass
	dd	2147483647
	dd	26
	dw	46,32,84,104,101,32,112,114,101,118,105,111,117,115,32,115
	dw	116,97,116,101,32,119,97,115,32,91
	align	4
_104:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	32,119,105,116,104,32,80,46,79,46,32,110,117,109,98,101
	dw	114,32
	align	4
_103:
	dd	_bbStringClass
	dd	2147483647
	dd	36
	dw	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	dw	32,115,101,110,116,32,102,111,114,32,114,101,112,97,105,114
	dw	32,97,116,32
	align	4
_102:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	83,101,110,116,32,102,111,114,32,82,101,112,97,105,114,59
	dw	91
	align	4
_108:
	dd	_bbStringClass
	dd	2147483647
	dd	56
	dw	32,116,111,32,91,73,78,83,69,82,84,32,83,84,85,68
	dw	69,78,84,32,78,65,77,69,32,72,69,82,69,93,46,32
	dw	84,104,101,32,112,114,101,118,105,111,117,115,32,115,116,97
	dw	116,101,32,119,97,115,32,91
	align	4
_107:
	dd	_bbStringClass
	dd	2147483647
	dd	32
	dw	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	dw	32,99,104,101,99,107,101,100,32,111,117,116,32,97,116,32
	align	4
_106:
	dd	_bbStringClass
	dd	2147483647
	dd	13
	dw	67,104,101,99,107,101,100,32,79,117,116,59,91
	align	4
_111:
	dd	_bbStringClass
	dd	2147483647
	dd	58
	dw	32,102,114,111,109,32,91,73,78,83,69,82,84,32,83,84
	dw	85,68,69,78,84,32,78,65,77,69,32,72,69,82,69,93
	dw	46,32,84,104,101,32,112,114,101,118,105,111,117,115,32,115
	dw	116,97,116,101,32,119,97,115,32,91
	align	4
_110:
	dd	_bbStringClass
	dd	2147483647
	dd	31
	dw	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	dw	32,99,104,101,99,107,101,100,32,105,110,32,97,116,32
	align	4
_109:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	67,104,101,99,107,101,100,32,73,110,59,91
	align	4
_114:
	dd	_bbStringClass
	dd	2147483647
	dd	66
	dw	32,98,121,32,116,104,101,32,111,119,110,101,114,32,91,73
	dw	78,83,69,82,84,32,83,84,85,68,69,78,84,32,78,65
	dw	77,69,32,72,69,82,69,93,46,32,84,104,101,32,112,114
	dw	101,118,105,111,117,115,32,115,116,97,116,101,32,119,97,115
	dw	32,91
	align	4
_113:
	dd	_bbStringClass
	dd	2147483647
	dd	40
	dw	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	dw	32,114,101,112,111,114,116,101,100,32,108,111,115,116,32,116
	dw	111,32,117,115,32,97,116,32
	align	4
_112:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	105,80,97,100,32,119,97,115,32,76,111,115,116,59,91
	align	4
_115:
	dd	_bbStringClass
	dd	2147483647
	dd	30
	dw	80,111,108,105,99,101,32,82,101,112,111,114,116,32,82,101
	dw	102,101,114,101,110,99,101,32,78,117,109,98,101,114
	align	4
_119:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	59,101,111,110
	align	4
_118:
	dd	_bbStringClass
	dd	2147483647
	dd	41
	dw	93,46,32,84,104,101,32,112,111,108,105,99,101,32,114,101
	dw	112,111,114,116,32,114,101,102,101,114,101,110,99,101,32,110
	dw	117,109,98,101,114,32,105,115,32
	align	4
_117:
	dd	_bbStringClass
	dd	2147483647
	dd	42
	dw	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	dw	32,114,101,112,111,114,116,101,100,32,115,116,111,108,101,110
	dw	32,116,111,32,117,115,32,97,116,32
	align	4
_116:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	105,80,97,100,32,119,97,115,32,83,116,111,108,101,110,59
	dw	91
	align	4
_121:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	93,59,48,59,78,101,119,32,68,101,116,97,105,108,115,59
	dw	101,111,110
	align	4
_120:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	78,101,119,32,78,111,116,101,59,91
	align	4
_122:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	93,59,49,59,78,101,119,32,68,101,116,97,105,108,115,59
	dw	101,111,110
	align	4
_123:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	93,59,50,59,78,101,119,32,68,101,116,97,105,108,115,59
	dw	101,111,110
	align	4
_136:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	78,101,101,100,32,83,101,114,105,97,108
	align	4
_139:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	32,97,116,32
	align	4
_138:
	dd	_bbStringClass
	dd	2147483647
	dd	23
	dw	93,59,49,59,82,101,103,105,115,116,101,114,101,100,32,105
	dw	80,97,100,32,111,110,32
	align	4
_137:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	82,101,103,105,115,116,101,114,101,100,32,105,80,97,100,59
	dw	91
	align	4
_140:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	45,45,45,45
	align	4
_144:
	dd	_bbStringClass
	dd	2147483647
	dd	72
	dw	84,104,105,115,32,105,80,97,100,32,105,115,32,99,117,114
	dw	114,101,110,116,108,121,32,98,101,105,110,103,32,97,99,99
	dw	101,115,115,101,100,32,101,108,115,101,119,104,101,114,101,44
	dw	32,112,108,101,97,115,101,32,116,114,121,32,97,103,97,105
	dw	110,32,108,97,116,101,114,46
	align	4
_152:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	93,32,67,114,101,97,116,101,63
	align	4
_151:
	dd	_bbStringClass
	dd	2147483647
	dd	51
	dw	84,104,101,114,101,32,105,115,32,110,111,32,101,110,116,114
	dw	121,32,105,110,32,39,105,112,97,100,100,98,46,99,115,118
	dw	39,32,119,105,116,104,32,116,104,101,32,83,101,114,105,97
	dw	108,32,91
	align	4
_162:
	dd	_bbStringClass
	dd	2147483647
	dd	52
	dw	84,104,101,114,101,32,105,115,32,110,111,32,101,110,116,114
	dw	121,32,105,110,32,39,105,112,97,100,100,98,46,99,115,118
	dw	39,32,119,105,116,104,32,116,104,101,32,66,97,114,99,111
	dw	100,101,32,91
	align	4
_163:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	105,80,97,100,32,78,111,116,101,32
	align	4
_164:
	dd	_bbStringClass
	dd	2147483647
	dd	33
	dw	87,111,117,108,100,32,121,111,117,32,108,105,107,101,32,116
	dw	111,32,115,97,118,101,32,116,104,105,115,32,110,111,116,101
	dw	63
	align	4
_165:
	dd	_bbStringClass
	dd	2147483647
	dd	27
	dw	89,111,117,32,99,97,110,110,111,116,32,117,115,101,32,99
	dw	111,109,109,97,115,32,104,101,114,101,46
	align	4
_166:
	dd	_bbStringClass
	dd	2147483647
	dd	31
	dw	89,111,117,32,99,97,110,110,111,116,32,117,115,101,32,115
	dw	101,109,105,99,111,108,111,110,115,32,104,101,114,101,46
	align	4
_167:
	dd	_bbStringClass
	dd	2147483647
	dd	81
	dw	84,104,101,32,114,101,116,117,114,110,32,107,101,121,32,119
	dw	97,115,32,117,115,101,100,32,105,110,32,97,32,110,111,116
	dw	101,46,32,84,104,105,115,32,109,97,121,32,104,97,118,101
	dw	32,99,111,114,114,117,112,116,101,100,32,116,104,101,32,100
	dw	97,116,97,32,102,111,114,32,116,104,101,32,105,80,97,100
	dw	46
	align	4
_168:
	dd	_bbStringClass
	dd	2147483647
	dd	235
	dw	89,111,117,32,99,97,110,110,111,116,32,117,115,101,32,116
	dw	104,101,32,69,110,116,101,114,32,75,101,121,32,104,101,114
	dw	101,46,32,84,104,101,32,119,97,121,32,111,117,114,32,100
	dw	97,116,97,98,97,115,101,32,105,115,32,115,116,114,117,99
	dw	116,117,114,101,100,32,105,116,32,119,105,108,108,32,99,111
	dw	114,114,117,112,116,32,97,108,108,32,116,104,101,32,100,97
	dw	116,97,32,97,110,100,32,101,114,97,115,101,32,97,108,108
	dw	32,110,111,116,101,115,32,102,111,114,32,116,104,105,115,32
	dw	105,80,97,100,46,32,80,108,101,97,115,101,32,98,97,99
	dw	107,115,112,97,99,101,32,116,111,32,99,111,110,116,105,110
	dw	117,101,32,111,110,32,116,104,101,32,108,105,110,101,32,121
	dw	111,117,32,119,101,114,101,32,119,114,105,116,105,110,103,32
	dw	111,110,32,97,110,100,32,100,111,32,110,111,116,32,117,115
	dw	101,32,116,104,101,32,101,110,116,101,114,32,107,101,121,32
	dw	104,101,114,101,32,97,103,97,105,110,46
	align	4
_170:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	83,116,117,100,101,110,116,32,80,114,111,102,105,108,101
	align	4
_171:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	70,105,114,115,116,32,78,97,109,101
	align	4
_172:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	76,97,115,116,32,78,97,109,101
	align	4
_173:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	71,114,97,100,101
	align	4
_174:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	65,115,115,105,103,110,32,105,80,97,100
	align	4
_175:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	67,104,97,114,103,101,114,32,70,101,101
	align	4
_1589:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	87,97,108,108,32,67,104,97,114,103,101,114,32,91,36,49
	dw	56,93
	align	4
_1595:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	85,83,66,32,67,97,98,108,101,32,91,36,49,56,93
	align	4
_1602:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	67,97,115,101,32,91,36,51,54,93
	align	4
_180:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	69,120,116,114,97,32,70,101,101
	align	4
_181:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	76,111,115,116,32,105,80,97,100
	align	4
_182:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	83,116,111,108,101,110,32,105,80,97,100
	align	4
_183:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	68,97,109,97,103,101,100,32,105,80,97,100
	align	4
_184:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	105,80,97,100,32,80,114,111,102,105,108,101
	align	4
_191:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	82,101,116,117,114,110,32,105,80,97,100
	align	4
_192:
	dd	_bbStringClass
	dd	2147483647
	dd	28
	dw	84,104,105,115,32,105,115,32,119,104,101,114,101,32,121,111
	dw	117,32,119,111,117,108,100,32,115,97,118,101
	align	4
_203:
	dd	_bbStringClass
	dd	2147483647
	dd	38
	dw	93,59,49,59,82,101,103,105,115,116,101,114,101,100,32,115
	dw	116,117,100,101,110,116,32,105,110,116,111,32,115,121,115,116
	dw	101,109,32,111,110,32
	align	4
_202:
	dd	_bbStringClass
	dd	2147483647
	dd	20
	dw	82,101,103,105,115,116,101,114,101,100,32,83,116,117,100,101
	dw	110,116,59,91
	align	4
_204:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	70,105,110,100,32,98,121,32,108,97,115,116,32,110,97,109
	dw	101,32
	align	4
_205:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	68,101,98,117,103,32,76,111,103
	align	4
_1769:
	dd	_bbStringClass
	dd	2147483647
	dd	26
	dw	83,116,97,114,116,101,100,32,105,80,97,100,32,77,97,110
	dw	97,103,101,114,32,48,46,48,46,49
	align	4
_207:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	58,32,91,73,78,70,79,93,32
	align	4
_208:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	58,32,91,87,65,82,78,73,78,71,93,32
	align	4
_209:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	58,32,91,69,82,82,79,82,93,32
	align	4
_210:
	dd	_bbStringClass
	dd	2147483647
	dd	25
	dw	108,111,103,87,105,110,32,116,114,105,103,103,101,114,101,100
	dw	32,97,110,32,101,118,101,110,116
	align	4
_211:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	83,101,110,100,105,110,103,32,107,105,108,108,32,115,105,103
	dw	110,97,108
	align	4
_276:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	105,80,97,100,32,77,97,110,97,103,101,114,32,48,46,48
	dw	46,49
	align	4
_215:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	70,105,114,115,116
	align	4
_216:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	76,97,115,116
	align	4
_217:
	dd	_bbStringClass
	dd	2147483647
	dd	2
	dw	71,111
	align	4
_218:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	77,111,100,101
	align	4
_219:
	dd	_bbStringClass
	dd	2147483647
	dd	20
	dw	81,117,105,99,107,32,77,111,100,101,32,91,116,117,114,110
	dw	32,111,110,93
	align	4
_220:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	72,101,108,112
	align	4
_221:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	65,98,111,117,116
	align	4
_222:
	dd	_bbStringClass
	dd	2147483647
	dd	21
	dw	81,117,105,99,107,32,77,111,100,101,32,91,116,117,114,110
	dw	32,111,102,102,93
	align	4
_1887:
	dd	_bbStringClass
	dd	2147483647
	dd	145
	dw	105,80,97,100,32,77,97,110,97,103,101,114,32,48,46,48
	dw	46,49,32,119,97,115,32,112,114,111,103,114,97,109,109,101
	dw	100,32,98,121,32,67,111,114,101,121,32,39,82,121,97,110
	dw	39,32,68,101,97,110,32,102,111,114,32,84,97,108,108,97
	dw	115,115,101,101,32,67,105,116,121,32,83,99,104,111,111,108
	dw	115,32,102,111,114,32,117,115,101,32,111,102,32,109,97,110
	dw	97,103,105,110,103,32,116,104,101,32,115,116,117,100,101,110
	dw	116,32,105,80,97,100,115,32,97,116,32,84,97,108,108,97
	dw	115,115,101,101,32,72,105,103,104,32,83,99,104,111,111,108
	dw	46
	align	4
_224:
	dd	_bbStringClass
	dd	2147483647
	dd	25
	dw	72,69,76,80,77,69,78,85,32,65,67,84,73,79,78,32
	dw	71,79,69,83,32,72,69,82,69
	align	4
_225:
	dd	_bbStringClass
	dd	2147483647
	dd	32
	dw	83,101,97,114,99,104,105,110,103,32,102,111,114,32,101,110
	dw	116,114,121,32,119,105,116,104,32,115,101,114,105,97,108,32
	align	4
_265:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	48
	align	4
_226:
	dd	_bbStringClass
	dd	2147483647
	dd	32
	dw	83,101,97,114,99,104,105,110,32,102,111,114,32,101,110,116
	dw	114,121,32,119,105,116,104,32,66,97,114,99,111,100,101,32
	align	4
_227:
	dd	_bbStringClass
	dd	2147483647
	dd	36
	dw	83,101,97,114,99,104,105,110,103,32,102,111,114,32,101,110
	dw	116,114,121,32,119,105,116,104,32,70,105,114,115,116,32,78
	dw	97,109,101,32
	align	4
_228:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	91,98,108,97,110,107,93
	align	4
_229:
	dd	_bbStringClass
	dd	2147483647
	dd	35
	dw	83,101,97,114,99,104,105,110,103,32,102,111,114,32,101,110
	dw	116,114,121,32,119,105,116,104,32,76,97,115,116,32,78,97
	dw	109,101,32
	align	4
_234:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	32,73,68,58
	align	4
_233:
	dd	_bbStringClass
	dd	2147483647
	dd	35
	dw	80,76,69,65,83,69,32,83,69,84,32,85,80,32,69,86
	dw	69,78,84,32,72,65,78,68,76,69,32,70,79,82,32,83
	dw	82,67,58
	align	4
_256:
	dd	_bbStringClass
	dd	2147483647
	dd	77
	dw	65,114,101,32,121,111,117,32,115,117,114,101,63,32,82,101
	dw	109,101,109,98,101,114,32,116,104,97,116,32,116,104,101,114
	dw	101,32,105,115,32,110,111,32,119,97,121,32,116,111,32,117
	dw	110,100,111,32,119,104,97,116,32,121,111,117,32,97,114,101
	dw	32,97,98,111,117,116,32,116,111,32,100,111,46
	align	4
_263:
	dd	_bbStringClass
	dd	2147483647
	dd	3
	dw	101,111,110
	align	4
_177:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	93
	align	4
_264:
	dd	_bbStringClass
	dd	2147483647
	dd	26
	dw	84,104,101,32,110,111,116,101,32,100,97,116,97,32,105,115
	dw	32,99,111,114,114,117,112,116,32,91
