	format	MS COFF
	extrn	___bb_appstub_appstub
	extrn	___bb_audio_audio
	extrn	___bb_basic_basic
	extrn	___bb_blitz_blitz
	extrn	___bb_bmploader_bmploader
	extrn	___bb_d3d7max2d_d3d7max2d
	extrn	___bb_d3d9max2d_d3d9max2d
	extrn	___bb_data_data
	extrn	___bb_directsoundaudio_directsoundaudio
	extrn	___bb_drivers_drivers
	extrn	___bb_eventqueue_eventqueue
	extrn	___bb_freeaudioaudio_freeaudioaudio
	extrn	___bb_freejoy_freejoy
	extrn	___bb_freeprocess_freeprocess
	extrn	___bb_freetypefont_freetypefont
	extrn	___bb_glew_glew
	extrn	___bb_gnet_gnet
	extrn	___bb_jpgloader_jpgloader
	extrn	___bb_macos_macos
	extrn	___bb_maxlua_maxlua
	extrn	___bb_maxutil_maxutil
	extrn	___bb_oggloader_oggloader
	extrn	___bb_openalaudio_openalaudio
	extrn	___bb_pngloader_pngloader
	extrn	___bb_retro_retro
	extrn	___bb_tgaloader_tgaloader
	extrn	___bb_threads_threads
	extrn	___bb_timer_timer
	extrn	___bb_wavloader_wavloader
	extrn	_bbEmptyString
	extrn	_bbEnd
	extrn	_bbGCFree
	extrn	_bbNullObject
	extrn	_bbObjectClass
	extrn	_bbObjectCompare
	extrn	_bbObjectCtor
	extrn	_bbObjectDowncast
	extrn	_bbObjectFree
	extrn	_bbObjectNew
	extrn	_bbObjectRegisterType
	extrn	_bbObjectReserved
	extrn	_bbObjectSendMessage
	extrn	_bbObjectToString
	extrn	_bbOnDebugEnterScope
	extrn	_bbOnDebugEnterStm
	extrn	_bbOnDebugLeaveScope
	extrn	_bbStringClass
	extrn	_bbStringCompare
	extrn	_bbStringConcat
	extrn	_bbStringFromInt
	extrn	_bbStringToInt
	extrn	_brl_blitz_NullObjectError
	extrn	_brl_eventqueue_CurrentEvent
	extrn	_brl_eventqueue_EventID
	extrn	_brl_eventqueue_EventSource
	extrn	_brl_eventqueue_WaitEvent
	extrn	_brl_filesystem_CloseFile
	extrn	_brl_filesystem_OpenFile
	extrn	_brl_filesystem_WriteFile
	extrn	_brl_linkedlist_ListAddLast
	extrn	_brl_linkedlist_ListContains
	extrn	_brl_linkedlist_ListRemove
	extrn	_brl_linkedlist_TList
	extrn	_brl_retro_Left
	extrn	_brl_retro_Right
	extrn	_brl_stream_Eof
	extrn	_brl_stream_ReadLine
	extrn	_brl_stream_WriteLine
	extrn	_brl_system_Confirm
	extrn	_brl_system_CurrentDate
	extrn	_brl_system_CurrentTime
	extrn	_brl_system_Notify
	extrn	_maxgui_maxgui_ActivateGadget
	extrn	_maxgui_maxgui_ActiveGadget
	extrn	_maxgui_maxgui_AddGadgetItem
	extrn	_maxgui_maxgui_ButtonState
	extrn	_maxgui_maxgui_CreateButton
	extrn	_maxgui_maxgui_CreateLabel
	extrn	_maxgui_maxgui_CreateListBox
	extrn	_maxgui_maxgui_CreateMenu
	extrn	_maxgui_maxgui_CreateTextArea
	extrn	_maxgui_maxgui_CreateTextField
	extrn	_maxgui_maxgui_CreateWindow
	extrn	_maxgui_maxgui_FreeGadget
	extrn	_maxgui_maxgui_GadgetItemText
	extrn	_maxgui_maxgui_GadgetText
	extrn	_maxgui_maxgui_GadgetWidth
	extrn	_maxgui_maxgui_HideGadget
	extrn	_maxgui_maxgui_ModifyGadgetItem
	extrn	_maxgui_maxgui_PopupWindowMenu
	extrn	_maxgui_maxgui_RemoveGadgetItem
	extrn	_maxgui_maxgui_SelectedGadgetItem
	extrn	_maxgui_maxgui_SetButtonState
	extrn	_maxgui_maxgui_SetGadgetFilter
	extrn	_maxgui_maxgui_SetGadgetText
	extrn	_maxgui_maxgui_ShowGadget
	extrn	_maxgui_maxgui_TGadget
	extrn	_maxgui_maxgui_TextAreaText
	extrn	_maxgui_maxgui_TextFieldText
	extrn	_maxgui_maxgui_UpdateWindowMenu
	extrn	_maxgui_maxgui_WindowMenu
	public	__bb_Button_Delete
	public	__bb_Button_New
	public	__bb_Button__cstate
	public	__bb_Button__init
	public	__bb_Button__state
	public	__bb_ComboBox_Delete
	public	__bb_ComboBox_New
	public	__bb_ListBox_Delete
	public	__bb_ListBox_New
	public	__bb_ListBox__add
	public	__bb_ListBox__getText
	public	__bb_ListBox__init
	public	__bb_ListBox__modify
	public	__bb_ListBox__remove
	public	__bb_MainWindow_Delete
	public	__bb_MainWindow_New
	public	__bb_MainWindow__end
	public	__bb_MainWindow__init
	public	__bb_MainWindow__populate
	public	__bb_MainWindow__update
	public	__bb_Note_Delete
	public	__bb_Note_New
	public	__bb_Note__hide
	public	__bb_Note__init
	public	__bb_Note__save
	public	__bb_Note__typeByInt
	public	__bb_Note__update
	public	__bb_Student_Delete
	public	__bb_Student_New
	public	__bb_Student__addNote
	public	__bb_Student__build
	public	__bb_Student__hide
	public	__bb_Student__init
	public	__bb_Student__noteBySummary
	public	__bb_Student__save
	public	__bb_Student__show
	public	__bb_Student__update
	public	__bb_TextArea_Delete
	public	__bb_TextArea_New
	public	__bb_TextArea__clear
	public	__bb_TextArea__getText
	public	__bb_TextArea__init
	public	__bb_TextField_Delete
	public	__bb_TextField_New
	public	__bb_TextField__clear
	public	__bb_TextField__getText
	public	__bb_TextField__init
	public	__bb_Window_Delete
	public	__bb_Window_New
	public	__bb_Window__hide
	public	__bb_Window__new
	public	__bb_Window__show
	public	__bb_Window__update
	public	__bb_iPad_Delete
	public	__bb_iPad_New
	public	__bb_iPad__addNote
	public	__bb_iPad__build
	public	__bb_iPad__hide
	public	__bb_iPad__init
	public	__bb_iPad__noteBySummary
	public	__bb_iPad__save
	public	__bb_iPad__show
	public	__bb_iPad__update
	public	__bb_logWin_Delete
	public	__bb_logWin_New
	public	__bb_logWin__end
	public	__bb_logWin__init
	public	__bb_logWin__new
	public	__bb_logWin__populate
	public	__bb_logWin__update
	public	__bb_main
	public	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Application
	public	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete
	public	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New
	public	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Resources
	public	__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete
	public	__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New
	public	__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_Delete
	public	__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_New
	public	_bb_Button
	public	_bb_ComboBox
	public	_bb_ListBox
	public	_bb_MainWindow
	public	_bb_Note
	public	_bb_Student
	public	_bb_TextArea
	public	_bb_TextField
	public	_bb_Window
	public	_bb_convertFromCSV
	public	_bb_convertFromSSV
	public	_bb_dataDir
	public	_bb_debugWin
	public	_bb_dlog
	public	_bb_findByBC
	public	_bb_findByFirst
	public	_bb_findByLast
	public	_bb_findBySN
	public	_bb_iPad
	public	_bb_iPaddb
	public	_bb_insertIpadEntry
	public	_bb_ipadList
	public	_bb_ipaddbn
	public	_bb_logWin
	public	_bb_newBlankIpadBC
	public	_bb_newBlankIpadSN
	public	_bb_newBlankStudent
	public	_bb_newButton
	public	_bb_newIpad
	public	_bb_newListBox
	public	_bb_newNote
	public	_bb_newStudent
	public	_bb_newTextArea
	public	_bb_newTextField
	public	_bb_noComma
	public	_bb_noteWinList
	public	_bb_prompt
	public	_bb_quickMode
	public	_bb_readNote
	public	_bb_resetFile
	public	_bb_studdb
	public	_bb_studdbn
	public	_bb_studentList
	public	_bb_winList
	section	"code" code
__bb_main:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	push	esi
	push	edi
	cmp	dword [_1001],0
	je	_1002
	mov	eax,0
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_1002:
	mov	dword [_1001],1
	mov	dword [ebp-4],_bbNullObject
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbNullObject
	mov	dword [ebp-16],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_965
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	___bb_blitz_blitz
	call	___bb_drivers_drivers
	call	___bb_appstub_appstub
	call	___bb_audio_audio
	call	___bb_basic_basic
	call	___bb_bmploader_bmploader
	call	___bb_d3d7max2d_d3d7max2d
	call	___bb_d3d9max2d_d3d9max2d
	call	___bb_data_data
	call	___bb_directsoundaudio_directsoundaudio
	call	___bb_eventqueue_eventqueue
	call	___bb_freeaudioaudio_freeaudioaudio
	call	___bb_freetypefont_freetypefont
	call	___bb_gnet_gnet
	call	___bb_jpgloader_jpgloader
	call	___bb_maxlua_maxlua
	call	___bb_maxutil_maxutil
	call	___bb_oggloader_oggloader
	call	___bb_openalaudio_openalaudio
	call	___bb_pngloader_pngloader
	call	___bb_retro_retro
	call	___bb_tgaloader_tgaloader
	call	___bb_threads_threads
	call	___bb_timer_timer
	call	___bb_wavloader_wavloader
	call	___bb_freejoy_freejoy
	call	___bb_freeprocess_freeprocess
	call	___bb_glew_glew
	call	___bb_macos_macos
	push	_28
	call	_bbObjectRegisterType
	add	esp,4
	push	_29
	call	_bbObjectRegisterType
	add	esp,4
	push	_816
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_818
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_35
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Window
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Button
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_TextField
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_ComboBox
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_ListBox
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_iPad
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Note
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Student
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_logWin
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_MainWindow
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_TextArea
	call	_bbObjectRegisterType
	add	esp,4
	push	_819
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_821
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_824]
	and	eax,1
	cmp	eax,0
	jne	_825
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_winList],eax
	or	dword [_824],1
_825:
	push	_826
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_824]
	and	eax,2
	cmp	eax,0
	jne	_828
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_ipadList],eax
	or	dword [_824],2
_828:
	push	_829
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_824]
	and	eax,4
	cmp	eax,0
	jne	_831
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_studentList],eax
	or	dword [_824],4
_831:
	push	_832
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_833
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_824]
	and	eax,8
	cmp	eax,0
	jne	_835
	push	_bb_logWin
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_debugWin],eax
	or	dword [_824],8
_835:
	push	_836
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_debugWin]
	cmp	ebx,_bbNullObject
	jne	_838
	call	_brl_blitz_NullObjectError
_838:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	_839
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_840
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_841
	call	_bb_dlog
	add	esp,8
	push	_842
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_824]
	and	eax,16
	cmp	eax,0
	jne	_845
	push	_39
	push	dword [_bb_dataDir]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_ipaddbn],eax
	or	dword [_824],16
_845:
	push	_846
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_824]
	and	eax,32
	cmp	eax,0
	jne	_848
	push	1
	push	1
	push	dword [_bb_ipaddbn]
	call	_brl_filesystem_OpenFile
	add	esp,12
	inc	dword [eax+4]
	mov	dword [_bb_iPaddb],eax
	or	dword [_824],32
_848:
	push	_849
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_iPaddb],_bbNullObject
	jne	_850
	mov	eax,ebp
	push	eax
	push	_857
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_851
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_ipaddbn]
	call	_brl_filesystem_WriteFile
	add	esp,4
	push	_852
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	1
	push	dword [_bb_ipaddbn]
	call	_brl_filesystem_OpenFile
	add	esp,12
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_856
	push	eax
	call	_bbGCFree
	add	esp,4
_856:
	mov	dword [_bb_iPaddb],ebx
	call	dword [_bbOnDebugLeaveScope]
_850:
	push	_858
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_824]
	and	eax,64
	cmp	eax,0
	jne	_861
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_noteWinList],eax
	or	dword [_824],64
_861:
	push	_862
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_824]
	and	eax,128
	cmp	eax,0
	jne	_865
	push	_169
	push	dword [_bb_dataDir]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_studdbn],eax
	or	dword [_824],128
_865:
	push	_866
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_824]
	and	eax,256
	cmp	eax,0
	jne	_868
	push	1
	push	1
	push	dword [_bb_studdbn]
	call	_brl_filesystem_OpenFile
	add	esp,12
	inc	dword [eax+4]
	mov	dword [_bb_studdb],eax
	or	dword [_824],256
_868:
	push	_869
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_studdb],_bbNullObject
	jne	_870
	mov	eax,ebp
	push	eax
	push	_877
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_871
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_studdbn]
	call	_brl_filesystem_WriteFile
	add	esp,4
	push	_872
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	1
	push	dword [_bb_studdbn]
	call	_brl_filesystem_OpenFile
	add	esp,12
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_studdb]
	dec	dword [eax+4]
	jnz	_876
	push	eax
	call	_bbGCFree
	add	esp,4
_876:
	mov	dword [_bb_studdb],ebx
	call	dword [_bbOnDebugLeaveScope]
_870:
	push	_878
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	xor	eax,eax
	cmp	eax,0
	je	_880
	mov	eax,ebp
	push	eax
	push	_888
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_881
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_36
	push	1
	push	1
	push	0
	push	_257
	call	_bb_prompt
	add	esp,16
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_882
	mov	eax,ebp
	push	eax
	push	_887
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_883
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	_885
	call	_brl_system_Notify
	add	esp,8
	push	_886
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bbEnd
	call	dword [_bbOnDebugLeaveScope]
_882:
	call	dword [_bbOnDebugLeaveScope]
_880:
	push	_889
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_MainWindow
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-4],eax
	push	_891
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_893
	call	_brl_blitz_NullObjectError
_893:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	_894
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_260
_262:
	mov	eax,ebp
	push	eax
	push	_957
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_895
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_WaitEvent
	push	_896
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_898
	call	_brl_blitz_NullObjectError
_898:
	push	dword [_brl_eventqueue_CurrentEvent]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,8
	cmp	eax,0
	je	_899
	mov	eax,ebp
	push	eax
	push	_901
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_900
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_451
_899:
	push	_902
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_debugWin]
	cmp	ebx,_bbNullObject
	jne	_904
	call	_brl_blitz_NullObjectError
_904:
	cmp	dword [ebx+8],_bbNullObject
	je	_905
	mov	eax,ebp
	push	eax
	push	_909
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_906
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_debugWin]
	cmp	ebx,_bbNullObject
	jne	_908
	call	_brl_blitz_NullObjectError
_908:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_905:
	push	_910
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbNullObject
	mov	edi,dword [_bb_ipadList]
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_914
	call	_brl_blitz_NullObjectError
_914:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	esi,eax
	jmp	_263
_265:
	mov	ebx,esi
	cmp	ebx,_bbNullObject
	jne	_919
	call	_brl_blitz_NullObjectError
_919:
	push	_bb_iPad
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	je	_263
	mov	eax,ebp
	push	eax
	push	_923
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_920
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_922
	call	_brl_blitz_NullObjectError
_922:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+72]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_263:
	mov	ebx,esi
	cmp	ebx,_bbNullObject
	jne	_917
	call	_brl_blitz_NullObjectError
_917:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_265
_264:
	push	_925
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbNullObject
	mov	edi,dword [_bb_studentList]
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_929
	call	_brl_blitz_NullObjectError
_929:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	esi,eax
	jmp	_266
_268:
	mov	ebx,esi
	cmp	ebx,_bbNullObject
	jne	_934
	call	_brl_blitz_NullObjectError
_934:
	push	_bb_Student
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	je	_266
	mov	eax,ebp
	push	eax
	push	_938
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_935
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_937
	call	_brl_blitz_NullObjectError
_937:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+72]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_266:
	mov	ebx,esi
	cmp	ebx,_bbNullObject
	jne	_932
	call	_brl_blitz_NullObjectError
_932:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_268
_267:
	push	_941
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],_bbNullObject
	mov	edi,dword [_bb_noteWinList]
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_945
	call	_brl_blitz_NullObjectError
_945:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	esi,eax
	jmp	_269
_271:
	mov	ebx,esi
	cmp	ebx,_bbNullObject
	jne	_950
	call	_brl_blitz_NullObjectError
_950:
	push	_bb_Note
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-16],eax
	cmp	dword [ebp-16],_bbNullObject
	je	_269
	mov	eax,ebp
	push	eax
	push	_954
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_951
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_953
	call	_brl_blitz_NullObjectError
_953:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_269:
	mov	ebx,esi
	cmp	ebx,_bbNullObject
	jne	_948
	call	_brl_blitz_NullObjectError
_948:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_271
_270:
	call	dword [_bbOnDebugLeaveScope]
_260:
	mov	eax,1
	cmp	eax,0
	jne	_262
_261:
	push	_958
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_960
	call	_brl_blitz_NullObjectError
_960:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,4
	push	_961
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_debugWin]
	cmp	ebx,_bbNullObject
	jne	_963
	call	_brl_blitz_NullObjectError
_963:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+64]
	add	esp,4
	push	_964
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bbEnd
	mov	ebx,0
	jmp	_451
_451:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1004
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_28
	push	ebp
	push	_1003
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_454
_454:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_Delete:
	push	ebp
	mov	ebp,esp
_457:
	mov	eax,0
	jmp	_1007
_1007:
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1009
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_29
	push	ebp
	push	_1008
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_460
_460:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete:
	push	ebp
	mov	ebp,esp
_463:
	mov	eax,0
	jmp	_1011
_1011:
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1013
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_35
	push	ebp
	push	_1012
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_466
_466:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete:
	push	ebp
	mov	ebp,esp
_469:
	mov	eax,0
	jmp	_1014
_1014:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Window_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1020
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_Window
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],edx
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	dword [edx+16],eax
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],edx
	push	ebp
	push	_1019
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_472
_472:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Window_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_475:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_1023
	push	eax
	call	_bbGCFree
	add	esp,4
_1023:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_1025
	push	eax
	call	_bbGCFree
	add	esp,4
_1025:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_1027
	push	eax
	call	_bbGCFree
	add	esp,4
_1027:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_1029
	push	eax
	call	_bbGCFree
	add	esp,4
_1029:
	mov	eax,0
	jmp	_1021
_1021:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Window__new:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp+32]
	mov	dword [ebp-28],eax
	push	ebp
	push	_1050
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1030
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1033
	call	_brl_blitz_NullObjectError
_1033:
	push	dword [ebp-28]
	push	_bbNullObject
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-24]
	call	_maxgui_maxgui_CreateWindow
	add	esp,28
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_1038
	push	eax
	call	_bbGCFree
	add	esp,4
_1038:
	mov	dword [ebx+8],esi
	push	_1039
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	dword [_bb_winList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_1040
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1042
	call	_brl_blitz_NullObjectError
_1042:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1045
	call	_brl_blitz_NullObjectError
_1045:
	push	dword [esi+8]
	call	_maxgui_maxgui_WindowMenu
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_1049
	push	eax
	call	_bbGCFree
	add	esp,4
_1049:
	mov	dword [ebx+20],esi
	mov	ebx,0
	jmp	_484
_484:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Window__show:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1060
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1057
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1059
	call	_brl_blitz_NullObjectError
_1059:
	push	dword [ebx+8]
	call	_maxgui_maxgui_ShowGadget
	add	esp,4
	mov	ebx,0
	jmp	_487
_487:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Window__hide:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1064
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1061
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1063
	call	_brl_blitz_NullObjectError
_1063:
	push	dword [ebx+8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	mov	ebx,0
	jmp	_490
_490:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Window__update:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1065
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	mov	ebx,0
	jmp	_493
_493:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Button_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1068
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_Button
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],edx
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],0
	push	ebp
	push	_1067
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_496
_496:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Button_Delete:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_499:
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_1071
	push	eax
	call	_bbGCFree
	add	esp,4
_1071:
	mov	eax,0
	jmp	_1069
_1069:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Button__init:
	push	ebp
	mov	ebp,esp
	sub	esp,32
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp+32]
	mov	dword [ebp-28],eax
	mov	eax,dword [ebp+36]
	mov	dword [ebp-32],eax
	push	ebp
	push	_1088
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1072
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1075
	call	_brl_blitz_NullObjectError
_1075:
	mov	esi,dword [ebp-28]
	cmp	esi,_bbNullObject
	jne	_1078
	call	_brl_blitz_NullObjectError
_1078:
	push	dword [ebp-32]
	push	dword [esi+8]
	push	dword [ebp-24]
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	call	_maxgui_maxgui_CreateButton
	add	esp,28
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_1082
	push	eax
	call	_bbGCFree
	add	esp,4
_1082:
	mov	dword [ebx+8],esi
	push	_1083
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-28]
	cmp	esi,_bbNullObject
	jne	_1085
	call	_brl_blitz_NullObjectError
_1085:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1087
	call	_brl_blitz_NullObjectError
_1087:
	push	dword [ebx+8]
	push	dword [esi+16]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	ebx,0
	jmp	_509
_509:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Button__state:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1105
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1091
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1093
	call	_brl_blitz_NullObjectError
_1093:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1096
	call	_brl_blitz_NullObjectError
_1096:
	mov	eax,dword [esi+12]
	cmp	eax,0
	sete	al
	movzx	eax,al
	mov	dword [ebx+12],eax
	push	_1097
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1099
	call	_brl_blitz_NullObjectError
_1099:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1101
	call	_brl_blitz_NullObjectError
_1101:
	push	dword [ebx+12]
	push	dword [esi+8]
	call	_maxgui_maxgui_SetButtonState
	add	esp,8
	push	_1102
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1104
	call	_brl_blitz_NullObjectError
_1104:
	push	dword [ebx+8]
	call	_maxgui_maxgui_ButtonState
	add	esp,4
	mov	ebx,eax
	jmp	_512
_512:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Button__cstate:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1109
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1106
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1108
	call	_brl_blitz_NullObjectError
_1108:
	mov	ebx,dword [ebx+12]
	jmp	_515
_515:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newButton:
	push	ebp
	mov	ebp,esp
	sub	esp,32
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp+32]
	mov	dword [ebp-28],eax
	mov	dword [ebp-32],_bbNullObject
	push	ebp
	push	_1116
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1110
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_Button
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-32],eax
	push	_1112
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_1114
	call	_brl_blitz_NullObjectError
_1114:
	push	dword [ebp-28]
	push	dword [ebp-24]
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-4]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,32
	push	_1115
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	jmp	_524
_524:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextField_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1121
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_TextField
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],edx
	push	ebp
	push	_1120
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_527
_527:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextField_Delete:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_530:
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_1124
	push	eax
	call	_bbGCFree
	add	esp,4
_1124:
	mov	eax,0
	jmp	_1122
_1122:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextField__init:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp+32]
	mov	dword [ebp-28],eax
	push	ebp
	push	_1141
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1125
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1128
	call	_brl_blitz_NullObjectError
_1128:
	mov	esi,dword [ebp-24]
	cmp	esi,_bbNullObject
	jne	_1131
	call	_brl_blitz_NullObjectError
_1131:
	push	dword [ebp-28]
	push	dword [esi+8]
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	call	_maxgui_maxgui_CreateTextField
	add	esp,24
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_1135
	push	eax
	call	_bbGCFree
	add	esp,4
_1135:
	mov	dword [ebx+8],esi
	push	_1136
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-24]
	cmp	esi,_bbNullObject
	jne	_1138
	call	_brl_blitz_NullObjectError
_1138:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1140
	call	_brl_blitz_NullObjectError
_1140:
	push	dword [ebx+8]
	push	dword [esi+16]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	ebx,0
	jmp	_539
_539:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextField__getText:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1146
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1143
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1145
	call	_brl_blitz_NullObjectError
_1145:
	push	dword [ebx+8]
	call	_maxgui_maxgui_TextFieldText
	add	esp,4
	mov	ebx,eax
	jmp	_542
_542:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextField__clear:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1150
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1147
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1149
	call	_brl_blitz_NullObjectError
_1149:
	push	_1
	push	dword [ebx+8]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	mov	ebx,0
	jmp	_545
_545:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newTextField:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	dword [ebp-28],_bbNullObject
	push	ebp
	push	_1157
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1151
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_TextField
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-28],eax
	push	_1153
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1155
	call	_brl_blitz_NullObjectError
_1155:
	push	dword [ebp-24]
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-4]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,28
	push	_1156
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	jmp	_553
_553:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ComboBox_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1161
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_ComboBox
	push	ebp
	push	_1160
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_556
_556:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ComboBox_Delete:
	push	ebp
	mov	ebp,esp
_559:
	mov	eax,0
	jmp	_1163
_1163:
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1166
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_ListBox
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],edx
	push	ebp
	push	_1165
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_562
_562:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox_Delete:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_565:
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_1169
	push	eax
	call	_bbGCFree
	add	esp,4
_1169:
	mov	eax,0
	jmp	_1167
_1167:
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox__init:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp+32]
	mov	dword [ebp-28],eax
	push	ebp
	push	_1186
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1170
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1173
	call	_brl_blitz_NullObjectError
_1173:
	mov	esi,dword [ebp-24]
	cmp	esi,_bbNullObject
	jne	_1176
	call	_brl_blitz_NullObjectError
_1176:
	push	dword [ebp-28]
	push	dword [esi+8]
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	call	_maxgui_maxgui_CreateListBox
	add	esp,24
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_1180
	push	eax
	call	_bbGCFree
	add	esp,4
_1180:
	mov	dword [ebx+8],esi
	push	_1181
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-24]
	cmp	esi,_bbNullObject
	jne	_1183
	call	_brl_blitz_NullObjectError
_1183:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1185
	call	_brl_blitz_NullObjectError
_1185:
	push	dword [ebx+8]
	push	dword [esi+16]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	ebx,0
	jmp	_574
_574:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox__add:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	push	ebp
	push	_1190
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1187
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1189
	call	_brl_blitz_NullObjectError
_1189:
	push	_bbNullObject
	push	_1
	push	-1
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebx+8]
	call	_maxgui_maxgui_AddGadgetItem
	add	esp,24
	mov	ebx,0
	jmp	_579
_579:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox__modify:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_1198
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1193
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1195
	call	_brl_blitz_NullObjectError
_1195:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1197
	call	_brl_blitz_NullObjectError
_1197:
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	dword [ebp-8]
	push	dword [esi+8]
	call	_maxgui_maxgui_SelectedGadgetItem
	add	esp,4
	push	eax
	push	dword [ebx+8]
	call	_maxgui_maxgui_ModifyGadgetItem
	add	esp,28
	mov	ebx,0
	jmp	_583
_583:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox__remove:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1204
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1199
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1201
	call	_brl_blitz_NullObjectError
_1201:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1203
	call	_brl_blitz_NullObjectError
_1203:
	push	dword [esi+8]
	call	_maxgui_maxgui_SelectedGadgetItem
	add	esp,4
	push	eax
	push	dword [ebx+8]
	call	_maxgui_maxgui_RemoveGadgetItem
	add	esp,8
	mov	ebx,0
	jmp	_586
_586:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_ListBox__getText:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1210
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1205
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1207
	call	_brl_blitz_NullObjectError
_1207:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1209
	call	_brl_blitz_NullObjectError
_1209:
	push	dword [esi+8]
	call	_maxgui_maxgui_SelectedGadgetItem
	add	esp,4
	push	eax
	push	dword [ebx+8]
	call	_maxgui_maxgui_GadgetItemText
	add	esp,8
	mov	ebx,eax
	jmp	_589
_589:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newListBox:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	dword [ebp-28],_bbNullObject
	push	ebp
	push	_1217
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1211
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_ListBox
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-28],eax
	push	_1213
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1215
	call	_brl_blitz_NullObjectError
_1215:
	push	dword [ebp-24]
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-4]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,28
	push	_1216
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	jmp	_597
_597:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1246
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_iPad
	push	_bb_Window
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	dword [edx+8],eax
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],0
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+24],edx
	mov	eax,dword [ebp-4]
	mov	dword [eax+28],0
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+32],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+36],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+40],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+44],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+48],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+52],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+56],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+60],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+64],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+68],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+72],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+76],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+80],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+84],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+88],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+92],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+96],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+100],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+104],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+108],edx
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	dword [edx+112],eax
	push	ebp
	push	_1245
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_600
_600:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_603:
	mov	eax,dword [ebx+112]
	dec	dword [eax+4]
	jnz	_1249
	push	eax
	call	_bbGCFree
	add	esp,4
_1249:
	mov	eax,dword [ebx+108]
	dec	dword [eax+4]
	jnz	_1251
	push	eax
	call	_bbGCFree
	add	esp,4
_1251:
	mov	eax,dword [ebx+104]
	dec	dword [eax+4]
	jnz	_1253
	push	eax
	call	_bbGCFree
	add	esp,4
_1253:
	mov	eax,dword [ebx+100]
	dec	dword [eax+4]
	jnz	_1255
	push	eax
	call	_bbGCFree
	add	esp,4
_1255:
	mov	eax,dword [ebx+96]
	dec	dword [eax+4]
	jnz	_1257
	push	eax
	call	_bbGCFree
	add	esp,4
_1257:
	mov	eax,dword [ebx+92]
	dec	dword [eax+4]
	jnz	_1259
	push	eax
	call	_bbGCFree
	add	esp,4
_1259:
	mov	eax,dword [ebx+88]
	dec	dword [eax+4]
	jnz	_1261
	push	eax
	call	_bbGCFree
	add	esp,4
_1261:
	mov	eax,dword [ebx+84]
	dec	dword [eax+4]
	jnz	_1263
	push	eax
	call	_bbGCFree
	add	esp,4
_1263:
	mov	eax,dword [ebx+80]
	dec	dword [eax+4]
	jnz	_1265
	push	eax
	call	_bbGCFree
	add	esp,4
_1265:
	mov	eax,dword [ebx+76]
	dec	dword [eax+4]
	jnz	_1267
	push	eax
	call	_bbGCFree
	add	esp,4
_1267:
	mov	eax,dword [ebx+72]
	dec	dword [eax+4]
	jnz	_1269
	push	eax
	call	_bbGCFree
	add	esp,4
_1269:
	mov	eax,dword [ebx+68]
	dec	dword [eax+4]
	jnz	_1271
	push	eax
	call	_bbGCFree
	add	esp,4
_1271:
	mov	eax,dword [ebx+64]
	dec	dword [eax+4]
	jnz	_1273
	push	eax
	call	_bbGCFree
	add	esp,4
_1273:
	mov	eax,dword [ebx+60]
	dec	dword [eax+4]
	jnz	_1275
	push	eax
	call	_bbGCFree
	add	esp,4
_1275:
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_1277
	push	eax
	call	_bbGCFree
	add	esp,4
_1277:
	mov	eax,dword [ebx+52]
	dec	dword [eax+4]
	jnz	_1279
	push	eax
	call	_bbGCFree
	add	esp,4
_1279:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_1281
	push	eax
	call	_bbGCFree
	add	esp,4
_1281:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_1283
	push	eax
	call	_bbGCFree
	add	esp,4
_1283:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_1285
	push	eax
	call	_bbGCFree
	add	esp,4
_1285:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_1287
	push	eax
	call	_bbGCFree
	add	esp,4
_1287:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_1289
	push	eax
	call	_bbGCFree
	add	esp,4
_1289:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_1291
	push	eax
	call	_bbGCFree
	add	esp,4
_1291:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_1293
	push	eax
	call	_bbGCFree
	add	esp,4
_1293:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_1295
	push	eax
	call	_bbGCFree
	add	esp,4
_1295:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_1297
	push	eax
	call	_bbGCFree
	add	esp,4
_1297:
	mov	eax,0
	jmp	_1247
_1247:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__init:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,ebp
	push	eax
	push	_1474
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1298
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1300
	call	_brl_blitz_NullObjectError
_1300:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_1302
	call	_brl_blitz_NullObjectError
_1302:
	push	21
	push	_40
	push	300
	push	400
	push	0
	push	0
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,28
	push	_1303
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	dword [_bb_ipadList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_1304
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1306
	call	_brl_blitz_NullObjectError
_1306:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1309
	call	_brl_blitz_NullObjectError
_1309:
	push	0
	push	dword [ebx+8]
	push	170
	push	375
	push	75
	push	10
	call	_bb_newListBox
	add	esp,24
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+96]
	dec	dword [eax+4]
	jnz	_1313
	push	eax
	call	_bbGCFree
	add	esp,4
_1313:
	mov	dword [esi+96],ebx
	push	_1314
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1316
	call	_brl_blitz_NullObjectError
_1316:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1319
	call	_brl_blitz_NullObjectError
_1319:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_1321
	call	_brl_blitz_NullObjectError
_1321:
	push	0
	push	0
	push	dword [ebx+20]
	push	0
	push	_41
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+32]
	dec	dword [eax+4]
	jnz	_1325
	push	eax
	call	_bbGCFree
	add	esp,4
_1325:
	mov	dword [esi+32],ebx
	push	_1326
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1328
	call	_brl_blitz_NullObjectError
_1328:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1331
	call	_brl_blitz_NullObjectError
_1331:
	push	0
	push	0
	push	dword [ebx+32]
	push	0
	push	_42
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+44]
	dec	dword [eax+4]
	jnz	_1335
	push	eax
	call	_bbGCFree
	add	esp,4
_1335:
	mov	dword [esi+44],ebx
	push	_1336
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1338
	call	_brl_blitz_NullObjectError
_1338:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1341
	call	_brl_blitz_NullObjectError
_1341:
	push	0
	push	0
	push	dword [ebx+32]
	push	0
	push	_43
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+48]
	dec	dword [eax+4]
	jnz	_1345
	push	eax
	call	_bbGCFree
	add	esp,4
_1345:
	mov	dword [esi+48],ebx
	push	_1346
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1348
	call	_brl_blitz_NullObjectError
_1348:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1351
	call	_brl_blitz_NullObjectError
_1351:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_1353
	call	_brl_blitz_NullObjectError
_1353:
	push	0
	push	0
	push	dword [ebx+20]
	push	0
	push	_44
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+40]
	dec	dword [eax+4]
	jnz	_1357
	push	eax
	call	_bbGCFree
	add	esp,4
_1357:
	mov	dword [esi+40],ebx
	push	_1358
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1360
	call	_brl_blitz_NullObjectError
_1360:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1363
	call	_brl_blitz_NullObjectError
_1363:
	push	0
	push	0
	push	dword [ebx+40]
	push	0
	push	_45
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+60]
	dec	dword [eax+4]
	jnz	_1367
	push	eax
	call	_bbGCFree
	add	esp,4
_1367:
	mov	dword [esi+60],ebx
	push	_1368
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1370
	call	_brl_blitz_NullObjectError
_1370:
	push	0
	push	0
	push	dword [ebx+40]
	push	0
	push	_1
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	push	_1371
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1373
	call	_brl_blitz_NullObjectError
_1373:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1376
	call	_brl_blitz_NullObjectError
_1376:
	push	0
	push	0
	push	dword [ebx+40]
	push	0
	push	_46
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+52]
	dec	dword [eax+4]
	jnz	_1380
	push	eax
	call	_bbGCFree
	add	esp,4
_1380:
	mov	dword [esi+52],ebx
	push	_1381
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1383
	call	_brl_blitz_NullObjectError
_1383:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1386
	call	_brl_blitz_NullObjectError
_1386:
	push	0
	push	0
	push	dword [ebx+40]
	push	0
	push	_47
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+56]
	dec	dword [eax+4]
	jnz	_1390
	push	eax
	call	_bbGCFree
	add	esp,4
_1390:
	mov	dword [esi+56],ebx
	push	_1391
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1393
	call	_brl_blitz_NullObjectError
_1393:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1396
	call	_brl_blitz_NullObjectError
_1396:
	push	0
	push	0
	push	dword [ebx+40]
	push	0
	push	_48
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+64]
	dec	dword [eax+4]
	jnz	_1400
	push	eax
	call	_bbGCFree
	add	esp,4
_1400:
	mov	dword [esi+64],ebx
	push	_1401
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1403
	call	_brl_blitz_NullObjectError
_1403:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1406
	call	_brl_blitz_NullObjectError
_1406:
	push	0
	push	0
	push	dword [ebx+40]
	push	0
	push	_49
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+68]
	dec	dword [eax+4]
	jnz	_1410
	push	eax
	call	_bbGCFree
	add	esp,4
_1410:
	mov	dword [esi+68],ebx
	push	_1411
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1413
	call	_brl_blitz_NullObjectError
_1413:
	push	0
	push	0
	push	_bbNullObject
	push	0
	push	_50
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+80]
	dec	dword [eax+4]
	jnz	_1418
	push	eax
	call	_bbGCFree
	add	esp,4
_1418:
	mov	dword [esi+80],ebx
	push	_1419
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1421
	call	_brl_blitz_NullObjectError
_1421:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1424
	call	_brl_blitz_NullObjectError
_1424:
	push	0
	push	0
	push	dword [ebx+80]
	push	0
	push	_41
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+72]
	dec	dword [eax+4]
	jnz	_1428
	push	eax
	call	_bbGCFree
	add	esp,4
_1428:
	mov	dword [esi+72],ebx
	push	_1429
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1431
	call	_brl_blitz_NullObjectError
_1431:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1434
	call	_brl_blitz_NullObjectError
_1434:
	push	0
	push	0
	push	dword [ebx+80]
	push	0
	push	_51
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+76]
	dec	dword [eax+4]
	jnz	_1438
	push	eax
	call	_bbGCFree
	add	esp,4
_1438:
	mov	dword [esi+76],ebx
	push	_1439
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1441
	call	_brl_blitz_NullObjectError
_1441:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1444
	call	_brl_blitz_NullObjectError
_1444:
	push	0
	push	0
	push	dword [ebx+76]
	push	0
	push	_52
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+84]
	dec	dword [eax+4]
	jnz	_1448
	push	eax
	call	_bbGCFree
	add	esp,4
_1448:
	mov	dword [esi+84],ebx
	push	_1449
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1451
	call	_brl_blitz_NullObjectError
_1451:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1454
	call	_brl_blitz_NullObjectError
_1454:
	push	0
	push	0
	push	dword [ebx+76]
	push	0
	push	_44
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+88]
	dec	dword [eax+4]
	jnz	_1458
	push	eax
	call	_bbGCFree
	add	esp,4
_1458:
	mov	dword [esi+88],ebx
	push	_1459
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1461
	call	_brl_blitz_NullObjectError
_1461:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1464
	call	_brl_blitz_NullObjectError
_1464:
	push	0
	push	0
	push	dword [ebx+76]
	push	0
	push	_53
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+92]
	dec	dword [eax+4]
	jnz	_1468
	push	eax
	call	_bbGCFree
	add	esp,4
_1468:
	mov	dword [esi+92],ebx
	push	_1469
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1471
	call	_brl_blitz_NullObjectError
_1471:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_1473
	call	_brl_blitz_NullObjectError
_1473:
	push	dword [ebx+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	mov	ebx,0
	jmp	_606
_606:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__show:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1480
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1475
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1477
	call	_brl_blitz_NullObjectError
_1477:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_1479
	call	_brl_blitz_NullObjectError
_1479:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	mov	ebx,0
	jmp	_609
_609:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__build:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbEmptyString
	mov	dword [ebp-12],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_1601
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1481
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbEmptyString
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1484
	call	_brl_blitz_NullObjectError
_1484:
	mov	eax,dword [ebx+112]
	mov	dword [ebp-16],eax
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_1487
	call	_brl_blitz_NullObjectError
_1487:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_54
_56:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_1492
	call	_brl_blitz_NullObjectError
_1492:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	je	_54
	mov	eax,ebp
	push	eax
	push	_1500
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1493
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_Note
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1495
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1497
	call	_brl_blitz_NullObjectError
_1497:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_1499
	call	_brl_blitz_NullObjectError
_1499:
	push	dword [ebx+24]
	push	dword [esi+112]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_54:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_1490
	call	_brl_blitz_NullObjectError
_1490:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_56
_55:
	push	_1502
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1504
	call	_brl_blitz_NullObjectError
_1504:
	mov	edi,ebx
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1507
	call	_brl_blitz_NullObjectError
_1507:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1509
	call	_brl_blitz_NullObjectError
_1509:
	mov	esi,dword [esi+8]
	cmp	esi,_bbNullObject
	jne	_1511
	call	_brl_blitz_NullObjectError
_1511:
	push	0
	push	dword [esi+8]
	push	15
	push	75
	push	10
	push	10
	push	dword [ebx+16]
	push	_57
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+100]
	dec	dword [eax+4]
	jnz	_1515
	push	eax
	call	_bbGCFree
	add	esp,4
_1515:
	mov	dword [edi+100],ebx
	push	_1516
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1518
	call	_brl_blitz_NullObjectError
_1518:
	mov	edi,ebx
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1521
	call	_brl_blitz_NullObjectError
_1521:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1523
	call	_brl_blitz_NullObjectError
_1523:
	mov	esi,dword [esi+8]
	cmp	esi,_bbNullObject
	jne	_1525
	call	_brl_blitz_NullObjectError
_1525:
	push	0
	push	dword [esi+8]
	push	15
	push	125
	push	25
	push	10
	push	dword [ebx+20]
	push	_58
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+104]
	dec	dword [eax+4]
	jnz	_1529
	push	eax
	call	_bbGCFree
	add	esp,4
_1529:
	mov	dword [edi+104],ebx
	push	_1530
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1532
	call	_brl_blitz_NullObjectError
_1532:
	mov	edi,ebx
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1535
	call	_brl_blitz_NullObjectError
_1535:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1537
	call	_brl_blitz_NullObjectError
_1537:
	mov	esi,dword [esi+8]
	cmp	esi,_bbNullObject
	jne	_1539
	call	_brl_blitz_NullObjectError
_1539:
	push	0
	push	dword [esi+8]
	push	15
	push	150
	push	40
	push	10
	push	dword [ebx+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+108]
	dec	dword [eax+4]
	jnz	_1543
	push	eax
	call	_bbGCFree
	add	esp,4
_1543:
	mov	dword [edi+108],ebx
	push	_1544
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1546
	call	_brl_blitz_NullObjectError
_1546:
	mov	ebx,dword [ebx+24]
	push	_60
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1549
	push	_61
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1550
	push	_62
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1551
	push	_65
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1552
	push	_66
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1553
	push	_67
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1554
	push	_68
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1555
	jmp	_1548
_1549:
	mov	eax,ebp
	push	eax
	push	_1559
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1556
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1558
	call	_brl_blitz_NullObjectError
_1558:
	push	_45
	push	dword [ebx+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1548
_1550:
	mov	eax,ebp
	push	eax
	push	_1563
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1560
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1562
	call	_brl_blitz_NullObjectError
_1562:
	push	_45
	push	dword [ebx+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1548
_1551:
	mov	eax,ebp
	push	eax
	push	_1579
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1564
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1566
	call	_brl_blitz_NullObjectError
_1566:
	push	_63
	push	dword [ebx+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_1567
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1569
	call	_brl_blitz_NullObjectError
_1569:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1572
	call	_brl_blitz_NullObjectError
_1572:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_1574
	call	_brl_blitz_NullObjectError
_1574:
	push	0
	push	0
	push	dword [ebx+20]
	push	0
	push	_64
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+36]
	dec	dword [eax+4]
	jnz	_1578
	push	eax
	call	_bbGCFree
	add	esp,4
_1578:
	mov	dword [esi+36],ebx
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1548
_1552:
	mov	eax,ebp
	push	eax
	push	_1583
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1580
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1582
	call	_brl_blitz_NullObjectError
_1582:
	push	_63
	push	dword [ebx+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1548
_1553:
	mov	eax,ebp
	push	eax
	push	_1587
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1584
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1586
	call	_brl_blitz_NullObjectError
_1586:
	push	_63
	push	dword [ebx+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1548
_1554:
	mov	eax,ebp
	push	eax
	push	_1591
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1588
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1590
	call	_brl_blitz_NullObjectError
_1590:
	push	_63
	push	dword [ebx+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1548
_1555:
	mov	eax,ebp
	push	eax
	push	_1595
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1592
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1594
	call	_brl_blitz_NullObjectError
_1594:
	push	_63
	push	dword [ebx+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1548
_1548:
	push	_1596
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1598
	call	_brl_blitz_NullObjectError
_1598:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_1600
	call	_brl_blitz_NullObjectError
_1600:
	push	dword [ebx+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	mov	ebx,0
	jmp	_612
_612:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__hide:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_1657
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1602
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_quickMode],0
	je	_1603
	mov	eax,ebp
	push	eax
	push	_1607
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1604
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1606
	call	_brl_blitz_NullObjectError
_1606:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+64]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1608
_1603:
	mov	eax,ebp
	push	eax
	push	_1615
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1609
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_69
	call	_brl_system_Confirm
	add	esp,8
	cmp	eax,0
	je	_1610
	mov	eax,ebp
	push	eax
	push	_1614
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1611
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1613
	call	_brl_blitz_NullObjectError
_1613:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+64]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1610:
	call	dword [_bbOnDebugLeaveScope]
_1608:
	push	_1616
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1618
	call	_brl_blitz_NullObjectError
_1618:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_1620
	call	_brl_blitz_NullObjectError
_1620:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	push	_1621
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbNullObject
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1624
	call	_brl_blitz_NullObjectError
_1624:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_1626
	call	_brl_blitz_NullObjectError
_1626:
	mov	esi,dword [ebx+16]
	cmp	esi,_bbNullObject
	jne	_1629
	call	_brl_blitz_NullObjectError
_1629:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_70
_72:
	cmp	ebx,_bbNullObject
	jne	_1634
	call	_brl_blitz_NullObjectError
_1634:
	push	_maxgui_maxgui_TGadget
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	je	_70
	mov	eax,ebp
	push	eax
	push	_1637
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1635
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	push	_1636
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_70:
	cmp	ebx,_bbNullObject
	jne	_1632
	call	_brl_blitz_NullObjectError
_1632:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_72
_71:
	push	_1638
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1640
	call	_brl_blitz_NullObjectError
_1640:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_1642
	call	_brl_blitz_NullObjectError
_1642:
	push	dword [ebx+8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	push	_1643
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1645
	call	_brl_blitz_NullObjectError
_1645:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_1647
	call	_brl_blitz_NullObjectError
_1647:
	push	dword [ebx+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	push	_1648
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1650
	call	_brl_blitz_NullObjectError
_1650:
	mov	ebx,_bbNullObject
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_1655
	push	eax
	call	_bbGCFree
	add	esp,4
_1655:
	mov	dword [esi+8],ebx
	push	_1656
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	dword [_bb_ipadList]
	call	_brl_linkedlist_ListRemove
	add	esp,8
	mov	ebx,0
	jmp	_615
_615:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__save:
	push	ebp
	mov	ebp,esp
	sub	esp,24
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbEmptyString
	mov	dword [ebp-12],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_1690
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1658
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_1660
	call	_brl_blitz_NullObjectError
_1660:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1662
	call	_brl_blitz_NullObjectError
_1662:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1664
	call	_brl_blitz_NullObjectError
_1664:
	push	_73
	push	dword [ebx+24]
	push	_73
	push	dword [esi+20]
	push	_73
	push	dword [edi+16]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1666
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbNullObject
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1669
	call	_brl_blitz_NullObjectError
_1669:
	mov	eax,dword [ebx+112]
	mov	dword [ebp-24],eax
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_1672
	call	_brl_blitz_NullObjectError
_1672:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-20],eax
	jmp	_74
_76:
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_1677
	call	_brl_blitz_NullObjectError
_1677:
	push	_bb_Note
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	je	_74
	mov	eax,ebp
	push	eax
	push	_1687
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1678
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	mov	dword [ebp-16],eax
	cmp	dword [ebp-16],_bbNullObject
	jne	_1680
	call	_brl_blitz_NullObjectError
_1680:
	mov	edi,dword [ebp-12]
	cmp	edi,_bbNullObject
	jne	_1682
	call	_brl_blitz_NullObjectError
_1682:
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_1684
	call	_brl_blitz_NullObjectError
_1684:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_1686
	call	_brl_blitz_NullObjectError
_1686:
	push	_78
	push	_77
	push	dword [ebx+32]
	push	_77
	push	dword [esi+28]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_77
	push	dword [edi+36]
	push	_77
	mov	eax,dword [ebp-16]
	push	dword [eax+24]
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_74:
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_1675
	call	_brl_blitz_NullObjectError
_1675:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_76
_75:
	push	_1688
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_79
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1689
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	_bb_insertIpadEntry
	add	esp,8
	mov	ebx,0
	jmp	_618
_618:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__addNote:
	push	ebp
	mov	ebp,esp
	sub	esp,32
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],_bbNullObject
	mov	dword [ebp-16],0
	mov	dword [ebp-20],_bbNullObject
	mov	dword [ebp-24],_bbEmptyString
	mov	eax,ebp
	push	eax
	push	_1765
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1692
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_bb_convertFromSSV
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1694
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],1
	push	_1696
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],_bbNullObject
	push	_1698
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],_bbEmptyString
	mov	eax,dword [ebp-12]
	mov	dword [ebp-32],eax
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_1702
	call	_brl_blitz_NullObjectError
_1702:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-28],eax
	jmp	_80
_82:
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1707
	call	_brl_blitz_NullObjectError
_1707:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	je	_80
	mov	eax,ebp
	push	eax
	push	_1762
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1708
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],1
	jne	_1709
	mov	eax,ebp
	push	eax
	push	_1722
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1710
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_newNote
	mov	dword [ebp-20],eax
	push	_1711
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1713
	call	_brl_blitz_NullObjectError
_1713:
	push	dword [ebp-20]
	push	dword [ebx+112]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_1714
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1716
	call	_brl_blitz_NullObjectError
_1716:
	mov	ebx,dword [ebp-24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_1721
	push	eax
	call	_bbGCFree
	add	esp,4
_1721:
	mov	dword [esi+24],ebx
	call	dword [_bbOnDebugLeaveScope]
_1709:
	push	_1723
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],2
	jne	_1724
	mov	eax,ebp
	push	eax
	push	_1742
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1725
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1727
	call	_brl_blitz_NullObjectError
_1727:
	mov	ebx,dword [ebp-24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+36]
	dec	dword [eax+4]
	jnz	_1732
	push	eax
	call	_bbGCFree
	add	esp,4
_1732:
	mov	dword [esi+36],ebx
	push	_1733
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1735
	call	_brl_blitz_NullObjectError
_1735:
	mov	edi,dword [ebx+96]
	cmp	edi,_bbNullObject
	jne	_1737
	call	_brl_blitz_NullObjectError
_1737:
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1739
	call	_brl_blitz_NullObjectError
_1739:
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_1741
	call	_brl_blitz_NullObjectError
_1741:
	push	1
	push	dword [ebx+24]
	push	_32
	push	dword [esi+36]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	edi
	mov	eax,dword [edi]
	call	dword [eax+52]
	add	esp,12
	call	dword [_bbOnDebugLeaveScope]
_1724:
	push	_1743
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],3
	jne	_1744
	mov	eax,ebp
	push	eax
	push	_1749
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1745
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_1747
	call	_brl_blitz_NullObjectError
_1747:
	push	dword [ebp-24]
	call	_bbStringToInt
	add	esp,4
	mov	dword [ebx+28],eax
	call	dword [_bbOnDebugLeaveScope]
_1744:
	push	_1750
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],4
	jne	_1751
	mov	eax,ebp
	push	eax
	push	_1760
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1752
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1754
	call	_brl_blitz_NullObjectError
_1754:
	mov	ebx,dword [ebp-24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+32]
	dec	dword [eax+4]
	jnz	_1759
	push	eax
	call	_bbGCFree
	add	esp,4
_1759:
	mov	dword [esi+32],ebx
	call	dword [_bbOnDebugLeaveScope]
_1751:
	push	_1761
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-16],1
	call	dword [_bbOnDebugLeaveScope]
_80:
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1705
	call	_brl_blitz_NullObjectError
_1705:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_82
_81:
	push	_1764
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	jmp	_622
_622:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__update:
	push	ebp
	mov	ebp,esp
	sub	esp,56
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],0
	mov	dword [ebp-20],_bbEmptyString
	mov	dword [ebp-24],_bbNullObject
	mov	dword [ebp-28],_bbEmptyString
	mov	dword [ebp-32],_bbEmptyString
	mov	dword [ebp-36],_bbNullObject
	mov	dword [ebp-40],_bbNullObject
	mov	dword [ebp-44],_bbNullObject
	mov	dword [ebp-48],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_2205
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1768
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventSource
	mov	ebx,eax
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1773
	call	_brl_blitz_NullObjectError
_1773:
	cmp	ebx,dword [esi+44]
	je	_1771
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1776
	call	_brl_blitz_NullObjectError
_1776:
	cmp	ebx,dword [esi+48]
	je	_1774
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1779
	call	_brl_blitz_NullObjectError
_1779:
	cmp	ebx,dword [esi+36]
	je	_1777
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1782
	call	_brl_blitz_NullObjectError
_1782:
	cmp	ebx,dword [esi+52]
	je	_1780
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1785
	call	_brl_blitz_NullObjectError
_1785:
	cmp	ebx,dword [esi+56]
	je	_1783
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1788
	call	_brl_blitz_NullObjectError
_1788:
	cmp	ebx,dword [esi+60]
	je	_1786
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1791
	call	_brl_blitz_NullObjectError
_1791:
	cmp	ebx,dword [esi+64]
	je	_1789
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1794
	call	_brl_blitz_NullObjectError
_1794:
	cmp	ebx,dword [esi+68]
	je	_1792
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1797
	call	_brl_blitz_NullObjectError
_1797:
	mov	esi,dword [esi+96]
	cmp	esi,_bbNullObject
	jne	_1799
	call	_brl_blitz_NullObjectError
_1799:
	cmp	ebx,dword [esi+8]
	je	_1795
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1802
	call	_brl_blitz_NullObjectError
_1802:
	cmp	ebx,dword [esi+72]
	je	_1800
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1805
	call	_brl_blitz_NullObjectError
_1805:
	cmp	ebx,dword [esi+84]
	je	_1803
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1808
	call	_brl_blitz_NullObjectError
_1808:
	cmp	ebx,dword [esi+88]
	je	_1806
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1811
	call	_brl_blitz_NullObjectError
_1811:
	cmp	ebx,dword [esi+92]
	je	_1809
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1814
	call	_brl_blitz_NullObjectError
_1814:
	mov	esi,dword [esi+8]
	cmp	esi,_bbNullObject
	jne	_1816
	call	_brl_blitz_NullObjectError
_1816:
	cmp	ebx,dword [esi+8]
	je	_1812
	jmp	_1770
_1771:
	mov	eax,ebp
	push	eax
	push	_1855
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1817
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],0
	push	_1819
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_quickMode],0
	je	_1820
	mov	eax,ebp
	push	eax
	push	_1822
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1821
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1823
_1820:
	mov	eax,ebp
	push	eax
	push	_1827
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1824
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1826
	call	_brl_blitz_NullObjectError
_1826:
	push	0
	push	_85
	push	_37
	push	_84
	push	dword [ebx+12]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_83
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_system_Confirm
	add	esp,8
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_1823:
	push	_1828
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_1829
	mov	eax,ebp
	push	eax
	push	_1853
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1830
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	4
	push	_86
	call	_bb_prompt
	add	esp,16
	mov	dword [ebp-12],eax
	push	_1832
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1833
	mov	eax,ebp
	push	eax
	push	_1852
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1834
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1836
	call	_brl_blitz_NullObjectError
_1836:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1838
	call	_brl_blitz_NullObjectError
_1838:
	push	_90
	push	dword [ebp-12]
	push	_89
	push	dword [esi+16]
	push	_88
	call	_brl_system_CurrentDate
	push	eax
	push	_87
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_1839
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1841
	call	_brl_blitz_NullObjectError
_1841:
	mov	ebx,dword [ebp-12]
	inc	dword [ebx+4]
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_1846
	push	eax
	call	_bbGCFree
	add	esp,4
_1846:
	mov	dword [esi+16],ebx
	push	_1847
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1849
	call	_brl_blitz_NullObjectError
_1849:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1851
	call	_brl_blitz_NullObjectError
_1851:
	push	dword [esi+16]
	push	_57
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebx+100]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_1833:
	call	dword [_bbOnDebugLeaveScope]
_1829:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1774:
	mov	eax,ebp
	push	eax
	push	_1895
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1857
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	push	_1859
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_quickMode],0
	je	_1860
	mov	eax,ebp
	push	eax
	push	_1862
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1861
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1863
_1860:
	mov	eax,ebp
	push	eax
	push	_1867
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1864
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1866
	call	_brl_blitz_NullObjectError
_1866:
	push	0
	push	_92
	push	_37
	push	_84
	push	dword [ebx+12]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_91
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_system_Confirm
	add	esp,8
	mov	dword [ebp-16],eax
	call	dword [_bbOnDebugLeaveScope]
_1863:
	push	_1868
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],0
	je	_1869
	mov	eax,ebp
	push	eax
	push	_1893
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1870
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	12
	push	_93
	call	_bb_prompt
	add	esp,16
	mov	dword [ebp-20],eax
	push	_1872
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	dword [ebp-20]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_1873
	mov	eax,ebp
	push	eax
	push	_1892
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1874
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1876
	call	_brl_blitz_NullObjectError
_1876:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1878
	call	_brl_blitz_NullObjectError
_1878:
	push	_90
	push	dword [ebp-20]
	push	_89
	push	dword [esi+20]
	push	_95
	call	_brl_system_CurrentDate
	push	eax
	push	_94
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_1879
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1881
	call	_brl_blitz_NullObjectError
_1881:
	mov	ebx,dword [ebp-20]
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_1886
	push	eax
	call	_bbGCFree
	add	esp,4
_1886:
	mov	dword [esi+20],ebx
	push	_1887
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1889
	call	_brl_blitz_NullObjectError
_1889:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1891
	call	_brl_blitz_NullObjectError
_1891:
	push	dword [esi+20]
	push	_58
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebx+104]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_1873:
	call	dword [_bbOnDebugLeaveScope]
_1869:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1777:
	mov	eax,ebp
	push	eax
	push	_1902
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1896
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1898
	call	_brl_blitz_NullObjectError
_1898:
	cmp	dword [ebx+36],_bbNullObject
	je	_1899
	mov	eax,ebp
	push	eax
	push	_1901
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1900
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_96
	call	_bb_dlog
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_1899:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1780:
	mov	eax,ebp
	push	eax
	push	_1930
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1903
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1905
	call	_brl_blitz_NullObjectError
_1905:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1907
	call	_brl_blitz_NullObjectError
_1907:
	push	_100
	push	dword [esi+24]
	push	_99
	call	_brl_system_CurrentTime
	push	eax
	push	_98
	call	_brl_system_CurrentDate
	push	eax
	push	_97
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	mov	dword [ebp-24],eax
	push	_1909
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1911
	call	_brl_blitz_NullObjectError
_1911:
	mov	ebx,_66
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_1916
	push	eax
	call	_bbGCFree
	add	esp,4
_1916:
	mov	dword [esi+24],ebx
	push	_1917
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1919
	call	_brl_blitz_NullObjectError
_1919:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1921
	call	_brl_blitz_NullObjectError
_1921:
	push	dword [esi+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebx+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_1922
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_1924
	call	_brl_blitz_NullObjectError
_1924:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	_1925
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_1927
	call	_brl_blitz_NullObjectError
_1927:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_1929
	call	_brl_blitz_NullObjectError
_1929:
	push	dword [ebx+8]
	call	_maxgui_maxgui_ActivateGadget
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1783:
	mov	eax,ebp
	push	eax
	push	_1951
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1931
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	0
	push	_101
	call	_bb_prompt
	add	esp,16
	mov	dword [ebp-28],eax
	push	_1933
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1935
	call	_brl_blitz_NullObjectError
_1935:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1937
	call	_brl_blitz_NullObjectError
_1937:
	push	_90
	push	dword [esi+24]
	push	_105
	push	dword [ebp-28]
	push	_104
	call	_brl_system_CurrentTime
	push	eax
	push	_103
	call	_brl_system_CurrentDate
	push	eax
	push	_102
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_1938
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1940
	call	_brl_blitz_NullObjectError
_1940:
	mov	ebx,_65
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_1945
	push	eax
	call	_bbGCFree
	add	esp,4
_1945:
	mov	dword [esi+24],ebx
	push	_1946
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1948
	call	_brl_blitz_NullObjectError
_1948:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1950
	call	_brl_blitz_NullObjectError
_1950:
	push	dword [esi+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebx+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1786:
	mov	eax,ebp
	push	eax
	push	_2022
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1953
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1955
	call	_brl_blitz_NullObjectError
_1955:
	push	_45
	push	dword [ebx+60]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_1956
	mov	eax,ebp
	push	eax
	push	_1990
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1957
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1959
	call	_brl_blitz_NullObjectError
_1959:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1961
	call	_brl_blitz_NullObjectError
_1961:
	push	_90
	push	dword [esi+24]
	push	_108
	call	_brl_system_CurrentTime
	push	eax
	push	_107
	call	_brl_system_CurrentDate
	push	eax
	push	_106
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_1962
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1964
	call	_brl_blitz_NullObjectError
_1964:
	mov	ebx,_62
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_1969
	push	eax
	call	_bbGCFree
	add	esp,4
_1969:
	mov	dword [esi+24],ebx
	push	_1970
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1972
	call	_brl_blitz_NullObjectError
_1972:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1974
	call	_brl_blitz_NullObjectError
_1974:
	push	dword [esi+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebx+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_1975
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1977
	call	_brl_blitz_NullObjectError
_1977:
	push	_63
	push	dword [ebx+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_1978
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1980
	call	_brl_blitz_NullObjectError
_1980:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1983
	call	_brl_blitz_NullObjectError
_1983:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_1985
	call	_brl_blitz_NullObjectError
_1985:
	push	0
	push	0
	push	dword [ebx+20]
	push	0
	push	_64
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+36]
	dec	dword [eax+4]
	jnz	_1989
	push	eax
	call	_bbGCFree
	add	esp,4
_1989:
	mov	dword [esi+36],ebx
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1991
_1956:
	mov	eax,ebp
	push	eax
	push	_2016
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1992
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_1994
	call	_brl_blitz_NullObjectError
_1994:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1996
	call	_brl_blitz_NullObjectError
_1996:
	push	_90
	push	dword [esi+24]
	push	_111
	call	_brl_system_CurrentTime
	push	eax
	push	_110
	call	_brl_system_CurrentDate
	push	eax
	push	_109
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_1997
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_1999
	call	_brl_blitz_NullObjectError
_1999:
	mov	ebx,_61
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_2004
	push	eax
	call	_bbGCFree
	add	esp,4
_2004:
	mov	dword [esi+24],ebx
	push	_2005
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2007
	call	_brl_blitz_NullObjectError
_2007:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2009
	call	_brl_blitz_NullObjectError
_2009:
	push	dword [esi+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebx+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_2010
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2012
	call	_brl_blitz_NullObjectError
_2012:
	push	_45
	push	dword [ebx+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_2013
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2015
	call	_brl_blitz_NullObjectError
_2015:
	push	dword [ebx+36]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_1991:
	push	_2017
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2019
	call	_brl_blitz_NullObjectError
_2019:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_2021
	call	_brl_blitz_NullObjectError
_2021:
	push	dword [ebx+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1789:
	mov	eax,ebp
	push	eax
	push	_2041
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2023
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2025
	call	_brl_blitz_NullObjectError
_2025:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2027
	call	_brl_blitz_NullObjectError
_2027:
	push	_90
	push	dword [esi+24]
	push	_114
	call	_brl_system_CurrentTime
	push	eax
	push	_113
	call	_brl_system_CurrentDate
	push	eax
	push	_112
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_2028
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2030
	call	_brl_blitz_NullObjectError
_2030:
	mov	ebx,_67
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_2035
	push	eax
	call	_bbGCFree
	add	esp,4
_2035:
	mov	dword [esi+24],ebx
	push	_2036
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2038
	call	_brl_blitz_NullObjectError
_2038:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2040
	call	_brl_blitz_NullObjectError
_2040:
	push	dword [esi+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebx+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1792:
	mov	eax,ebp
	push	eax
	push	_2062
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2042
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	0
	push	_115
	call	_bb_prompt
	add	esp,16
	mov	dword [ebp-32],eax
	push	_2044
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2046
	call	_brl_blitz_NullObjectError
_2046:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2048
	call	_brl_blitz_NullObjectError
_2048:
	push	_119
	push	dword [ebp-32]
	push	_118
	push	dword [esi+24]
	push	_114
	call	_brl_system_CurrentTime
	push	eax
	push	_117
	call	_brl_system_CurrentDate
	push	eax
	push	_116
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_2049
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2051
	call	_brl_blitz_NullObjectError
_2051:
	mov	ebx,_68
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_2056
	push	eax
	call	_bbGCFree
	add	esp,4
_2056:
	mov	dword [esi+24],ebx
	push	_2057
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2059
	call	_brl_blitz_NullObjectError
_2059:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2061
	call	_brl_blitz_NullObjectError
_2061:
	push	dword [esi+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebx+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1795:
	mov	eax,ebp
	push	eax
	push	_2085
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2064
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_2067
	cmp	eax,8196
	je	_2068
	jmp	_2066
_2067:
	mov	eax,ebp
	push	eax
	push	_2076
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2069
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2071
	call	_brl_blitz_NullObjectError
_2071:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2073
	call	_brl_blitz_NullObjectError
_2073:
	mov	esi,dword [esi+96]
	cmp	esi,_bbNullObject
	jne	_2075
	call	_brl_blitz_NullObjectError
_2075:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+64]
	add	esp,4
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+76]
	add	esp,8
	push	eax
	call	_bb_readNote
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2066
_2068:
	mov	eax,ebp
	push	eax
	push	_2084
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2077
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2079
	call	_brl_blitz_NullObjectError
_2079:
	mov	esi,dword [ebx+8]
	cmp	esi,_bbNullObject
	jne	_2081
	call	_brl_blitz_NullObjectError
_2081:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2083
	call	_brl_blitz_NullObjectError
_2083:
	push	_bbNullObject
	push	dword [ebx+80]
	push	dword [esi+8]
	call	_maxgui_maxgui_PopupWindowMenu
	add	esp,12
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2066
_2066:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1800:
	mov	eax,ebp
	push	eax
	push	_2100
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2086
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2088
	call	_brl_blitz_NullObjectError
_2088:
	mov	ebx,dword [ebx+96]
	cmp	ebx,_bbNullObject
	jne	_2090
	call	_brl_blitz_NullObjectError
_2090:
	call	_maxgui_maxgui_ActiveGadget
	cmp	eax,dword [ebx+8]
	jne	_2091
	mov	eax,ebp
	push	eax
	push	_2099
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2092
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2094
	call	_brl_blitz_NullObjectError
_2094:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2096
	call	_brl_blitz_NullObjectError
_2096:
	mov	esi,dword [esi+96]
	cmp	esi,_bbNullObject
	jne	_2098
	call	_brl_blitz_NullObjectError
_2098:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+64]
	add	esp,4
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+76]
	add	esp,8
	push	eax
	call	_bb_readNote
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_2091:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1803:
	mov	eax,ebp
	push	eax
	push	_2108
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2101
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2103
	call	_brl_blitz_NullObjectError
_2103:
	push	_121
	call	_brl_system_CurrentDate
	push	eax
	push	_120
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	mov	dword [ebp-36],eax
	push	_2105
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-36]
	cmp	ebx,_bbNullObject
	jne	_2107
	call	_brl_blitz_NullObjectError
_2107:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1806:
	mov	eax,ebp
	push	eax
	push	_2116
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2109
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2111
	call	_brl_blitz_NullObjectError
_2111:
	push	_122
	call	_brl_system_CurrentDate
	push	eax
	push	_120
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	mov	dword [ebp-40],eax
	push	_2113
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-40]
	cmp	ebx,_bbNullObject
	jne	_2115
	call	_brl_blitz_NullObjectError
_2115:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1809:
	mov	eax,ebp
	push	eax
	push	_2124
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2117
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2119
	call	_brl_blitz_NullObjectError
_2119:
	push	_123
	call	_brl_system_CurrentDate
	push	eax
	push	_120
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	mov	dword [ebp-44],eax
	push	_2121
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-44]
	cmp	ebx,_bbNullObject
	jne	_2123
	call	_brl_blitz_NullObjectError
_2123:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1812:
	mov	eax,ebp
	push	eax
	push	_2133
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2125
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	je	_2128
	jmp	_2127
_2128:
	mov	eax,ebp
	push	eax
	push	_2132
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2129
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2131
	call	_brl_blitz_NullObjectError
_2131:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2127
_2127:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_1770
_1770:
	push	_2134
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-48],_bbNullObject
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2137
	call	_brl_blitz_NullObjectError
_2137:
	mov	eax,dword [ebx+112]
	mov	dword [ebp-52],eax
	mov	ebx,dword [ebp-52]
	cmp	ebx,_bbNullObject
	jne	_2140
	call	_brl_blitz_NullObjectError
_2140:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-56],eax
	jmp	_124
_126:
	mov	ebx,dword [ebp-56]
	cmp	ebx,_bbNullObject
	jne	_2145
	call	_brl_blitz_NullObjectError
_2145:
	push	_bb_Note
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-48],eax
	cmp	dword [ebp-48],_bbNullObject
	je	_124
	mov	eax,ebp
	push	eax
	push	_2204
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2146
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-48],_bbNullObject
	je	_2147
	mov	eax,ebp
	push	eax
	push	_2203
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2148
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventSource
	mov	ebx,eax
	mov	esi,dword [ebp-48]
	cmp	esi,_bbNullObject
	jne	_2153
	call	_brl_blitz_NullObjectError
_2153:
	mov	esi,dword [esi+8]
	cmp	esi,_bbNullObject
	jne	_2155
	call	_brl_blitz_NullObjectError
_2155:
	cmp	ebx,dword [esi+8]
	je	_2151
	jmp	_2150
_2151:
	mov	eax,ebp
	push	eax
	push	_2202
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2156
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	je	_2159
	jmp	_2158
_2159:
	mov	eax,ebp
	push	eax
	push	_2201
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2160
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-48]
	cmp	ebx,_bbNullObject
	jne	_2162
	call	_brl_blitz_NullObjectError
_2162:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,4
	cmp	eax,0
	je	_2163
	mov	eax,ebp
	push	eax
	push	_2197
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2164
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-48]
	cmp	esi,_bbNullObject
	jne	_2166
	call	_brl_blitz_NullObjectError
_2166:
	mov	ebx,dword [ebp-48]
	cmp	ebx,_bbNullObject
	jne	_2169
	call	_brl_blitz_NullObjectError
_2169:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_2171
	call	_brl_blitz_NullObjectError
_2171:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_2175
	push	eax
	call	_bbGCFree
	add	esp,4
_2175:
	mov	dword [esi+24],ebx
	push	_2176
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-48]
	cmp	esi,_bbNullObject
	jne	_2178
	call	_brl_blitz_NullObjectError
_2178:
	mov	ebx,dword [ebp-48]
	cmp	ebx,_bbNullObject
	jne	_2181
	call	_brl_blitz_NullObjectError
_2181:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_2183
	call	_brl_blitz_NullObjectError
_2183:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+32]
	dec	dword [eax+4]
	jnz	_2187
	push	eax
	call	_bbGCFree
	add	esp,4
_2187:
	mov	dword [esi+32],ebx
	push	_2188
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2190
	call	_brl_blitz_NullObjectError
_2190:
	mov	edi,dword [ebx+96]
	cmp	edi,_bbNullObject
	jne	_2192
	call	_brl_blitz_NullObjectError
_2192:
	mov	esi,dword [ebp-48]
	cmp	esi,_bbNullObject
	jne	_2194
	call	_brl_blitz_NullObjectError
_2194:
	mov	ebx,dword [ebp-48]
	cmp	ebx,_bbNullObject
	jne	_2196
	call	_brl_blitz_NullObjectError
_2196:
	push	dword [ebx+24]
	push	_32
	push	dword [esi+36]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	edi
	mov	eax,dword [edi]
	call	dword [eax+56]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_2163:
	push	_2198
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-48]
	cmp	ebx,_bbNullObject
	jne	_2200
	call	_brl_blitz_NullObjectError
_2200:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+64]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2158
_2158:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2150
_2150:
	call	dword [_bbOnDebugLeaveScope]
_2147:
	call	dword [_bbOnDebugLeaveScope]
_124:
	mov	ebx,dword [ebp-56]
	cmp	ebx,_bbNullObject
	jne	_2143
	call	_brl_blitz_NullObjectError
_2143:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_126
_125:
	mov	ebx,0
	jmp	_625
_625:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_iPad__noteBySummary:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_2227
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2206
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbNullObject
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2209
	call	_brl_blitz_NullObjectError
_2209:
	mov	eax,dword [ebx+112]
	mov	dword [ebp-16],eax
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_2212
	call	_brl_blitz_NullObjectError
_2212:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_127
_129:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_2217
	call	_brl_blitz_NullObjectError
_2217:
	push	_bb_Note
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	je	_127
	mov	eax,ebp
	push	eax
	push	_2226
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2218
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_2220
	call	_brl_blitz_NullObjectError
_2220:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_2222
	call	_brl_blitz_NullObjectError
_2222:
	push	dword [ebp-8]
	push	dword [ebx+24]
	push	_32
	push	dword [esi+36]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_2223
	mov	eax,ebp
	push	eax
	push	_2225
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2224
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_629
_2223:
	call	dword [_bbOnDebugLeaveScope]
_127:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_2215
	call	_brl_blitz_NullObjectError
_2215:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_129
_128:
	mov	ebx,_bbNullObject
	jmp	_629
_629:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_insertIpadEntry:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],_bbNullObject
	mov	dword [ebp-16],0
	mov	dword [ebp-20],_bbEmptyString
	mov	eax,ebp
	push	eax
	push	_2274
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2229
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-12],eax
	push	_2231
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_132:
	mov	eax,ebp
	push	eax
	push	_2233
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2232
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_iPaddb]
	call	_brl_stream_ReadLine
	add	esp,4
	push	eax
	push	dword [ebp-12]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_130:
	push	dword [_bb_iPaddb]
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_132
_131:
	push	_2234
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_iPaddb]
	call	_brl_filesystem_CloseFile
	add	esp,4
	push	_2235
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	1
	push	dword [_bb_ipaddbn]
	call	_brl_filesystem_OpenFile
	add	esp,12
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_2239
	push	eax
	call	_bbGCFree
	add	esp,4
_2239:
	mov	dword [_bb_iPaddb],ebx
	push	_2240
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],1
	push	_2242
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],_bbEmptyString
	mov	edi,dword [ebp-12]
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_2246
	call	_brl_blitz_NullObjectError
_2246:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	esi,eax
	jmp	_133
_135:
	mov	ebx,esi
	cmp	ebx,_bbNullObject
	jne	_2251
	call	_brl_blitz_NullObjectError
_2251:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-20],eax
	cmp	dword [ebp-20],_bbNullObject
	je	_133
	mov	eax,ebp
	push	eax
	push	_2262
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2252
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2254
	call	_brl_blitz_NullObjectError
_2254:
	mov	eax,dword [ebx+12]
	cmp	dword [ebp-16],eax
	jne	_2255
	mov	eax,ebp
	push	eax
	push	_2257
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2256
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	push	dword [_bb_iPaddb]
	call	_brl_stream_WriteLine
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2258
_2255:
	mov	eax,ebp
	push	eax
	push	_2260
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2259
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	push	dword [_bb_iPaddb]
	call	_brl_stream_WriteLine
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_2258:
	push	_2261
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-16],1
	call	dword [_bbOnDebugLeaveScope]
_133:
	mov	ebx,esi
	cmp	ebx,_bbNullObject
	jne	_2249
	call	_brl_blitz_NullObjectError
_2249:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_135
_134:
	push	_2263
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2265
	call	_brl_blitz_NullObjectError
_2265:
	cmp	dword [ebx+12],-1
	jne	_2266
	mov	eax,ebp
	push	eax
	push	_2268
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2267
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	push	dword [_bb_iPaddb]
	call	_brl_stream_WriteLine
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_2266:
	push	_2269
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_2273
	push	eax
	call	_bbGCFree
	add	esp,4
_2273:
	mov	dword [_bb_iPaddb],ebx
	mov	ebx,0
	jmp	_633
_633:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newIpad:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	dword [ebp-4],_bbNullObject
	push	ebp
	push	_2284
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2278
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_iPad
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-4],eax
	push	_2280
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2282
	call	_brl_blitz_NullObjectError
_2282:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	_2283
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	jmp	_635
_635:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newBlankIpadBC:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_2323
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2287
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_newIpad
	mov	dword [ebp-8],eax
	push	_2289
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_2291
	call	_brl_blitz_NullObjectError
_2291:
	mov	dword [ebx+12],-1
	push	_2293
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_2295
	call	_brl_blitz_NullObjectError
_2295:
	mov	ebx,dword [ebp-4]
	inc	dword [ebx+4]
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_2300
	push	eax
	call	_bbGCFree
	add	esp,4
_2300:
	mov	dword [esi+16],ebx
	push	_2301
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_2303
	call	_brl_blitz_NullObjectError
_2303:
	mov	ebx,_136
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_2308
	push	eax
	call	_bbGCFree
	add	esp,4
_2308:
	mov	dword [esi+20],ebx
	push	_2309
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_2311
	call	_brl_blitz_NullObjectError
_2311:
	mov	ebx,_60
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_2316
	push	eax
	call	_bbGCFree
	add	esp,4
_2316:
	mov	dword [esi+24],ebx
	push	_2317
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_2319
	call	_brl_blitz_NullObjectError
_2319:
	push	_119
	call	_brl_system_CurrentTime
	push	eax
	push	_139
	call	_brl_system_CurrentDate
	push	eax
	push	_138
	call	_brl_system_CurrentDate
	push	eax
	push	_137
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_2320
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_2322
	call	_brl_blitz_NullObjectError
_2322:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	mov	ebx,_bbNullObject
	jmp	_638
_638:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newBlankIpadSN:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_2361
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2325
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_newIpad
	mov	dword [ebp-8],eax
	push	_2327
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_2329
	call	_brl_blitz_NullObjectError
_2329:
	mov	dword [ebx+12],-1
	push	_2331
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_2333
	call	_brl_blitz_NullObjectError
_2333:
	mov	ebx,_140
	inc	dword [ebx+4]
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_2338
	push	eax
	call	_bbGCFree
	add	esp,4
_2338:
	mov	dword [esi+16],ebx
	push	_2339
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_2341
	call	_brl_blitz_NullObjectError
_2341:
	mov	ebx,dword [ebp-4]
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_2346
	push	eax
	call	_bbGCFree
	add	esp,4
_2346:
	mov	dword [esi+20],ebx
	push	_2347
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_2349
	call	_brl_blitz_NullObjectError
_2349:
	mov	ebx,_60
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_2354
	push	eax
	call	_bbGCFree
	add	esp,4
_2354:
	mov	dword [esi+24],ebx
	push	_2355
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_2357
	call	_brl_blitz_NullObjectError
_2357:
	push	_119
	call	_brl_system_CurrentTime
	push	eax
	push	_139
	call	_brl_system_CurrentDate
	push	eax
	push	_138
	call	_brl_system_CurrentDate
	push	eax
	push	_137
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_2358
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_2360
	call	_brl_blitz_NullObjectError
_2360:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	mov	ebx,_bbNullObject
	jmp	_641
_641:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_findBySN:
	push	ebp
	mov	ebp,esp
	sub	esp,40
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-24],_bbNullObject
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbNullObject
	mov	dword [ebp-20],0
	mov	dword [ebp-28],0
	mov	dword [ebp-32],_bbNullObject
	mov	dword [ebp-36],_bbEmptyString
	mov	eax,ebp
	push	eax
	push	_2491
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2363
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],_bbNullObject
	mov	edi,dword [_bb_ipadList]
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_2367
	call	_brl_blitz_NullObjectError
_2367:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	esi,eax
	jmp	_141
_143:
	mov	ebx,esi
	cmp	ebx,_bbNullObject
	jne	_2372
	call	_brl_blitz_NullObjectError
_2372:
	push	_bb_iPad
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	je	_141
	mov	eax,ebp
	push	eax
	push	_2385
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2373
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_2375
	call	_brl_blitz_NullObjectError
_2375:
	push	dword [ebp-4]
	push	dword [ebx+20]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_2376
	mov	eax,ebp
	push	eax
	push	_2384
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2377
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_2381
	push	eax
	call	_bbGCFree
	add	esp,4
_2381:
	mov	dword [_bb_iPaddb],ebx
	push	_2382
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_144
	call	_brl_system_Notify
	add	esp,8
	push	_2383
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_bbNullObject
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_644
_2376:
	call	dword [_bbOnDebugLeaveScope]
_141:
	mov	ebx,esi
	cmp	ebx,_bbNullObject
	jne	_2370
	call	_brl_blitz_NullObjectError
_2370:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_143
_142:
	push	_2387
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_iPaddb]
	mov	dword [ebp-8],eax
	push	_2389
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbEmptyString
	push	_2391
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],_bbNullObject
	push	_2393
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],1
	push	_2395
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_147:
	mov	eax,ebp
	push	eax
	push	_2474
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2396
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_brl_stream_ReadLine
	add	esp,4
	mov	dword [ebp-12],eax
	push	_2397
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	call	_bb_convertFromCSV
	add	esp,4
	mov	dword [ebp-16],eax
	push	_2398
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	dword [ebp-16]
	call	_brl_linkedlist_ListContains
	add	esp,8
	cmp	eax,0
	je	_2399
	mov	eax,ebp
	push	eax
	push	_2471
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2400
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-28],1
	push	_2402
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_newIpad
	mov	dword [ebp-32],eax
	push	_2404
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2406
	call	_brl_blitz_NullObjectError
_2406:
	mov	eax,dword [ebp-20]
	mov	dword [ebx+12],eax
	push	_2408
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-36],_bbEmptyString
	mov	eax,dword [ebp-16]
	mov	dword [ebp-40],eax
	mov	ebx,dword [ebp-40]
	cmp	ebx,_bbNullObject
	jne	_2412
	call	_brl_blitz_NullObjectError
_2412:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_148
_150:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_2417
	call	_brl_blitz_NullObjectError
_2417:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-36],eax
	cmp	dword [ebp-36],_bbNullObject
	je	_148
	mov	eax,ebp
	push	eax
	push	_2461
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2418
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],1
	jne	_2419
	mov	eax,ebp
	push	eax
	push	_2428
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2420
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2422
	call	_brl_blitz_NullObjectError
_2422:
	mov	esi,ebx
	mov	eax,dword [ebp-36]
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_2427
	push	eax
	call	_bbGCFree
	add	esp,4
_2427:
	mov	dword [esi+16],ebx
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2429
_2419:
	mov	eax,ebp
	push	eax
	push	_2459
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2430
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],2
	jne	_2431
	mov	eax,ebp
	push	eax
	push	_2440
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2432
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2434
	call	_brl_blitz_NullObjectError
_2434:
	mov	esi,ebx
	mov	eax,dword [ebp-36]
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_2439
	push	eax
	call	_bbGCFree
	add	esp,4
_2439:
	mov	dword [esi+20],ebx
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2441
_2431:
	mov	eax,ebp
	push	eax
	push	_2458
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2442
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],3
	jne	_2443
	mov	eax,ebp
	push	eax
	push	_2452
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2444
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2446
	call	_brl_blitz_NullObjectError
_2446:
	mov	esi,ebx
	mov	eax,dword [ebp-36]
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_2451
	push	eax
	call	_bbGCFree
	add	esp,4
_2451:
	mov	dword [esi+24],ebx
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2453
_2443:
	mov	eax,ebp
	push	eax
	push	_2457
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2454
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2456
	call	_brl_blitz_NullObjectError
_2456:
	push	dword [ebp-36]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_2453:
	call	dword [_bbOnDebugLeaveScope]
_2441:
	call	dword [_bbOnDebugLeaveScope]
_2429:
	push	_2460
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-28],1
	call	dword [_bbOnDebugLeaveScope]
_148:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_2415
	call	_brl_blitz_NullObjectError
_2415:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_150
_149:
	push	_2462
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_2466
	push	eax
	call	_bbGCFree
	add	esp,4
_2466:
	mov	dword [_bb_iPaddb],ebx
	push	_2467
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2469
	call	_brl_blitz_NullObjectError
_2469:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	push	_2470
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_644
_2399:
	push	_2473
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-20],1
	call	dword [_bbOnDebugLeaveScope]
_145:
	push	dword [ebp-8]
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_147
_146:
	push	_2475
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_quickMode],0
	je	_2476
	mov	eax,ebp
	push	eax
	push	_2478
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2477
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_bb_newBlankIpadSN
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2479
_2476:
	mov	eax,ebp
	push	eax
	push	_2484
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2480
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_152
	push	dword [ebp-4]
	push	_151
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_system_Confirm
	add	esp,8
	cmp	eax,0
	je	_2481
	mov	eax,ebp
	push	eax
	push	_2483
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2482
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_bb_newBlankIpadSN
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_2481:
	call	dword [_bbOnDebugLeaveScope]
_2479:
	push	_2485
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_2489
	push	eax
	call	_bbGCFree
	add	esp,4
_2489:
	mov	dword [_bb_iPaddb],ebx
	push	_2490
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_bbNullObject
	jmp	_644
_644:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_findByBC:
	push	ebp
	mov	ebp,esp
	sub	esp,40
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-24],_bbNullObject
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbNullObject
	mov	dword [ebp-20],0
	mov	dword [ebp-28],0
	mov	dword [ebp-32],_bbNullObject
	mov	dword [ebp-36],_bbEmptyString
	mov	eax,ebp
	push	eax
	push	_2620
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2494
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],_bbNullObject
	mov	edi,dword [_bb_ipadList]
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_2498
	call	_brl_blitz_NullObjectError
_2498:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	esi,eax
	jmp	_153
_155:
	mov	ebx,esi
	cmp	ebx,_bbNullObject
	jne	_2503
	call	_brl_blitz_NullObjectError
_2503:
	push	_bb_iPad
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	je	_153
	mov	eax,ebp
	push	eax
	push	_2516
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2504
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_2506
	call	_brl_blitz_NullObjectError
_2506:
	push	dword [ebp-4]
	push	dword [ebx+16]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_2507
	mov	eax,ebp
	push	eax
	push	_2515
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2508
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_2512
	push	eax
	call	_bbGCFree
	add	esp,4
_2512:
	mov	dword [_bb_iPaddb],ebx
	push	_2513
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_144
	call	_brl_system_Notify
	add	esp,8
	push	_2514
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_bbNullObject
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_647
_2507:
	call	dword [_bbOnDebugLeaveScope]
_153:
	mov	ebx,esi
	cmp	ebx,_bbNullObject
	jne	_2501
	call	_brl_blitz_NullObjectError
_2501:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_155
_154:
	push	_2517
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_iPaddb]
	mov	dword [ebp-8],eax
	push	_2519
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbEmptyString
	push	_2521
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],_bbNullObject
	push	_2523
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],1
	push	_2525
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_158:
	mov	eax,ebp
	push	eax
	push	_2603
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2526
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_brl_stream_ReadLine
	add	esp,4
	mov	dword [ebp-12],eax
	push	_2527
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	call	_bb_convertFromCSV
	add	esp,4
	mov	dword [ebp-16],eax
	push	_2528
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	dword [ebp-16]
	call	_brl_linkedlist_ListContains
	add	esp,8
	cmp	eax,0
	je	_2529
	mov	eax,ebp
	push	eax
	push	_2601
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2530
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-28],1
	push	_2532
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_newIpad
	mov	dword [ebp-32],eax
	push	_2534
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2536
	call	_brl_blitz_NullObjectError
_2536:
	mov	eax,dword [ebp-20]
	mov	dword [ebx+12],eax
	push	_2538
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-36],_bbEmptyString
	mov	eax,dword [ebp-16]
	mov	dword [ebp-40],eax
	mov	ebx,dword [ebp-40]
	cmp	ebx,_bbNullObject
	jne	_2542
	call	_brl_blitz_NullObjectError
_2542:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_159
_161:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_2547
	call	_brl_blitz_NullObjectError
_2547:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-36],eax
	cmp	dword [ebp-36],_bbNullObject
	je	_159
	mov	eax,ebp
	push	eax
	push	_2591
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2548
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],1
	jne	_2549
	mov	eax,ebp
	push	eax
	push	_2558
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2550
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2552
	call	_brl_blitz_NullObjectError
_2552:
	mov	esi,ebx
	mov	eax,dword [ebp-36]
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_2557
	push	eax
	call	_bbGCFree
	add	esp,4
_2557:
	mov	dword [esi+16],ebx
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2559
_2549:
	mov	eax,ebp
	push	eax
	push	_2589
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2560
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],2
	jne	_2561
	mov	eax,ebp
	push	eax
	push	_2570
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2562
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2564
	call	_brl_blitz_NullObjectError
_2564:
	mov	esi,ebx
	mov	eax,dword [ebp-36]
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_2569
	push	eax
	call	_bbGCFree
	add	esp,4
_2569:
	mov	dword [esi+20],ebx
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2571
_2561:
	mov	eax,ebp
	push	eax
	push	_2588
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2572
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],3
	jne	_2573
	mov	eax,ebp
	push	eax
	push	_2582
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2574
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2576
	call	_brl_blitz_NullObjectError
_2576:
	mov	esi,ebx
	mov	eax,dword [ebp-36]
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_2581
	push	eax
	call	_bbGCFree
	add	esp,4
_2581:
	mov	dword [esi+24],ebx
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2583
_2573:
	mov	eax,ebp
	push	eax
	push	_2587
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2584
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2586
	call	_brl_blitz_NullObjectError
_2586:
	push	dword [ebp-36]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_2583:
	call	dword [_bbOnDebugLeaveScope]
_2571:
	call	dword [_bbOnDebugLeaveScope]
_2559:
	push	_2590
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-28],1
	call	dword [_bbOnDebugLeaveScope]
_159:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_2545
	call	_brl_blitz_NullObjectError
_2545:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_161
_160:
	push	_2592
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_2596
	push	eax
	call	_bbGCFree
	add	esp,4
_2596:
	mov	dword [_bb_iPaddb],ebx
	push	_2597
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_2599
	call	_brl_blitz_NullObjectError
_2599:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	push	_2600
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_647
_2529:
	push	_2602
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-20],1
	call	dword [_bbOnDebugLeaveScope]
_156:
	push	dword [ebp-8]
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_158
_157:
	push	_2604
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_quickMode],0
	je	_2605
	mov	eax,ebp
	push	eax
	push	_2607
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2606
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_bb_newBlankIpadBC
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2608
_2605:
	mov	eax,ebp
	push	eax
	push	_2613
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2609
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_152
	push	dword [ebp-4]
	push	_162
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_system_Confirm
	add	esp,8
	cmp	eax,0
	je	_2610
	mov	eax,ebp
	push	eax
	push	_2612
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2611
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_bb_newBlankIpadBC
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_2610:
	call	dword [_bbOnDebugLeaveScope]
_2608:
	push	_2614
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_ipaddbn]
	push	dword [_bb_iPaddb]
	call	_bb_resetFile
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_iPaddb]
	dec	dword [eax+4]
	jnz	_2618
	push	eax
	call	_bbGCFree
	add	esp,4
_2618:
	mov	dword [_bb_iPaddb],ebx
	push	_2619
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_bbNullObject
	jmp	_647
_647:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_2630
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_Note
	push	_bb_Window
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	dword [edx+8],eax
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+24],edx
	mov	eax,dword [ebp-4]
	mov	dword [eax+28],0
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+32],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+36],edx
	push	ebp
	push	_2629
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_650
_650:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_653:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_2633
	push	eax
	call	_bbGCFree
	add	esp,4
_2633:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_2635
	push	eax
	call	_bbGCFree
	add	esp,4
_2635:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_2637
	push	eax
	call	_bbGCFree
	add	esp,4
_2637:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_2639
	push	eax
	call	_bbGCFree
	add	esp,4
_2639:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_2641
	push	eax
	call	_bbGCFree
	add	esp,4
_2641:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_2643
	push	eax
	call	_bbGCFree
	add	esp,4
_2643:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_2645
	push	eax
	call	_bbGCFree
	add	esp,4
_2645:
	mov	eax,0
	jmp	_2631
_2631:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note__init:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,ebp
	push	eax
	push	_2714
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2646
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2648
	call	_brl_blitz_NullObjectError
_2648:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_2650
	call	_brl_blitz_NullObjectError
_2650:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2652
	call	_brl_blitz_NullObjectError
_2652:
	push	21
	push	dword [esi+36]
	push	_163
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	300
	push	400
	push	0
	push	450
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,28
	push	_2653
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	dword [_bb_noteWinList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_2654
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2656
	call	_brl_blitz_NullObjectError
_2656:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2659
	call	_brl_blitz_NullObjectError
_2659:
	push	0
	push	dword [ebx+8]
	push	20
	push	150
	push	10
	push	10
	call	_bb_newTextField
	add	esp,24
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_2663
	push	eax
	call	_bbGCFree
	add	esp,4
_2663:
	mov	dword [esi+12],ebx
	push	_2664
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2666
	call	_brl_blitz_NullObjectError
_2666:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_2668
	call	_brl_blitz_NullObjectError
_2668:
	push	_bbNullObject
	push	_bb_noComma
	push	dword [ebx+8]
	call	_maxgui_maxgui_SetGadgetFilter
	add	esp,12
	push	_2669
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2671
	call	_brl_blitz_NullObjectError
_2671:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2674
	call	_brl_blitz_NullObjectError
_2674:
	push	0
	push	dword [ebx+8]
	push	215
	push	375
	push	35
	push	10
	call	_bb_newTextArea
	add	esp,24
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_2678
	push	eax
	call	_bbGCFree
	add	esp,4
_2678:
	mov	dword [esi+16],ebx
	push	_2679
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2681
	call	_brl_blitz_NullObjectError
_2681:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_2683
	call	_brl_blitz_NullObjectError
_2683:
	push	_bbNullObject
	push	_bb_noComma
	push	dword [ebx+8]
	call	_maxgui_maxgui_SetGadgetFilter
	add	esp,12
	push	_2684
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2686
	call	_brl_blitz_NullObjectError
_2686:
	mov	dword [ebp-8],ebx
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2689
	call	_brl_blitz_NullObjectError
_2689:
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_2691
	call	_brl_blitz_NullObjectError
_2691:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2693
	call	_brl_blitz_NullObjectError
_2693:
	mov	esi,dword [esi+8]
	cmp	esi,_bbNullObject
	jne	_2695
	call	_brl_blitz_NullObjectError
_2695:
	push	0
	push	dword [esi+8]
	push	20
	push	150
	push	10
	push	200
	push	dword [edi+28]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,8
	push	eax
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+20]
	dec	dword [eax+4]
	jnz	_2699
	push	eax
	call	_bbGCFree
	add	esp,4
_2699:
	mov	eax,dword [ebp-8]
	mov	dword [eax+20],ebx
	push	_2700
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2702
	call	_brl_blitz_NullObjectError
_2702:
	mov	esi,dword [ebx+12]
	cmp	esi,_bbNullObject
	jne	_2704
	call	_brl_blitz_NullObjectError
_2704:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2706
	call	_brl_blitz_NullObjectError
_2706:
	push	dword [ebx+24]
	push	dword [esi+8]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_2707
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2709
	call	_brl_blitz_NullObjectError
_2709:
	mov	esi,dword [ebx+16]
	cmp	esi,_bbNullObject
	jne	_2711
	call	_brl_blitz_NullObjectError
_2711:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2713
	call	_brl_blitz_NullObjectError
_2713:
	push	dword [ebx+32]
	push	dword [esi+8]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	mov	ebx,0
	jmp	_656
_656:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note__typeByInt:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_2726
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2715
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	cmp	eax,0
	je	_2718
	cmp	eax,1
	je	_2719
	push	ebp
	push	_2721
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2720
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_53
	call	dword [_bbOnDebugLeaveScope]
	jmp	_660
_2718:
	push	ebp
	push	_2723
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2722
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_52
	call	dword [_bbOnDebugLeaveScope]
	jmp	_660
_2719:
	push	ebp
	push	_2725
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2724
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_44
	call	dword [_bbOnDebugLeaveScope]
	jmp	_660
_660:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note__update:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_2742
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2727
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventSource
	mov	ebx,eax
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2732
	call	_brl_blitz_NullObjectError
_2732:
	mov	esi,dword [esi+8]
	cmp	esi,_bbNullObject
	jne	_2734
	call	_brl_blitz_NullObjectError
_2734:
	cmp	ebx,dword [esi+8]
	je	_2730
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2737
	call	_brl_blitz_NullObjectError
_2737:
	mov	esi,dword [esi+12]
	cmp	esi,_bbNullObject
	jne	_2739
	call	_brl_blitz_NullObjectError
_2739:
	cmp	ebx,dword [esi+8]
	je	_2735
	jmp	_2729
_2730:
	push	ebp
	push	_2740
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2729
_2735:
	push	ebp
	push	_2741
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2729
_2729:
	mov	ebx,0
	jmp	_663
_663:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note__save:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_2756
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2743
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],0
	push	_2745
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_quickMode],0
	je	_2746
	push	ebp
	push	_2748
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2747
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2749
_2746:
	push	ebp
	push	_2754
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2750
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_164
	call	_brl_system_Confirm
	add	esp,8
	cmp	eax,0
	je	_2751
	push	ebp
	push	_2753
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2752
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],1
	call	dword [_bbOnDebugLeaveScope]
_2751:
	call	dword [_bbOnDebugLeaveScope]
_2749:
	push	_2755
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	jmp	_666
_666:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Note__hide:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_2763
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2758
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2760
	call	_brl_blitz_NullObjectError
_2760:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_2762
	call	_brl_blitz_NullObjectError
_2762:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	mov	ebx,0
	jmp	_669
_669:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_noComma:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_2788
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2764
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2766
	call	_brl_blitz_NullObjectError
_2766:
	cmp	dword [ebx+8],515
	jne	_2767
	push	ebp
	push	_2786
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2768
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2770
	call	_brl_blitz_NullObjectError
_2770:
	mov	eax,dword [ebx+16]
	cmp	eax,44
	je	_2773
	cmp	eax,59
	je	_2774
	cmp	eax,13
	je	_2775
	jmp	_2772
_2773:
	push	ebp
	push	_2778
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2776
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	_165
	call	_bb_dlog
	add	esp,8
	push	_2777
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_673
_2774:
	push	ebp
	push	_2781
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2779
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	_166
	call	_bb_dlog
	add	esp,8
	push	_2780
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_673
_2775:
	push	ebp
	push	_2785
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2782
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	_167
	call	_bb_dlog
	add	esp,8
	push	_2783
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	_168
	call	_brl_system_Notify
	add	esp,8
	push	_2784
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_673
_2772:
	call	dword [_bbOnDebugLeaveScope]
_2767:
	push	_2787
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	jmp	_673
_673:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_readNote:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_2811
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2794
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2796
	call	_brl_blitz_NullObjectError
_2796:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_2798
	call	_brl_blitz_NullObjectError
_2798:
	cmp	dword [ebx+8],_bbNullObject
	je	_2799
	push	ebp
	push	_2805
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2800
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2802
	call	_brl_blitz_NullObjectError
_2802:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_2804
	call	_brl_blitz_NullObjectError
_2804:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_2806
_2799:
	push	ebp
	push	_2810
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2807
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2809
	call	_brl_blitz_NullObjectError
_2809:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_2806:
	mov	ebx,0
	jmp	_676
_676:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newNote:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	dword [ebp-4],_bbNullObject
	push	ebp
	push	_2817
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2814
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_Note
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-4],eax
	push	_2816
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	jmp	_678
_678:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_2841
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_Student
	push	_bb_Window
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	dword [edx+8],eax
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],0
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],edx
	mov	eax,dword [ebp-4]
	mov	dword [eax+24],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+28],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+32],0
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+36],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+40],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+44],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+48],edx
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	dword [edx+52],eax
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+56],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+60],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+64],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+68],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+72],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+76],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+80],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+84],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+88],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+92],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+96],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+100],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+104],edx
	push	ebp
	push	_2840
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_681
_681:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_684:
	mov	eax,dword [ebx+104]
	dec	dword [eax+4]
	jnz	_2844
	push	eax
	call	_bbGCFree
	add	esp,4
_2844:
	mov	eax,dword [ebx+100]
	dec	dword [eax+4]
	jnz	_2846
	push	eax
	call	_bbGCFree
	add	esp,4
_2846:
	mov	eax,dword [ebx+96]
	dec	dword [eax+4]
	jnz	_2848
	push	eax
	call	_bbGCFree
	add	esp,4
_2848:
	mov	eax,dword [ebx+92]
	dec	dword [eax+4]
	jnz	_2850
	push	eax
	call	_bbGCFree
	add	esp,4
_2850:
	mov	eax,dword [ebx+88]
	dec	dword [eax+4]
	jnz	_2852
	push	eax
	call	_bbGCFree
	add	esp,4
_2852:
	mov	eax,dword [ebx+84]
	dec	dword [eax+4]
	jnz	_2854
	push	eax
	call	_bbGCFree
	add	esp,4
_2854:
	mov	eax,dword [ebx+80]
	dec	dword [eax+4]
	jnz	_2856
	push	eax
	call	_bbGCFree
	add	esp,4
_2856:
	mov	eax,dword [ebx+76]
	dec	dword [eax+4]
	jnz	_2858
	push	eax
	call	_bbGCFree
	add	esp,4
_2858:
	mov	eax,dword [ebx+72]
	dec	dword [eax+4]
	jnz	_2860
	push	eax
	call	_bbGCFree
	add	esp,4
_2860:
	mov	eax,dword [ebx+68]
	dec	dword [eax+4]
	jnz	_2862
	push	eax
	call	_bbGCFree
	add	esp,4
_2862:
	mov	eax,dword [ebx+64]
	dec	dword [eax+4]
	jnz	_2864
	push	eax
	call	_bbGCFree
	add	esp,4
_2864:
	mov	eax,dword [ebx+60]
	dec	dword [eax+4]
	jnz	_2866
	push	eax
	call	_bbGCFree
	add	esp,4
_2866:
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_2868
	push	eax
	call	_bbGCFree
	add	esp,4
_2868:
	mov	eax,dword [ebx+52]
	dec	dword [eax+4]
	jnz	_2870
	push	eax
	call	_bbGCFree
	add	esp,4
_2870:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_2872
	push	eax
	call	_bbGCFree
	add	esp,4
_2872:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_2874
	push	eax
	call	_bbGCFree
	add	esp,4
_2874:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_2876
	push	eax
	call	_bbGCFree
	add	esp,4
_2876:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_2878
	push	eax
	call	_bbGCFree
	add	esp,4
_2878:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_2880
	push	eax
	call	_bbGCFree
	add	esp,4
_2880:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_2882
	push	eax
	call	_bbGCFree
	add	esp,4
_2882:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_2884
	push	eax
	call	_bbGCFree
	add	esp,4
_2884:
	mov	eax,0
	jmp	_2842
_2842:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__init:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbNullObject
	mov	dword [ebp-16],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_3029
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_2885
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2887
	call	_brl_blitz_NullObjectError
_2887:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_2889
	call	_brl_blitz_NullObjectError
_2889:
	push	21
	push	_170
	push	300
	push	400
	push	400
	push	10
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,28
	push	_2890
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	dword [_bb_studentList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_2891
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2893
	call	_brl_blitz_NullObjectError
_2893:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2896
	call	_brl_blitz_NullObjectError
_2896:
	push	0
	push	dword [ebx+8]
	push	170
	push	375
	push	75
	push	10
	call	_bb_newListBox
	add	esp,24
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+36]
	dec	dword [eax+4]
	jnz	_2900
	push	eax
	call	_bbGCFree
	add	esp,4
_2900:
	mov	dword [esi+36],ebx
	push	_2901
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2903
	call	_brl_blitz_NullObjectError
_2903:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_2905
	call	_brl_blitz_NullObjectError
_2905:
	push	0
	push	0
	push	dword [ebx+20]
	push	0
	push	_41
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	dword [ebp-8],eax
	push	_2907
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2909
	call	_brl_blitz_NullObjectError
_2909:
	push	0
	push	0
	push	dword [ebp-8]
	push	0
	push	_171
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+60]
	dec	dword [eax+4]
	jnz	_2914
	push	eax
	call	_bbGCFree
	add	esp,4
_2914:
	mov	dword [esi+60],ebx
	push	_2915
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2917
	call	_brl_blitz_NullObjectError
_2917:
	push	0
	push	0
	push	dword [ebp-8]
	push	0
	push	_172
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+64]
	dec	dword [eax+4]
	jnz	_2922
	push	eax
	call	_bbGCFree
	add	esp,4
_2922:
	mov	dword [esi+64],ebx
	push	_2923
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2925
	call	_brl_blitz_NullObjectError
_2925:
	push	0
	push	0
	push	dword [ebp-8]
	push	0
	push	_173
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+68]
	dec	dword [eax+4]
	jnz	_2930
	push	eax
	call	_bbGCFree
	add	esp,4
_2930:
	mov	dword [esi+68],ebx
	push	_2931
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_2933
	call	_brl_blitz_NullObjectError
_2933:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_2935
	call	_brl_blitz_NullObjectError
_2935:
	push	0
	push	0
	push	dword [ebx+20]
	push	0
	push	_44
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	dword [ebp-12],eax
	push	_2937
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2939
	call	_brl_blitz_NullObjectError
_2939:
	push	0
	push	0
	push	dword [ebp-12]
	push	0
	push	_174
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+72]
	dec	dword [eax+4]
	jnz	_2944
	push	eax
	call	_bbGCFree
	add	esp,4
_2944:
	mov	dword [esi+72],ebx
	push	_2945
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	dword [ebp-12]
	push	0
	push	_1
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	push	_2946
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	dword [ebp-12]
	push	0
	push	_175
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	dword [ebp-16],eax
	push	_2948
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2950
	call	_brl_blitz_NullObjectError
_2950:
	push	0
	push	0
	push	dword [ebp-16]
	push	0
	push	_2954
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+76]
	dec	dword [eax+4]
	jnz	_2958
	push	eax
	call	_bbGCFree
	add	esp,4
_2958:
	mov	dword [esi+76],ebx
	push	_2959
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2961
	call	_brl_blitz_NullObjectError
_2961:
	push	0
	push	0
	push	dword [ebp-16]
	push	0
	push	_2964
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+80]
	dec	dword [eax+4]
	jnz	_2968
	push	eax
	call	_bbGCFree
	add	esp,4
_2968:
	mov	dword [esi+80],ebx
	push	_2969
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2971
	call	_brl_blitz_NullObjectError
_2971:
	push	0
	push	0
	push	dword [ebp-16]
	push	0
	push	_2975
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+84]
	dec	dword [eax+4]
	jnz	_2979
	push	eax
	call	_bbGCFree
	add	esp,4
_2979:
	mov	dword [esi+84],ebx
	push	_2980
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2982
	call	_brl_blitz_NullObjectError
_2982:
	push	0
	push	0
	push	dword [ebp-16]
	push	0
	push	_180
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+88]
	dec	dword [eax+4]
	jnz	_2987
	push	eax
	call	_bbGCFree
	add	esp,4
_2987:
	mov	dword [esi+88],ebx
	push	_2988
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2990
	call	_brl_blitz_NullObjectError
_2990:
	push	0
	push	0
	push	dword [ebp-12]
	push	0
	push	_181
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+92]
	dec	dword [eax+4]
	jnz	_2995
	push	eax
	call	_bbGCFree
	add	esp,4
_2995:
	mov	dword [esi+92],ebx
	push	_2996
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_2998
	call	_brl_blitz_NullObjectError
_2998:
	push	0
	push	0
	push	dword [ebp-12]
	push	0
	push	_182
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+96]
	dec	dword [eax+4]
	jnz	_3003
	push	eax
	call	_bbGCFree
	add	esp,4
_3003:
	mov	dword [esi+96],ebx
	push	_3004
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3006
	call	_brl_blitz_NullObjectError
_3006:
	push	0
	push	0
	push	dword [ebp-12]
	push	0
	push	_183
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+100]
	dec	dword [eax+4]
	jnz	_3011
	push	eax
	call	_bbGCFree
	add	esp,4
_3011:
	mov	dword [esi+100],ebx
	push	_3012
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3014
	call	_brl_blitz_NullObjectError
_3014:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3017
	call	_brl_blitz_NullObjectError
_3017:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3019
	call	_brl_blitz_NullObjectError
_3019:
	push	0
	push	0
	push	dword [ebx+20]
	push	0
	push	_184
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+104]
	dec	dword [eax+4]
	jnz	_3023
	push	eax
	call	_bbGCFree
	add	esp,4
_3023:
	mov	dword [esi+104],ebx
	push	_3024
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3026
	call	_brl_blitz_NullObjectError
_3026:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3028
	call	_brl_blitz_NullObjectError
_3028:
	push	dword [ebx+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	mov	ebx,0
	jmp	_687
_687:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__show:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_3037
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3032
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3034
	call	_brl_blitz_NullObjectError
_3034:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3036
	call	_brl_blitz_NullObjectError
_3036:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	mov	ebx,0
	jmp	_690
_690:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__hide:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_3093
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3038
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_quickMode],0
	je	_3039
	mov	eax,ebp
	push	eax
	push	_3043
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3040
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3042
	call	_brl_blitz_NullObjectError
_3042:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+64]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3044
_3039:
	mov	eax,ebp
	push	eax
	push	_3051
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3045
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_69
	call	_brl_system_Confirm
	add	esp,8
	cmp	eax,0
	je	_3046
	mov	eax,ebp
	push	eax
	push	_3050
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3047
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3049
	call	_brl_blitz_NullObjectError
_3049:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+64]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_3046:
	call	dword [_bbOnDebugLeaveScope]
_3044:
	push	_3052
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3054
	call	_brl_blitz_NullObjectError
_3054:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3056
	call	_brl_blitz_NullObjectError
_3056:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	push	_3057
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbNullObject
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3060
	call	_brl_blitz_NullObjectError
_3060:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3062
	call	_brl_blitz_NullObjectError
_3062:
	mov	esi,dword [ebx+16]
	cmp	esi,_bbNullObject
	jne	_3065
	call	_brl_blitz_NullObjectError
_3065:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_185
_187:
	cmp	ebx,_bbNullObject
	jne	_3070
	call	_brl_blitz_NullObjectError
_3070:
	push	_maxgui_maxgui_TGadget
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	je	_185
	mov	eax,ebp
	push	eax
	push	_3073
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3071
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	push	_3072
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_185:
	cmp	ebx,_bbNullObject
	jne	_3068
	call	_brl_blitz_NullObjectError
_3068:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_187
_186:
	push	_3074
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3076
	call	_brl_blitz_NullObjectError
_3076:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3078
	call	_brl_blitz_NullObjectError
_3078:
	push	dword [ebx+8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	push	_3079
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3081
	call	_brl_blitz_NullObjectError
_3081:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3083
	call	_brl_blitz_NullObjectError
_3083:
	push	dword [ebx+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	push	_3084
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3086
	call	_brl_blitz_NullObjectError
_3086:
	mov	ebx,_bbNullObject
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_3091
	push	eax
	call	_bbGCFree
	add	esp,4
_3091:
	mov	dword [esi+8],ebx
	push	_3092
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	dword [_bb_studentList]
	call	_brl_linkedlist_ListRemove
	add	esp,8
	mov	ebx,0
	jmp	_693
_693:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__build:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbEmptyString
	mov	dword [ebp-12],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_3173
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3094
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbEmptyString
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3097
	call	_brl_blitz_NullObjectError
_3097:
	mov	eax,dword [ebx+52]
	mov	dword [ebp-16],eax
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_3100
	call	_brl_blitz_NullObjectError
_3100:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_188
_190:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_3105
	call	_brl_blitz_NullObjectError
_3105:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	je	_188
	mov	eax,ebp
	push	eax
	push	_3113
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3106
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_Note
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-12],eax
	push	_3108
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3110
	call	_brl_blitz_NullObjectError
_3110:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_3112
	call	_brl_blitz_NullObjectError
_3112:
	push	dword [ebx+24]
	push	dword [esi+52]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_188:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_3103
	call	_brl_blitz_NullObjectError
_3103:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_190
_189:
	push	_3114
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3116
	call	_brl_blitz_NullObjectError
_3116:
	mov	dword [ebp-20],ebx
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_3119
	call	_brl_blitz_NullObjectError
_3119:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3121
	call	_brl_blitz_NullObjectError
_3121:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3123
	call	_brl_blitz_NullObjectError
_3123:
	mov	esi,dword [esi+8]
	cmp	esi,_bbNullObject
	jne	_3125
	call	_brl_blitz_NullObjectError
_3125:
	push	0
	push	dword [esi+8]
	push	20
	push	150
	push	10
	push	10
	push	dword [ebx+20]
	push	_32
	push	dword [edi+16]
	push	_191
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [ebp-20]
	mov	eax,dword [eax+40]
	dec	dword [eax+4]
	jnz	_3129
	push	eax
	call	_bbGCFree
	add	esp,4
_3129:
	mov	eax,dword [ebp-20]
	mov	dword [eax+40],ebx
	push	_3130
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3132
	call	_brl_blitz_NullObjectError
_3132:
	mov	edi,ebx
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3135
	call	_brl_blitz_NullObjectError
_3135:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3137
	call	_brl_blitz_NullObjectError
_3137:
	mov	esi,dword [esi+8]
	cmp	esi,_bbNullObject
	jne	_3139
	call	_brl_blitz_NullObjectError
_3139:
	push	0
	push	dword [esi+8]
	push	20
	push	150
	push	10
	push	200
	push	dword [ebx+24]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_192
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+44]
	dec	dword [eax+4]
	jnz	_3143
	push	eax
	call	_bbGCFree
	add	esp,4
_3143:
	mov	dword [edi+44],ebx
	push	_3144
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3146
	call	_brl_blitz_NullObjectError
_3146:
	mov	edi,ebx
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3149
	call	_brl_blitz_NullObjectError
_3149:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3151
	call	_brl_blitz_NullObjectError
_3151:
	mov	esi,dword [esi+8]
	cmp	esi,_bbNullObject
	jne	_3153
	call	_brl_blitz_NullObjectError
_3153:
	push	0
	push	dword [esi+8]
	push	20
	push	150
	push	35
	push	10
	push	dword [ebx+32]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_193
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_maxgui_maxgui_CreateLabel
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+48]
	dec	dword [eax+4]
	jnz	_3157
	push	eax
	call	_bbGCFree
	add	esp,4
_3157:
	mov	dword [edi+48],ebx
	push	_3158
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3160
	call	_brl_blitz_NullObjectError
_3160:
	cmp	dword [ebx+56],_bbNullObject
	jne	_3161
	mov	eax,ebp
	push	eax
	push	_3162
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3163
_3161:
	mov	eax,ebp
	push	eax
	push	_3167
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3164
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3166
	call	_brl_blitz_NullObjectError
_3166:
	push	_194
	push	dword [ebx+72]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_3163:
	push	_3168
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3170
	call	_brl_blitz_NullObjectError
_3170:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3172
	call	_brl_blitz_NullObjectError
_3172:
	push	dword [ebx+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	mov	ebx,0
	jmp	_696
_696:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__save:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_3175
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3174
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_195
	call	_bb_dlog
	add	esp,8
	mov	ebx,0
	jmp	_699
_699:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__addNote:
	push	ebp
	mov	ebp,esp
	sub	esp,32
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],_bbNullObject
	mov	dword [ebp-16],0
	mov	dword [ebp-20],_bbNullObject
	mov	dword [ebp-24],_bbEmptyString
	mov	eax,ebp
	push	eax
	push	_3248
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3176
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_bb_convertFromSSV
	add	esp,4
	mov	dword [ebp-12],eax
	push	_3178
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],1
	push	_3180
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],_bbNullObject
	push	_3182
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],_bbEmptyString
	mov	eax,dword [ebp-12]
	mov	dword [ebp-32],eax
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_3186
	call	_brl_blitz_NullObjectError
_3186:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-28],eax
	jmp	_196
_198:
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_3191
	call	_brl_blitz_NullObjectError
_3191:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	je	_196
	mov	eax,ebp
	push	eax
	push	_3246
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3192
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],1
	jne	_3193
	mov	eax,ebp
	push	eax
	push	_3206
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3194
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_newNote
	mov	dword [ebp-20],eax
	push	_3195
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3197
	call	_brl_blitz_NullObjectError
_3197:
	push	dword [ebp-20]
	push	dword [ebx+52]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_3198
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_3200
	call	_brl_blitz_NullObjectError
_3200:
	mov	ebx,dword [ebp-24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_3205
	push	eax
	call	_bbGCFree
	add	esp,4
_3205:
	mov	dword [esi+24],ebx
	call	dword [_bbOnDebugLeaveScope]
_3193:
	push	_3207
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],2
	jne	_3208
	mov	eax,ebp
	push	eax
	push	_3226
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3209
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_3211
	call	_brl_blitz_NullObjectError
_3211:
	mov	ebx,dword [ebp-24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+36]
	dec	dword [eax+4]
	jnz	_3216
	push	eax
	call	_bbGCFree
	add	esp,4
_3216:
	mov	dword [esi+36],ebx
	push	_3217
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3219
	call	_brl_blitz_NullObjectError
_3219:
	mov	edi,dword [ebx+36]
	cmp	edi,_bbNullObject
	jne	_3221
	call	_brl_blitz_NullObjectError
_3221:
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_3223
	call	_brl_blitz_NullObjectError
_3223:
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_3225
	call	_brl_blitz_NullObjectError
_3225:
	push	1
	push	dword [ebx+24]
	push	_32
	push	dword [esi+36]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	edi
	mov	eax,dword [edi]
	call	dword [eax+52]
	add	esp,12
	call	dword [_bbOnDebugLeaveScope]
_3208:
	push	_3227
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],3
	jne	_3228
	mov	eax,ebp
	push	eax
	push	_3233
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3229
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_3231
	call	_brl_blitz_NullObjectError
_3231:
	push	dword [ebp-24]
	call	_bbStringToInt
	add	esp,4
	mov	dword [ebx+28],eax
	call	dword [_bbOnDebugLeaveScope]
_3228:
	push	_3234
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],4
	jne	_3235
	mov	eax,ebp
	push	eax
	push	_3244
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3236
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_3238
	call	_brl_blitz_NullObjectError
_3238:
	mov	ebx,dword [ebp-24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+32]
	dec	dword [eax+4]
	jnz	_3243
	push	eax
	call	_bbGCFree
	add	esp,4
_3243:
	mov	dword [esi+32],ebx
	call	dword [_bbOnDebugLeaveScope]
_3235:
	push	_3245
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-16],1
	call	dword [_bbOnDebugLeaveScope]
_196:
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_3189
	call	_brl_blitz_NullObjectError
_3189:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_198
_197:
	push	_3247
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	jmp	_703
_703:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__update:
	push	ebp
	mov	ebp,esp
	sub	esp,40
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],0
	mov	dword [ebp-20],_bbEmptyString
	mov	dword [ebp-24],0
	mov	dword [ebp-28],_bbEmptyString
	mov	dword [ebp-32],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_3571
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3249
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventSource
	mov	ebx,eax
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3254
	call	_brl_blitz_NullObjectError
_3254:
	cmp	ebx,dword [esi+72]
	je	_3252
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3257
	call	_brl_blitz_NullObjectError
_3257:
	cmp	ebx,dword [esi+68]
	je	_3255
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3260
	call	_brl_blitz_NullObjectError
_3260:
	cmp	ebx,dword [esi+64]
	je	_3258
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3263
	call	_brl_blitz_NullObjectError
_3263:
	cmp	ebx,dword [esi+60]
	je	_3261
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3266
	call	_brl_blitz_NullObjectError
_3266:
	mov	esi,dword [esi+36]
	cmp	esi,_bbNullObject
	jne	_3268
	call	_brl_blitz_NullObjectError
_3268:
	cmp	ebx,dword [esi+8]
	je	_3264
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3271
	call	_brl_blitz_NullObjectError
_3271:
	mov	esi,dword [esi+8]
	cmp	esi,_bbNullObject
	jne	_3273
	call	_brl_blitz_NullObjectError
_3273:
	cmp	ebx,dword [esi+8]
	je	_3269
	jmp	_3251
_3252:
	mov	eax,ebp
	push	eax
	push	_3364
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3274
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3276
	call	_brl_blitz_NullObjectError
_3276:
	cmp	dword [ebx+56],_bbNullObject
	jne	_3277
	mov	eax,ebp
	push	eax
	push	_3360
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3278
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3280
	call	_brl_blitz_NullObjectError
_3280:
	push	0
	push	0
	push	4
	push	_199
	call	_bb_prompt
	add	esp,16
	push	eax
	call	_bb_findByBC
	add	esp,4
	mov	esi,eax
	inc	dword [esi+4]
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_3285
	push	eax
	call	_bbGCFree
	add	esp,4
_3285:
	mov	dword [ebx+56],esi
	push	_3286
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3288
	call	_brl_blitz_NullObjectError
_3288:
	mov	ebx,dword [ebx+56]
	cmp	ebx,_bbNullObject
	jne	_3290
	call	_brl_blitz_NullObjectError
_3290:
	push	_61
	push	dword [ebx+24]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_3291
	mov	eax,ebp
	push	eax
	push	_3356
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3292
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3294
	call	_brl_blitz_NullObjectError
_3294:
	mov	ebx,dword [ebx+56]
	cmp	ebx,_bbNullObject
	jne	_3296
	call	_brl_blitz_NullObjectError
_3296:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3298
	call	_brl_blitz_NullObjectError
_3298:
	mov	esi,dword [esi+56]
	cmp	esi,_bbNullObject
	jne	_3300
	call	_brl_blitz_NullObjectError
_3300:
	push	_90
	push	dword [esi+24]
	push	_108
	call	_brl_system_CurrentTime
	push	eax
	push	_107
	call	_brl_system_CurrentDate
	push	eax
	push	_106
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_3301
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3303
	call	_brl_blitz_NullObjectError
_3303:
	mov	esi,dword [ebx+56]
	cmp	esi,_bbNullObject
	jne	_3305
	call	_brl_blitz_NullObjectError
_3305:
	mov	ebx,_62
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_3310
	push	eax
	call	_bbGCFree
	add	esp,4
_3310:
	mov	dword [esi+24],ebx
	push	_3311
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3313
	call	_brl_blitz_NullObjectError
_3313:
	mov	ebx,dword [ebx+56]
	cmp	ebx,_bbNullObject
	jne	_3315
	call	_brl_blitz_NullObjectError
_3315:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3317
	call	_brl_blitz_NullObjectError
_3317:
	mov	esi,dword [esi+56]
	cmp	esi,_bbNullObject
	jne	_3319
	call	_brl_blitz_NullObjectError
_3319:
	push	dword [esi+24]
	push	_59
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebx+108]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_3320
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3322
	call	_brl_blitz_NullObjectError
_3322:
	mov	ebx,dword [ebx+56]
	cmp	ebx,_bbNullObject
	jne	_3324
	call	_brl_blitz_NullObjectError
_3324:
	push	_63
	push	dword [ebx+60]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_3325
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3327
	call	_brl_blitz_NullObjectError
_3327:
	mov	esi,dword [ebx+56]
	cmp	esi,_bbNullObject
	jne	_3329
	call	_brl_blitz_NullObjectError
_3329:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3332
	call	_brl_blitz_NullObjectError
_3332:
	mov	ebx,dword [ebx+56]
	cmp	ebx,_bbNullObject
	jne	_3334
	call	_brl_blitz_NullObjectError
_3334:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3336
	call	_brl_blitz_NullObjectError
_3336:
	push	0
	push	0
	push	dword [ebx+20]
	push	0
	push	_64
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+36]
	dec	dword [eax+4]
	jnz	_3340
	push	eax
	call	_bbGCFree
	add	esp,4
_3340:
	mov	dword [esi+36],ebx
	push	_3341
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3343
	call	_brl_blitz_NullObjectError
_3343:
	push	_194
	push	dword [ebx+72]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	push	_3344
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3346
	call	_brl_blitz_NullObjectError
_3346:
	mov	ebx,dword [ebx+56]
	cmp	ebx,_bbNullObject
	jne	_3348
	call	_brl_blitz_NullObjectError
_3348:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3350
	call	_brl_blitz_NullObjectError
_3350:
	push	dword [ebx+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	push	_3351
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3353
	call	_brl_blitz_NullObjectError
_3353:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3355
	call	_brl_blitz_NullObjectError
_3355:
	push	dword [ebx+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3357
_3291:
	mov	eax,ebp
	push	eax
	push	_3359
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3358
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_200
	call	_brl_system_Notify
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_3357:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3361
_3277:
	mov	eax,ebp
	push	eax
	push	_3363
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3362
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_201
	call	_bb_dlog
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_3361:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3251
_3255:
	mov	eax,ebp
	push	eax
	push	_3397
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3365
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],0
	push	_3367
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_quickMode],0
	je	_3368
	mov	eax,ebp
	push	eax
	push	_3370
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3369
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3371
_3368:
	mov	eax,ebp
	push	eax
	push	_3373
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3372
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_202
	call	_brl_system_Confirm
	add	esp,8
	mov	dword [ebp-8],eax
	call	dword [_bbOnDebugLeaveScope]
_3371:
	push	_3374
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_3375
	mov	eax,ebp
	push	eax
	push	_3395
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3376
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	1
	push	_203
	call	_bb_prompt
	add	esp,16
	mov	dword [ebp-12],eax
	push	_3378
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_3379
	mov	eax,ebp
	push	eax
	push	_3394
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3380
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3382
	call	_brl_blitz_NullObjectError
_3382:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3384
	call	_brl_blitz_NullObjectError
_3384:
	push	_206
	push	dword [ebp-12]
	push	_89
	push	dword [esi+32]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_205
	call	_brl_system_CurrentDate
	push	eax
	push	_204
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_3385
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3387
	call	_brl_blitz_NullObjectError
_3387:
	push	dword [ebp-12]
	call	_bbStringToInt
	add	esp,4
	mov	dword [ebx+32],eax
	push	_3389
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3391
	call	_brl_blitz_NullObjectError
_3391:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3393
	call	_brl_blitz_NullObjectError
_3393:
	push	dword [esi+32]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_193
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebx+48]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_3379:
	call	dword [_bbOnDebugLeaveScope]
_3375:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3251
_3258:
	mov	eax,ebp
	push	eax
	push	_3436
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3398
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	push	_3400
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_quickMode],0
	je	_3401
	mov	eax,ebp
	push	eax
	push	_3403
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3402
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3404
_3401:
	mov	eax,ebp
	push	eax
	push	_3406
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3405
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_207
	call	_brl_system_Confirm
	add	esp,8
	mov	dword [ebp-16],eax
	call	dword [_bbOnDebugLeaveScope]
_3404:
	push	_3407
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],0
	je	_3408
	mov	eax,ebp
	push	eax
	push	_3434
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3409
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	0
	push	_208
	call	_bb_prompt
	add	esp,16
	mov	dword [ebp-20],eax
	push	_3411
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	dword [ebp-20]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_3412
	mov	eax,ebp
	push	eax
	push	_3433
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3413
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3415
	call	_brl_blitz_NullObjectError
_3415:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3417
	call	_brl_blitz_NullObjectError
_3417:
	push	_206
	push	dword [ebp-20]
	push	_89
	push	dword [esi+20]
	push	_210
	call	_brl_system_CurrentDate
	push	eax
	push	_209
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_3418
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3420
	call	_brl_blitz_NullObjectError
_3420:
	mov	ebx,dword [ebp-20]
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_3425
	push	eax
	call	_bbGCFree
	add	esp,4
_3425:
	mov	dword [esi+20],ebx
	push	_3426
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_3428
	call	_brl_blitz_NullObjectError
_3428:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3430
	call	_brl_blitz_NullObjectError
_3430:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3432
	call	_brl_blitz_NullObjectError
_3432:
	push	dword [ebx+20]
	push	_32
	push	dword [esi+16]
	push	_191
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [edi+40]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_3412:
	call	dword [_bbOnDebugLeaveScope]
_3408:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3251
_3261:
	mov	eax,ebp
	push	eax
	push	_3475
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3437
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],0
	push	_3439
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_quickMode],0
	je	_3440
	mov	eax,ebp
	push	eax
	push	_3442
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3441
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3443
_3440:
	mov	eax,ebp
	push	eax
	push	_3445
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3444
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_211
	call	_brl_system_Confirm
	add	esp,8
	mov	dword [ebp-24],eax
	call	dword [_bbOnDebugLeaveScope]
_3443:
	push	_3446
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-24],0
	je	_3447
	mov	eax,ebp
	push	eax
	push	_3473
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3448
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	0
	push	_212
	call	_bb_prompt
	add	esp,16
	mov	dword [ebp-28],eax
	push	_3450
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	dword [ebp-28]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_3451
	mov	eax,ebp
	push	eax
	push	_3472
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3452
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3454
	call	_brl_blitz_NullObjectError
_3454:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3456
	call	_brl_blitz_NullObjectError
_3456:
	push	_206
	push	dword [ebp-28]
	push	_89
	push	dword [esi+16]
	push	_214
	call	_brl_system_CurrentDate
	push	eax
	push	_213
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_3457
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3459
	call	_brl_blitz_NullObjectError
_3459:
	mov	ebx,dword [ebp-28]
	inc	dword [ebx+4]
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_3464
	push	eax
	call	_bbGCFree
	add	esp,4
_3464:
	mov	dword [esi+16],ebx
	push	_3465
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	edi,dword [ebp-4]
	cmp	edi,_bbNullObject
	jne	_3467
	call	_brl_blitz_NullObjectError
_3467:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3469
	call	_brl_blitz_NullObjectError
_3469:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3471
	call	_brl_blitz_NullObjectError
_3471:
	push	dword [ebx+20]
	push	_32
	push	dword [esi+16]
	push	_191
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [edi+40]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_3451:
	call	dword [_bbOnDebugLeaveScope]
_3447:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3251
_3264:
	mov	eax,ebp
	push	eax
	push	_3490
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3476
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_3479
	cmp	eax,8196
	je	_3480
	jmp	_3478
_3479:
	mov	eax,ebp
	push	eax
	push	_3488
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3481
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3483
	call	_brl_blitz_NullObjectError
_3483:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3485
	call	_brl_blitz_NullObjectError
_3485:
	mov	esi,dword [esi+36]
	cmp	esi,_bbNullObject
	jne	_3487
	call	_brl_blitz_NullObjectError
_3487:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+64]
	add	esp,4
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+76]
	add	esp,8
	push	eax
	call	_bb_readNote
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3478
_3480:
	mov	eax,ebp
	push	eax
	push	_3489
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3478
_3478:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3251
_3269:
	mov	eax,ebp
	push	eax
	push	_3499
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3491
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	je	_3494
	jmp	_3493
_3494:
	mov	eax,ebp
	push	eax
	push	_3498
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3495
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3497
	call	_brl_blitz_NullObjectError
_3497:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3493
_3493:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3251
_3251:
	push	_3500
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-32],_bbNullObject
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3503
	call	_brl_blitz_NullObjectError
_3503:
	mov	eax,dword [ebx+52]
	mov	dword [ebp-36],eax
	mov	ebx,dword [ebp-36]
	cmp	ebx,_bbNullObject
	jne	_3506
	call	_brl_blitz_NullObjectError
_3506:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-40],eax
	jmp	_215
_217:
	mov	ebx,dword [ebp-40]
	cmp	ebx,_bbNullObject
	jne	_3511
	call	_brl_blitz_NullObjectError
_3511:
	push	_bb_Note
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-32],eax
	cmp	dword [ebp-32],_bbNullObject
	je	_215
	mov	eax,ebp
	push	eax
	push	_3570
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3512
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-32],_bbNullObject
	je	_3513
	mov	eax,ebp
	push	eax
	push	_3569
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3514
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventSource
	mov	ebx,eax
	mov	esi,dword [ebp-32]
	cmp	esi,_bbNullObject
	jne	_3519
	call	_brl_blitz_NullObjectError
_3519:
	mov	esi,dword [esi+8]
	cmp	esi,_bbNullObject
	jne	_3521
	call	_brl_blitz_NullObjectError
_3521:
	cmp	ebx,dword [esi+8]
	je	_3517
	jmp	_3516
_3517:
	mov	eax,ebp
	push	eax
	push	_3568
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3522
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	je	_3525
	jmp	_3524
_3525:
	mov	eax,ebp
	push	eax
	push	_3567
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3526
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_3528
	call	_brl_blitz_NullObjectError
_3528:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,4
	cmp	eax,0
	je	_3529
	mov	eax,ebp
	push	eax
	push	_3563
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3530
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-32]
	cmp	esi,_bbNullObject
	jne	_3532
	call	_brl_blitz_NullObjectError
_3532:
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_3535
	call	_brl_blitz_NullObjectError
_3535:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_3537
	call	_brl_blitz_NullObjectError
_3537:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_3541
	push	eax
	call	_bbGCFree
	add	esp,4
_3541:
	mov	dword [esi+24],ebx
	push	_3542
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-32]
	cmp	esi,_bbNullObject
	jne	_3544
	call	_brl_blitz_NullObjectError
_3544:
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_3547
	call	_brl_blitz_NullObjectError
_3547:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_3549
	call	_brl_blitz_NullObjectError
_3549:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+32]
	dec	dword [eax+4]
	jnz	_3553
	push	eax
	call	_bbGCFree
	add	esp,4
_3553:
	mov	dword [esi+32],ebx
	push	_3554
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3556
	call	_brl_blitz_NullObjectError
_3556:
	mov	edi,dword [ebx+36]
	cmp	edi,_bbNullObject
	jne	_3558
	call	_brl_blitz_NullObjectError
_3558:
	mov	esi,dword [ebp-32]
	cmp	esi,_bbNullObject
	jne	_3560
	call	_brl_blitz_NullObjectError
_3560:
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_3562
	call	_brl_blitz_NullObjectError
_3562:
	push	dword [ebx+24]
	push	_32
	push	dword [esi+36]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	edi
	mov	eax,dword [edi]
	call	dword [eax+56]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_3529:
	push	_3564
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-32]
	cmp	ebx,_bbNullObject
	jne	_3566
	call	_brl_blitz_NullObjectError
_3566:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+64]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3524
_3524:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3516
_3516:
	call	dword [_bbOnDebugLeaveScope]
_3513:
	call	dword [_bbOnDebugLeaveScope]
_215:
	mov	ebx,dword [ebp-40]
	cmp	ebx,_bbNullObject
	jne	_3509
	call	_brl_blitz_NullObjectError
_3509:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_217
_216:
	mov	ebx,0
	jmp	_706
_706:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student__noteBySummary:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_3593
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3572
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbNullObject
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3575
	call	_brl_blitz_NullObjectError
_3575:
	mov	eax,dword [ebx+52]
	mov	dword [ebp-16],eax
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_3578
	call	_brl_blitz_NullObjectError
_3578:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_218
_220:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_3583
	call	_brl_blitz_NullObjectError
_3583:
	push	_bb_Note
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	je	_218
	mov	eax,ebp
	push	eax
	push	_3592
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3584
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_3586
	call	_brl_blitz_NullObjectError
_3586:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_3588
	call	_brl_blitz_NullObjectError
_3588:
	push	dword [ebp-8]
	push	dword [ebx+24]
	push	_32
	push	dword [esi+36]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_3589
	mov	eax,ebp
	push	eax
	push	_3591
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3590
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_710
_3589:
	call	dword [_bbOnDebugLeaveScope]
_218:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_3581
	call	_brl_blitz_NullObjectError
_3581:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_220
_219:
	mov	ebx,_bbNullObject
	jmp	_710
_710:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newStudent:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	dword [ebp-4],_bbNullObject
	push	ebp
	push	_3600
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3594
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_Student
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-4],eax
	push	_3596
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3598
	call	_brl_blitz_NullObjectError
_3598:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	push	_3599
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	jmp	_712
_712:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newBlankStudent:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_3640
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3603
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_newStudent
	mov	dword [ebp-12],eax
	push	_3605
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_3607
	call	_brl_blitz_NullObjectError
_3607:
	mov	dword [ebx+12],-1
	push	_3609
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_3611
	call	_brl_blitz_NullObjectError
_3611:
	mov	ebx,dword [ebp-4]
	inc	dword [ebx+4]
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_3616
	push	eax
	call	_bbGCFree
	add	esp,4
_3616:
	mov	dword [esi+16],ebx
	push	_3617
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_3619
	call	_brl_blitz_NullObjectError
_3619:
	mov	ebx,dword [ebp-8]
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_3624
	push	eax
	call	_bbGCFree
	add	esp,4
_3624:
	mov	dword [esi+20],ebx
	push	_3625
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_3627
	call	_brl_blitz_NullObjectError
_3627:
	mov	dword [ebx+24],0
	push	_3629
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_3631
	call	_brl_blitz_NullObjectError
_3631:
	mov	dword [ebx+32],9
	push	_3633
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_3635
	call	_brl_blitz_NullObjectError
_3635:
	push	_119
	call	_brl_system_CurrentTime
	push	eax
	push	_139
	call	_brl_system_CurrentDate
	push	eax
	push	_222
	call	_brl_system_CurrentDate
	push	eax
	push	_221
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_3636
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_3638
	call	_brl_blitz_NullObjectError
_3638:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+60]
	add	esp,4
	push	_3639
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	jmp	_716
_716:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_findByFirst:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	push	ebp
	push	_3647
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3644
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_newStudent
	mov	dword [ebp-8],eax
	push	_3646
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	jmp	_719
_719:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_findByLast:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_3651
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3650
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-4]
	push	_223
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	mov	ebx,_bbNullObject
	jmp	_722
_722:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_3657
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_logWin
	push	_bb_Window
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	dword [edx+8],eax
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],edx
	push	ebp
	push	_3656
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_725
_725:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_728:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_3660
	push	eax
	call	_bbGCFree
	add	esp,4
_3660:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_3662
	push	eax
	call	_bbGCFree
	add	esp,4
_3662:
	mov	eax,0
	jmp	_3658
_3658:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin__init:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_3680
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3663
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3666
	call	_brl_blitz_NullObjectError
_3666:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3668
	call	_brl_blitz_NullObjectError
_3668:
	push	17
	push	_224
	push	180
	push	800
	push	800
	push	240
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,28
	push	_3669
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3671
	call	_brl_blitz_NullObjectError
_3671:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	_3672
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	xor	eax,eax
	cmp	eax,0
	je	_3673
	push	ebp
	push	_3679
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3674
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3676
	call	_brl_blitz_NullObjectError
_3676:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3678
	call	_brl_blitz_NullObjectError
_3678:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_3673:
	mov	ebx,0
	jmp	_731
_731:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin__populate:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_3695
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3681
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3683
	call	_brl_blitz_NullObjectError
_3683:
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3686
	call	_brl_blitz_NullObjectError
_3686:
	push	0
	push	dword [esi+8]
	push	140
	push	775
	push	10
	push	10
	call	_bb_newListBox
	add	esp,24
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_3690
	push	eax
	call	_bbGCFree
	add	esp,4
_3690:
	mov	dword [ebx+12],esi
	push	_3691
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3693
	call	_brl_blitz_NullObjectError
_3693:
	push	0
	push	_3694
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,12
	mov	ebx,0
	jmp	_734
_734:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin__new:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,ebp
	push	eax
	push	_3720
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3696
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	eax,0
	je	_3699
	cmp	eax,1
	je	_3700
	cmp	eax,2
	je	_3701
	jmp	_3698
_3699:
	mov	eax,ebp
	push	eax
	push	_3707
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3702
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3704
	call	_brl_blitz_NullObjectError
_3704:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_3706
	call	_brl_blitz_NullObjectError
_3706:
	push	1
	push	dword [ebp-8]
	push	_226
	call	_brl_system_CurrentTime
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,12
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3698
_3700:
	mov	eax,ebp
	push	eax
	push	_3713
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3708
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3710
	call	_brl_blitz_NullObjectError
_3710:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_3712
	call	_brl_blitz_NullObjectError
_3712:
	push	1
	push	dword [ebp-8]
	push	_227
	call	_brl_system_CurrentTime
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,12
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3698
_3701:
	mov	eax,ebp
	push	eax
	push	_3719
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3714
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3716
	call	_brl_blitz_NullObjectError
_3716:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_3718
	call	_brl_blitz_NullObjectError
_3718:
	push	1
	push	dword [ebp-8]
	push	_228
	call	_brl_system_CurrentTime
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,12
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3698
_3698:
	mov	ebx,0
	jmp	_739
_739:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin__update:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_3740
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3721
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3723
	call	_brl_blitz_NullObjectError
_3723:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3725
	call	_brl_blitz_NullObjectError
_3725:
	call	_brl_eventqueue_EventSource
	cmp	eax,dword [ebx+8]
	jne	_3726
	push	ebp
	push	_3739
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3727
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3729
	call	_brl_blitz_NullObjectError
_3729:
	push	0
	push	_229
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,12
	push	_3730
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	je	_3733
	jmp	_3732
_3733:
	push	ebp
	push	_3737
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3734
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3736
	call	_brl_blitz_NullObjectError
_3736:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+64]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3732
_3732:
	push	_3738
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_742
_3726:
	mov	ebx,0
	jmp	_742
_742:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_logWin__end:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_3782
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3741
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3743
	call	_brl_blitz_NullObjectError
_3743:
	push	0
	push	_230
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,12
	push	_3744
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbNullObject
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3747
	call	_brl_blitz_NullObjectError
_3747:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3749
	call	_brl_blitz_NullObjectError
_3749:
	mov	esi,dword [ebx+16]
	cmp	esi,_bbNullObject
	jne	_3752
	call	_brl_blitz_NullObjectError
_3752:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_231
_233:
	cmp	ebx,_bbNullObject
	jne	_3757
	call	_brl_blitz_NullObjectError
_3757:
	push	_maxgui_maxgui_TGadget
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	je	_231
	mov	eax,ebp
	push	eax
	push	_3760
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3758
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	push	_3759
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_231:
	cmp	ebx,_bbNullObject
	jne	_3755
	call	_brl_blitz_NullObjectError
_3755:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_233
_232:
	push	_3762
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3764
	call	_brl_blitz_NullObjectError
_3764:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3766
	call	_brl_blitz_NullObjectError
_3766:
	push	dword [ebx+8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	push	_3767
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3769
	call	_brl_blitz_NullObjectError
_3769:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3771
	call	_brl_blitz_NullObjectError
_3771:
	push	dword [ebx+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	push	_3772
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3774
	call	_brl_blitz_NullObjectError
_3774:
	mov	ebx,_bbNullObject
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_3779
	push	eax
	call	_bbGCFree
	add	esp,4
_3779:
	mov	dword [esi+8],ebx
	push	_3780
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	dword [_bb_winList]
	call	_brl_linkedlist_ListRemove
	add	esp,8
	push	_3781
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	jmp	_745
_745:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_dlog:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_3786
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3783
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_debugWin]
	cmp	ebx,_bbNullObject
	jne	_3785
	call	_brl_blitz_NullObjectError
_3785:
	push	dword [ebp-8]
	push	dword [ebp-4]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,12
	mov	ebx,0
	jmp	_749
_749:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_MainWindow_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_3800
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_MainWindow
	push	_bb_Window
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	dword [edx+8],eax
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+24],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+28],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+32],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+36],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+40],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+44],edx
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+48],edx
	push	ebp
	push	_3799
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_752
_752:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_MainWindow_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_755:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_3803
	push	eax
	call	_bbGCFree
	add	esp,4
_3803:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_3805
	push	eax
	call	_bbGCFree
	add	esp,4
_3805:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_3807
	push	eax
	call	_bbGCFree
	add	esp,4
_3807:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_3809
	push	eax
	call	_bbGCFree
	add	esp,4
_3809:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_3811
	push	eax
	call	_bbGCFree
	add	esp,4
_3811:
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_3813
	push	eax
	call	_bbGCFree
	add	esp,4
_3813:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_3815
	push	eax
	call	_bbGCFree
	add	esp,4
_3815:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_3817
	push	eax
	call	_bbGCFree
	add	esp,4
_3817:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_3819
	push	eax
	call	_bbGCFree
	add	esp,4
_3819:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_3821
	push	eax
	call	_bbGCFree
	add	esp,4
_3821:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_3823
	push	eax
	call	_bbGCFree
	add	esp,4
_3823:
	mov	eax,0
	jmp	_3801
_3801:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_MainWindow__init:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_3838
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3824
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3827
	call	_brl_blitz_NullObjectError
_3827:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3829
	call	_brl_blitz_NullObjectError
_3829:
	push	589
	push	_295
	push	300
	push	400
	push	0
	push	0
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,28
	push	_3830
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3832
	call	_brl_blitz_NullObjectError
_3832:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	_3833
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3835
	call	_brl_blitz_NullObjectError
_3835:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3837
	call	_brl_blitz_NullObjectError
_3837:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	mov	ebx,0
	jmp	_758
_758:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_MainWindow__populate:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,ebp
	push	eax
	push	_3984
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3839
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3841
	call	_brl_blitz_NullObjectError
_3841:
	mov	edi,ebx
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3844
	call	_brl_blitz_NullObjectError
_3844:
	mov	esi,dword [ebx+8]
	cmp	esi,_bbNullObject
	jne	_3846
	call	_brl_blitz_NullObjectError
_3846:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3848
	call	_brl_blitz_NullObjectError
_3848:
	push	0
	push	dword [ebx+8]
	push	20
	push	150
	push	50
	push	dword [esi+8]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	sub	eax,75
	push	eax
	call	_bb_newTextField
	add	esp,24
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+12]
	dec	dword [eax+4]
	jnz	_3852
	push	eax
	call	_bbGCFree
	add	esp,4
_3852:
	mov	dword [edi+12],ebx
	push	_3853
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3855
	call	_brl_blitz_NullObjectError
_3855:
	mov	edi,ebx
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3858
	call	_brl_blitz_NullObjectError
_3858:
	mov	esi,dword [ebx+8]
	cmp	esi,_bbNullObject
	jne	_3860
	call	_brl_blitz_NullObjectError
_3860:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3862
	call	_brl_blitz_NullObjectError
_3862:
	push	2
	push	dword [ebx+8]
	push	15
	push	75
	push	85
	push	dword [esi+8]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	sub	eax,75
	push	eax
	push	_42
	call	_bb_newButton
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+16]
	dec	dword [eax+4]
	jnz	_3866
	push	eax
	call	_bbGCFree
	add	esp,4
_3866:
	mov	dword [edi+16],ebx
	push	_3867
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3869
	call	_brl_blitz_NullObjectError
_3869:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_3871
	call	_brl_blitz_NullObjectError
_3871:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	_3872
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3874
	call	_brl_blitz_NullObjectError
_3874:
	mov	edi,ebx
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3877
	call	_brl_blitz_NullObjectError
_3877:
	mov	esi,dword [ebx+8]
	cmp	esi,_bbNullObject
	jne	_3879
	call	_brl_blitz_NullObjectError
_3879:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3881
	call	_brl_blitz_NullObjectError
_3881:
	push	2
	push	dword [ebx+8]
	push	15
	push	75
	push	105
	push	dword [esi+8]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	sub	eax,75
	push	eax
	push	_43
	call	_bb_newButton
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+20]
	dec	dword [eax+4]
	jnz	_3885
	push	eax
	call	_bbGCFree
	add	esp,4
_3885:
	mov	dword [edi+20],ebx
	push	_3886
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3888
	call	_brl_blitz_NullObjectError
_3888:
	mov	edi,ebx
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3891
	call	_brl_blitz_NullObjectError
_3891:
	mov	esi,dword [ebx+8]
	cmp	esi,_bbNullObject
	jne	_3893
	call	_brl_blitz_NullObjectError
_3893:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3895
	call	_brl_blitz_NullObjectError
_3895:
	push	2
	push	dword [ebx+8]
	push	15
	push	75
	push	85
	push	dword [esi+8]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	push	eax
	push	_234
	call	_bb_newButton
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+24]
	dec	dword [eax+4]
	jnz	_3899
	push	eax
	call	_bbGCFree
	add	esp,4
_3899:
	mov	dword [edi+24],ebx
	push	_3900
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3902
	call	_brl_blitz_NullObjectError
_3902:
	mov	edi,ebx
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3905
	call	_brl_blitz_NullObjectError
_3905:
	mov	esi,dword [ebx+8]
	cmp	esi,_bbNullObject
	jne	_3907
	call	_brl_blitz_NullObjectError
_3907:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3909
	call	_brl_blitz_NullObjectError
_3909:
	push	2
	push	dword [ebx+8]
	push	15
	push	75
	push	105
	push	dword [esi+8]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	push	eax
	push	_235
	call	_bb_newButton
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+28]
	dec	dword [eax+4]
	jnz	_3913
	push	eax
	call	_bbGCFree
	add	esp,4
_3913:
	mov	dword [edi+28],ebx
	push	_3914
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3916
	call	_brl_blitz_NullObjectError
_3916:
	mov	edi,ebx
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3919
	call	_brl_blitz_NullObjectError
_3919:
	mov	esi,dword [ebx+8]
	cmp	esi,_bbNullObject
	jne	_3921
	call	_brl_blitz_NullObjectError
_3921:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3923
	call	_brl_blitz_NullObjectError
_3923:
	push	8
	push	dword [ebx+8]
	push	40
	push	75
	push	150
	push	dword [esi+8]
	call	_maxgui_maxgui_GadgetWidth
	add	esp,4
	cdq
	and	edx,1
	add	eax,edx
	sar	eax,1
	sub	eax,37
	push	eax
	push	_236
	call	_bb_newButton
	add	esp,28
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+32]
	dec	dword [eax+4]
	jnz	_3927
	push	eax
	call	_bbGCFree
	add	esp,4
_3927:
	mov	dword [edi+32],ebx
	push	_3928
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3930
	call	_brl_blitz_NullObjectError
_3930:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3933
	call	_brl_blitz_NullObjectError
_3933:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3935
	call	_brl_blitz_NullObjectError
_3935:
	push	0
	push	0
	push	dword [ebx+20]
	push	0
	push	_237
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+36]
	dec	dword [eax+4]
	jnz	_3939
	push	eax
	call	_bbGCFree
	add	esp,4
_3939:
	mov	dword [esi+36],ebx
	push	_3940
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3942
	call	_brl_blitz_NullObjectError
_3942:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3945
	call	_brl_blitz_NullObjectError
_3945:
	push	0
	push	0
	push	dword [ebx+36]
	push	0
	push	_238
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+48]
	dec	dword [eax+4]
	jnz	_3949
	push	eax
	call	_bbGCFree
	add	esp,4
_3949:
	mov	dword [esi+48],ebx
	push	_3950
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3952
	call	_brl_blitz_NullObjectError
_3952:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3955
	call	_brl_blitz_NullObjectError
_3955:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3957
	call	_brl_blitz_NullObjectError
_3957:
	push	0
	push	0
	push	dword [ebx+20]
	push	0
	push	_239
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+44]
	dec	dword [eax+4]
	jnz	_3961
	push	eax
	call	_bbGCFree
	add	esp,4
_3961:
	mov	dword [esi+44],ebx
	push	_3962
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3964
	call	_brl_blitz_NullObjectError
_3964:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3967
	call	_brl_blitz_NullObjectError
_3967:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3969
	call	_brl_blitz_NullObjectError
_3969:
	push	0
	push	0
	push	dword [ebx+20]
	push	0
	push	_240
	call	_maxgui_maxgui_CreateMenu
	add	esp,20
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [esi+40]
	dec	dword [eax+4]
	jnz	_3973
	push	eax
	call	_bbGCFree
	add	esp,4
_3973:
	mov	dword [esi+40],ebx
	push	_3974
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3976
	call	_brl_blitz_NullObjectError
_3976:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_3978
	call	_brl_blitz_NullObjectError
_3978:
	push	dword [ebx+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	push	_3979
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_3981
	call	_brl_blitz_NullObjectError
_3981:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_3983
	call	_brl_blitz_NullObjectError
_3983:
	push	dword [ebx+8]
	call	_maxgui_maxgui_ActivateGadget
	add	esp,4
	mov	ebx,0
	jmp	_761
_761:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_MainWindow__update:
	push	ebp
	mov	ebp,esp
	sub	esp,44
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbNullObject
	mov	dword [ebp-20],_bbEmptyString
	mov	dword [ebp-24],_bbNullObject
	mov	dword [ebp-28],_bbNullObject
	mov	dword [ebp-32],_bbEmptyString
	mov	dword [ebp-36],_bbNullObject
	mov	dword [ebp-40],_bbNullObject
	mov	dword [ebp-44],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_4449
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_3985
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventSource
	mov	ebx,eax
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3990
	call	_brl_blitz_NullObjectError
_3990:
	cmp	ebx,dword [esi+48]
	je	_3988
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3993
	call	_brl_blitz_NullObjectError
_3993:
	cmp	ebx,dword [esi+40]
	je	_3991
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3996
	call	_brl_blitz_NullObjectError
_3996:
	cmp	ebx,dword [esi+44]
	je	_3994
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_3999
	call	_brl_blitz_NullObjectError
_3999:
	mov	esi,dword [esi+12]
	cmp	esi,_bbNullObject
	jne	_4001
	call	_brl_blitz_NullObjectError
_4001:
	cmp	ebx,dword [esi+8]
	je	_3997
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_4004
	call	_brl_blitz_NullObjectError
_4004:
	mov	esi,dword [esi+32]
	cmp	esi,_bbNullObject
	jne	_4006
	call	_brl_blitz_NullObjectError
_4006:
	cmp	ebx,dword [esi+8]
	je	_4002
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_4009
	call	_brl_blitz_NullObjectError
_4009:
	mov	esi,dword [esi+16]
	cmp	esi,_bbNullObject
	jne	_4011
	call	_brl_blitz_NullObjectError
_4011:
	cmp	ebx,dword [esi+8]
	je	_4007
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_4014
	call	_brl_blitz_NullObjectError
_4014:
	mov	esi,dword [esi+20]
	cmp	esi,_bbNullObject
	jne	_4016
	call	_brl_blitz_NullObjectError
_4016:
	cmp	ebx,dword [esi+8]
	je	_4012
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_4019
	call	_brl_blitz_NullObjectError
_4019:
	mov	esi,dword [esi+24]
	cmp	esi,_bbNullObject
	jne	_4021
	call	_brl_blitz_NullObjectError
_4021:
	cmp	ebx,dword [esi+8]
	je	_4017
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_4024
	call	_brl_blitz_NullObjectError
_4024:
	mov	esi,dword [esi+28]
	cmp	esi,_bbNullObject
	jne	_4026
	call	_brl_blitz_NullObjectError
_4026:
	cmp	ebx,dword [esi+8]
	je	_4022
	jmp	_3987
_3988:
	mov	eax,ebp
	push	eax
	push	_4045
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4027
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [_bb_quickMode],0
	je	_4028
	mov	eax,ebp
	push	eax
	push	_4033
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4029
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_bb_quickMode],0
	push	_4030
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4032
	call	_brl_blitz_NullObjectError
_4032:
	push	_238
	push	dword [ebx+48]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4034
_4028:
	mov	eax,ebp
	push	eax
	push	_4039
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4035
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_bb_quickMode],1
	push	_4036
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4038
	call	_brl_blitz_NullObjectError
_4038:
	push	_241
	push	dword [ebx+48]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_4034:
	push	_4040
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4042
	call	_brl_blitz_NullObjectError
_4042:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_4044
	call	_brl_blitz_NullObjectError
_4044:
	push	dword [ebx+8]
	call	_maxgui_maxgui_UpdateWindowMenu
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3987
_3991:
	mov	eax,ebp
	push	eax
	push	_4049
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4046
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_4047
	call	_brl_system_Notify
	add	esp,8
	push	_4048
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_765
_3994:
	mov	eax,ebp
	push	eax
	push	_4052
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4050
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_243
	call	_bb_dlog
	add	esp,8
	push	_4051
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_765
_3997:
	mov	eax,ebp
	push	eax
	push	_4142
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4053
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_4056
	jmp	_4055
_4056:
	mov	eax,ebp
	push	eax
	push	_4141
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4057
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4059
	call	_brl_blitz_NullObjectError
_4059:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_4061
	call	_brl_blitz_NullObjectError
_4061:
	push	dword [ebx+8]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	mov	dword [ebp-12],eax
	push	_4063
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4065
	call	_brl_blitz_NullObjectError
_4065:
	mov	ebx,dword [ebx+20]
	cmp	ebx,_bbNullObject
	jne	_4067
	call	_brl_blitz_NullObjectError
_4067:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_4068
	mov	eax,ebp
	push	eax
	push	_4087
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4069
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	dword [eax+8],12
	jne	_4070
	mov	eax,ebp
	push	eax
	push	_4086
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4071
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-12]
	push	_244
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	push	_4072
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	call	_bb_findBySN
	add	esp,4
	mov	dword [ebp-16],eax
	push	_4074
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-16],_bbNullObject
	je	_4075
	mov	eax,ebp
	push	eax
	push	_4079
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4076
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	cmp	ebx,_bbNullObject
	jne	_4078
	call	_brl_blitz_NullObjectError
_4078:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4075:
	push	_4080
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4082
	call	_brl_blitz_NullObjectError
_4082:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_4084
	call	_brl_blitz_NullObjectError
_4084:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	push	_4085
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_765
_4070:
	call	dword [_bbOnDebugLeaveScope]
_4068:
	push	_4088
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4090
	call	_brl_blitz_NullObjectError
_4090:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_4092
	call	_brl_blitz_NullObjectError
_4092:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_4093
	mov	eax,ebp
	push	eax
	push	_4138
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4094
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-12]
	call	_brl_retro_Left
	add	esp,8
	mov	dword [ebp-20],eax
	push	_4096
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_284
	push	dword [ebp-20]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_4097
	mov	eax,ebp
	push	eax
	push	_4117
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4098
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	dword [eax+8],5
	jne	_4099
	mov	eax,ebp
	push	eax
	push	_4116
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4100
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4
	push	dword [ebp-12]
	call	_brl_retro_Right
	add	esp,8
	mov	dword [ebp-12],eax
	push	_4101
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-12]
	push	_245
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	push	_4102
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	call	_bb_findByBC
	add	esp,4
	mov	dword [ebp-24],eax
	push	_4104
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-24],_bbNullObject
	je	_4105
	mov	eax,ebp
	push	eax
	push	_4109
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4106
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_4108
	call	_brl_blitz_NullObjectError
_4108:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4105:
	push	_4110
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4112
	call	_brl_blitz_NullObjectError
_4112:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_4114
	call	_brl_blitz_NullObjectError
_4114:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	push	_4115
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_765
_4099:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4118
_4097:
	mov	eax,ebp
	push	eax
	push	_4137
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4119
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	cmp	dword [eax+8],4
	jne	_4120
	mov	eax,ebp
	push	eax
	push	_4136
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4121
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-12]
	push	_245
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	push	_4122
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	call	_bb_findByBC
	add	esp,4
	mov	dword [ebp-28],eax
	push	_4124
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],_bbNullObject
	je	_4125
	mov	eax,ebp
	push	eax
	push	_4129
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4126
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_4128
	call	_brl_blitz_NullObjectError
_4128:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4125:
	push	_4130
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4132
	call	_brl_blitz_NullObjectError
_4132:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_4134
	call	_brl_blitz_NullObjectError
_4134:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	push	_4135
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_765
_4120:
	call	dword [_bbOnDebugLeaveScope]
_4118:
	call	dword [_bbOnDebugLeaveScope]
_4093:
	push	_4140
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_765
_4055:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3987
_4002:
	mov	eax,ebp
	push	eax
	push	_4186
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4143
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_4146
	jmp	_4145
_4146:
	mov	eax,ebp
	push	eax
	push	_4185
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4147
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4149
	call	_brl_blitz_NullObjectError
_4149:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_4151
	call	_brl_blitz_NullObjectError
_4151:
	push	dword [ebx+8]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	mov	dword [ebp-32],eax
	push	_4153
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4155
	call	_brl_blitz_NullObjectError
_4155:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_4157
	call	_brl_blitz_NullObjectError
_4157:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_4158
	mov	eax,ebp
	push	eax
	push	_4168
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4159
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-32]
	push	_246
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	push	_4160
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_247
	push	dword [ebp-32]
	call	_bb_newBlankStudent
	add	esp,8
	mov	dword [ebp-36],eax
	push	_4162
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4164
	call	_brl_blitz_NullObjectError
_4164:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_4166
	call	_brl_blitz_NullObjectError
_4166:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	push	_4167
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_765
_4158:
	push	_4169
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4171
	call	_brl_blitz_NullObjectError
_4171:
	mov	ebx,dword [ebx+28]
	cmp	ebx,_bbNullObject
	jne	_4173
	call	_brl_blitz_NullObjectError
_4173:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_4174
	mov	eax,ebp
	push	eax
	push	_4184
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4175
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [ebp-32]
	push	_248
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	push	_4176
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-32]
	push	_247
	call	_bb_newBlankStudent
	add	esp,8
	mov	dword [ebp-40],eax
	push	_4178
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4180
	call	_brl_blitz_NullObjectError
_4180:
	mov	ebx,dword [ebx+12]
	cmp	ebx,_bbNullObject
	jne	_4182
	call	_brl_blitz_NullObjectError
_4182:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	push	_4183
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_765
_4174:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4145
_4145:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3987
_4007:
	mov	eax,ebp
	push	eax
	push	_4240
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4187
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_4190
	jmp	_4189
_4190:
	mov	eax,ebp
	push	eax
	push	_4239
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4191
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4193
	call	_brl_blitz_NullObjectError
_4193:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_4195
	call	_brl_blitz_NullObjectError
_4195:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	_4196
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4198
	call	_brl_blitz_NullObjectError
_4198:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_4200
	call	_brl_blitz_NullObjectError
_4200:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_4201
	mov	eax,ebp
	push	eax
	push	_4238
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4202
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4204
	call	_brl_blitz_NullObjectError
_4204:
	mov	ebx,dword [ebx+20]
	cmp	ebx,_bbNullObject
	jne	_4206
	call	_brl_blitz_NullObjectError
_4206:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_4207
	mov	eax,ebp
	push	eax
	push	_4213
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4208
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4210
	call	_brl_blitz_NullObjectError
_4210:
	mov	ebx,dword [ebx+20]
	cmp	ebx,_bbNullObject
	jne	_4212
	call	_brl_blitz_NullObjectError
_4212:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4207:
	push	_4214
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4216
	call	_brl_blitz_NullObjectError
_4216:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_4218
	call	_brl_blitz_NullObjectError
_4218:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_4219
	mov	eax,ebp
	push	eax
	push	_4225
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4220
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4222
	call	_brl_blitz_NullObjectError
_4222:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_4224
	call	_brl_blitz_NullObjectError
_4224:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4219:
	push	_4226
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4228
	call	_brl_blitz_NullObjectError
_4228:
	mov	ebx,dword [ebx+28]
	cmp	ebx,_bbNullObject
	jne	_4230
	call	_brl_blitz_NullObjectError
_4230:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_4231
	mov	eax,ebp
	push	eax
	push	_4237
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4232
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4234
	call	_brl_blitz_NullObjectError
_4234:
	mov	ebx,dword [ebx+28]
	cmp	ebx,_bbNullObject
	jne	_4236
	call	_brl_blitz_NullObjectError
_4236:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4231:
	call	dword [_bbOnDebugLeaveScope]
_4201:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4189
_4189:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3987
_4012:
	mov	eax,ebp
	push	eax
	push	_4294
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4241
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_4244
	jmp	_4243
_4244:
	mov	eax,ebp
	push	eax
	push	_4293
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4245
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4247
	call	_brl_blitz_NullObjectError
_4247:
	mov	ebx,dword [ebx+20]
	cmp	ebx,_bbNullObject
	jne	_4249
	call	_brl_blitz_NullObjectError
_4249:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	_4250
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4252
	call	_brl_blitz_NullObjectError
_4252:
	mov	ebx,dword [ebx+20]
	cmp	ebx,_bbNullObject
	jne	_4254
	call	_brl_blitz_NullObjectError
_4254:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_4255
	mov	eax,ebp
	push	eax
	push	_4292
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4256
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4258
	call	_brl_blitz_NullObjectError
_4258:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_4260
	call	_brl_blitz_NullObjectError
_4260:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_4261
	mov	eax,ebp
	push	eax
	push	_4267
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4262
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4264
	call	_brl_blitz_NullObjectError
_4264:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_4266
	call	_brl_blitz_NullObjectError
_4266:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4261:
	push	_4268
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4270
	call	_brl_blitz_NullObjectError
_4270:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_4272
	call	_brl_blitz_NullObjectError
_4272:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_4273
	mov	eax,ebp
	push	eax
	push	_4279
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4274
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4276
	call	_brl_blitz_NullObjectError
_4276:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_4278
	call	_brl_blitz_NullObjectError
_4278:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4273:
	push	_4280
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4282
	call	_brl_blitz_NullObjectError
_4282:
	mov	ebx,dword [ebx+28]
	cmp	ebx,_bbNullObject
	jne	_4284
	call	_brl_blitz_NullObjectError
_4284:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_4285
	mov	eax,ebp
	push	eax
	push	_4291
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4286
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4288
	call	_brl_blitz_NullObjectError
_4288:
	mov	ebx,dword [ebx+28]
	cmp	ebx,_bbNullObject
	jne	_4290
	call	_brl_blitz_NullObjectError
_4290:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4285:
	call	dword [_bbOnDebugLeaveScope]
_4255:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4243
_4243:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3987
_4017:
	mov	eax,ebp
	push	eax
	push	_4348
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4295
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_4298
	jmp	_4297
_4298:
	mov	eax,ebp
	push	eax
	push	_4347
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4299
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4301
	call	_brl_blitz_NullObjectError
_4301:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_4303
	call	_brl_blitz_NullObjectError
_4303:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	_4304
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4306
	call	_brl_blitz_NullObjectError
_4306:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_4308
	call	_brl_blitz_NullObjectError
_4308:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_4309
	mov	eax,ebp
	push	eax
	push	_4346
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4310
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4312
	call	_brl_blitz_NullObjectError
_4312:
	mov	ebx,dword [ebx+20]
	cmp	ebx,_bbNullObject
	jne	_4314
	call	_brl_blitz_NullObjectError
_4314:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_4315
	mov	eax,ebp
	push	eax
	push	_4321
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4316
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4318
	call	_brl_blitz_NullObjectError
_4318:
	mov	ebx,dword [ebx+20]
	cmp	ebx,_bbNullObject
	jne	_4320
	call	_brl_blitz_NullObjectError
_4320:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4315:
	push	_4322
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4324
	call	_brl_blitz_NullObjectError
_4324:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_4326
	call	_brl_blitz_NullObjectError
_4326:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_4327
	mov	eax,ebp
	push	eax
	push	_4333
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4328
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4330
	call	_brl_blitz_NullObjectError
_4330:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_4332
	call	_brl_blitz_NullObjectError
_4332:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4327:
	push	_4334
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4336
	call	_brl_blitz_NullObjectError
_4336:
	mov	ebx,dword [ebx+28]
	cmp	ebx,_bbNullObject
	jne	_4338
	call	_brl_blitz_NullObjectError
_4338:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_4339
	mov	eax,ebp
	push	eax
	push	_4345
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4340
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4342
	call	_brl_blitz_NullObjectError
_4342:
	mov	ebx,dword [ebx+28]
	cmp	ebx,_bbNullObject
	jne	_4344
	call	_brl_blitz_NullObjectError
_4344:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4339:
	call	dword [_bbOnDebugLeaveScope]
_4309:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4297
_4297:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3987
_4022:
	mov	eax,ebp
	push	eax
	push	_4402
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4349
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_4352
	jmp	_4351
_4352:
	mov	eax,ebp
	push	eax
	push	_4401
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4353
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4355
	call	_brl_blitz_NullObjectError
_4355:
	mov	ebx,dword [ebx+28]
	cmp	ebx,_bbNullObject
	jne	_4357
	call	_brl_blitz_NullObjectError
_4357:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	_4358
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4360
	call	_brl_blitz_NullObjectError
_4360:
	mov	ebx,dword [ebx+28]
	cmp	ebx,_bbNullObject
	jne	_4362
	call	_brl_blitz_NullObjectError
_4362:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_4363
	mov	eax,ebp
	push	eax
	push	_4400
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4364
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4366
	call	_brl_blitz_NullObjectError
_4366:
	mov	ebx,dword [ebx+20]
	cmp	ebx,_bbNullObject
	jne	_4368
	call	_brl_blitz_NullObjectError
_4368:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,1
	jne	_4369
	mov	eax,ebp
	push	eax
	push	_4375
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4370
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4372
	call	_brl_blitz_NullObjectError
_4372:
	mov	ebx,dword [ebx+20]
	cmp	ebx,_bbNullObject
	jne	_4374
	call	_brl_blitz_NullObjectError
_4374:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4369:
	push	_4376
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4378
	call	_brl_blitz_NullObjectError
_4378:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_4380
	call	_brl_blitz_NullObjectError
_4380:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_4381
	mov	eax,ebp
	push	eax
	push	_4387
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4382
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4384
	call	_brl_blitz_NullObjectError
_4384:
	mov	ebx,dword [ebx+24]
	cmp	ebx,_bbNullObject
	jne	_4386
	call	_brl_blitz_NullObjectError
_4386:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4381:
	push	_4388
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4390
	call	_brl_blitz_NullObjectError
_4390:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_4392
	call	_brl_blitz_NullObjectError
_4392:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	cmp	eax,0
	je	_4393
	mov	eax,ebp
	push	eax
	push	_4399
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4394
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4396
	call	_brl_blitz_NullObjectError
_4396:
	mov	ebx,dword [ebx+16]
	cmp	ebx,_bbNullObject
	jne	_4398
	call	_brl_blitz_NullObjectError
_4398:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_4393:
	call	dword [_bbOnDebugLeaveScope]
_4363:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4351
_4351:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_3987
_3987:
	push	_4403
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4405
	call	_brl_blitz_NullObjectError
_4405:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_4407
	call	_brl_blitz_NullObjectError
_4407:
	call	_brl_eventqueue_EventSource
	cmp	eax,dword [ebx+8]
	jne	_4408
	mov	eax,ebp
	push	eax
	push	_4416
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4409
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	je	_4412
	jmp	_4411
_4412:
	mov	eax,ebp
	push	eax
	push	_4414
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4413
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_765
_4411:
	push	_4415
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	jmp	_765
_4408:
	push	_4417
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-44],_bbNullObject
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4420
	call	_brl_blitz_NullObjectError
_4420:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_4422
	call	_brl_blitz_NullObjectError
_4422:
	mov	esi,dword [ebx+16]
	cmp	esi,_bbNullObject
	jne	_4425
	call	_brl_blitz_NullObjectError
_4425:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_249
_251:
	cmp	ebx,_bbNullObject
	jne	_4430
	call	_brl_blitz_NullObjectError
_4430:
	push	_maxgui_maxgui_TGadget
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-44],eax
	cmp	dword [ebp-44],_bbNullObject
	je	_249
	mov	eax,ebp
	push	eax
	push	_4446
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4431
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventSource
	cmp	eax,dword [ebp-44]
	jne	_4432
	mov	eax,ebp
	push	eax
	push	_4445
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4433
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,8200
	je	_4436
	cmp	eax,8193
	je	_4437
	mov	eax,ebp
	push	eax
	push	_4441
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4438
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventSource
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	jne	_4440
	call	_brl_blitz_NullObjectError
_4440:
	push	0
	call	_brl_eventqueue_EventID
	push	eax
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_253
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+24]
	add	esp,4
	push	eax
	push	_252
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4435
_4436:
	mov	eax,ebp
	push	eax
	push	_4442
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4435
_4437:
	mov	eax,ebp
	push	eax
	push	_4443
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4435
_4435:
	push	_4444
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_765
_4432:
	call	dword [_bbOnDebugLeaveScope]
_249:
	cmp	ebx,_bbNullObject
	jne	_4428
	call	_brl_blitz_NullObjectError
_4428:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_251
_250:
	push	_4448
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	jmp	_765
_765:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_MainWindow__end:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_4487
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4450
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbNullObject
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4453
	call	_brl_blitz_NullObjectError
_4453:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_4455
	call	_brl_blitz_NullObjectError
_4455:
	mov	esi,dword [ebx+16]
	cmp	esi,_bbNullObject
	jne	_4458
	call	_brl_blitz_NullObjectError
_4458:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_254
_256:
	cmp	ebx,_bbNullObject
	jne	_4463
	call	_brl_blitz_NullObjectError
_4463:
	push	_maxgui_maxgui_TGadget
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	je	_254
	mov	eax,ebp
	push	eax
	push	_4466
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4464
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	push	_4465
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
_254:
	cmp	ebx,_bbNullObject
	jne	_4461
	call	_brl_blitz_NullObjectError
_4461:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_256
_255:
	push	_4467
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4469
	call	_brl_blitz_NullObjectError
_4469:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_4471
	call	_brl_blitz_NullObjectError
_4471:
	push	dword [ebx+8]
	call	_maxgui_maxgui_HideGadget
	add	esp,4
	push	_4472
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4474
	call	_brl_blitz_NullObjectError
_4474:
	mov	ebx,dword [ebx+8]
	cmp	ebx,_bbNullObject
	jne	_4476
	call	_brl_blitz_NullObjectError
_4476:
	push	dword [ebx+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	push	_4477
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_4479
	call	_brl_blitz_NullObjectError
_4479:
	mov	ebx,_bbNullObject
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_4484
	push	eax
	call	_bbGCFree
	add	esp,4
_4484:
	mov	dword [esi+8],ebx
	push	_4485
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	dword [_bb_winList]
	call	_brl_linkedlist_ListRemove
	add	esp,8
	push	_4486
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	jmp	_768
_768:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_prompt:
	push	ebp
	mov	ebp,esp
	sub	esp,36
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	dword [ebp-20],_bbNullObject
	mov	dword [ebp-24],_bbNullObject
	mov	dword [ebp-28],_bbNullObject
	mov	dword [ebp-32],0
	mov	dword [ebp-36],_bbEmptyString
	push	ebp
	push	_4588
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4488
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_Window
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-20],eax
	push	_4490
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_4492
	call	_brl_blitz_NullObjectError
_4492:
	push	577
	push	dword [ebp-4]
	push	65
	push	215
	push	0
	push	0
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,28
	push	_4493
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	push	dword [ebp-20]
	push	20
	push	150
	push	10
	push	10
	call	_bb_newTextField
	add	esp,24
	mov	dword [ebp-24],eax
	push	_4495
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	8
	push	dword [ebp-20]
	push	20
	push	40
	push	10
	push	165
	push	_236
	call	_bb_newButton
	add	esp,28
	mov	dword [ebp-28],eax
	push	_4497
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_4499
	call	_brl_blitz_NullObjectError
_4499:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	_4500
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_4502
	call	_brl_blitz_NullObjectError
_4502:
	push	dword [ebx+8]
	call	_maxgui_maxgui_ActivateGadget
	add	esp,4
	push	_4503
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_272
_274:
	push	ebp
	push	_4587
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4504
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_WaitEvent
	push	_4505
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventSource
	mov	ebx,eax
	mov	esi,dword [ebp-24]
	cmp	esi,_bbNullObject
	jne	_4510
	call	_brl_blitz_NullObjectError
_4510:
	cmp	ebx,dword [esi+8]
	je	_4508
	mov	esi,dword [ebp-28]
	cmp	esi,_bbNullObject
	jne	_4513
	call	_brl_blitz_NullObjectError
_4513:
	cmp	ebx,dword [esi+8]
	je	_4511
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_4516
	call	_brl_blitz_NullObjectError
_4516:
	cmp	ebx,dword [esi+8]
	je	_4514
	jmp	_4507
_4508:
	push	ebp
	push	_4530
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4517
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-8],0
	je	_4518
	push	ebp
	push	_4529
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4519
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_4521
	call	_brl_blitz_NullObjectError
_4521:
	push	dword [ebx+8]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	mov	edx,dword [ebp-8]
	cmp	dword [eax+8],edx
	jle	_4522
	push	ebp
	push	_4528
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4523
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_4525
	call	_brl_blitz_NullObjectError
_4525:
	mov	esi,dword [ebp-24]
	cmp	esi,_bbNullObject
	jne	_4527
	call	_brl_blitz_NullObjectError
_4527:
	push	dword [ebp-8]
	push	dword [esi+8]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	push	eax
	call	_brl_retro_Left
	add	esp,8
	push	eax
	push	dword [ebx+8]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_4522:
	call	dword [_bbOnDebugLeaveScope]
_4518:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4507
_4511:
	push	ebp
	push	_4566
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4531
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-32],0
	push	_4533
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_quickMode]
	cmp	eax,0
	jne	_4534
	mov	eax,dword [ebp-12]
_4534:
	cmp	eax,0
	je	_4536
	push	ebp
	push	_4538
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4537
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-32],1
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4539
_4536:
	push	ebp
	push	_4541
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4540
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_275
	call	_brl_system_Confirm
	add	esp,8
	mov	dword [ebp-32],eax
	call	dword [_bbOnDebugLeaveScope]
_4539:
	push	_4542
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-32],0
	je	_4543
	push	ebp
	push	_4564
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4544
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_4546
	call	_brl_blitz_NullObjectError
_4546:
	push	dword [ebx+8]
	call	_maxgui_maxgui_GadgetText
	add	esp,4
	mov	dword [ebp-36],eax
	push	_4548
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_4550
	call	_brl_blitz_NullObjectError
_4550:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	push	_4551
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_4553
	call	_brl_blitz_NullObjectError
_4553:
	push	dword [ebx+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	push	_4554
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_4556
	call	_brl_blitz_NullObjectError
_4556:
	push	dword [ebx+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	push	_4557
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_4559
	call	_brl_blitz_NullObjectError
_4559:
	push	dword [ebx+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	push	_4560
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],_bbNullObject
	push	_4561
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-28],_bbNullObject
	push	_4562
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],_bbNullObject
	push	_4563
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-36]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_774
_4543:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4507
_4514:
	push	ebp
	push	_4586
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4567
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,16387
	jne	_4568
	push	ebp
	push	_4585
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4569
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_4571
	call	_brl_blitz_NullObjectError
_4571:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+56]
	add	esp,4
	push	_4572
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_4574
	call	_brl_blitz_NullObjectError
_4574:
	push	dword [ebx+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	push	_4575
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_4577
	call	_brl_blitz_NullObjectError
_4577:
	push	dword [ebx+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	push	_4578
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-20]
	cmp	ebx,_bbNullObject
	jne	_4580
	call	_brl_blitz_NullObjectError
_4580:
	push	dword [ebx+8]
	call	_maxgui_maxgui_FreeGadget
	add	esp,4
	push	_4581
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],_bbNullObject
	push	_4582
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-28],_bbNullObject
	push	_4583
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],_bbNullObject
	push	_4584
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_1
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_774
_4568:
	call	dword [_bbOnDebugLeaveScope]
	jmp	_4507
_4507:
	call	dword [_bbOnDebugLeaveScope]
_272:
	mov	eax,1
	cmp	eax,0
	jne	_274
_273:
	mov	ebx,_bbEmptyString
	jmp	_774
_774:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_convertFromCSV:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbEmptyString
	mov	dword [ebp-20],0
	push	ebp
	push	_4622
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4595
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-8],eax
	push	_4597
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbEmptyString
	push	_4599
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],_bbEmptyString
	push	_4601
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+8]
	mov	dword [ebp-20],eax
	push	_4603
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_278:
	push	ebp
	push	_4620
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4604
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_brl_retro_Left
	add	esp,8
	mov	dword [ebp-16],eax
	push	_4605
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_73
	push	dword [ebp-16]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_4606
	push	ebp
	push	_4613
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4607
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_79
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_4608
	push	ebp
	push	_4610
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4609
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_277
_4608:
	push	_4611
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	push	dword [ebp-8]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_4612
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_1
	call	dword [_bbOnDebugLeaveScope]
_4606:
	push	_4614
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_73
	push	dword [ebp-16]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_4615
	push	ebp
	push	_4617
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4616
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	push	dword [ebp-12]
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-12],eax
	call	dword [_bbOnDebugLeaveScope]
_4615:
	push	_4618
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	sub	dword [ebp-20],1
	push	_4619
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	push	dword [ebp-4]
	call	_brl_retro_Right
	add	esp,8
	mov	dword [ebp-4],eax
	call	dword [_bbOnDebugLeaveScope]
_276:
	push	_1
	push	dword [ebp-4]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_278
_277:
	push	_4621
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	jmp	_777
_777:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_convertFromSSV:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbEmptyString
	mov	dword [ebp-20],0
	push	ebp
	push	_4656
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4626
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-8],eax
	push	_4628
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbEmptyString
	push	_4630
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],_bbEmptyString
	push	_4632
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+8]
	mov	dword [ebp-20],eax
	push	_4634
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_281:
	push	ebp
	push	_4647
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4635
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_brl_retro_Left
	add	esp,8
	mov	dword [ebp-16],eax
	push	_4636
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_77
	push	dword [ebp-16]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_4637
	push	ebp
	push	_4640
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4638
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	push	dword [ebp-8]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_4639
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_1
	call	dword [_bbOnDebugLeaveScope]
_4637:
	push	_4641
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_77
	push	dword [ebp-16]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_4642
	push	ebp
	push	_4644
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4643
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	push	dword [ebp-12]
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-12],eax
	call	dword [_bbOnDebugLeaveScope]
_4642:
	push	_4645
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	sub	dword [ebp-20],1
	push	_4646
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	push	dword [ebp-4]
	call	_brl_retro_Right
	add	esp,8
	mov	dword [ebp-4],eax
	call	dword [_bbOnDebugLeaveScope]
_279:
	push	_1
	push	dword [ebp-4]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_281
_280:
	push	_4648
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_282
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_4649
	push	ebp
	push	_4651
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4650
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_780
_4649:
	push	_4652
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_4654
	call	_brl_blitz_NullObjectError
_4654:
	push	2
	push	_177
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+24]
	add	esp,4
	push	eax
	push	_283
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bb_dlog
	add	esp,8
	push	_4655
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	jmp	_780
_780:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_resetFile:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_4661
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4658
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_filesystem_CloseFile
	add	esp,4
	push	_4659
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	1
	push	dword [ebp-8]
	call	_brl_filesystem_OpenFile
	add	esp,12
	mov	dword [ebp-4],eax
	push	_4660
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	jmp	_784
_784:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextArea_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_4666
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_TextArea
	mov	edx,_bbNullObject
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],edx
	push	ebp
	push	_4665
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_787
_787:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextArea_Delete:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_790:
	mov	eax,dword [eax+8]
	dec	dword [eax+4]
	jnz	_4669
	push	eax
	call	_bbGCFree
	add	esp,4
_4669:
	mov	eax,0
	jmp	_4667
_4667:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextArea__init:
	push	ebp
	mov	ebp,esp
	sub	esp,36
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp+32]
	mov	dword [ebp-28],eax
	mov	eax,dword [ebp+36]
	mov	dword [ebp-32],eax
	mov	dword [ebp-36],0
	push	ebp
	push	_4696
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4670
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-36],0
	push	_4673
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],0
	je	_4674
	push	ebp
	push	_4676
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4675
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-36],2
	call	dword [_bbOnDebugLeaveScope]
_4674:
	push	_4677
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-32],0
	je	_4678
	push	ebp
	push	_4680
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4679
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-36],1
	call	dword [_bbOnDebugLeaveScope]
_4678:
	push	_4681
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4683
	call	_brl_blitz_NullObjectError
_4683:
	mov	esi,dword [ebp-24]
	cmp	esi,_bbNullObject
	jne	_4686
	call	_brl_blitz_NullObjectError
_4686:
	push	dword [ebp-36]
	push	dword [esi+8]
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	call	_maxgui_maxgui_CreateTextArea
	add	esp,24
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_4690
	push	eax
	call	_bbGCFree
	add	esp,4
_4690:
	mov	dword [ebx+8],esi
	push	_4691
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-24]
	cmp	esi,_bbNullObject
	jne	_4693
	call	_brl_blitz_NullObjectError
_4693:
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4695
	call	_brl_blitz_NullObjectError
_4695:
	push	dword [ebx+8]
	push	dword [esi+16]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	ebx,0
	jmp	_800
_800:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextArea__getText:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_4702
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4699
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4701
	call	_brl_blitz_NullObjectError
_4701:
	push	1
	push	-1
	push	0
	push	dword [ebx+8]
	call	_maxgui_maxgui_TextAreaText
	add	esp,16
	mov	ebx,eax
	jmp	_803
_803:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TextArea__clear:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_4706
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4703
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_4705
	call	_brl_blitz_NullObjectError
_4705:
	push	_1
	push	dword [ebx+8]
	call	_maxgui_maxgui_SetGadgetText
	add	esp,8
	mov	ebx,0
	jmp	_806
_806:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_newTextArea:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp+20]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp+24]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp+28]
	mov	dword [ebp-24],eax
	mov	dword [ebp-28],_bbNullObject
	push	ebp
	push	_4713
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_4707
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_TextArea
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-28],eax
	push	_4709
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_4711
	call	_brl_blitz_NullObjectError
_4711:
	push	1
	push	dword [ebp-24]
	push	dword [ebp-20]
	push	dword [ebp-16]
	push	dword [ebp-12]
	push	dword [ebp-8]
	push	dword [ebp-4]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,32
	push	_4712
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-28]
	jmp	_814
_814:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_1001:
	dd	0
_966:
	db	"iPad Manager",0
_967:
	db	"dm",0
_307:
	db	"i",0
	align	4
_311:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	49
_968:
	db	"password",0
_304:
	db	"$",0
	align	4
_969:
	dd	_bbStringClass
	dd	2147483646
	dd	8
	dw	52,51,50,49,120,103,51,116
_970:
	db	"winStyleDefault",0
	align	4
_971:
	dd	_bbStringClass
	dd	2147483646
	dd	3
	dw	53,56,57
_972:
	db	"ProgramInfo",0
	align	4
_315:
	dd	_bbStringClass
	dd	2147483646
	dd	18
	dw	105,80,97,100,32,77,97,110,97,103,101,114,32,48,46,48
	dw	46,49
_316:
	db	"Platform",0
	align	4
_317:
	dd	_bbStringClass
	dd	2147483646
	dd	5
	dw	87,105,110,51,50
_973:
	db	"nbsp",0
	align	4
_974:
	dd	_bbStringClass
	dd	2147483646
	dd	147
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32
_975:
	db	"chargerPrice",0
	align	4
_976:
	dd	_bbStringClass
	dd	2147483646
	dd	2
	dw	49,56
_977:
	db	"cablePrice",0
_978:
	db	"casePrice",0
	align	4
_979:
	dd	_bbStringClass
	dd	2147483646
	dd	2
	dw	51,54
_980:
	db	"ipadRentPrice",0
	align	4
_981:
	dd	_bbStringClass
	dd	2147483646
	dd	2
	dw	53,48
_982:
	db	"ipadPrice",0
	align	4
_983:
	dd	_bbStringClass
	dd	2147483646
	dd	3
	dw	53,48,56
_984:
	db	"My",0
_985:
	db	":z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea",0
	align	4
_820:
	dd	_bbNullObject
_986:
	db	"winList",0
_329:
	db	":TList",0
	align	4
_bb_winList:
	dd	_bbNullObject
_987:
	db	"ipadList",0
	align	4
_bb_ipadList:
	dd	_bbNullObject
_988:
	db	"studentList",0
	align	4
_bb_studentList:
	dd	_bbNullObject
_989:
	db	"quickMode",0
	align	4
_bb_quickMode:
	dd	0
_990:
	db	"debugWin",0
_991:
	db	":logWin",0
	align	4
_bb_debugWin:
	dd	_bbNullObject
_992:
	db	"dataDir",0
	align	4
_1:
	dd	_bbStringClass
	dd	2147483647
	dd	0
	align	4
_bb_dataDir:
	dd	_1
_993:
	db	"ipaddbn",0
	align	4
_bb_ipaddbn:
	dd	_bbEmptyString
_994:
	db	"iPaddb",0
_995:
	db	":TStream",0
	align	4
_bb_iPaddb:
	dd	_bbNullObject
_996:
	db	"noteWinList",0
	align	4
_bb_noteWinList:
	dd	_bbNullObject
_997:
	db	"studdbn",0
	align	4
_bb_studdbn:
	dd	_bbEmptyString
_998:
	db	"studdb",0
	align	4
_bb_studdb:
	dd	_bbNullObject
_999:
	db	"mainWin",0
_1000:
	db	":MainWindow",0
	align	4
_965:
	dd	1
	dd	_966
	dd	1
	dd	_967
	dd	_307
	dd	_311
	dd	1
	dd	_968
	dd	_304
	dd	_969
	dd	1
	dd	_970
	dd	_307
	dd	_971
	dd	1
	dd	_972
	dd	_304
	dd	_315
	dd	1
	dd	_316
	dd	_304
	dd	_317
	dd	1
	dd	_973
	dd	_304
	dd	_974
	dd	1
	dd	_975
	dd	_307
	dd	_976
	dd	1
	dd	_977
	dd	_307
	dd	_976
	dd	1
	dd	_978
	dd	_307
	dd	_979
	dd	1
	dd	_980
	dd	_307
	dd	_981
	dd	1
	dd	_982
	dd	_307
	dd	_983
	dd	4
	dd	_984
	dd	_985
	dd	_820
	dd	4
	dd	_986
	dd	_329
	dd	_bb_winList
	dd	4
	dd	_987
	dd	_329
	dd	_bb_ipadList
	dd	4
	dd	_988
	dd	_329
	dd	_bb_studentList
	dd	4
	dd	_989
	dd	_307
	dd	_bb_quickMode
	dd	4
	dd	_990
	dd	_991
	dd	_bb_debugWin
	dd	4
	dd	_992
	dd	_304
	dd	_bb_dataDir
	dd	4
	dd	_993
	dd	_304
	dd	_bb_ipaddbn
	dd	4
	dd	_994
	dd	_995
	dd	_bb_iPaddb
	dd	4
	dd	_996
	dd	_329
	dd	_bb_noteWinList
	dd	4
	dd	_997
	dd	_304
	dd	_bb_studdbn
	dd	4
	dd	_998
	dd	_995
	dd	_bb_studdb
	dd	2
	dd	_999
	dd	_1000
	dd	-4
	dd	0
_297:
	db	"z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0",0
_298:
	db	"New",0
_299:
	db	"()i",0
_300:
	db	"Delete",0
	align	4
_296:
	dd	2
	dd	_297
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	0
	align	4
_28:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_296
	dd	8
	dd	__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_New
	dd	__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_302:
	db	"z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea",0
_303:
	db	"Name",0
	align	4
_305:
	dd	_bbStringClass
	dd	2147483646
	dd	12
	dw	105,80,97,100,32,77,97,110,97,103,101,114
_306:
	db	"MajorVersion",0
	align	4
_308:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	48
_309:
	db	"MinorVersion",0
_310:
	db	"Revision",0
_312:
	db	"VersionString",0
	align	4
_313:
	dd	_bbStringClass
	dd	2147483646
	dd	5
	dw	48,46,48,46,49
_314:
	db	"AssemblyInfo",0
_318:
	db	"Architecture",0
	align	4
_319:
	dd	_bbStringClass
	dd	2147483646
	dd	3
	dw	120,56,54
_320:
	db	"DebugOn",0
	align	4
_301:
	dd	2
	dd	_302
	dd	1
	dd	_303
	dd	_304
	dd	_305
	dd	1
	dd	_306
	dd	_307
	dd	_308
	dd	1
	dd	_309
	dd	_307
	dd	_308
	dd	1
	dd	_310
	dd	_307
	dd	_311
	dd	1
	dd	_312
	dd	_304
	dd	_313
	dd	1
	dd	_314
	dd	_304
	dd	_315
	dd	1
	dd	_316
	dd	_304
	dd	_317
	dd	1
	dd	_318
	dd	_304
	dd	_319
	dd	1
	dd	_320
	dd	_307
	dd	_311
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	0
	align	4
_29:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_301
	dd	8
	dd	__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New
	dd	__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_817:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/iPad Manager.bmx",0
	align	4
_816:
	dd	_817
	dd	76
	dd	5
	align	4
__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Application:
	dd	_bbNullObject
	align	4
_818:
	dd	_817
	dd	77
	dd	5
	align	4
__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Resources:
	dd	_bbNullObject
_322:
	db	"z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea",0
	align	4
_321:
	dd	2
	dd	_322
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	0
	align	4
_35:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_321
	dd	8
	dd	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New
	dd	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_324:
	db	"Window",0
_325:
	db	"handle",0
_326:
	db	":TGadget",0
_327:
	db	"id",0
_328:
	db	"gadgetList",0
_330:
	db	"menu",0
_331:
	db	"_new",0
_332:
	db	"(i,i,i,i,$,i)i",0
_333:
	db	"_show",0
_334:
	db	"_hide",0
_335:
	db	"_update",0
	align	4
_323:
	dd	2
	dd	_324
	dd	3
	dd	_325
	dd	_326
	dd	8
	dd	3
	dd	_327
	dd	_304
	dd	12
	dd	3
	dd	_328
	dd	_329
	dd	16
	dd	3
	dd	_330
	dd	_326
	dd	20
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	6
	dd	_331
	dd	_332
	dd	48
	dd	6
	dd	_333
	dd	_299
	dd	52
	dd	6
	dd	_334
	dd	_299
	dd	56
	dd	6
	dd	_335
	dd	_299
	dd	60
	dd	0
	align	4
_bb_Window:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_323
	dd	24
	dd	__bb_Window_New
	dd	__bb_Window_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_Window__new
	dd	__bb_Window__show
	dd	__bb_Window__hide
	dd	__bb_Window__update
_337:
	db	"Button",0
_338:
	db	"state",0
_339:
	db	"_init",0
_340:
	db	"($,i,i,i,i,:Window,i)i",0
_341:
	db	"_state",0
_342:
	db	"_cstate",0
	align	4
_336:
	dd	2
	dd	_337
	dd	3
	dd	_325
	dd	_326
	dd	8
	dd	3
	dd	_338
	dd	_307
	dd	12
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	6
	dd	_339
	dd	_340
	dd	48
	dd	6
	dd	_341
	dd	_299
	dd	52
	dd	6
	dd	_342
	dd	_299
	dd	56
	dd	0
	align	4
_bb_Button:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_336
	dd	16
	dd	__bb_Button_New
	dd	__bb_Button_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_Button__init
	dd	__bb_Button__state
	dd	__bb_Button__cstate
_344:
	db	"TextField",0
_345:
	db	"(i,i,i,i,:Window,i)i",0
_346:
	db	"_getText",0
_347:
	db	"()$",0
_348:
	db	"_clear",0
	align	4
_343:
	dd	2
	dd	_344
	dd	3
	dd	_325
	dd	_326
	dd	8
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	6
	dd	_339
	dd	_345
	dd	48
	dd	6
	dd	_346
	dd	_347
	dd	52
	dd	6
	dd	_348
	dd	_299
	dd	56
	dd	0
	align	4
_bb_TextField:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_343
	dd	12
	dd	__bb_TextField_New
	dd	__bb_TextField_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_TextField__init
	dd	__bb_TextField__getText
	dd	__bb_TextField__clear
_350:
	db	"ComboBox",0
	align	4
_349:
	dd	2
	dd	_350
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	0
	align	4
_bb_ComboBox:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_349
	dd	8
	dd	__bb_ComboBox_New
	dd	__bb_ComboBox_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_352:
	db	"ListBox",0
_353:
	db	"_add",0
_354:
	db	"($,i)i",0
_355:
	db	"_modify",0
_356:
	db	"($)i",0
_357:
	db	"_remove",0
	align	4
_351:
	dd	2
	dd	_352
	dd	3
	dd	_325
	dd	_326
	dd	8
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	6
	dd	_339
	dd	_345
	dd	48
	dd	6
	dd	_353
	dd	_354
	dd	52
	dd	6
	dd	_355
	dd	_356
	dd	56
	dd	6
	dd	_357
	dd	_299
	dd	60
	dd	6
	dd	_346
	dd	_347
	dd	64
	dd	0
	align	4
_bb_ListBox:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_351
	dd	12
	dd	__bb_ListBox_New
	dd	__bb_ListBox_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_ListBox__init
	dd	__bb_ListBox__add
	dd	__bb_ListBox__modify
	dd	__bb_ListBox__remove
	dd	__bb_ListBox__getText
_359:
	db	"iPad",0
_360:
	db	":Window",0
_361:
	db	"pos",0
_362:
	db	"bc",0
_363:
	db	"sn",0
_364:
	db	"ownerPos",0
_365:
	db	"iEditMenu",0
_366:
	db	"ownerMenu",0
_367:
	db	"actionMenu",0
_368:
	db	"bcEdit",0
_369:
	db	"snEdit",0
_370:
	db	"damageAction",0
_371:
	db	"repairAction",0
_372:
	db	"coutAction",0
_373:
	db	"lostAction",0
_374:
	db	"stolenAction",0
_375:
	db	"nEditMenu",0
_376:
	db	"newMenu",0
_377:
	db	"contextMenu",0
_378:
	db	"infNew",0
_379:
	db	"actNew",0
_380:
	db	"misNew",0
_381:
	db	"noteList",0
_382:
	db	":ListBox",0
_383:
	db	"bcLabel",0
_384:
	db	"snLabel",0
_385:
	db	"stLabel",0
_386:
	db	"noteTypeList",0
_387:
	db	"_build",0
_388:
	db	"_save",0
_389:
	db	"_addNote",0
_390:
	db	"($):Note",0
_391:
	db	"_noteBySummary",0
	align	4
_358:
	dd	2
	dd	_359
	dd	3
	dd	_325
	dd	_360
	dd	8
	dd	3
	dd	_361
	dd	_307
	dd	12
	dd	3
	dd	_362
	dd	_304
	dd	16
	dd	3
	dd	_363
	dd	_304
	dd	20
	dd	3
	dd	_338
	dd	_304
	dd	24
	dd	3
	dd	_364
	dd	_307
	dd	28
	dd	3
	dd	_365
	dd	_326
	dd	32
	dd	3
	dd	_366
	dd	_326
	dd	36
	dd	3
	dd	_367
	dd	_326
	dd	40
	dd	3
	dd	_368
	dd	_326
	dd	44
	dd	3
	dd	_369
	dd	_326
	dd	48
	dd	3
	dd	_370
	dd	_326
	dd	52
	dd	3
	dd	_371
	dd	_326
	dd	56
	dd	3
	dd	_372
	dd	_326
	dd	60
	dd	3
	dd	_373
	dd	_326
	dd	64
	dd	3
	dd	_374
	dd	_326
	dd	68
	dd	3
	dd	_375
	dd	_326
	dd	72
	dd	3
	dd	_376
	dd	_326
	dd	76
	dd	3
	dd	_377
	dd	_326
	dd	80
	dd	3
	dd	_378
	dd	_326
	dd	84
	dd	3
	dd	_379
	dd	_326
	dd	88
	dd	3
	dd	_380
	dd	_326
	dd	92
	dd	3
	dd	_381
	dd	_382
	dd	96
	dd	3
	dd	_383
	dd	_326
	dd	100
	dd	3
	dd	_384
	dd	_326
	dd	104
	dd	3
	dd	_385
	dd	_326
	dd	108
	dd	3
	dd	_386
	dd	_329
	dd	112
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	6
	dd	_339
	dd	_299
	dd	48
	dd	6
	dd	_333
	dd	_299
	dd	52
	dd	6
	dd	_387
	dd	_299
	dd	56
	dd	6
	dd	_334
	dd	_299
	dd	60
	dd	6
	dd	_388
	dd	_299
	dd	64
	dd	6
	dd	_389
	dd	_390
	dd	68
	dd	6
	dd	_335
	dd	_299
	dd	72
	dd	6
	dd	_391
	dd	_390
	dd	76
	dd	0
	align	4
_bb_iPad:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_358
	dd	116
	dd	__bb_iPad_New
	dd	__bb_iPad_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_iPad__init
	dd	__bb_iPad__show
	dd	__bb_iPad__build
	dd	__bb_iPad__hide
	dd	__bb_iPad__save
	dd	__bb_iPad__addNote
	dd	__bb_iPad__update
	dd	__bb_iPad__noteBySummary
_393:
	db	"Note",0
_394:
	db	"sumField",0
_395:
	db	":TextField",0
_396:
	db	"detField",0
_397:
	db	":TextArea",0
_398:
	db	"NTypeLabel",0
_399:
	db	"summary",0
_400:
	db	"nType",0
_401:
	db	"details",0
_402:
	db	"date",0
_403:
	db	"_typeByInt",0
_404:
	db	"(i)$",0
	align	4
_392:
	dd	2
	dd	_393
	dd	3
	dd	_325
	dd	_360
	dd	8
	dd	3
	dd	_394
	dd	_395
	dd	12
	dd	3
	dd	_396
	dd	_397
	dd	16
	dd	3
	dd	_398
	dd	_326
	dd	20
	dd	3
	dd	_399
	dd	_304
	dd	24
	dd	3
	dd	_400
	dd	_307
	dd	28
	dd	3
	dd	_401
	dd	_304
	dd	32
	dd	3
	dd	_402
	dd	_304
	dd	36
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	6
	dd	_339
	dd	_299
	dd	48
	dd	6
	dd	_403
	dd	_404
	dd	52
	dd	6
	dd	_335
	dd	_299
	dd	56
	dd	6
	dd	_388
	dd	_299
	dd	60
	dd	6
	dd	_334
	dd	_299
	dd	64
	dd	0
	align	4
_bb_Note:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_392
	dd	40
	dd	__bb_Note_New
	dd	__bb_Note_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_Note__init
	dd	__bb_Note__typeByInt
	dd	__bb_Note__update
	dd	__bb_Note__save
	dd	__bb_Note__hide
_406:
	db	"Student",0
_407:
	db	"first",0
_408:
	db	"last",0
_409:
	db	"balance",0
_410:
	db	"ipadPos",0
_411:
	db	"grade",0
_412:
	db	"fnLabel",0
_413:
	db	"balLabel",0
_414:
	db	"grLabel",0
_415:
	db	"sipad",0
_416:
	db	":Ipad",0
_417:
	db	"fnEdit",0
_418:
	db	"lnEdit",0
_419:
	db	"gEdit",0
_420:
	db	"ipadAction",0
_421:
	db	"chargerFee",0
_422:
	db	"cableFee",0
_423:
	db	"caseFee",0
_424:
	db	"extraFee",0
_425:
	db	"lostIpad",0
_426:
	db	"stolenIpad",0
_427:
	db	"damagedIpad",0
_428:
	db	"iPadMenu",0
	align	4
_405:
	dd	2
	dd	_406
	dd	3
	dd	_325
	dd	_360
	dd	8
	dd	3
	dd	_361
	dd	_307
	dd	12
	dd	3
	dd	_407
	dd	_304
	dd	16
	dd	3
	dd	_408
	dd	_304
	dd	20
	dd	3
	dd	_409
	dd	_307
	dd	24
	dd	3
	dd	_410
	dd	_307
	dd	28
	dd	3
	dd	_411
	dd	_307
	dd	32
	dd	3
	dd	_381
	dd	_382
	dd	36
	dd	3
	dd	_412
	dd	_326
	dd	40
	dd	3
	dd	_413
	dd	_326
	dd	44
	dd	3
	dd	_414
	dd	_326
	dd	48
	dd	3
	dd	_386
	dd	_329
	dd	52
	dd	3
	dd	_415
	dd	_416
	dd	56
	dd	3
	dd	_417
	dd	_326
	dd	60
	dd	3
	dd	_418
	dd	_326
	dd	64
	dd	3
	dd	_419
	dd	_326
	dd	68
	dd	3
	dd	_420
	dd	_326
	dd	72
	dd	3
	dd	_421
	dd	_326
	dd	76
	dd	3
	dd	_422
	dd	_326
	dd	80
	dd	3
	dd	_423
	dd	_326
	dd	84
	dd	3
	dd	_424
	dd	_326
	dd	88
	dd	3
	dd	_425
	dd	_326
	dd	92
	dd	3
	dd	_426
	dd	_326
	dd	96
	dd	3
	dd	_427
	dd	_326
	dd	100
	dd	3
	dd	_428
	dd	_326
	dd	104
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	6
	dd	_339
	dd	_299
	dd	48
	dd	6
	dd	_333
	dd	_299
	dd	52
	dd	6
	dd	_334
	dd	_299
	dd	56
	dd	6
	dd	_387
	dd	_299
	dd	60
	dd	6
	dd	_388
	dd	_299
	dd	64
	dd	6
	dd	_389
	dd	_390
	dd	68
	dd	6
	dd	_335
	dd	_299
	dd	72
	dd	6
	dd	_391
	dd	_390
	dd	76
	dd	0
	align	4
_bb_Student:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_405
	dd	108
	dd	__bb_Student_New
	dd	__bb_Student_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_Student__init
	dd	__bb_Student__show
	dd	__bb_Student__hide
	dd	__bb_Student__build
	dd	__bb_Student__save
	dd	__bb_Student__addNote
	dd	__bb_Student__update
	dd	__bb_Student__noteBySummary
_430:
	db	"logWin",0
_431:
	db	"entryList",0
_432:
	db	"_populate",0
_433:
	db	"_end",0
	align	4
_429:
	dd	2
	dd	_430
	dd	3
	dd	_325
	dd	_360
	dd	8
	dd	3
	dd	_431
	dd	_382
	dd	12
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	6
	dd	_339
	dd	_299
	dd	48
	dd	6
	dd	_432
	dd	_299
	dd	52
	dd	6
	dd	_331
	dd	_354
	dd	56
	dd	6
	dd	_335
	dd	_299
	dd	60
	dd	6
	dd	_433
	dd	_299
	dd	64
	dd	0
	align	4
_bb_logWin:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_429
	dd	16
	dd	__bb_logWin_New
	dd	__bb_logWin_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_logWin__init
	dd	__bb_logWin__populate
	dd	__bb_logWin__new
	dd	__bb_logWin__update
	dd	__bb_logWin__end
_435:
	db	"MainWindow",0
_436:
	db	"snTF",0
_437:
	db	"bcBox",0
_438:
	db	":Button",0
_439:
	db	"snBox",0
_440:
	db	"sfnBox",0
_441:
	db	"slnBox",0
_442:
	db	"goBut",0
_443:
	db	"modeMenu",0
_444:
	db	"aboutMenu",0
_445:
	db	"helpMenu",0
_446:
	db	"qmFile",0
_447:
	db	"(:TEvent)i",0
	align	4
_434:
	dd	2
	dd	_435
	dd	3
	dd	_325
	dd	_360
	dd	8
	dd	3
	dd	_436
	dd	_395
	dd	12
	dd	3
	dd	_437
	dd	_438
	dd	16
	dd	3
	dd	_439
	dd	_438
	dd	20
	dd	3
	dd	_440
	dd	_438
	dd	24
	dd	3
	dd	_441
	dd	_438
	dd	28
	dd	3
	dd	_442
	dd	_438
	dd	32
	dd	3
	dd	_443
	dd	_326
	dd	36
	dd	3
	dd	_444
	dd	_326
	dd	40
	dd	3
	dd	_445
	dd	_326
	dd	44
	dd	3
	dd	_446
	dd	_326
	dd	48
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	6
	dd	_339
	dd	_299
	dd	48
	dd	6
	dd	_432
	dd	_299
	dd	52
	dd	6
	dd	_335
	dd	_447
	dd	56
	dd	6
	dd	_433
	dd	_299
	dd	60
	dd	0
	align	4
_bb_MainWindow:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_434
	dd	52
	dd	__bb_MainWindow_New
	dd	__bb_MainWindow_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_MainWindow__init
	dd	__bb_MainWindow__populate
	dd	__bb_MainWindow__update
	dd	__bb_MainWindow__end
_449:
	db	"TextArea",0
_450:
	db	"(i,i,i,i,:Window,i,i)i",0
	align	4
_448:
	dd	2
	dd	_449
	dd	3
	dd	_325
	dd	_326
	dd	8
	dd	6
	dd	_298
	dd	_299
	dd	16
	dd	6
	dd	_300
	dd	_299
	dd	20
	dd	6
	dd	_339
	dd	_450
	dd	48
	dd	6
	dd	_346
	dd	_347
	dd	52
	dd	6
	dd	_348
	dd	_299
	dd	56
	dd	0
	align	4
_bb_TextArea:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_448
	dd	12
	dd	__bb_TextArea_New
	dd	__bb_TextArea_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_TextArea__init
	dd	__bb_TextArea__getText
	dd	__bb_TextArea__clear
	align	4
_819:
	dd	_817
	dd	81
	dd	1
_822:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/Globals.bmx",0
	align	4
_821:
	dd	_822
	dd	9
	dd	1
	align	4
_824:
	dd	0
	align	4
_826:
	dd	_822
	dd	10
	dd	1
	align	4
_829:
	dd	_822
	dd	11
	dd	1
	align	4
_832:
	dd	_822
	dd	13
	dd	1
	align	4
_833:
	dd	_822
	dd	15
	dd	1
	align	4
_836:
	dd	_822
	dd	16
	dd	1
	align	4
_839:
	dd	_822
	dd	18
	dd	1
	align	4
_840:
	dd	_822
	dd	23
	dd	1
	align	4
_841:
	dd	_bbStringClass
	dd	2147483647
	dd	24
	dw	89,111,117,32,97,114,101,32,114,117,110,110,105,110,103,32
	dw	111,110,32,87,105,110,51,50
_843:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/Items/iPad.bmx",0
	align	4
_842:
	dd	_843
	dd	7
	dd	1
	align	4
_39:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	105,112,97,100,100,98,46,99,115,118
	align	4
_846:
	dd	_843
	dd	8
	dd	1
	align	4
_849:
	dd	_843
	dd	9
	dd	1
	align	4
_857:
	dd	3
	dd	0
	dd	0
	align	4
_851:
	dd	_843
	dd	10
	dd	2
	align	4
_852:
	dd	_843
	dd	11
	dd	2
_859:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/Items/Note.bmx",0
	align	4
_858:
	dd	_859
	dd	5
	dd	1
_863:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/Items/Student.bmx",0
	align	4
_862:
	dd	_863
	dd	6
	dd	1
	align	4
_169:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	115,116,117,100,100,98,46,99,115,118
	align	4
_866:
	dd	_863
	dd	7
	dd	1
	align	4
_869:
	dd	_863
	dd	8
	dd	1
	align	4
_877:
	dd	3
	dd	0
	dd	0
	align	4
_871:
	dd	_863
	dd	9
	dd	2
	align	4
_872:
	dd	_863
	dd	10
	dd	2
_879:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/Main.bmx",0
	align	4
_878:
	dd	_879
	dd	2
	dd	1
	align	4
_888:
	dd	3
	dd	0
	dd	0
	align	4
_881:
	dd	_879
	dd	3
	dd	2
	align	4
_36:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	52,51,50,49,120,103,51,116
	align	4
_257:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	80,97,115,115,119,111,114,100,58,32
	align	4
_887:
	dd	3
	dd	0
	dd	0
	align	4
_883:
	dd	_879
	dd	4
	dd	3
	align	4
_885:
	dd	_bbStringClass
	dd	2147483647
	dd	97
	dw	89,111,117,32,109,117,115,116,32,101,110,116,101,114,32,116
	dw	104,101,32,99,111,114,114,101,99,116,32,112,97,115,115,119
	dw	111,114,100,32,102,111,114,32,105,80,97,100,32,77,97,110
	dw	97,103,101,114,32,48,46,48,46,49,32,116,111,32,119,111
	dw	114,107,46,32,80,108,101,97,115,101,32,97,115,107,32,73
	dw	84,32,102,111,114,32,97,115,115,105,115,116,97,110,99,101
	dw	46
	align	4
_886:
	dd	_879
	dd	5
	dd	3
	align	4
_889:
	dd	_879
	dd	8
	dd	1
	align	4
_891:
	dd	_879
	dd	9
	dd	1
	align	4
_894:
	dd	_879
	dd	11
	dd	1
	align	4
_957:
	dd	3
	dd	0
	dd	0
	align	4
_895:
	dd	_879
	dd	12
	dd	2
	align	4
_896:
	dd	_879
	dd	13
	dd	2
	align	4
_901:
	dd	3
	dd	0
	dd	0
	align	4
_900:
	dd	_879
	dd	13
	dd	35
	align	4
_902:
	dd	_879
	dd	14
	dd	2
	align	4
_909:
	dd	3
	dd	0
	dd	0
	align	4
_906:
	dd	_879
	dd	15
	dd	3
	align	4
_910:
	dd	_879
	dd	17
	dd	2
_924:
	db	":iPad",0
	align	4
_923:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_924
	dd	-8
	dd	0
	align	4
_920:
	dd	_879
	dd	18
	dd	3
	align	4
_925:
	dd	_879
	dd	20
	dd	2
_939:
	db	"s",0
_940:
	db	":Student",0
	align	4
_938:
	dd	3
	dd	0
	dd	2
	dd	_939
	dd	_940
	dd	-12
	dd	0
	align	4
_935:
	dd	_879
	dd	21
	dd	3
	align	4
_941:
	dd	_879
	dd	23
	dd	2
_955:
	db	"e",0
_956:
	db	":Note",0
	align	4
_954:
	dd	3
	dd	0
	dd	2
	dd	_955
	dd	_956
	dd	-16
	dd	0
	align	4
_951:
	dd	_879
	dd	24
	dd	3
	align	4
_958:
	dd	_879
	dd	29
	dd	1
	align	4
_961:
	dd	_879
	dd	30
	dd	1
	align	4
_964:
	dd	_879
	dd	31
	dd	1
_1005:
	db	"Self",0
_1006:
	db	":z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0",0
	align	4
_1004:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_1006
	dd	-4
	dd	0
	align	4
_1003:
	dd	3
	dd	0
	dd	0
_1010:
	db	":z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea",0
	align	4
_1009:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_1010
	dd	-4
	dd	0
	align	4
_1008:
	dd	3
	dd	0
	dd	0
	align	4
_1013:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_985
	dd	-4
	dd	0
	align	4
_1012:
	dd	3
	dd	0
	dd	0
	align	4
_1020:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_360
	dd	-4
	dd	0
	align	4
_1019:
	dd	3
	dd	0
	dd	0
_1051:
	db	"x",0
_1052:
	db	"y",0
_1053:
	db	"w",0
_1054:
	db	"h",0
_1055:
	db	"title",0
_1056:
	db	"style",0
	align	4
_1050:
	dd	1
	dd	_331
	dd	2
	dd	_1005
	dd	_360
	dd	-4
	dd	2
	dd	_1051
	dd	_307
	dd	-8
	dd	2
	dd	_1052
	dd	_307
	dd	-12
	dd	2
	dd	_1053
	dd	_307
	dd	-16
	dd	2
	dd	_1054
	dd	_307
	dd	-20
	dd	2
	dd	_1055
	dd	_304
	dd	-24
	dd	2
	dd	_1056
	dd	_307
	dd	-28
	dd	0
_1031:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/GUI/Window.bmx",0
	align	4
_1030:
	dd	_1031
	dd	12
	dd	3
	align	4
_1039:
	dd	_1031
	dd	13
	dd	3
	align	4
_1040:
	dd	_1031
	dd	14
	dd	3
	align	4
_1060:
	dd	1
	dd	_333
	dd	2
	dd	_1005
	dd	_360
	dd	-4
	dd	0
	align	4
_1057:
	dd	_1031
	dd	17
	dd	3
	align	4
_1064:
	dd	1
	dd	_334
	dd	2
	dd	_1005
	dd	_360
	dd	-4
	dd	0
	align	4
_1061:
	dd	_1031
	dd	20
	dd	3
	align	4
_1065:
	dd	1
	dd	_335
	dd	2
	dd	_1005
	dd	_360
	dd	-4
	dd	0
	align	4
_1068:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_438
	dd	-4
	dd	0
	align	4
_1067:
	dd	3
	dd	0
	dd	0
_1089:
	db	"label",0
_1090:
	db	"win",0
	align	4
_1088:
	dd	1
	dd	_339
	dd	2
	dd	_1005
	dd	_438
	dd	-4
	dd	2
	dd	_1089
	dd	_304
	dd	-8
	dd	2
	dd	_1051
	dd	_307
	dd	-12
	dd	2
	dd	_1052
	dd	_307
	dd	-16
	dd	2
	dd	_1053
	dd	_307
	dd	-20
	dd	2
	dd	_1054
	dd	_307
	dd	-24
	dd	2
	dd	_1090
	dd	_360
	dd	-28
	dd	2
	dd	_1056
	dd	_307
	dd	-32
	dd	0
_1073:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/GUI/Button.bmx",0
	align	4
_1072:
	dd	_1073
	dd	9
	dd	3
	align	4
_1083:
	dd	_1073
	dd	10
	dd	3
	align	4
_1105:
	dd	1
	dd	_341
	dd	2
	dd	_1005
	dd	_438
	dd	-4
	dd	0
	align	4
_1091:
	dd	_1073
	dd	13
	dd	3
	align	4
_1097:
	dd	_1073
	dd	14
	dd	3
	align	4
_1102:
	dd	_1073
	dd	15
	dd	3
	align	4
_1109:
	dd	1
	dd	_342
	dd	2
	dd	_1005
	dd	_438
	dd	-4
	dd	0
	align	4
_1106:
	dd	_1073
	dd	18
	dd	3
_1117:
	db	"newButton",0
_1118:
	db	"retButton",0
	align	4
_1116:
	dd	1
	dd	_1117
	dd	2
	dd	_1089
	dd	_304
	dd	-4
	dd	2
	dd	_1051
	dd	_307
	dd	-8
	dd	2
	dd	_1052
	dd	_307
	dd	-12
	dd	2
	dd	_1053
	dd	_307
	dd	-16
	dd	2
	dd	_1054
	dd	_307
	dd	-20
	dd	2
	dd	_1090
	dd	_360
	dd	-24
	dd	2
	dd	_1056
	dd	_307
	dd	-28
	dd	2
	dd	_1118
	dd	_438
	dd	-32
	dd	0
	align	4
_1110:
	dd	_1073
	dd	23
	dd	2
	align	4
_1112:
	dd	_1073
	dd	24
	dd	2
	align	4
_1115:
	dd	_1073
	dd	25
	dd	2
	align	4
_1121:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_395
	dd	-4
	dd	0
	align	4
_1120:
	dd	3
	dd	0
	dd	0
_1142:
	db	"mask",0
	align	4
_1141:
	dd	1
	dd	_339
	dd	2
	dd	_1005
	dd	_395
	dd	-4
	dd	2
	dd	_1051
	dd	_307
	dd	-8
	dd	2
	dd	_1052
	dd	_307
	dd	-12
	dd	2
	dd	_1053
	dd	_307
	dd	-16
	dd	2
	dd	_1054
	dd	_307
	dd	-20
	dd	2
	dd	_1090
	dd	_360
	dd	-24
	dd	2
	dd	_1142
	dd	_307
	dd	-28
	dd	0
_1126:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/GUI/TextField.bmx",0
	align	4
_1125:
	dd	_1126
	dd	8
	dd	3
	align	4
_1136:
	dd	_1126
	dd	9
	dd	3
	align	4
_1146:
	dd	1
	dd	_346
	dd	2
	dd	_1005
	dd	_395
	dd	-4
	dd	0
	align	4
_1143:
	dd	_1126
	dd	12
	dd	3
	align	4
_1150:
	dd	1
	dd	_348
	dd	2
	dd	_1005
	dd	_395
	dd	-4
	dd	0
	align	4
_1147:
	dd	_1126
	dd	15
	dd	3
_1158:
	db	"newTextField",0
_1159:
	db	"retTF",0
	align	4
_1157:
	dd	1
	dd	_1158
	dd	2
	dd	_1051
	dd	_307
	dd	-4
	dd	2
	dd	_1052
	dd	_307
	dd	-8
	dd	2
	dd	_1053
	dd	_307
	dd	-12
	dd	2
	dd	_1054
	dd	_307
	dd	-16
	dd	2
	dd	_1090
	dd	_360
	dd	-20
	dd	2
	dd	_1142
	dd	_307
	dd	-24
	dd	2
	dd	_1159
	dd	_395
	dd	-28
	dd	0
	align	4
_1151:
	dd	_1126
	dd	20
	dd	2
	align	4
_1153:
	dd	_1126
	dd	21
	dd	2
	align	4
_1156:
	dd	_1126
	dd	22
	dd	2
_1162:
	db	":ComboBox",0
	align	4
_1161:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_1162
	dd	-4
	dd	0
	align	4
_1160:
	dd	3
	dd	0
	dd	0
	align	4
_1166:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_382
	dd	-4
	dd	0
	align	4
_1165:
	dd	3
	dd	0
	dd	0
	align	4
_1186:
	dd	1
	dd	_339
	dd	2
	dd	_1005
	dd	_382
	dd	-4
	dd	2
	dd	_1051
	dd	_307
	dd	-8
	dd	2
	dd	_1052
	dd	_307
	dd	-12
	dd	2
	dd	_1053
	dd	_307
	dd	-16
	dd	2
	dd	_1054
	dd	_307
	dd	-20
	dd	2
	dd	_1090
	dd	_360
	dd	-24
	dd	2
	dd	_1056
	dd	_307
	dd	-28
	dd	0
_1171:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/GUI/ListBox.bmx",0
	align	4
_1170:
	dd	_1171
	dd	8
	dd	3
	align	4
_1181:
	dd	_1171
	dd	9
	dd	3
_1191:
	db	"item",0
_1192:
	db	"df",0
	align	4
_1190:
	dd	1
	dd	_353
	dd	2
	dd	_1005
	dd	_382
	dd	-4
	dd	2
	dd	_1191
	dd	_304
	dd	-8
	dd	2
	dd	_1192
	dd	_307
	dd	-12
	dd	0
	align	4
_1187:
	dd	_1171
	dd	12
	dd	3
	align	4
_1198:
	dd	1
	dd	_355
	dd	2
	dd	_1005
	dd	_382
	dd	-4
	dd	2
	dd	_1191
	dd	_304
	dd	-8
	dd	0
	align	4
_1193:
	dd	_1171
	dd	15
	dd	3
	align	4
_1204:
	dd	1
	dd	_357
	dd	2
	dd	_1005
	dd	_382
	dd	-4
	dd	0
	align	4
_1199:
	dd	_1171
	dd	18
	dd	3
	align	4
_1210:
	dd	1
	dd	_346
	dd	2
	dd	_1005
	dd	_382
	dd	-4
	dd	0
	align	4
_1205:
	dd	_1171
	dd	21
	dd	3
_1218:
	db	"newListBox",0
_1219:
	db	"retLB",0
	align	4
_1217:
	dd	1
	dd	_1218
	dd	2
	dd	_1051
	dd	_307
	dd	-4
	dd	2
	dd	_1052
	dd	_307
	dd	-8
	dd	2
	dd	_1053
	dd	_307
	dd	-12
	dd	2
	dd	_1054
	dd	_307
	dd	-16
	dd	2
	dd	_1090
	dd	_360
	dd	-20
	dd	2
	dd	_1056
	dd	_307
	dd	-24
	dd	2
	dd	_1219
	dd	_382
	dd	-28
	dd	0
	align	4
_1211:
	dd	_1171
	dd	26
	dd	2
	align	4
_1213:
	dd	_1171
	dd	27
	dd	2
	align	4
_1216:
	dd	_1171
	dd	28
	dd	2
	align	4
_1246:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_924
	dd	-4
	dd	0
	align	4
_1245:
	dd	3
	dd	0
	dd	0
	align	4
_1474:
	dd	1
	dd	_339
	dd	2
	dd	_1005
	dd	_924
	dd	-4
	dd	0
	align	4
_1298:
	dd	_843
	dd	37
	dd	3
	align	4
_40:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	83,116,117,100,101,110,116,32,105,80,97,100
	align	4
_1303:
	dd	_843
	dd	38
	dd	3
	align	4
_1304:
	dd	_843
	dd	39
	dd	3
	align	4
_1314:
	dd	_843
	dd	41
	dd	3
	align	4
_41:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	69,100,105,116
	align	4
_1326:
	dd	_843
	dd	42
	dd	4
	align	4
_42:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	66,97,114,99,111,100,101
	align	4
_1336:
	dd	_843
	dd	43
	dd	4
	align	4
_43:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	83,101,114,105,97,108
	align	4
_1346:
	dd	_843
	dd	44
	dd	3
	align	4
_44:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	65,99,116,105,111,110
	align	4
_1358:
	dd	_843
	dd	45
	dd	4
	align	4
_45:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	67,104,101,99,107,32,79,117,116
	align	4
_1368:
	dd	_843
	dd	46
	dd	4
	align	4
_1371:
	dd	_843
	dd	47
	dd	4
	align	4
_46:
	dd	_bbStringClass
	dd	2147483647
	dd	13
	dw	82,101,112,111,114,116,32,68,97,109,97,103,101
	align	4
_1381:
	dd	_843
	dd	48
	dd	4
	align	4
_47:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	83,101,110,100,32,102,111,114,32,82,101,112,97,105,114
	align	4
_1391:
	dd	_843
	dd	49
	dd	4
	align	4
_48:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	77,97,114,107,32,97,115,32,76,111,115,116
	align	4
_1401:
	dd	_843
	dd	50
	dd	4
	align	4
_49:
	dd	_bbStringClass
	dd	2147483647
	dd	14
	dw	77,97,114,107,32,97,115,32,83,116,111,108,101,110
	align	4
_1411:
	dd	_843
	dd	53
	dd	3
	align	4
_50:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	99,111,110,116,101,120,116,77,101,110,117
	align	4
_1419:
	dd	_843
	dd	54
	dd	4
	align	4
_1429:
	dd	_843
	dd	55
	dd	4
	align	4
_51:
	dd	_bbStringClass
	dd	2147483647
	dd	3
	dw	78,101,119
	align	4
_1439:
	dd	_843
	dd	56
	dd	3
	align	4
_52:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	73,110,102,111,114,109,97,116,105,111,110
	align	4
_1449:
	dd	_843
	dd	57
	dd	4
	align	4
_1459:
	dd	_843
	dd	58
	dd	4
	align	4
_53:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	79,116,104,101,114
	align	4
_1469:
	dd	_843
	dd	62
	dd	3
	align	4
_1480:
	dd	1
	dd	_333
	dd	2
	dd	_1005
	dd	_924
	dd	-4
	dd	0
	align	4
_1475:
	dd	_843
	dd	66
	dd	3
	align	4
_1601:
	dd	1
	dd	_387
	dd	2
	dd	_1005
	dd	_924
	dd	-4
	dd	0
	align	4
_1481:
	dd	_843
	dd	70
	dd	3
_1501:
	db	"nNote",0
	align	4
_1500:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_304
	dd	-8
	dd	2
	dd	_1501
	dd	_956
	dd	-12
	dd	0
	align	4
_1493:
	dd	_843
	dd	71
	dd	4
	align	4
_1495:
	dd	_843
	dd	72
	dd	4
	align	4
_1502:
	dd	_843
	dd	74
	dd	3
	align	4
_57:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	66,97,114,99,111,100,101,58,32
	align	4
_1516:
	dd	_843
	dd	75
	dd	3
	align	4
_58:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	83,101,114,105,97,108,58,32
	align	4
_1530:
	dd	_843
	dd	76
	dd	3
	align	4
_59:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	83,116,97,116,101,58,32
	align	4
_1544:
	dd	_843
	dd	78
	dd	3
	align	4
_60:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	82,101,103,105,115,116,101,114,101,100
	align	4
_61:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	67,104,101,99,107,101,100,32,73,110
	align	4
_62:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	67,104,101,99,107,101,100,32,79,117,116
	align	4
_65:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	83,101,110,116,32,102,111,114,32,82,101,112,97,105,114
	align	4
_66:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	68,97,109,97,103,101,100
	align	4
_67:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	76,111,115,116
	align	4
_68:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	83,116,111,108,101,110
	align	4
_1559:
	dd	3
	dd	0
	dd	0
	align	4
_1556:
	dd	_843
	dd	80
	dd	5
	align	4
_1563:
	dd	3
	dd	0
	dd	0
	align	4
_1560:
	dd	_843
	dd	82
	dd	5
	align	4
_1579:
	dd	3
	dd	0
	dd	0
	align	4
_1564:
	dd	_843
	dd	84
	dd	5
	align	4
_63:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	67,104,101,99,107,32,73,110
	align	4
_1567:
	dd	_843
	dd	85
	dd	5
	align	4
_64:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	79,119,110,101,114
	align	4
_1583:
	dd	3
	dd	0
	dd	0
	align	4
_1580:
	dd	_843
	dd	87
	dd	5
	align	4
_1587:
	dd	3
	dd	0
	dd	0
	align	4
_1584:
	dd	_843
	dd	90
	dd	5
	align	4
_1591:
	dd	3
	dd	0
	dd	0
	align	4
_1588:
	dd	_843
	dd	92
	dd	5
	align	4
_1595:
	dd	3
	dd	0
	dd	0
	align	4
_1592:
	dd	_843
	dd	95
	dd	5
	align	4
_1596:
	dd	_843
	dd	99
	dd	3
	align	4
_1657:
	dd	1
	dd	_334
	dd	2
	dd	_1005
	dd	_924
	dd	-4
	dd	0
	align	4
_1602:
	dd	_843
	dd	103
	dd	3
	align	4
_1607:
	dd	3
	dd	0
	dd	0
	align	4
_1604:
	dd	_843
	dd	104
	dd	4
	align	4
_1615:
	dd	3
	dd	0
	dd	0
	align	4
_1609:
	dd	_843
	dd	105
	dd	3
	align	4
_69:
	dd	_bbStringClass
	dd	2147483647
	dd	58
	dw	87,111,117,108,100,32,121,111,117,32,108,105,107,101,32,116
	dw	111,32,119,114,105,116,101,32,121,111,117,114,32,99,104,97
	dw	110,103,101,115,32,98,97,99,107,32,116,111,32,116,104,101
	dw	32,100,97,116,97,98,97,115,101,63
	align	4
_1614:
	dd	3
	dd	0
	dd	0
	align	4
_1611:
	dd	_843
	dd	106
	dd	4
	align	4
_1616:
	dd	_843
	dd	108
	dd	3
	align	4
_1621:
	dd	_843
	dd	109
	dd	3
	align	4
_1637:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_326
	dd	-8
	dd	0
	align	4
_1635:
	dd	_843
	dd	110
	dd	4
	align	4
_1636:
	dd	_843
	dd	111
	dd	4
	align	4
_1638:
	dd	_843
	dd	113
	dd	3
	align	4
_1643:
	dd	_843
	dd	114
	dd	3
	align	4
_1648:
	dd	_843
	dd	115
	dd	3
	align	4
_1656:
	dd	_843
	dd	116
	dd	3
_1691:
	db	"line",0
	align	4
_1690:
	dd	1
	dd	_388
	dd	2
	dd	_1005
	dd	_924
	dd	-4
	dd	2
	dd	_1691
	dd	_304
	dd	-8
	dd	0
	align	4
_1658:
	dd	_843
	dd	120
	dd	3
	align	4
_73:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	44
	align	4
_1666:
	dd	_843
	dd	121
	dd	3
	align	4
_1687:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_956
	dd	-12
	dd	0
	align	4
_1678:
	dd	_843
	dd	122
	dd	4
	align	4
_78:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	101,111,110,44
	align	4
_77:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	59
	align	4
_1688:
	dd	_843
	dd	124
	dd	3
	align	4
_79:
	dd	_bbStringClass
	dd	2147483647
	dd	3
	dw	101,111,101
	align	4
_1689:
	dd	_843
	dd	125
	dd	3
_1766:
	db	"command",0
_1767:
	db	"lineArray",0
	align	4
_1765:
	dd	1
	dd	_389
	dd	2
	dd	_1005
	dd	_924
	dd	-4
	dd	2
	dd	_1766
	dd	_304
	dd	-8
	dd	2
	dd	_1767
	dd	_329
	dd	-12
	dd	2
	dd	_307
	dd	_307
	dd	-16
	dd	2
	dd	_1501
	dd	_956
	dd	-20
	dd	0
	align	4
_1692:
	dd	_843
	dd	128
	dd	3
	align	4
_1694:
	dd	_843
	dd	129
	dd	3
	align	4
_1696:
	dd	_843
	dd	130
	dd	3
	align	4
_1698:
	dd	_843
	dd	131
	dd	3
_1763:
	db	"n",0
	align	4
_1762:
	dd	3
	dd	0
	dd	2
	dd	_1763
	dd	_304
	dd	-24
	dd	0
	align	4
_1708:
	dd	_843
	dd	132
	dd	4
	align	4
_1722:
	dd	3
	dd	0
	dd	0
	align	4
_1710:
	dd	_843
	dd	133
	dd	5
	align	4
_1711:
	dd	_843
	dd	134
	dd	5
	align	4
_1714:
	dd	_843
	dd	135
	dd	5
	align	4
_1723:
	dd	_843
	dd	137
	dd	4
	align	4
_1742:
	dd	3
	dd	0
	dd	0
	align	4
_1725:
	dd	_843
	dd	138
	dd	5
	align	4
_1733:
	dd	_843
	dd	139
	dd	5
	align	4
_32:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	32
	align	4
_1743:
	dd	_843
	dd	141
	dd	4
	align	4
_1749:
	dd	3
	dd	0
	dd	0
	align	4
_1745:
	dd	_843
	dd	142
	dd	5
	align	4
_1750:
	dd	_843
	dd	144
	dd	4
	align	4
_1760:
	dd	3
	dd	0
	dd	0
	align	4
_1752:
	dd	_843
	dd	145
	dd	5
	align	4
_1761:
	dd	_843
	dd	147
	dd	4
	align	4
_1764:
	dd	_843
	dd	151
	dd	3
	align	4
_2205:
	dd	1
	dd	_335
	dd	2
	dd	_1005
	dd	_924
	dd	-4
	dd	0
	align	4
_1768:
	dd	_843
	dd	154
	dd	3
_1856:
	db	"conf",0
	align	4
_1855:
	dd	3
	dd	0
	dd	2
	dd	_1856
	dd	_307
	dd	-8
	dd	0
	align	4
_1817:
	dd	_843
	dd	156
	dd	5
	align	4
_1819:
	dd	_843
	dd	157
	dd	5
	align	4
_1822:
	dd	3
	dd	0
	dd	0
	align	4
_1821:
	dd	_843
	dd	158
	dd	6
	align	4
_1827:
	dd	3
	dd	0
	dd	0
	align	4
_1824:
	dd	_843
	dd	160
	dd	6
	align	4
_85:
	dd	_bbStringClass
	dd	2147483647
	dd	188
	dw	84,104,105,115,32,119,105,108,108,32,99,104,97,110,103,101
	dw	32,116,104,101,32,73,68,32,105,110,32,116,104,101,32,100
	dw	97,116,97,98,97,115,101,32,97,110,100,32,121,111,117,32
	dw	119,105,108,108,32,110,111,32,108,111,110,103,101,114,32,102
	dw	105,110,100,32,116,104,101,32,105,80,97,100,32,119,105,116
	dw	104,32,116,104,101,32,111,108,100,32,66,97,114,99,111,100
	dw	101,46,32,65,108,115,111,32,105,102,32,121,111,117,32,112
	dw	114,111,118,105,100,101,32,97,110,32,97,108,114,101,97,100
	dw	121,32,117,115,101,100,32,98,97,114,99,111,100,101,32,116
	dw	104,105,115,32,101,110,116,114,121,32,119,105,108,108,32,111
	dw	118,101,114,119,114,105,116,101,32,116,104,101,32,112,114,101
	dw	118,105,111,117,115,32,101,110,116,114,121,46
	align	4
_37:
	dd	_bbStringClass
	dd	2147483647
	dd	147
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	dw	32,32,32
	align	4
_84:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	63
	align	4
_83:
	dd	_bbStringClass
	dd	2147483647
	dd	58
	dw	65,114,101,32,121,111,117,32,115,117,114,101,32,116,104,97
	dw	116,32,121,111,117,32,119,97,110,116,32,116,111,32,99,104
	dw	97,110,103,101,32,116,104,101,32,66,97,114,99,111,100,101
	dw	32,102,111,114,32,105,80,97,100,32
	align	4
_1828:
	dd	_843
	dd	162
	dd	5
_1854:
	db	"tbc",0
	align	4
_1853:
	dd	3
	dd	0
	dd	2
	dd	_1854
	dd	_304
	dd	-12
	dd	0
	align	4
_1830:
	dd	_843
	dd	163
	dd	6
	align	4
_86:
	dd	_bbStringClass
	dd	2147483647
	dd	24
	dw	80,108,101,97,115,101,32,101,110,116,101,114,32,110,101,119
	dw	32,66,97,114,99,111,100,101
	align	4
_1832:
	dd	_843
	dd	164
	dd	6
	align	4
_1852:
	dd	3
	dd	0
	dd	0
	align	4
_1834:
	dd	_843
	dd	165
	dd	7
	align	4
_90:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	93,59,101,111,110
	align	4
_89:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	93,32,116,111,32,91
	align	4
_88:
	dd	_bbStringClass
	dd	2147483647
	dd	34
	dw	93,59,49,59,84,104,101,32,98,97,114,99,111,100,101,32
	dw	119,97,115,32,99,104,97,110,103,101,100,32,102,114,111,109
	dw	32,91
	align	4
_87:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	67,104,97,110,103,101,100,32,66,97,114,99,111,100,101,59
	dw	91
	align	4
_1839:
	dd	_843
	dd	166
	dd	7
	align	4
_1847:
	dd	_843
	dd	167
	dd	7
	align	4
_1895:
	dd	3
	dd	0
	dd	2
	dd	_1856
	dd	_307
	dd	-16
	dd	0
	align	4
_1857:
	dd	_843
	dd	171
	dd	5
	align	4
_1859:
	dd	_843
	dd	172
	dd	5
	align	4
_1862:
	dd	3
	dd	0
	dd	0
	align	4
_1861:
	dd	_843
	dd	173
	dd	6
	align	4
_1867:
	dd	3
	dd	0
	dd	0
	align	4
_1864:
	dd	_843
	dd	175
	dd	6
	align	4
_92:
	dd	_bbStringClass
	dd	2147483647
	dd	204
	dw	84,104,105,115,32,105,115,32,100,97,110,103,101,114,111,117
	dw	115,32,97,110,100,32,119,105,108,108,32,99,104,97,110,103
	dw	101,32,116,104,101,32,73,68,32,105,110,32,116,104,101,32
	dw	100,97,116,97,98,97,115,101,32,97,110,100,32,121,111,117
	dw	32,119,105,108,108,32,110,111,32,108,111,110,103,101,114,32
	dw	102,105,110,100,32,116,104,101,32,105,80,97,100,32,119,105
	dw	116,104,32,116,104,101,32,111,108,100,32,115,101,114,105,97
	dw	108,46,32,65,108,115,111,32,105,102,32,121,111,117,32,112
	dw	114,111,118,105,100,101,32,97,110,32,97,108,114,101,97,100
	dw	121,32,117,115,101,100,32,98,97,114,99,111,100,101,32,116
	dw	104,105,115,32,101,110,116,114,121,32,119,105,108,108,32,111
	dw	118,101,114,119,114,105,116,101,32,116,104,101,32,112,114,101
	dw	118,105,111,117,115,32,101,110,116,114,121,46
	align	4
_91:
	dd	_bbStringClass
	dd	2147483647
	dd	57
	dw	65,114,101,32,121,111,117,32,115,117,114,101,32,116,104,97
	dw	116,32,121,111,117,32,119,97,110,116,32,116,111,32,99,104
	dw	97,110,103,101,32,116,104,101,32,83,101,114,105,97,108,32
	dw	102,111,114,32,105,80,97,100,32
	align	4
_1868:
	dd	_843
	dd	177
	dd	5
_1894:
	db	"tsn",0
	align	4
_1893:
	dd	3
	dd	0
	dd	2
	dd	_1894
	dd	_304
	dd	-20
	dd	0
	align	4
_1870:
	dd	_843
	dd	178
	dd	6
	align	4
_93:
	dd	_bbStringClass
	dd	2147483647
	dd	23
	dw	80,108,101,97,115,101,32,101,110,116,101,114,32,110,101,119
	dw	32,83,101,114,105,97,108
	align	4
_1872:
	dd	_843
	dd	179
	dd	6
	align	4
_1892:
	dd	3
	dd	0
	dd	0
	align	4
_1874:
	dd	_843
	dd	180
	dd	7
	align	4
_95:
	dd	_bbStringClass
	dd	2147483647
	dd	33
	dw	93,59,49,59,84,104,101,32,115,101,114,105,97,108,32,119
	dw	97,115,32,99,104,97,110,103,101,100,32,102,114,111,109,32
	dw	91
	align	4
_94:
	dd	_bbStringClass
	dd	2147483647
	dd	16
	dw	67,104,97,110,103,101,100,32,83,101,114,105,97,108,59,91
	align	4
_1879:
	dd	_843
	dd	181
	dd	7
	align	4
_1887:
	dd	_843
	dd	182
	dd	7
	align	4
_1902:
	dd	3
	dd	0
	dd	0
	align	4
_1896:
	dd	_843
	dd	186
	dd	5
	align	4
_1901:
	dd	3
	dd	0
	dd	0
	align	4
_1900:
	dd	_843
	dd	187
	dd	6
	align	4
_96:
	dd	_bbStringClass
	dd	2147483647
	dd	22
	dw	83,72,79,87,32,79,87,78,69,82,32,65,67,84,73,79
	dw	78,32,72,69,82,69
	align	4
_1930:
	dd	3
	dd	0
	dd	2
	dd	_1501
	dd	_956
	dd	-24
	dd	0
	align	4
_1903:
	dd	_843
	dd	190
	dd	5
	align	4
_100:
	dd	_bbStringClass
	dd	2147483647
	dd	61
	dw	93,46,32,80,108,101,97,115,101,32,97,100,100,32,97,110
	dw	121,32,101,120,116,114,97,32,110,111,116,101,115,32,100,101
	dw	115,99,114,105,98,105,110,103,32,116,104,101,32,100,97,109
	dw	97,103,101,32,104,101,114,101,46,59,101,111,110
	align	4
_99:
	dd	_bbStringClass
	dd	2147483647
	dd	80
	dw	46,32,84,104,101,32,111,119,110,101,114,32,114,101,115,112
	dw	111,110,115,105,98,108,101,32,119,97,115,32,91,73,78,83
	dw	69,82,84,32,83,84,85,68,69,78,84,32,78,65,77,69
	dw	32,72,69,82,69,93,46,32,84,104,101,32,112,114,101,118
	dw	105,111,117,115,32,115,116,97,116,101,32,119,97,115,32,91
	align	4
_98:
	dd	_bbStringClass
	dd	2147483647
	dd	43
	dw	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	dw	32,114,101,112,111,114,116,101,100,32,100,97,109,97,103,101
	dw	100,32,116,111,32,117,115,32,97,116,32
	align	4
_97:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	105,80,97,100,32,119,97,115,32,68,97,109,97,103,101,100
	dw	59,91
	align	4
_1909:
	dd	_843
	dd	191
	dd	5
	align	4
_1917:
	dd	_843
	dd	192
	dd	5
	align	4
_1922:
	dd	_843
	dd	193
	dd	5
	align	4
_1925:
	dd	_843
	dd	194
	dd	5
_1952:
	db	"po",0
	align	4
_1951:
	dd	3
	dd	0
	dd	2
	dd	_1952
	dd	_304
	dd	-28
	dd	0
	align	4
_1931:
	dd	_843
	dd	196
	dd	5
	align	4
_101:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	65,112,112,108,101,32,80,79,32,78,117,109,98,101,114
	align	4
_1933:
	dd	_843
	dd	197
	dd	5
	align	4
_105:
	dd	_bbStringClass
	dd	2147483647
	dd	26
	dw	46,32,84,104,101,32,112,114,101,118,105,111,117,115,32,115
	dw	116,97,116,101,32,119,97,115,32,91
	align	4
_104:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	32,119,105,116,104,32,80,46,79,46,32,110,117,109,98,101
	dw	114,32
	align	4
_103:
	dd	_bbStringClass
	dd	2147483647
	dd	36
	dw	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	dw	32,115,101,110,116,32,102,111,114,32,114,101,112,97,105,114
	dw	32,97,116,32
	align	4
_102:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	83,101,110,116,32,102,111,114,32,82,101,112,97,105,114,59
	dw	91
	align	4
_1938:
	dd	_843
	dd	198
	dd	5
	align	4
_1946:
	dd	_843
	dd	199
	dd	5
	align	4
_2022:
	dd	3
	dd	0
	dd	0
	align	4
_1953:
	dd	_843
	dd	201
	dd	5
	align	4
_1990:
	dd	3
	dd	0
	dd	0
	align	4
_1957:
	dd	_843
	dd	202
	dd	6
	align	4
_108:
	dd	_bbStringClass
	dd	2147483647
	dd	56
	dw	32,116,111,32,91,73,78,83,69,82,84,32,83,84,85,68
	dw	69,78,84,32,78,65,77,69,32,72,69,82,69,93,46,32
	dw	84,104,101,32,112,114,101,118,105,111,117,115,32,115,116,97
	dw	116,101,32,119,97,115,32,91
	align	4
_107:
	dd	_bbStringClass
	dd	2147483647
	dd	32
	dw	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	dw	32,99,104,101,99,107,101,100,32,111,117,116,32,97,116,32
	align	4
_106:
	dd	_bbStringClass
	dd	2147483647
	dd	13
	dw	67,104,101,99,107,101,100,32,79,117,116,59,91
	align	4
_1962:
	dd	_843
	dd	203
	dd	6
	align	4
_1970:
	dd	_843
	dd	204
	dd	6
	align	4
_1975:
	dd	_843
	dd	205
	dd	6
	align	4
_1978:
	dd	_843
	dd	206
	dd	6
	align	4
_2016:
	dd	3
	dd	0
	dd	0
	align	4
_1992:
	dd	_843
	dd	208
	dd	6
	align	4
_111:
	dd	_bbStringClass
	dd	2147483647
	dd	58
	dw	32,102,114,111,109,32,91,73,78,83,69,82,84,32,83,84
	dw	85,68,69,78,84,32,78,65,77,69,32,72,69,82,69,93
	dw	46,32,84,104,101,32,112,114,101,118,105,111,117,115,32,115
	dw	116,97,116,101,32,119,97,115,32,91
	align	4
_110:
	dd	_bbStringClass
	dd	2147483647
	dd	31
	dw	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	dw	32,99,104,101,99,107,101,100,32,105,110,32,97,116,32
	align	4
_109:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	67,104,101,99,107,101,100,32,73,110,59,91
	align	4
_1997:
	dd	_843
	dd	209
	dd	6
	align	4
_2005:
	dd	_843
	dd	210
	dd	6
	align	4
_2010:
	dd	_843
	dd	211
	dd	6
	align	4
_2013:
	dd	_843
	dd	212
	dd	6
	align	4
_2017:
	dd	_843
	dd	214
	dd	5
	align	4
_2041:
	dd	3
	dd	0
	dd	0
	align	4
_2023:
	dd	_843
	dd	216
	dd	5
	align	4
_114:
	dd	_bbStringClass
	dd	2147483647
	dd	66
	dw	32,98,121,32,116,104,101,32,111,119,110,101,114,32,91,73
	dw	78,83,69,82,84,32,83,84,85,68,69,78,84,32,78,65
	dw	77,69,32,72,69,82,69,93,46,32,84,104,101,32,112,114
	dw	101,118,105,111,117,115,32,115,116,97,116,101,32,119,97,115
	dw	32,91
	align	4
_113:
	dd	_bbStringClass
	dd	2147483647
	dd	40
	dw	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	dw	32,114,101,112,111,114,116,101,100,32,108,111,115,116,32,116
	dw	111,32,117,115,32,97,116,32
	align	4
_112:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	105,80,97,100,32,119,97,115,32,76,111,115,116,59,91
	align	4
_2028:
	dd	_843
	dd	217
	dd	5
	align	4
_2036:
	dd	_843
	dd	218
	dd	5
_2063:
	db	"pr",0
	align	4
_2062:
	dd	3
	dd	0
	dd	2
	dd	_2063
	dd	_304
	dd	-32
	dd	0
	align	4
_2042:
	dd	_843
	dd	220
	dd	5
	align	4
_115:
	dd	_bbStringClass
	dd	2147483647
	dd	30
	dw	80,111,108,105,99,101,32,82,101,112,111,114,116,32,82,101
	dw	102,101,114,101,110,99,101,32,78,117,109,98,101,114
	align	4
_2044:
	dd	_843
	dd	221
	dd	5
	align	4
_119:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	59,101,111,110
	align	4
_118:
	dd	_bbStringClass
	dd	2147483647
	dd	41
	dw	93,46,32,84,104,101,32,112,111,108,105,99,101,32,114,101
	dw	112,111,114,116,32,114,101,102,101,114,101,110,99,101,32,110
	dw	117,109,98,101,114,32,105,115,32
	align	4
_117:
	dd	_bbStringClass
	dd	2147483647
	dd	42
	dw	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	dw	32,114,101,112,111,114,116,101,100,32,115,116,111,108,101,110
	dw	32,116,111,32,117,115,32,97,116,32
	align	4
_116:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	105,80,97,100,32,119,97,115,32,83,116,111,108,101,110,59
	dw	91
	align	4
_2049:
	dd	_843
	dd	222
	dd	5
	align	4
_2057:
	dd	_843
	dd	223
	dd	5
	align	4
_2085:
	dd	3
	dd	0
	dd	0
	align	4
_2064:
	dd	_843
	dd	226
	dd	5
	align	4
_2076:
	dd	3
	dd	0
	dd	0
	align	4
_2069:
	dd	_843
	dd	228
	dd	7
	align	4
_2084:
	dd	3
	dd	0
	dd	0
	align	4
_2077:
	dd	_843
	dd	230
	dd	7
	align	4
_2100:
	dd	3
	dd	0
	dd	0
	align	4
_2086:
	dd	_843
	dd	234
	dd	5
	align	4
_2099:
	dd	3
	dd	0
	dd	0
	align	4
_2092:
	dd	_843
	dd	235
	dd	6
	align	4
_2108:
	dd	3
	dd	0
	dd	2
	dd	_1501
	dd	_956
	dd	-36
	dd	0
	align	4
_2101:
	dd	_843
	dd	238
	dd	5
	align	4
_121:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	93,59,48,59,78,101,119,32,68,101,116,97,105,108,115,59
	dw	101,111,110
	align	4
_120:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	78,101,119,32,78,111,116,101,59,91
	align	4
_2105:
	dd	_843
	dd	239
	dd	5
	align	4
_2116:
	dd	3
	dd	0
	dd	2
	dd	_1501
	dd	_956
	dd	-40
	dd	0
	align	4
_2109:
	dd	_843
	dd	241
	dd	5
	align	4
_122:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	93,59,49,59,78,101,119,32,68,101,116,97,105,108,115,59
	dw	101,111,110
	align	4
_2113:
	dd	_843
	dd	242
	dd	5
	align	4
_2124:
	dd	3
	dd	0
	dd	2
	dd	_1501
	dd	_956
	dd	-44
	dd	0
	align	4
_2117:
	dd	_843
	dd	244
	dd	5
	align	4
_123:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	93,59,50,59,78,101,119,32,68,101,116,97,105,108,115,59
	dw	101,111,110
	align	4
_2121:
	dd	_843
	dd	245
	dd	5
	align	4
_2133:
	dd	3
	dd	0
	dd	0
	align	4
_2125:
	dd	_843
	dd	247
	dd	5
	align	4
_2132:
	dd	3
	dd	0
	dd	0
	align	4
_2129:
	dd	_843
	dd	249
	dd	7
	align	4
_2134:
	dd	_843
	dd	252
	dd	3
	align	4
_2204:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_956
	dd	-48
	dd	0
	align	4
_2146:
	dd	_843
	dd	253
	dd	4
	align	4
_2203:
	dd	3
	dd	0
	dd	0
	align	4
_2148:
	dd	_843
	dd	254
	dd	5
	align	4
_2202:
	dd	3
	dd	0
	dd	0
	align	4
_2156:
	dd	_843
	dd	256
	dd	7
	align	4
_2201:
	dd	3
	dd	0
	dd	0
	align	4
_2160:
	dd	_843
	dd	258
	dd	9
	align	4
_2197:
	dd	3
	dd	0
	dd	0
	align	4
_2164:
	dd	_843
	dd	259
	dd	10
	align	4
_2176:
	dd	_843
	dd	260
	dd	10
	align	4
_2188:
	dd	_843
	dd	261
	dd	10
	align	4
_2198:
	dd	_843
	dd	263
	dd	9
_2228:
	db	"sum",0
	align	4
_2227:
	dd	1
	dd	_391
	dd	2
	dd	_1005
	dd	_924
	dd	-4
	dd	2
	dd	_2228
	dd	_304
	dd	-8
	dd	0
	align	4
_2206:
	dd	_843
	dd	270
	dd	3
	align	4
_2226:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_956
	dd	-12
	dd	0
	align	4
_2218:
	dd	_843
	dd	271
	dd	4
	align	4
_2225:
	dd	3
	dd	0
	dd	0
	align	4
_2224:
	dd	_843
	dd	272
	dd	5
_2275:
	db	"insertIpadEntry",0
_2276:
	db	"nIpad",0
_2277:
	db	"b",0
	align	4
_2274:
	dd	1
	dd	_2275
	dd	2
	dd	_2276
	dd	_924
	dd	-4
	dd	2
	dd	_1691
	dd	_304
	dd	-8
	dd	2
	dd	_431
	dd	_329
	dd	-12
	dd	2
	dd	_2277
	dd	_307
	dd	-16
	dd	0
	align	4
_2229:
	dd	_843
	dd	279
	dd	2
	align	4
_2231:
	dd	_843
	dd	282
	dd	2
	align	4
_2233:
	dd	3
	dd	0
	dd	0
	align	4
_2232:
	dd	_843
	dd	281
	dd	3
	align	4
_2234:
	dd	_843
	dd	283
	dd	2
	align	4
_2235:
	dd	_843
	dd	284
	dd	2
	align	4
_2240:
	dd	_843
	dd	285
	dd	2
	align	4
_2242:
	dd	_843
	dd	286
	dd	2
	align	4
_2262:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_304
	dd	-20
	dd	0
	align	4
_2252:
	dd	_843
	dd	287
	dd	3
	align	4
_2257:
	dd	3
	dd	0
	dd	0
	align	4
_2256:
	dd	_843
	dd	288
	dd	4
	align	4
_2260:
	dd	3
	dd	0
	dd	0
	align	4
_2259:
	dd	_843
	dd	290
	dd	4
	align	4
_2261:
	dd	_843
	dd	292
	dd	3
	align	4
_2263:
	dd	_843
	dd	294
	dd	2
	align	4
_2268:
	dd	3
	dd	0
	dd	0
	align	4
_2267:
	dd	_843
	dd	295
	dd	3
	align	4
_2269:
	dd	_843
	dd	297
	dd	2
_2285:
	db	"newIpad",0
_2286:
	db	"ni",0
	align	4
_2284:
	dd	1
	dd	_2285
	dd	2
	dd	_2286
	dd	_924
	dd	-4
	dd	0
	align	4
_2278:
	dd	_843
	dd	302
	dd	2
	align	4
_2280:
	dd	_843
	dd	303
	dd	2
	align	4
_2283:
	dd	_843
	dd	304
	dd	2
_2324:
	db	"newBlankIpadBC",0
	align	4
_2323:
	dd	1
	dd	_2324
	dd	2
	dd	_362
	dd	_304
	dd	-4
	dd	2
	dd	_2286
	dd	_924
	dd	-8
	dd	0
	align	4
_2287:
	dd	_843
	dd	308
	dd	2
	align	4
_2289:
	dd	_843
	dd	309
	dd	2
	align	4
_2293:
	dd	_843
	dd	310
	dd	2
	align	4
_2301:
	dd	_843
	dd	311
	dd	2
	align	4
_136:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	78,101,101,100,32,83,101,114,105,97,108
	align	4
_2309:
	dd	_843
	dd	312
	dd	2
	align	4
_2317:
	dd	_843
	dd	313
	dd	2
	align	4
_139:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	32,97,116,32
	align	4
_138:
	dd	_bbStringClass
	dd	2147483647
	dd	23
	dw	93,59,49,59,82,101,103,105,115,116,101,114,101,100,32,105
	dw	80,97,100,32,111,110,32
	align	4
_137:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	82,101,103,105,115,116,101,114,101,100,32,105,80,97,100,59
	dw	91
	align	4
_2320:
	dd	_843
	dd	314
	dd	2
_2362:
	db	"newBlankIpadSN",0
	align	4
_2361:
	dd	1
	dd	_2362
	dd	2
	dd	_363
	dd	_304
	dd	-4
	dd	2
	dd	_2286
	dd	_924
	dd	-8
	dd	0
	align	4
_2325:
	dd	_843
	dd	318
	dd	2
	align	4
_2327:
	dd	_843
	dd	319
	dd	2
	align	4
_2331:
	dd	_843
	dd	320
	dd	2
	align	4
_140:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	45,45,45,45
	align	4
_2339:
	dd	_843
	dd	321
	dd	2
	align	4
_2347:
	dd	_843
	dd	322
	dd	2
	align	4
_2355:
	dd	_843
	dd	323
	dd	2
	align	4
_2358:
	dd	_843
	dd	324
	dd	2
_2492:
	db	"findBySN",0
_2493:
	db	"file",0
	align	4
_2491:
	dd	1
	dd	_2492
	dd	2
	dd	_363
	dd	_304
	dd	-4
	dd	2
	dd	_2493
	dd	_995
	dd	-8
	dd	2
	dd	_1691
	dd	_304
	dd	-12
	dd	2
	dd	_1767
	dd	_329
	dd	-16
	dd	2
	dd	_361
	dd	_307
	dd	-20
	dd	0
	align	4
_2363:
	dd	_843
	dd	329
	dd	2
_2386:
	db	"t",0
	align	4
_2385:
	dd	3
	dd	0
	dd	2
	dd	_2386
	dd	_924
	dd	-24
	dd	0
	align	4
_2373:
	dd	_843
	dd	330
	dd	3
	align	4
_2384:
	dd	3
	dd	0
	dd	0
	align	4
_2377:
	dd	_843
	dd	331
	dd	4
	align	4
_2382:
	dd	_843
	dd	332
	dd	4
	align	4
_144:
	dd	_bbStringClass
	dd	2147483647
	dd	72
	dw	84,104,105,115,32,105,80,97,100,32,105,115,32,99,117,114
	dw	114,101,110,116,108,121,32,98,101,105,110,103,32,97,99,99
	dw	101,115,115,101,100,32,101,108,115,101,119,104,101,114,101,44
	dw	32,112,108,101,97,115,101,32,116,114,121,32,97,103,97,105
	dw	110,32,108,97,116,101,114,46
	align	4
_2383:
	dd	_843
	dd	333
	dd	4
	align	4
_2387:
	dd	_843
	dd	336
	dd	2
	align	4
_2389:
	dd	_843
	dd	337
	dd	2
	align	4
_2391:
	dd	_843
	dd	338
	dd	2
	align	4
_2393:
	dd	_843
	dd	339
	dd	2
	align	4
_2395:
	dd	_843
	dd	364
	dd	2
	align	4
_2474:
	dd	3
	dd	0
	dd	0
	align	4
_2396:
	dd	_843
	dd	341
	dd	3
	align	4
_2397:
	dd	_843
	dd	342
	dd	3
	align	4
_2398:
	dd	_843
	dd	343
	dd	3
_2472:
	db	"l",0
	align	4
_2471:
	dd	3
	dd	0
	dd	2
	dd	_2472
	dd	_307
	dd	-28
	dd	2
	dd	_2286
	dd	_924
	dd	-32
	dd	0
	align	4
_2400:
	dd	_843
	dd	344
	dd	4
	align	4
_2402:
	dd	_843
	dd	345
	dd	4
	align	4
_2404:
	dd	_843
	dd	346
	dd	4
	align	4
_2408:
	dd	_843
	dd	347
	dd	4
	align	4
_2461:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_304
	dd	-36
	dd	0
	align	4
_2418:
	dd	_843
	dd	348
	dd	5
	align	4
_2428:
	dd	3
	dd	0
	dd	0
	align	4
_2420:
	dd	_843
	dd	349
	dd	6
	align	4
_2459:
	dd	3
	dd	0
	dd	0
	align	4
_2430:
	dd	_843
	dd	350
	dd	5
	align	4
_2440:
	dd	3
	dd	0
	dd	0
	align	4
_2432:
	dd	_843
	dd	351
	dd	6
	align	4
_2458:
	dd	3
	dd	0
	dd	0
	align	4
_2442:
	dd	_843
	dd	352
	dd	5
	align	4
_2452:
	dd	3
	dd	0
	dd	0
	align	4
_2444:
	dd	_843
	dd	353
	dd	6
	align	4
_2457:
	dd	3
	dd	0
	dd	0
	align	4
_2454:
	dd	_843
	dd	355
	dd	6
	align	4
_2460:
	dd	_843
	dd	357
	dd	5
	align	4
_2462:
	dd	_843
	dd	359
	dd	4
	align	4
_2467:
	dd	_843
	dd	360
	dd	4
	align	4
_2470:
	dd	_843
	dd	361
	dd	4
	align	4
_2473:
	dd	_843
	dd	363
	dd	3
	align	4
_2475:
	dd	_843
	dd	365
	dd	2
	align	4
_2478:
	dd	3
	dd	0
	dd	0
	align	4
_2477:
	dd	_843
	dd	366
	dd	3
	align	4
_2484:
	dd	3
	dd	0
	dd	0
	align	4
_2480:
	dd	_843
	dd	367
	dd	2
	align	4
_152:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	93,32,67,114,101,97,116,101,63
	align	4
_151:
	dd	_bbStringClass
	dd	2147483647
	dd	51
	dw	84,104,101,114,101,32,105,115,32,110,111,32,101,110,116,114
	dw	121,32,105,110,32,39,105,112,97,100,100,98,46,99,115,118
	dw	39,32,119,105,116,104,32,116,104,101,32,83,101,114,105,97
	dw	108,32,91
	align	4
_2483:
	dd	3
	dd	0
	dd	0
	align	4
_2482:
	dd	_843
	dd	368
	dd	3
	align	4
_2485:
	dd	_843
	dd	370
	dd	2
	align	4
_2490:
	dd	_843
	dd	371
	dd	2
_2621:
	db	"findByBC",0
	align	4
_2620:
	dd	1
	dd	_2621
	dd	2
	dd	_362
	dd	_304
	dd	-4
	dd	2
	dd	_2493
	dd	_995
	dd	-8
	dd	2
	dd	_1691
	dd	_304
	dd	-12
	dd	2
	dd	_1767
	dd	_329
	dd	-16
	dd	2
	dd	_361
	dd	_307
	dd	-20
	dd	0
	align	4
_2494:
	dd	_843
	dd	376
	dd	2
	align	4
_2516:
	dd	3
	dd	0
	dd	2
	dd	_2386
	dd	_924
	dd	-24
	dd	0
	align	4
_2504:
	dd	_843
	dd	377
	dd	3
	align	4
_2515:
	dd	3
	dd	0
	dd	0
	align	4
_2508:
	dd	_843
	dd	378
	dd	4
	align	4
_2513:
	dd	_843
	dd	379
	dd	4
	align	4
_2514:
	dd	_843
	dd	380
	dd	4
	align	4
_2517:
	dd	_843
	dd	383
	dd	2
	align	4
_2519:
	dd	_843
	dd	384
	dd	2
	align	4
_2521:
	dd	_843
	dd	385
	dd	2
	align	4
_2523:
	dd	_843
	dd	386
	dd	2
	align	4
_2525:
	dd	_843
	dd	411
	dd	2
	align	4
_2603:
	dd	3
	dd	0
	dd	0
	align	4
_2526:
	dd	_843
	dd	388
	dd	3
	align	4
_2527:
	dd	_843
	dd	389
	dd	3
	align	4
_2528:
	dd	_843
	dd	390
	dd	3
	align	4
_2601:
	dd	3
	dd	0
	dd	2
	dd	_2472
	dd	_307
	dd	-28
	dd	2
	dd	_2286
	dd	_924
	dd	-32
	dd	0
	align	4
_2530:
	dd	_843
	dd	391
	dd	4
	align	4
_2532:
	dd	_843
	dd	392
	dd	4
	align	4
_2534:
	dd	_843
	dd	393
	dd	4
	align	4
_2538:
	dd	_843
	dd	394
	dd	4
	align	4
_2591:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_304
	dd	-36
	dd	0
	align	4
_2548:
	dd	_843
	dd	395
	dd	5
	align	4
_2558:
	dd	3
	dd	0
	dd	0
	align	4
_2550:
	dd	_843
	dd	396
	dd	6
	align	4
_2589:
	dd	3
	dd	0
	dd	0
	align	4
_2560:
	dd	_843
	dd	397
	dd	5
	align	4
_2570:
	dd	3
	dd	0
	dd	0
	align	4
_2562:
	dd	_843
	dd	398
	dd	6
	align	4
_2588:
	dd	3
	dd	0
	dd	0
	align	4
_2572:
	dd	_843
	dd	399
	dd	5
	align	4
_2582:
	dd	3
	dd	0
	dd	0
	align	4
_2574:
	dd	_843
	dd	400
	dd	6
	align	4
_2587:
	dd	3
	dd	0
	dd	0
	align	4
_2584:
	dd	_843
	dd	402
	dd	6
	align	4
_2590:
	dd	_843
	dd	404
	dd	5
	align	4
_2592:
	dd	_843
	dd	406
	dd	4
	align	4
_2597:
	dd	_843
	dd	407
	dd	4
	align	4
_2600:
	dd	_843
	dd	408
	dd	4
	align	4
_2602:
	dd	_843
	dd	410
	dd	3
	align	4
_2604:
	dd	_843
	dd	412
	dd	2
	align	4
_2607:
	dd	3
	dd	0
	dd	0
	align	4
_2606:
	dd	_843
	dd	413
	dd	3
	align	4
_2613:
	dd	3
	dd	0
	dd	0
	align	4
_2609:
	dd	_843
	dd	414
	dd	2
	align	4
_162:
	dd	_bbStringClass
	dd	2147483647
	dd	52
	dw	84,104,101,114,101,32,105,115,32,110,111,32,101,110,116,114
	dw	121,32,105,110,32,39,105,112,97,100,100,98,46,99,115,118
	dw	39,32,119,105,116,104,32,116,104,101,32,66,97,114,99,111
	dw	100,101,32,91
	align	4
_2612:
	dd	3
	dd	0
	dd	0
	align	4
_2611:
	dd	_843
	dd	415
	dd	3
	align	4
_2614:
	dd	_843
	dd	417
	dd	2
	align	4
_2619:
	dd	_843
	dd	418
	dd	2
	align	4
_2630:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_956
	dd	-4
	dd	0
	align	4
_2629:
	dd	3
	dd	0
	dd	0
	align	4
_2714:
	dd	1
	dd	_339
	dd	2
	dd	_1005
	dd	_956
	dd	-4
	dd	0
	align	4
_2646:
	dd	_859
	dd	14
	dd	3
	align	4
_163:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	105,80,97,100,32,78,111,116,101,32
	align	4
_2653:
	dd	_859
	dd	15
	dd	3
	align	4
_2654:
	dd	_859
	dd	17
	dd	3
	align	4
_2664:
	dd	_859
	dd	18
	dd	3
	align	4
_2669:
	dd	_859
	dd	19
	dd	3
	align	4
_2679:
	dd	_859
	dd	20
	dd	3
	align	4
_2684:
	dd	_859
	dd	21
	dd	3
	align	4
_2700:
	dd	_859
	dd	22
	dd	3
	align	4
_2707:
	dd	_859
	dd	23
	dd	3
	align	4
_2726:
	dd	1
	dd	_403
	dd	2
	dd	_1005
	dd	_956
	dd	-4
	dd	2
	dd	_400
	dd	_307
	dd	-8
	dd	0
	align	4
_2715:
	dd	_859
	dd	27
	dd	3
	align	4
_2721:
	dd	3
	dd	0
	dd	0
	align	4
_2720:
	dd	_859
	dd	33
	dd	5
	align	4
_2723:
	dd	3
	dd	0
	dd	0
	align	4
_2722:
	dd	_859
	dd	29
	dd	5
	align	4
_2725:
	dd	3
	dd	0
	dd	0
	align	4
_2724:
	dd	_859
	dd	31
	dd	5
	align	4
_2742:
	dd	1
	dd	_335
	dd	2
	dd	_1005
	dd	_956
	dd	-4
	dd	0
	align	4
_2727:
	dd	_859
	dd	38
	dd	3
	align	4
_2740:
	dd	3
	dd	0
	dd	0
	align	4
_2741:
	dd	3
	dd	0
	dd	0
_2757:
	db	"choice",0
	align	4
_2756:
	dd	1
	dd	_388
	dd	2
	dd	_1005
	dd	_956
	dd	-4
	dd	2
	dd	_2757
	dd	_307
	dd	-8
	dd	0
	align	4
_2743:
	dd	_859
	dd	46
	dd	3
	align	4
_2745:
	dd	_859
	dd	47
	dd	3
	align	4
_2748:
	dd	3
	dd	0
	dd	0
	align	4
_2747:
	dd	_859
	dd	48
	dd	4
	align	4
_2754:
	dd	3
	dd	0
	dd	0
	align	4
_2750:
	dd	_859
	dd	49
	dd	3
	align	4
_164:
	dd	_bbStringClass
	dd	2147483647
	dd	33
	dw	87,111,117,108,100,32,121,111,117,32,108,105,107,101,32,116
	dw	111,32,115,97,118,101,32,116,104,105,115,32,110,111,116,101
	dw	63
	align	4
_2753:
	dd	3
	dd	0
	dd	0
	align	4
_2752:
	dd	_859
	dd	50
	dd	4
	align	4
_2755:
	dd	_859
	dd	52
	dd	3
	align	4
_2763:
	dd	1
	dd	_334
	dd	2
	dd	_1005
	dd	_956
	dd	-4
	dd	0
	align	4
_2758:
	dd	_859
	dd	56
	dd	3
_2789:
	db	"noComma",0
_2790:
	db	"event",0
_2791:
	db	":TEvent",0
_2792:
	db	"context",0
_2793:
	db	":Object",0
	align	4
_2788:
	dd	1
	dd	_2789
	dd	2
	dd	_2790
	dd	_2791
	dd	-4
	dd	2
	dd	_2792
	dd	_2793
	dd	-8
	dd	0
	align	4
_2764:
	dd	_859
	dd	61
	dd	2
	align	4
_2786:
	dd	3
	dd	0
	dd	0
	align	4
_2768:
	dd	_859
	dd	62
	dd	3
	align	4
_2778:
	dd	3
	dd	0
	dd	0
	align	4
_2776:
	dd	_859
	dd	64
	dd	5
	align	4
_165:
	dd	_bbStringClass
	dd	2147483647
	dd	27
	dw	89,111,117,32,99,97,110,110,111,116,32,117,115,101,32,99
	dw	111,109,109,97,115,32,104,101,114,101,46
	align	4
_2777:
	dd	_859
	dd	65
	dd	5
	align	4
_2781:
	dd	3
	dd	0
	dd	0
	align	4
_2779:
	dd	_859
	dd	67
	dd	5
	align	4
_166:
	dd	_bbStringClass
	dd	2147483647
	dd	31
	dw	89,111,117,32,99,97,110,110,111,116,32,117,115,101,32,115
	dw	101,109,105,99,111,108,111,110,115,32,104,101,114,101,46
	align	4
_2780:
	dd	_859
	dd	68
	dd	5
	align	4
_2785:
	dd	3
	dd	0
	dd	0
	align	4
_2782:
	dd	_859
	dd	70
	dd	5
	align	4
_167:
	dd	_bbStringClass
	dd	2147483647
	dd	81
	dw	84,104,101,32,114,101,116,117,114,110,32,107,101,121,32,119
	dw	97,115,32,117,115,101,100,32,105,110,32,97,32,110,111,116
	dw	101,46,32,84,104,105,115,32,109,97,121,32,104,97,118,101
	dw	32,99,111,114,114,117,112,116,101,100,32,116,104,101,32,100
	dw	97,116,97,32,102,111,114,32,116,104,101,32,105,80,97,100
	dw	46
	align	4
_2783:
	dd	_859
	dd	71
	dd	5
	align	4
_168:
	dd	_bbStringClass
	dd	2147483647
	dd	235
	dw	89,111,117,32,99,97,110,110,111,116,32,117,115,101,32,116
	dw	104,101,32,69,110,116,101,114,32,75,101,121,32,104,101,114
	dw	101,46,32,84,104,101,32,119,97,121,32,111,117,114,32,100
	dw	97,116,97,98,97,115,101,32,105,115,32,115,116,114,117,99
	dw	116,117,114,101,100,32,105,116,32,119,105,108,108,32,99,111
	dw	114,114,117,112,116,32,97,108,108,32,116,104,101,32,100,97
	dw	116,97,32,97,110,100,32,101,114,97,115,101,32,97,108,108
	dw	32,110,111,116,101,115,32,102,111,114,32,116,104,105,115,32
	dw	105,80,97,100,46,32,80,108,101,97,115,101,32,98,97,99
	dw	107,115,112,97,99,101,32,116,111,32,99,111,110,116,105,110
	dw	117,101,32,111,110,32,116,104,101,32,108,105,110,101,32,121
	dw	111,117,32,119,101,114,101,32,119,114,105,116,105,110,103,32
	dw	111,110,32,97,110,100,32,100,111,32,110,111,116,32,117,115
	dw	101,32,116,104,101,32,101,110,116,101,114,32,107,101,121,32
	dw	104,101,114,101,32,97,103,97,105,110,46
	align	4
_2784:
	dd	_859
	dd	72
	dd	5
	align	4
_2787:
	dd	_859
	dd	75
	dd	2
_2812:
	db	"readNote",0
_2813:
	db	"tNote",0
	align	4
_2811:
	dd	1
	dd	_2812
	dd	2
	dd	_2813
	dd	_956
	dd	-4
	dd	0
	align	4
_2794:
	dd	_859
	dd	80
	dd	2
	align	4
_2805:
	dd	3
	dd	0
	dd	0
	align	4
_2800:
	dd	_859
	dd	81
	dd	3
	align	4
_2810:
	dd	3
	dd	0
	dd	0
	align	4
_2807:
	dd	_859
	dd	83
	dd	3
_2818:
	db	"newNote",0
	align	4
_2817:
	dd	1
	dd	_2818
	dd	2
	dd	_2813
	dd	_956
	dd	-4
	dd	0
	align	4
_2814:
	dd	_859
	dd	88
	dd	2
	align	4
_2816:
	dd	_859
	dd	89
	dd	2
	align	4
_2841:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_940
	dd	-4
	dd	0
	align	4
_2840:
	dd	3
	dd	0
	dd	0
_3030:
	db	"editMenu",0
_3031:
	db	"feeAction",0
	align	4
_3029:
	dd	1
	dd	_339
	dd	2
	dd	_1005
	dd	_940
	dd	-4
	dd	2
	dd	_3030
	dd	_326
	dd	-8
	dd	2
	dd	_367
	dd	_326
	dd	-12
	dd	2
	dd	_3031
	dd	_326
	dd	-16
	dd	0
	align	4
_2885:
	dd	_863
	dd	31
	dd	3
	align	4
_170:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	83,116,117,100,101,110,116,32,80,114,111,102,105,108,101
	align	4
_2890:
	dd	_863
	dd	32
	dd	3
	align	4
_2891:
	dd	_863
	dd	33
	dd	3
	align	4
_2901:
	dd	_863
	dd	35
	dd	3
	align	4
_2907:
	dd	_863
	dd	36
	dd	4
	align	4
_171:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	70,105,114,115,116,32,78,97,109,101
	align	4
_2915:
	dd	_863
	dd	37
	dd	4
	align	4
_172:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	76,97,115,116,32,78,97,109,101
	align	4
_2923:
	dd	_863
	dd	38
	dd	4
	align	4
_173:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	71,114,97,100,101
	align	4
_2931:
	dd	_863
	dd	39
	dd	3
	align	4
_2937:
	dd	_863
	dd	40
	dd	4
	align	4
_174:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	65,115,115,105,103,110,32,105,80,97,100
	align	4
_2945:
	dd	_863
	dd	41
	dd	4
	align	4
_2946:
	dd	_863
	dd	42
	dd	4
	align	4
_175:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	67,104,97,114,103,101,114,32,70,101,101
	align	4
_2948:
	dd	_863
	dd	43
	dd	5
	align	4
_2954:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	87,97,108,108,32,67,104,97,114,103,101,114,32,91,36,49
	dw	56,93
	align	4
_2959:
	dd	_863
	dd	44
	dd	5
	align	4
_2964:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	85,83,66,32,67,97,98,108,101,32,91,36,49,56,93
	align	4
_2969:
	dd	_863
	dd	45
	dd	5
	align	4
_2975:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	67,97,115,101,32,91,36,51,54,93
	align	4
_2980:
	dd	_863
	dd	46
	dd	5
	align	4
_180:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	69,120,116,114,97,32,70,101,101
	align	4
_2988:
	dd	_863
	dd	47
	dd	4
	align	4
_181:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	76,111,115,116,32,105,80,97,100
	align	4
_2996:
	dd	_863
	dd	48
	dd	4
	align	4
_182:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	83,116,111,108,101,110,32,105,80,97,100
	align	4
_3004:
	dd	_863
	dd	49
	dd	4
	align	4
_183:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	68,97,109,97,103,101,100,32,105,80,97,100
	align	4
_3012:
	dd	_863
	dd	50
	dd	3
	align	4
_184:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	105,80,97,100,32,80,114,111,102,105,108,101
	align	4
_3024:
	dd	_863
	dd	53
	dd	3
	align	4
_3037:
	dd	1
	dd	_333
	dd	2
	dd	_1005
	dd	_940
	dd	-4
	dd	0
	align	4
_3032:
	dd	_863
	dd	57
	dd	3
	align	4
_3093:
	dd	1
	dd	_334
	dd	2
	dd	_1005
	dd	_940
	dd	-4
	dd	0
	align	4
_3038:
	dd	_863
	dd	61
	dd	3
	align	4
_3043:
	dd	3
	dd	0
	dd	0
	align	4
_3040:
	dd	_863
	dd	62
	dd	4
	align	4
_3051:
	dd	3
	dd	0
	dd	0
	align	4
_3045:
	dd	_863
	dd	63
	dd	3
	align	4
_3050:
	dd	3
	dd	0
	dd	0
	align	4
_3047:
	dd	_863
	dd	64
	dd	4
	align	4
_3052:
	dd	_863
	dd	66
	dd	3
	align	4
_3057:
	dd	_863
	dd	67
	dd	3
	align	4
_3073:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_326
	dd	-8
	dd	0
	align	4
_3071:
	dd	_863
	dd	68
	dd	4
	align	4
_3072:
	dd	_863
	dd	69
	dd	4
	align	4
_3074:
	dd	_863
	dd	71
	dd	3
	align	4
_3079:
	dd	_863
	dd	72
	dd	3
	align	4
_3084:
	dd	_863
	dd	73
	dd	3
	align	4
_3092:
	dd	_863
	dd	74
	dd	3
	align	4
_3173:
	dd	1
	dd	_387
	dd	2
	dd	_1005
	dd	_940
	dd	-4
	dd	0
	align	4
_3094:
	dd	_863
	dd	78
	dd	3
	align	4
_3113:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_304
	dd	-8
	dd	2
	dd	_1501
	dd	_956
	dd	-12
	dd	0
	align	4
_3106:
	dd	_863
	dd	79
	dd	4
	align	4
_3108:
	dd	_863
	dd	80
	dd	4
	align	4
_3114:
	dd	_863
	dd	83
	dd	3
	align	4
_191:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	78,97,109,101,58,32
	align	4
_3130:
	dd	_863
	dd	84
	dd	3
	align	4
_192:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	66,97,108,97,110,99,101,32,79,119,101,100,58,32,36
	align	4
_3144:
	dd	_863
	dd	85
	dd	3
	align	4
_193:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	71,114,97,100,101,58,32
	align	4
_3158:
	dd	_863
	dd	87
	dd	3
	align	4
_3162:
	dd	3
	dd	0
	dd	0
	align	4
_3167:
	dd	3
	dd	0
	dd	0
	align	4
_3164:
	dd	_863
	dd	90
	dd	4
	align	4
_194:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	82,101,116,117,114,110,32,105,80,97,100
	align	4
_3168:
	dd	_863
	dd	92
	dd	3
	align	4
_3175:
	dd	1
	dd	_388
	dd	2
	dd	_1005
	dd	_940
	dd	-4
	dd	0
	align	4
_3174:
	dd	_863
	dd	96
	dd	3
	align	4
_195:
	dd	_bbStringClass
	dd	2147483647
	dd	28
	dw	84,104,105,115,32,105,115,32,119,104,101,114,101,32,121,111
	dw	117,32,119,111,117,108,100,32,115,97,118,101
	align	4
_3248:
	dd	1
	dd	_389
	dd	2
	dd	_1005
	dd	_940
	dd	-4
	dd	2
	dd	_1766
	dd	_304
	dd	-8
	dd	2
	dd	_1767
	dd	_329
	dd	-12
	dd	2
	dd	_307
	dd	_307
	dd	-16
	dd	2
	dd	_1501
	dd	_956
	dd	-20
	dd	0
	align	4
_3176:
	dd	_863
	dd	100
	dd	3
	align	4
_3178:
	dd	_863
	dd	101
	dd	3
	align	4
_3180:
	dd	_863
	dd	102
	dd	3
	align	4
_3182:
	dd	_863
	dd	103
	dd	3
	align	4
_3246:
	dd	3
	dd	0
	dd	2
	dd	_1763
	dd	_304
	dd	-24
	dd	0
	align	4
_3192:
	dd	_863
	dd	104
	dd	4
	align	4
_3206:
	dd	3
	dd	0
	dd	0
	align	4
_3194:
	dd	_863
	dd	105
	dd	5
	align	4
_3195:
	dd	_863
	dd	106
	dd	5
	align	4
_3198:
	dd	_863
	dd	107
	dd	5
	align	4
_3207:
	dd	_863
	dd	109
	dd	4
	align	4
_3226:
	dd	3
	dd	0
	dd	0
	align	4
_3209:
	dd	_863
	dd	110
	dd	5
	align	4
_3217:
	dd	_863
	dd	111
	dd	5
	align	4
_3227:
	dd	_863
	dd	113
	dd	4
	align	4
_3233:
	dd	3
	dd	0
	dd	0
	align	4
_3229:
	dd	_863
	dd	114
	dd	5
	align	4
_3234:
	dd	_863
	dd	116
	dd	4
	align	4
_3244:
	dd	3
	dd	0
	dd	0
	align	4
_3236:
	dd	_863
	dd	117
	dd	5
	align	4
_3245:
	dd	_863
	dd	119
	dd	4
	align	4
_3247:
	dd	_863
	dd	123
	dd	3
	align	4
_3571:
	dd	1
	dd	_335
	dd	2
	dd	_1005
	dd	_940
	dd	-4
	dd	0
	align	4
_3249:
	dd	_863
	dd	127
	dd	3
	align	4
_3364:
	dd	3
	dd	0
	dd	0
	align	4
_3274:
	dd	_863
	dd	129
	dd	5
	align	4
_3360:
	dd	3
	dd	0
	dd	0
	align	4
_3278:
	dd	_863
	dd	130
	dd	6
	align	4
_199:
	dd	_bbStringClass
	dd	2147483647
	dd	21
	dw	80,108,101,97,115,101,32,101,110,116,101,114,32,66,97,114
	dw	99,111,100,101,58
	align	4
_3286:
	dd	_863
	dd	131
	dd	6
	align	4
_3356:
	dd	3
	dd	0
	dd	0
	align	4
_3292:
	dd	_863
	dd	132
	dd	7
	align	4
_3301:
	dd	_863
	dd	133
	dd	7
	align	4
_3311:
	dd	_863
	dd	134
	dd	7
	align	4
_3320:
	dd	_863
	dd	135
	dd	7
	align	4
_3325:
	dd	_863
	dd	136
	dd	7
	align	4
_3341:
	dd	_863
	dd	138
	dd	7
	align	4
_3344:
	dd	_863
	dd	139
	dd	7
	align	4
_3351:
	dd	_863
	dd	140
	dd	7
	align	4
_3359:
	dd	3
	dd	0
	dd	0
	align	4
_3358:
	dd	_863
	dd	142
	dd	7
	align	4
_200:
	dd	_bbStringClass
	dd	2147483647
	dd	43
	dw	84,104,105,115,32,105,80,97,100,32,105,115,32,110,111,116
	dw	32,99,104,101,99,107,101,100,32,105,110,32,116,111,32,116
	dw	104,101,32,108,105,98,114,97,114,121,46
	align	4
_3363:
	dd	3
	dd	0
	dd	0
	align	4
_3362:
	dd	_863
	dd	145
	dd	6
	align	4
_201:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	82,101,108,101,97,115,101,32,105,80,97,100
	align	4
_3397:
	dd	3
	dd	0
	dd	2
	dd	_1856
	dd	_307
	dd	-8
	dd	0
	align	4
_3365:
	dd	_863
	dd	148
	dd	5
	align	4
_3367:
	dd	_863
	dd	149
	dd	5
	align	4
_3370:
	dd	3
	dd	0
	dd	0
	align	4
_3369:
	dd	_863
	dd	150
	dd	6
	align	4
_3373:
	dd	3
	dd	0
	dd	0
	align	4
_3372:
	dd	_863
	dd	152
	dd	6
	align	4
_202:
	dd	_bbStringClass
	dd	2147483647
	dd	107
	dw	65,114,101,32,121,111,117,32,115,117,114,101,32,116,104,97
	dw	116,32,121,111,117,32,119,97,110,116,32,116,111,32,99,104
	dw	97,110,103,101,32,116,104,105,115,32,115,116,117,100,101,110
	dw	116,39,115,32,103,114,97,100,101,63,32,71,114,97,100,101
	dw	115,32,97,114,101,32,97,117,116,111,109,97,116,105,99,97
	dw	108,108,121,32,105,110,99,114,101,109,101,110,116,101,100,32
	dw	101,118,101,114,121,32,121,101,97,114,46
	align	4
_3374:
	dd	_863
	dd	154
	dd	5
_3396:
	db	"ng",0
	align	4
_3395:
	dd	3
	dd	0
	dd	2
	dd	_3396
	dd	_304
	dd	-12
	dd	0
	align	4
_3376:
	dd	_863
	dd	155
	dd	6
	align	4
_203:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	78,101,119,32,71,114,97,100,101,58
	align	4
_3378:
	dd	_863
	dd	156
	dd	6
	align	4
_3394:
	dd	3
	dd	0
	dd	0
	align	4
_3380:
	dd	_863
	dd	157
	dd	7
	align	4
_206:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	93,46,59,101,111,110
	align	4
_205:
	dd	_bbStringClass
	dd	2147483647
	dd	49
	dw	93,59,49,59,84,104,105,115,32,115,116,117,100,101,110,116
	dw	39,115,32,103,114,97,100,101,32,108,101,118,101,108,32,119
	dw	97,115,32,99,104,97,110,103,101,100,32,102,114,111,109,32
	dw	91
	align	4
_204:
	dd	_bbStringClass
	dd	2147483647
	dd	29
	dw	67,104,97,110,103,101,100,32,83,116,117,100,101,110,116,32
	dw	71,114,97,100,101,32,76,101,118,101,108,59,91
	align	4
_3385:
	dd	_863
	dd	158
	dd	7
	align	4
_3389:
	dd	_863
	dd	159
	dd	7
	align	4
_3436:
	dd	3
	dd	0
	dd	2
	dd	_1856
	dd	_307
	dd	-16
	dd	0
	align	4
_3398:
	dd	_863
	dd	163
	dd	5
	align	4
_3400:
	dd	_863
	dd	164
	dd	5
	align	4
_3403:
	dd	3
	dd	0
	dd	0
	align	4
_3402:
	dd	_863
	dd	165
	dd	6
	align	4
_3406:
	dd	3
	dd	0
	dd	0
	align	4
_3405:
	dd	_863
	dd	167
	dd	6
	align	4
_207:
	dd	_bbStringClass
	dd	2147483647
	dd	101
	dw	65,114,101,32,121,111,117,32,115,117,114,101,32,116,104,97
	dw	116,32,121,111,117,32,119,97,110,116,32,116,111,32,99,104
	dw	97,110,103,101,32,116,104,101,32,108,97,115,116,32,110,97
	dw	109,101,63,32,89,111,117,32,119,105,108,108,32,110,111,116
	dw	32,102,105,110,100,32,116,104,115,105,32,115,116,117,100,101
	dw	110,116,32,98,121,32,116,104,101,105,114,32,111,108,100,32
	dw	110,97,109,101,46
	align	4
_3407:
	dd	_863
	dd	169
	dd	5
_3435:
	db	"nln",0
	align	4
_3434:
	dd	3
	dd	0
	dd	2
	dd	_3435
	dd	_304
	dd	-20
	dd	0
	align	4
_3409:
	dd	_863
	dd	170
	dd	6
	align	4
_208:
	dd	_bbStringClass
	dd	2147483647
	dd	14
	dw	78,101,119,32,108,97,115,116,32,110,97,109,101,58
	align	4
_3411:
	dd	_863
	dd	171
	dd	6
	align	4
_3433:
	dd	3
	dd	0
	dd	0
	align	4
_3413:
	dd	_863
	dd	172
	dd	7
	align	4
_210:
	dd	_bbStringClass
	dd	2147483647
	dd	47
	dw	93,59,49,59,84,104,105,115,32,115,116,117,100,101,110,116
	dw	39,115,32,108,97,115,116,32,110,97,109,101,32,119,97,115
	dw	32,99,104,97,110,103,101,100,32,102,114,111,109,32,91
	align	4
_209:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	67,104,97,110,103,101,100,32,76,97,115,116,32,78,97,109
	dw	101,59,91
	align	4
_3418:
	dd	_863
	dd	173
	dd	7
	align	4
_3426:
	dd	_863
	dd	174
	dd	7
	align	4
_3475:
	dd	3
	dd	0
	dd	2
	dd	_1856
	dd	_307
	dd	-24
	dd	0
	align	4
_3437:
	dd	_863
	dd	178
	dd	5
	align	4
_3439:
	dd	_863
	dd	179
	dd	5
	align	4
_3442:
	dd	3
	dd	0
	dd	0
	align	4
_3441:
	dd	_863
	dd	180
	dd	6
	align	4
_3445:
	dd	3
	dd	0
	dd	0
	align	4
_3444:
	dd	_863
	dd	182
	dd	6
	align	4
_211:
	dd	_bbStringClass
	dd	2147483647
	dd	102
	dw	65,114,101,32,121,111,117,32,115,117,114,101,32,116,104,97
	dw	116,32,121,111,117,32,119,97,110,116,32,116,111,32,99,104
	dw	97,110,103,101,32,116,104,101,32,102,105,114,115,116,32,110
	dw	97,109,101,63,32,89,111,117,32,119,105,108,108,32,110,111
	dw	116,32,102,105,110,100,32,116,104,105,115,32,115,116,117,100
	dw	101,110,116,32,98,121,32,116,104,101,105,114,32,111,108,100
	dw	32,110,97,109,101,46
	align	4
_3446:
	dd	_863
	dd	184
	dd	5
_3474:
	db	"nfn",0
	align	4
_3473:
	dd	3
	dd	0
	dd	2
	dd	_3474
	dd	_304
	dd	-28
	dd	0
	align	4
_3448:
	dd	_863
	dd	185
	dd	6
	align	4
_212:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	78,101,119,32,102,105,114,115,116,32,110,97,109,101,58
	align	4
_3450:
	dd	_863
	dd	186
	dd	6
	align	4
_3472:
	dd	3
	dd	0
	dd	0
	align	4
_3452:
	dd	_863
	dd	187
	dd	7
	align	4
_214:
	dd	_bbStringClass
	dd	2147483647
	dd	48
	dw	93,59,49,59,84,104,105,115,32,115,116,117,100,101,110,116
	dw	39,115,32,102,105,114,115,116,32,110,97,109,101,32,119,97
	dw	115,32,99,104,97,110,103,101,100,32,102,114,111,109,32,91
	align	4
_213:
	dd	_bbStringClass
	dd	2147483647
	dd	20
	dw	67,104,97,110,103,101,100,32,70,105,114,115,116,32,78,97
	dw	109,101,59,91
	align	4
_3457:
	dd	_863
	dd	188
	dd	7
	align	4
_3465:
	dd	_863
	dd	189
	dd	7
	align	4
_3490:
	dd	3
	dd	0
	dd	0
	align	4
_3476:
	dd	_863
	dd	193
	dd	5
	align	4
_3488:
	dd	3
	dd	0
	dd	0
	align	4
_3481:
	dd	_863
	dd	195
	dd	7
	align	4
_3489:
	dd	3
	dd	0
	dd	0
	align	4
_3499:
	dd	3
	dd	0
	dd	0
	align	4
_3491:
	dd	_863
	dd	200
	dd	5
	align	4
_3498:
	dd	3
	dd	0
	dd	0
	align	4
_3495:
	dd	_863
	dd	202
	dd	7
	align	4
_3500:
	dd	_863
	dd	206
	dd	3
	align	4
_3570:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_956
	dd	-32
	dd	0
	align	4
_3512:
	dd	_863
	dd	207
	dd	4
	align	4
_3569:
	dd	3
	dd	0
	dd	0
	align	4
_3514:
	dd	_863
	dd	208
	dd	5
	align	4
_3568:
	dd	3
	dd	0
	dd	0
	align	4
_3522:
	dd	_863
	dd	210
	dd	7
	align	4
_3567:
	dd	3
	dd	0
	dd	0
	align	4
_3526:
	dd	_863
	dd	212
	dd	9
	align	4
_3563:
	dd	3
	dd	0
	dd	0
	align	4
_3530:
	dd	_863
	dd	213
	dd	10
	align	4
_3542:
	dd	_863
	dd	214
	dd	10
	align	4
_3554:
	dd	_863
	dd	215
	dd	10
	align	4
_3564:
	dd	_863
	dd	217
	dd	9
	align	4
_3593:
	dd	1
	dd	_391
	dd	2
	dd	_1005
	dd	_940
	dd	-4
	dd	2
	dd	_2228
	dd	_304
	dd	-8
	dd	0
	align	4
_3572:
	dd	_863
	dd	225
	dd	3
	align	4
_3592:
	dd	3
	dd	0
	dd	2
	dd	_307
	dd	_956
	dd	-12
	dd	0
	align	4
_3584:
	dd	_863
	dd	226
	dd	4
	align	4
_3591:
	dd	3
	dd	0
	dd	0
	align	4
_3590:
	dd	_863
	dd	227
	dd	5
_3601:
	db	"newStudent",0
_3602:
	db	"ns",0
	align	4
_3600:
	dd	1
	dd	_3601
	dd	2
	dd	_3602
	dd	_940
	dd	-4
	dd	0
	align	4
_3594:
	dd	_863
	dd	235
	dd	2
	align	4
_3596:
	dd	_863
	dd	236
	dd	2
	align	4
_3599:
	dd	_863
	dd	237
	dd	2
_3641:
	db	"newBlankStudent",0
_3642:
	db	"fn",0
_3643:
	db	"ls",0
	align	4
_3640:
	dd	1
	dd	_3641
	dd	2
	dd	_3642
	dd	_304
	dd	-4
	dd	2
	dd	_3643
	dd	_304
	dd	-8
	dd	2
	dd	_3602
	dd	_940
	dd	-12
	dd	0
	align	4
_3603:
	dd	_863
	dd	241
	dd	2
	align	4
_3605:
	dd	_863
	dd	242
	dd	2
	align	4
_3609:
	dd	_863
	dd	243
	dd	2
	align	4
_3617:
	dd	_863
	dd	244
	dd	2
	align	4
_3625:
	dd	_863
	dd	245
	dd	2
	align	4
_3629:
	dd	_863
	dd	246
	dd	2
	align	4
_3633:
	dd	_863
	dd	247
	dd	2
	align	4
_222:
	dd	_bbStringClass
	dd	2147483647
	dd	38
	dw	93,59,49,59,82,101,103,105,115,116,101,114,101,100,32,115
	dw	116,117,100,101,110,116,32,105,110,116,111,32,115,121,115,116
	dw	101,109,32,111,110,32
	align	4
_221:
	dd	_bbStringClass
	dd	2147483647
	dd	20
	dw	82,101,103,105,115,116,101,114,101,100,32,83,116,117,100,101
	dw	110,116,59,91
	align	4
_3636:
	dd	_863
	dd	248
	dd	2
	align	4
_3639:
	dd	_863
	dd	249
	dd	2
_3648:
	db	"findByFirst",0
_3649:
	db	"tmpStudent",0
	align	4
_3647:
	dd	1
	dd	_3648
	dd	2
	dd	_3642
	dd	_304
	dd	-4
	dd	2
	dd	_3649
	dd	_940
	dd	-8
	dd	0
	align	4
_3644:
	dd	_863
	dd	253
	dd	2
	align	4
_3646:
	dd	_863
	dd	255
	dd	2
_3652:
	db	"findByLast",0
_3653:
	db	"ln",0
	align	4
_3651:
	dd	1
	dd	_3652
	dd	2
	dd	_3653
	dd	_304
	dd	-4
	dd	0
	align	4
_3650:
	dd	_863
	dd	259
	dd	2
	align	4
_223:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	70,105,110,100,32,98,121,32,108,97,115,116,32,110,97,109
	dw	101,32
	align	4
_3657:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_991
	dd	-4
	dd	0
	align	4
_3656:
	dd	3
	dd	0
	dd	0
	align	4
_3680:
	dd	1
	dd	_339
	dd	2
	dd	_1005
	dd	_991
	dd	-4
	dd	0
_3664:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/log/logWin.bmx",0
	align	4
_3663:
	dd	_3664
	dd	11
	dd	3
	align	4
_224:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	68,101,98,117,103,32,76,111,103
	align	4
_3669:
	dd	_3664
	dd	12
	dd	3
	align	4
_3672:
	dd	_3664
	dd	13
	dd	3
	align	4
_3679:
	dd	3
	dd	0
	dd	0
	align	4
_3674:
	dd	_3664
	dd	14
	dd	4
	align	4
_3695:
	dd	1
	dd	_432
	dd	2
	dd	_1005
	dd	_991
	dd	-4
	dd	0
	align	4
_3681:
	dd	_3664
	dd	18
	dd	3
	align	4
_3691:
	dd	_3664
	dd	19
	dd	3
	align	4
_3694:
	dd	_bbStringClass
	dd	2147483647
	dd	26
	dw	83,116,97,114,116,101,100,32,105,80,97,100,32,77,97,110
	dw	97,103,101,114,32,48,46,48,46,49
	align	4
_3720:
	dd	1
	dd	_331
	dd	2
	dd	_1005
	dd	_991
	dd	-4
	dd	2
	dd	_1691
	dd	_304
	dd	-8
	dd	2
	dd	_1056
	dd	_307
	dd	-12
	dd	0
	align	4
_3696:
	dd	_3664
	dd	22
	dd	3
	align	4
_3707:
	dd	3
	dd	0
	dd	0
	align	4
_3702:
	dd	_3664
	dd	24
	dd	5
	align	4
_226:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	58,32,91,73,78,70,79,93,32
	align	4
_3713:
	dd	3
	dd	0
	dd	0
	align	4
_3708:
	dd	_3664
	dd	26
	dd	5
	align	4
_227:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	58,32,91,87,65,82,78,73,78,71,93,32
	align	4
_3719:
	dd	3
	dd	0
	dd	0
	align	4
_3714:
	dd	_3664
	dd	28
	dd	5
	align	4
_228:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	58,32,91,69,82,82,79,82,93,32
	align	4
_3740:
	dd	1
	dd	_335
	dd	2
	dd	_1005
	dd	_991
	dd	-4
	dd	0
	align	4
_3721:
	dd	_3664
	dd	33
	dd	3
	align	4
_3739:
	dd	3
	dd	0
	dd	0
	align	4
_3727:
	dd	_3664
	dd	34
	dd	4
	align	4
_229:
	dd	_bbStringClass
	dd	2147483647
	dd	25
	dw	108,111,103,87,105,110,32,116,114,105,103,103,101,114,101,100
	dw	32,97,110,32,101,118,101,110,116
	align	4
_3730:
	dd	_3664
	dd	35
	dd	4
	align	4
_3737:
	dd	3
	dd	0
	dd	0
	align	4
_3734:
	dd	_3664
	dd	37
	dd	6
	align	4
_3738:
	dd	_3664
	dd	39
	dd	4
	align	4
_3782:
	dd	1
	dd	_433
	dd	2
	dd	_1005
	dd	_991
	dd	-4
	dd	0
	align	4
_3741:
	dd	_3664
	dd	43
	dd	3
	align	4
_230:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	83,101,110,100,105,110,103,32,107,105,108,108,32,115,105,103
	dw	110,97,108
	align	4
_3744:
	dd	_3664
	dd	45
	dd	3
_3761:
	db	"g",0
	align	4
_3760:
	dd	3
	dd	0
	dd	2
	dd	_3761
	dd	_326
	dd	-8
	dd	0
	align	4
_3758:
	dd	_3664
	dd	46
	dd	4
	align	4
_3759:
	dd	_3664
	dd	47
	dd	4
	align	4
_3762:
	dd	_3664
	dd	49
	dd	3
	align	4
_3767:
	dd	_3664
	dd	50
	dd	3
	align	4
_3772:
	dd	_3664
	dd	51
	dd	3
	align	4
_3780:
	dd	_3664
	dd	52
	dd	3
	align	4
_3781:
	dd	_3664
	dd	53
	dd	3
_3787:
	db	"dlog",0
	align	4
_3786:
	dd	1
	dd	_3787
	dd	2
	dd	_1691
	dd	_304
	dd	-4
	dd	2
	dd	_1056
	dd	_307
	dd	-8
	dd	0
	align	4
_3783:
	dd	_3664
	dd	58
	dd	2
	align	4
_3800:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_1000
	dd	-4
	dd	0
	align	4
_3799:
	dd	3
	dd	0
	dd	0
	align	4
_3838:
	dd	1
	dd	_339
	dd	2
	dd	_1005
	dd	_1000
	dd	-4
	dd	0
_3825:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/MainWindow.bmx",0
	align	4
_3824:
	dd	_3825
	dd	16
	dd	3
	align	4
_295:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	105,80,97,100,32,77,97,110,97,103,101,114,32,48,46,48
	dw	46,49
	align	4
_3830:
	dd	_3825
	dd	18
	dd	3
	align	4
_3833:
	dd	_3825
	dd	19
	dd	3
	align	4
_3984:
	dd	1
	dd	_432
	dd	2
	dd	_1005
	dd	_1000
	dd	-4
	dd	0
	align	4
_3839:
	dd	_3825
	dd	22
	dd	3
	align	4
_3853:
	dd	_3825
	dd	23
	dd	3
	align	4
_3867:
	dd	_3825
	dd	24
	dd	3
	align	4
_3872:
	dd	_3825
	dd	25
	dd	3
	align	4
_3886:
	dd	_3825
	dd	26
	dd	3
	align	4
_234:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	70,105,114,115,116
	align	4
_3900:
	dd	_3825
	dd	27
	dd	3
	align	4
_235:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	76,97,115,116
	align	4
_3914:
	dd	_3825
	dd	28
	dd	3
	align	4
_236:
	dd	_bbStringClass
	dd	2147483647
	dd	2
	dw	71,111
	align	4
_3928:
	dd	_3825
	dd	31
	dd	3
	align	4
_237:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	77,111,100,101
	align	4
_3940:
	dd	_3825
	dd	32
	dd	3
	align	4
_238:
	dd	_bbStringClass
	dd	2147483647
	dd	20
	dw	81,117,105,99,107,32,77,111,100,101,32,91,116,117,114,110
	dw	32,111,110,93
	align	4
_3950:
	dd	_3825
	dd	33
	dd	3
	align	4
_239:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	72,101,108,112
	align	4
_3962:
	dd	_3825
	dd	34
	dd	3
	align	4
_240:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	65,98,111,117,116
	align	4
_3974:
	dd	_3825
	dd	35
	dd	3
	align	4
_3979:
	dd	_3825
	dd	36
	dd	3
	align	4
_4449:
	dd	1
	dd	_335
	dd	2
	dd	_1005
	dd	_1000
	dd	-4
	dd	2
	dd	_2790
	dd	_2791
	dd	-8
	dd	0
	align	4
_3985:
	dd	_3825
	dd	40
	dd	3
	align	4
_4045:
	dd	3
	dd	0
	dd	0
	align	4
_4027:
	dd	_3825
	dd	42
	dd	5
	align	4
_4033:
	dd	3
	dd	0
	dd	0
	align	4
_4029:
	dd	_3825
	dd	43
	dd	6
	align	4
_4030:
	dd	_3825
	dd	44
	dd	6
	align	4
_4039:
	dd	3
	dd	0
	dd	0
	align	4
_4035:
	dd	_3825
	dd	46
	dd	6
	align	4
_4036:
	dd	_3825
	dd	47
	dd	6
	align	4
_241:
	dd	_bbStringClass
	dd	2147483647
	dd	21
	dw	81,117,105,99,107,32,77,111,100,101,32,91,116,117,114,110
	dw	32,111,102,102,93
	align	4
_4040:
	dd	_3825
	dd	50
	dd	5
	align	4
_4049:
	dd	3
	dd	0
	dd	0
	align	4
_4046:
	dd	_3825
	dd	52
	dd	5
	align	4
_4047:
	dd	_bbStringClass
	dd	2147483647
	dd	145
	dw	105,80,97,100,32,77,97,110,97,103,101,114,32,48,46,48
	dw	46,49,32,119,97,115,32,112,114,111,103,114,97,109,109,101
	dw	100,32,98,121,32,67,111,114,101,121,32,39,82,121,97,110
	dw	39,32,68,101,97,110,32,102,111,114,32,84,97,108,108,97
	dw	115,115,101,101,32,67,105,116,121,32,83,99,104,111,111,108
	dw	115,32,102,111,114,32,117,115,101,32,111,102,32,109,97,110
	dw	97,103,105,110,103,32,116,104,101,32,115,116,117,100,101,110
	dw	116,32,105,80,97,100,115,32,97,116,32,84,97,108,108,97
	dw	115,115,101,101,32,72,105,103,104,32,83,99,104,111,111,108
	dw	46
	align	4
_4048:
	dd	_3825
	dd	53
	dd	5
	align	4
_4052:
	dd	3
	dd	0
	dd	0
	align	4
_4050:
	dd	_3825
	dd	55
	dd	5
	align	4
_243:
	dd	_bbStringClass
	dd	2147483647
	dd	25
	dw	72,69,76,80,77,69,78,85,32,65,67,84,73,79,78,32
	dw	71,79,69,83,32,72,69,82,69
	align	4
_4051:
	dd	_3825
	dd	56
	dd	5
	align	4
_4142:
	dd	3
	dd	0
	dd	0
	align	4
_4053:
	dd	_3825
	dd	58
	dd	5
	align	4
_4141:
	dd	3
	dd	0
	dd	2
	dd	_1691
	dd	_304
	dd	-12
	dd	0
	align	4
_4057:
	dd	_3825
	dd	60
	dd	7
	align	4
_4063:
	dd	_3825
	dd	61
	dd	7
	align	4
_4087:
	dd	3
	dd	0
	dd	0
	align	4
_4069:
	dd	_3825
	dd	62
	dd	8
	align	4
_4086:
	dd	3
	dd	0
	dd	2
	dd	_2286
	dd	_924
	dd	-16
	dd	0
	align	4
_4071:
	dd	_3825
	dd	63
	dd	9
	align	4
_244:
	dd	_bbStringClass
	dd	2147483647
	dd	32
	dw	83,101,97,114,99,104,105,110,103,32,102,111,114,32,101,110
	dw	116,114,121,32,119,105,116,104,32,115,101,114,105,97,108,32
	align	4
_4072:
	dd	_3825
	dd	64
	dd	9
	align	4
_4074:
	dd	_3825
	dd	65
	dd	9
	align	4
_4079:
	dd	3
	dd	0
	dd	0
	align	4
_4076:
	dd	_3825
	dd	66
	dd	10
	align	4
_4080:
	dd	_3825
	dd	68
	dd	9
	align	4
_4085:
	dd	_3825
	dd	69
	dd	9
	align	4
_4088:
	dd	_3825
	dd	72
	dd	7
_4139:
	db	"tline",0
	align	4
_4138:
	dd	3
	dd	0
	dd	2
	dd	_4139
	dd	_304
	dd	-20
	dd	0
	align	4
_4094:
	dd	_3825
	dd	73
	dd	8
	align	4
_4096:
	dd	_3825
	dd	74
	dd	8
	align	4
_284:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	48
	align	4
_4117:
	dd	3
	dd	0
	dd	0
	align	4
_4098:
	dd	_3825
	dd	75
	dd	9
	align	4
_4116:
	dd	3
	dd	0
	dd	2
	dd	_2286
	dd	_924
	dd	-24
	dd	0
	align	4
_4100:
	dd	_3825
	dd	76
	dd	10
	align	4
_4101:
	dd	_3825
	dd	77
	dd	10
	align	4
_245:
	dd	_bbStringClass
	dd	2147483647
	dd	32
	dw	83,101,97,114,99,104,105,110,32,102,111,114,32,101,110,116
	dw	114,121,32,119,105,116,104,32,66,97,114,99,111,100,101,32
	align	4
_4102:
	dd	_3825
	dd	78
	dd	10
	align	4
_4104:
	dd	_3825
	dd	79
	dd	10
	align	4
_4109:
	dd	3
	dd	0
	dd	0
	align	4
_4106:
	dd	_3825
	dd	80
	dd	11
	align	4
_4110:
	dd	_3825
	dd	82
	dd	10
	align	4
_4115:
	dd	_3825
	dd	83
	dd	10
	align	4
_4137:
	dd	3
	dd	0
	dd	0
	align	4
_4119:
	dd	_3825
	dd	85
	dd	13
	align	4
_4136:
	dd	3
	dd	0
	dd	2
	dd	_2286
	dd	_924
	dd	-28
	dd	0
	align	4
_4121:
	dd	_3825
	dd	86
	dd	9
	align	4
_4122:
	dd	_3825
	dd	87
	dd	9
	align	4
_4124:
	dd	_3825
	dd	88
	dd	9
	align	4
_4129:
	dd	3
	dd	0
	dd	0
	align	4
_4126:
	dd	_3825
	dd	89
	dd	10
	align	4
_4130:
	dd	_3825
	dd	91
	dd	9
	align	4
_4135:
	dd	_3825
	dd	92
	dd	9
	align	4
_4140:
	dd	_3825
	dd	96
	dd	7
	align	4
_4186:
	dd	3
	dd	0
	dd	0
	align	4
_4143:
	dd	_3825
	dd	100
	dd	5
	align	4
_4185:
	dd	3
	dd	0
	dd	2
	dd	_1691
	dd	_304
	dd	-32
	dd	0
	align	4
_4147:
	dd	_3825
	dd	102
	dd	7
	align	4
_4153:
	dd	_3825
	dd	103
	dd	7
	align	4
_4168:
	dd	3
	dd	0
	dd	2
	dd	_3649
	dd	_940
	dd	-36
	dd	0
	align	4
_4159:
	dd	_3825
	dd	104
	dd	8
	align	4
_246:
	dd	_bbStringClass
	dd	2147483647
	dd	36
	dw	83,101,97,114,99,104,105,110,103,32,102,111,114,32,101,110
	dw	116,114,121,32,119,105,116,104,32,70,105,114,115,116,32,78
	dw	97,109,101,32
	align	4
_4160:
	dd	_3825
	dd	105
	dd	8
	align	4
_247:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	91,98,108,97,110,107,93
	align	4
_4162:
	dd	_3825
	dd	106
	dd	8
	align	4
_4167:
	dd	_3825
	dd	107
	dd	8
	align	4
_4169:
	dd	_3825
	dd	109
	dd	7
	align	4
_4184:
	dd	3
	dd	0
	dd	2
	dd	_3649
	dd	_940
	dd	-40
	dd	0
	align	4
_4175:
	dd	_3825
	dd	110
	dd	8
	align	4
_248:
	dd	_bbStringClass
	dd	2147483647
	dd	35
	dw	83,101,97,114,99,104,105,110,103,32,102,111,114,32,101,110
	dw	116,114,121,32,119,105,116,104,32,76,97,115,116,32,78,97
	dw	109,101,32
	align	4
_4176:
	dd	_3825
	dd	111
	dd	8
	align	4
_4178:
	dd	_3825
	dd	112
	dd	8
	align	4
_4183:
	dd	_3825
	dd	113
	dd	8
	align	4
_4240:
	dd	3
	dd	0
	dd	0
	align	4
_4187:
	dd	_3825
	dd	117
	dd	5
	align	4
_4239:
	dd	3
	dd	0
	dd	0
	align	4
_4191:
	dd	_3825
	dd	119
	dd	7
	align	4
_4196:
	dd	_3825
	dd	120
	dd	7
	align	4
_4238:
	dd	3
	dd	0
	dd	0
	align	4
_4202:
	dd	_3825
	dd	121
	dd	8
	align	4
_4213:
	dd	3
	dd	0
	dd	0
	align	4
_4208:
	dd	_3825
	dd	122
	dd	9
	align	4
_4214:
	dd	_3825
	dd	124
	dd	8
	align	4
_4225:
	dd	3
	dd	0
	dd	0
	align	4
_4220:
	dd	_3825
	dd	125
	dd	9
	align	4
_4226:
	dd	_3825
	dd	127
	dd	8
	align	4
_4237:
	dd	3
	dd	0
	dd	0
	align	4
_4232:
	dd	_3825
	dd	128
	dd	9
	align	4
_4294:
	dd	3
	dd	0
	dd	0
	align	4
_4241:
	dd	_3825
	dd	133
	dd	5
	align	4
_4293:
	dd	3
	dd	0
	dd	0
	align	4
_4245:
	dd	_3825
	dd	135
	dd	7
	align	4
_4250:
	dd	_3825
	dd	136
	dd	7
	align	4
_4292:
	dd	3
	dd	0
	dd	0
	align	4
_4256:
	dd	_3825
	dd	137
	dd	8
	align	4
_4267:
	dd	3
	dd	0
	dd	0
	align	4
_4262:
	dd	_3825
	dd	138
	dd	9
	align	4
_4268:
	dd	_3825
	dd	140
	dd	8
	align	4
_4279:
	dd	3
	dd	0
	dd	0
	align	4
_4274:
	dd	_3825
	dd	141
	dd	9
	align	4
_4280:
	dd	_3825
	dd	143
	dd	8
	align	4
_4291:
	dd	3
	dd	0
	dd	0
	align	4
_4286:
	dd	_3825
	dd	144
	dd	9
	align	4
_4348:
	dd	3
	dd	0
	dd	0
	align	4
_4295:
	dd	_3825
	dd	149
	dd	5
	align	4
_4347:
	dd	3
	dd	0
	dd	0
	align	4
_4299:
	dd	_3825
	dd	151
	dd	7
	align	4
_4304:
	dd	_3825
	dd	152
	dd	7
	align	4
_4346:
	dd	3
	dd	0
	dd	0
	align	4
_4310:
	dd	_3825
	dd	153
	dd	8
	align	4
_4321:
	dd	3
	dd	0
	dd	0
	align	4
_4316:
	dd	_3825
	dd	154
	dd	9
	align	4
_4322:
	dd	_3825
	dd	156
	dd	8
	align	4
_4333:
	dd	3
	dd	0
	dd	0
	align	4
_4328:
	dd	_3825
	dd	157
	dd	9
	align	4
_4334:
	dd	_3825
	dd	159
	dd	8
	align	4
_4345:
	dd	3
	dd	0
	dd	0
	align	4
_4340:
	dd	_3825
	dd	160
	dd	9
	align	4
_4402:
	dd	3
	dd	0
	dd	0
	align	4
_4349:
	dd	_3825
	dd	165
	dd	5
	align	4
_4401:
	dd	3
	dd	0
	dd	0
	align	4
_4353:
	dd	_3825
	dd	167
	dd	7
	align	4
_4358:
	dd	_3825
	dd	168
	dd	7
	align	4
_4400:
	dd	3
	dd	0
	dd	0
	align	4
_4364:
	dd	_3825
	dd	169
	dd	8
	align	4
_4375:
	dd	3
	dd	0
	dd	0
	align	4
_4370:
	dd	_3825
	dd	170
	dd	9
	align	4
_4376:
	dd	_3825
	dd	172
	dd	8
	align	4
_4387:
	dd	3
	dd	0
	dd	0
	align	4
_4382:
	dd	_3825
	dd	173
	dd	9
	align	4
_4388:
	dd	_3825
	dd	175
	dd	8
	align	4
_4399:
	dd	3
	dd	0
	dd	0
	align	4
_4394:
	dd	_3825
	dd	176
	dd	9
	align	4
_4403:
	dd	_3825
	dd	183
	dd	3
	align	4
_4416:
	dd	3
	dd	0
	dd	0
	align	4
_4409:
	dd	_3825
	dd	184
	dd	4
	align	4
_4414:
	dd	3
	dd	0
	dd	0
	align	4
_4413:
	dd	_3825
	dd	186
	dd	6
	align	4
_4415:
	dd	_3825
	dd	188
	dd	4
	align	4
_4417:
	dd	_3825
	dd	190
	dd	3
_4447:
	db	"es",0
	align	4
_4446:
	dd	3
	dd	0
	dd	2
	dd	_4447
	dd	_326
	dd	-44
	dd	0
	align	4
_4431:
	dd	_3825
	dd	191
	dd	4
	align	4
_4445:
	dd	3
	dd	0
	dd	0
	align	4
_4433:
	dd	_3825
	dd	192
	dd	5
	align	4
_4441:
	dd	3
	dd	0
	dd	0
	align	4
_4438:
	dd	_3825
	dd	196
	dd	7
	align	4
_253:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	32,73,68,58
	align	4
_252:
	dd	_bbStringClass
	dd	2147483647
	dd	35
	dw	80,76,69,65,83,69,32,83,69,84,32,85,80,32,69,86
	dw	69,78,84,32,72,65,78,68,76,69,32,70,79,82,32,83
	dw	82,67,58
	align	4
_4442:
	dd	3
	dd	0
	dd	0
	align	4
_4443:
	dd	3
	dd	0
	dd	0
	align	4
_4444:
	dd	_3825
	dd	199
	dd	5
	align	4
_4448:
	dd	_3825
	dd	202
	dd	3
	align	4
_4487:
	dd	1
	dd	_433
	dd	2
	dd	_1005
	dd	_1000
	dd	-4
	dd	0
	align	4
_4450:
	dd	_3825
	dd	205
	dd	3
	align	4
_4466:
	dd	3
	dd	0
	dd	2
	dd	_3761
	dd	_326
	dd	-8
	dd	0
	align	4
_4464:
	dd	_3825
	dd	206
	dd	4
	align	4
_4465:
	dd	_3825
	dd	207
	dd	4
	align	4
_4467:
	dd	_3825
	dd	209
	dd	3
	align	4
_4472:
	dd	_3825
	dd	210
	dd	3
	align	4
_4477:
	dd	_3825
	dd	211
	dd	3
	align	4
_4485:
	dd	_3825
	dd	212
	dd	3
	align	4
_4486:
	dd	_3825
	dd	213
	dd	3
_4589:
	db	"prompt",0
_4590:
	db	"charLimit",0
_4591:
	db	"supress",0
_4592:
	db	"pwin",0
_4593:
	db	"TF",0
_4594:
	db	"ab",0
	align	4
_4588:
	dd	1
	dd	_4589
	dd	2
	dd	_1691
	dd	_304
	dd	-4
	dd	2
	dd	_4590
	dd	_307
	dd	-8
	dd	2
	dd	_4591
	dd	_307
	dd	-12
	dd	2
	dd	_968
	dd	_307
	dd	-16
	dd	2
	dd	_4592
	dd	_360
	dd	-20
	dd	2
	dd	_4593
	dd	_395
	dd	-24
	dd	2
	dd	_4594
	dd	_438
	dd	-28
	dd	0
	align	4
_4488:
	dd	_879
	dd	34
	dd	2
	align	4
_4490:
	dd	_879
	dd	35
	dd	2
	align	4
_4493:
	dd	_879
	dd	36
	dd	2
	align	4
_4495:
	dd	_879
	dd	37
	dd	2
	align	4
_4497:
	dd	_879
	dd	38
	dd	2
	align	4
_4500:
	dd	_879
	dd	39
	dd	2
	align	4
_4503:
	dd	_879
	dd	40
	dd	2
	align	4
_4587:
	dd	3
	dd	0
	dd	0
	align	4
_4504:
	dd	_879
	dd	41
	dd	3
	align	4
_4505:
	dd	_879
	dd	42
	dd	3
	align	4
_4530:
	dd	3
	dd	0
	dd	0
	align	4
_4517:
	dd	_879
	dd	44
	dd	5
	align	4
_4529:
	dd	3
	dd	0
	dd	0
	align	4
_4519:
	dd	_879
	dd	45
	dd	6
	align	4
_4528:
	dd	3
	dd	0
	dd	0
	align	4
_4523:
	dd	_879
	dd	46
	dd	7
	align	4
_4566:
	dd	3
	dd	0
	dd	2
	dd	_1856
	dd	_307
	dd	-32
	dd	0
	align	4
_4531:
	dd	_879
	dd	50
	dd	5
	align	4
_4533:
	dd	_879
	dd	51
	dd	5
	align	4
_4538:
	dd	3
	dd	0
	dd	0
	align	4
_4537:
	dd	_879
	dd	52
	dd	6
	align	4
_4541:
	dd	3
	dd	0
	dd	0
	align	4
_4540:
	dd	_879
	dd	54
	dd	6
	align	4
_275:
	dd	_bbStringClass
	dd	2147483647
	dd	77
	dw	65,114,101,32,121,111,117,32,115,117,114,101,63,32,82,101
	dw	109,101,109,98,101,114,32,116,104,97,116,32,116,104,101,114
	dw	101,32,105,115,32,110,111,32,119,97,121,32,116,111,32,117
	dw	110,100,111,32,119,104,97,116,32,121,111,117,32,97,114,101
	dw	32,97,98,111,117,116,32,116,111,32,100,111,46
	align	4
_4542:
	dd	_879
	dd	56
	dd	5
_4565:
	db	"rline",0
	align	4
_4564:
	dd	3
	dd	0
	dd	2
	dd	_4565
	dd	_304
	dd	-36
	dd	0
	align	4
_4544:
	dd	_879
	dd	57
	dd	6
	align	4
_4548:
	dd	_879
	dd	58
	dd	6
	align	4
_4551:
	dd	_879
	dd	59
	dd	6
	align	4
_4554:
	dd	_879
	dd	60
	dd	6
	align	4
_4557:
	dd	_879
	dd	61
	dd	6
	align	4
_4560:
	dd	_879
	dd	62
	dd	6
	align	4
_4561:
	dd	_879
	dd	63
	dd	6
	align	4
_4562:
	dd	_879
	dd	64
	dd	6
	align	4
_4563:
	dd	_879
	dd	65
	dd	6
	align	4
_4586:
	dd	3
	dd	0
	dd	0
	align	4
_4567:
	dd	_879
	dd	68
	dd	5
	align	4
_4585:
	dd	3
	dd	0
	dd	0
	align	4
_4569:
	dd	_879
	dd	69
	dd	6
	align	4
_4572:
	dd	_879
	dd	70
	dd	6
	align	4
_4575:
	dd	_879
	dd	71
	dd	6
	align	4
_4578:
	dd	_879
	dd	72
	dd	6
	align	4
_4581:
	dd	_879
	dd	73
	dd	6
	align	4
_4582:
	dd	_879
	dd	74
	dd	6
	align	4
_4583:
	dd	_879
	dd	75
	dd	6
	align	4
_4584:
	dd	_879
	dd	76
	dd	6
_4623:
	db	"convertFromCSV",0
_4624:
	db	"word",0
_4625:
	db	"letter",0
	align	4
_4622:
	dd	1
	dd	_4623
	dd	2
	dd	_1691
	dd	_304
	dd	-4
	dd	2
	dd	_1767
	dd	_329
	dd	-8
	dd	2
	dd	_4624
	dd	_304
	dd	-12
	dd	2
	dd	_4625
	dd	_304
	dd	-16
	dd	2
	dd	_307
	dd	_307
	dd	-20
	dd	0
	align	4
_4595:
	dd	_879
	dd	82
	dd	2
	align	4
_4597:
	dd	_879
	dd	83
	dd	2
	align	4
_4599:
	dd	_879
	dd	84
	dd	2
	align	4
_4601:
	dd	_879
	dd	85
	dd	2
	align	4
_4603:
	dd	_879
	dd	100
	dd	2
	align	4
_4620:
	dd	3
	dd	0
	dd	0
	align	4
_4604:
	dd	_879
	dd	87
	dd	3
	align	4
_4605:
	dd	_879
	dd	88
	dd	3
	align	4
_4613:
	dd	3
	dd	0
	dd	0
	align	4
_4607:
	dd	_879
	dd	89
	dd	4
	align	4
_4610:
	dd	3
	dd	0
	dd	0
	align	4
_4609:
	dd	_879
	dd	90
	dd	5
	align	4
_4611:
	dd	_879
	dd	92
	dd	4
	align	4
_4612:
	dd	_879
	dd	93
	dd	4
	align	4
_4614:
	dd	_879
	dd	95
	dd	3
	align	4
_4617:
	dd	3
	dd	0
	dd	0
	align	4
_4616:
	dd	_879
	dd	96
	dd	4
	align	4
_4618:
	dd	_879
	dd	98
	dd	3
	align	4
_4619:
	dd	_879
	dd	99
	dd	3
	align	4
_4621:
	dd	_879
	dd	101
	dd	2
_4657:
	db	"convertFromSSV",0
	align	4
_4656:
	dd	1
	dd	_4657
	dd	2
	dd	_1691
	dd	_304
	dd	-4
	dd	2
	dd	_1767
	dd	_329
	dd	-8
	dd	2
	dd	_4624
	dd	_304
	dd	-12
	dd	2
	dd	_4625
	dd	_304
	dd	-16
	dd	2
	dd	_307
	dd	_307
	dd	-20
	dd	0
	align	4
_4626:
	dd	_879
	dd	105
	dd	2
	align	4
_4628:
	dd	_879
	dd	106
	dd	2
	align	4
_4630:
	dd	_879
	dd	107
	dd	2
	align	4
_4632:
	dd	_879
	dd	108
	dd	2
	align	4
_4634:
	dd	_879
	dd	120
	dd	2
	align	4
_4647:
	dd	3
	dd	0
	dd	0
	align	4
_4635:
	dd	_879
	dd	110
	dd	3
	align	4
_4636:
	dd	_879
	dd	111
	dd	3
	align	4
_4640:
	dd	3
	dd	0
	dd	0
	align	4
_4638:
	dd	_879
	dd	112
	dd	4
	align	4
_4639:
	dd	_879
	dd	113
	dd	4
	align	4
_4641:
	dd	_879
	dd	115
	dd	3
	align	4
_4644:
	dd	3
	dd	0
	dd	0
	align	4
_4643:
	dd	_879
	dd	116
	dd	4
	align	4
_4645:
	dd	_879
	dd	118
	dd	3
	align	4
_4646:
	dd	_879
	dd	119
	dd	3
	align	4
_4648:
	dd	_879
	dd	121
	dd	2
	align	4
_282:
	dd	_bbStringClass
	dd	2147483647
	dd	3
	dw	101,111,110
	align	4
_4651:
	dd	3
	dd	0
	dd	0
	align	4
_4650:
	dd	_879
	dd	122
	dd	3
	align	4
_4652:
	dd	_879
	dd	124
	dd	2
	align	4
_177:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	93
	align	4
_283:
	dd	_bbStringClass
	dd	2147483647
	dd	26
	dw	84,104,101,32,110,111,116,101,32,100,97,116,97,32,105,115
	dw	32,99,111,114,114,117,112,116,32,91
	align	4
_4655:
	dd	_879
	dd	125
	dd	2
_4662:
	db	"resetFile",0
_4663:
	db	"filen",0
	align	4
_4661:
	dd	1
	dd	_4662
	dd	2
	dd	_2493
	dd	_995
	dd	-4
	dd	2
	dd	_4663
	dd	_304
	dd	-8
	dd	0
	align	4
_4658:
	dd	_879
	dd	129
	dd	2
	align	4
_4659:
	dd	_879
	dd	130
	dd	2
	align	4
_4660:
	dd	_879
	dd	131
	dd	2
	align	4
_4666:
	dd	1
	dd	_298
	dd	2
	dd	_1005
	dd	_397
	dd	-4
	dd	0
	align	4
_4665:
	dd	3
	dd	0
	dd	0
_4697:
	db	"readOnly",0
_4698:
	db	"wordWrap",0
	align	4
_4696:
	dd	1
	dd	_339
	dd	2
	dd	_1005
	dd	_397
	dd	-4
	dd	2
	dd	_1051
	dd	_307
	dd	-8
	dd	2
	dd	_1052
	dd	_307
	dd	-12
	dd	2
	dd	_1053
	dd	_307
	dd	-16
	dd	2
	dd	_1054
	dd	_307
	dd	-20
	dd	2
	dd	_1090
	dd	_360
	dd	-24
	dd	2
	dd	_4697
	dd	_307
	dd	-28
	dd	2
	dd	_4698
	dd	_307
	dd	-32
	dd	2
	dd	_1056
	dd	_307
	dd	-36
	dd	0
_4671:
	db	"C:/Users/ryan.dean/Desktop/fd.new/projects/iPad Manager/GUI/TextArea.bmx",0
	align	4
_4670:
	dd	_4671
	dd	8
	dd	3
	align	4
_4673:
	dd	_4671
	dd	9
	dd	3
	align	4
_4676:
	dd	3
	dd	0
	dd	0
	align	4
_4675:
	dd	_4671
	dd	10
	dd	4
	align	4
_4677:
	dd	_4671
	dd	12
	dd	3
	align	4
_4680:
	dd	3
	dd	0
	dd	0
	align	4
_4679:
	dd	_4671
	dd	13
	dd	4
	align	4
_4681:
	dd	_4671
	dd	15
	dd	3
	align	4
_4691:
	dd	_4671
	dd	16
	dd	3
	align	4
_4702:
	dd	1
	dd	_346
	dd	2
	dd	_1005
	dd	_397
	dd	-4
	dd	0
	align	4
_4699:
	dd	_4671
	dd	19
	dd	3
	align	4
_4706:
	dd	1
	dd	_348
	dd	2
	dd	_1005
	dd	_397
	dd	-4
	dd	0
	align	4
_4703:
	dd	_4671
	dd	22
	dd	3
_4714:
	db	"newTextArea",0
	align	4
_4713:
	dd	1
	dd	_4714
	dd	2
	dd	_1051
	dd	_307
	dd	-4
	dd	2
	dd	_1052
	dd	_307
	dd	-8
	dd	2
	dd	_1053
	dd	_307
	dd	-12
	dd	2
	dd	_1054
	dd	_307
	dd	-16
	dd	2
	dd	_1090
	dd	_360
	dd	-20
	dd	2
	dd	_1142
	dd	_307
	dd	-24
	dd	2
	dd	_1159
	dd	_397
	dd	-28
	dd	0
	align	4
_4707:
	dd	_4671
	dd	27
	dd	2
	align	4
_4709:
	dd	_4671
	dd	28
	dd	2
	align	4
_4712:
	dd	_4671
	dd	29
	dd	2
