	.reference	___bb_appstub_appstub
	.reference	___bb_audio_audio
	.reference	___bb_basic_basic
	.reference	___bb_blitz_blitz
	.reference	___bb_bmploader_bmploader
	.reference	___bb_d3d7max2d_d3d7max2d
	.reference	___bb_d3d9max2d_d3d9max2d
	.reference	___bb_data_data
	.reference	___bb_directsoundaudio_directsoundaudio
	.reference	___bb_drivers_drivers
	.reference	___bb_dxgraphics_dxgraphics
	.reference	___bb_eventqueue_eventqueue
	.reference	___bb_font_font
	.reference	___bb_freeaudioaudio_freeaudioaudio
	.reference	___bb_freejoy_freejoy
	.reference	___bb_freeprocess_freeprocess
	.reference	___bb_freetypefont_freetypefont
	.reference	___bb_glew_glew
	.reference	___bb_glgraphics_glgraphics
	.reference	___bb_glmax2d_glmax2d
	.reference	___bb_gnet_gnet
	.reference	___bb_jpgloader_jpgloader
	.reference	___bb_maxlua_maxlua
	.reference	___bb_maxutil_maxutil
	.reference	___bb_oggloader_oggloader
	.reference	___bb_openalaudio_openalaudio
	.reference	___bb_pngloader_pngloader
	.reference	___bb_retro_retro
	.reference	___bb_tgaloader_tgaloader
	.reference	___bb_threads_threads
	.reference	___bb_timer_timer
	.reference	___bb_wavloader_wavloader
	.reference	___bb_win32_win32
	.reference	_bbEmptyString
	.reference	_bbEnd
	.reference	_bbGCFree
	.reference	_bbNullObject
	.reference	_bbObjectClass
	.reference	_bbObjectCompare
	.reference	_bbObjectCtor
	.reference	_bbObjectDowncast
	.reference	_bbObjectFree
	.reference	_bbObjectNew
	.reference	_bbObjectRegisterType
	.reference	_bbObjectReserved
	.reference	_bbObjectSendMessage
	.reference	_bbObjectToString
	.reference	_bbStringClass
	.reference	_bbStringCompare
	.reference	_bbStringConcat
	.reference	_bbStringFromInt
	.reference	_bbStringToInt
	.reference	_brl_eventqueue_CurrentEvent
	.reference	_brl_eventqueue_EventID
	.reference	_brl_eventqueue_EventSource
	.reference	_brl_eventqueue_WaitEvent
	.reference	_brl_filesystem_CloseFile
	.reference	_brl_filesystem_OpenFile
	.reference	_brl_filesystem_WriteFile
	.reference	_brl_linkedlist_ListAddLast
	.reference	_brl_linkedlist_ListContains
	.reference	_brl_linkedlist_ListRemove
	.reference	_brl_linkedlist_TList
	.reference	_brl_retro_Left
	.reference	_brl_retro_Right
	.reference	_brl_stream_Eof
	.reference	_brl_stream_ReadLine
	.reference	_brl_stream_WriteLine
	.reference	_brl_system_Confirm
	.reference	_brl_system_CurrentDate
	.reference	_brl_system_CurrentTime
	.reference	_brl_system_Notify
	.reference	_maxgui_maxgui_ActivateGadget
	.reference	_maxgui_maxgui_ActiveGadget
	.reference	_maxgui_maxgui_AddGadgetItem
	.reference	_maxgui_maxgui_ButtonState
	.reference	_maxgui_maxgui_CreateButton
	.reference	_maxgui_maxgui_CreateLabel
	.reference	_maxgui_maxgui_CreateListBox
	.reference	_maxgui_maxgui_CreateMenu
	.reference	_maxgui_maxgui_CreateTextArea
	.reference	_maxgui_maxgui_CreateTextField
	.reference	_maxgui_maxgui_CreateWindow
	.reference	_maxgui_maxgui_FreeGadget
	.reference	_maxgui_maxgui_GadgetItemText
	.reference	_maxgui_maxgui_GadgetText
	.reference	_maxgui_maxgui_GadgetWidth
	.reference	_maxgui_maxgui_HideGadget
	.reference	_maxgui_maxgui_ModifyGadgetItem
	.reference	_maxgui_maxgui_PopupWindowMenu
	.reference	_maxgui_maxgui_RemoveGadgetItem
	.reference	_maxgui_maxgui_SelectedGadgetItem
	.reference	_maxgui_maxgui_SetButtonState
	.reference	_maxgui_maxgui_SetGadgetFilter
	.reference	_maxgui_maxgui_SetGadgetText
	.reference	_maxgui_maxgui_ShowGadget
	.reference	_maxgui_maxgui_TGadget
	.reference	_maxgui_maxgui_TextAreaText
	.reference	_maxgui_maxgui_TextFieldText
	.reference	_maxgui_maxgui_UpdateWindowMenu
	.reference	_maxgui_maxgui_WindowMenu
	.globl	__bb_Button_Delete
	.globl	__bb_Button_New
	.globl	__bb_Button__cstate
	.globl	__bb_Button__init
	.globl	__bb_Button__state
	.globl	__bb_ComboBox_Delete
	.globl	__bb_ComboBox_New
	.globl	__bb_ListBox_Delete
	.globl	__bb_ListBox_New
	.globl	__bb_ListBox__add
	.globl	__bb_ListBox__getText
	.globl	__bb_ListBox__init
	.globl	__bb_ListBox__modify
	.globl	__bb_ListBox__remove
	.globl	__bb_MainWindow_Delete
	.globl	__bb_MainWindow_New
	.globl	__bb_MainWindow__end
	.globl	__bb_MainWindow__init
	.globl	__bb_MainWindow__populate
	.globl	__bb_MainWindow__update
	.globl	__bb_Note_Delete
	.globl	__bb_Note_New
	.globl	__bb_Note__hide
	.globl	__bb_Note__init
	.globl	__bb_Note__save
	.globl	__bb_Note__typeByInt
	.globl	__bb_Note__update
	.globl	__bb_Student_Delete
	.globl	__bb_Student_New
	.globl	__bb_TextArea_Delete
	.globl	__bb_TextArea_New
	.globl	__bb_TextArea__clear
	.globl	__bb_TextArea__getText
	.globl	__bb_TextArea__init
	.globl	__bb_TextField_Delete
	.globl	__bb_TextField_New
	.globl	__bb_TextField__clear
	.globl	__bb_TextField__getText
	.globl	__bb_TextField__init
	.globl	__bb_Window_Delete
	.globl	__bb_Window_New
	.globl	__bb_Window__hide
	.globl	__bb_Window__new
	.globl	__bb_Window__show
	.globl	__bb_Window__update
	.globl	__bb_iPad_Delete
	.globl	__bb_iPad_New
	.globl	__bb_iPad__addNote
	.globl	__bb_iPad__build
	.globl	__bb_iPad__hide
	.globl	__bb_iPad__init
	.globl	__bb_iPad__noteBySummary
	.globl	__bb_iPad__save
	.globl	__bb_iPad__show
	.globl	__bb_iPad__update
	.globl	__bb_logWin_Delete
	.globl	__bb_logWin_New
	.globl	__bb_logWin__end
	.globl	__bb_logWin__init
	.globl	__bb_logWin__new
	.globl	__bb_logWin__populate
	.globl	__bb_logWin__update
	.globl	__bb_main
	.globl	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Application
	.globl	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete
	.globl	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New
	.globl	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Resources
	.globl	__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete
	.globl	__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New
	.globl	__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_Delete
	.globl	__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_New
	.globl	_bb_Button
	.globl	_bb_ComboBox
	.globl	_bb_ListBox
	.globl	_bb_MainWindow
	.globl	_bb_Note
	.globl	_bb_Student
	.globl	_bb_TextArea
	.globl	_bb_TextField
	.globl	_bb_Window
	.globl	_bb_convertFromCSV
	.globl	_bb_convertFromSSV
	.globl	_bb_dataDir
	.globl	_bb_debugWin
	.globl	_bb_dlog
	.globl	_bb_findByBC
	.globl	_bb_findByFirst
	.globl	_bb_findByLast
	.globl	_bb_findBySN
	.globl	_bb_iPad
	.globl	_bb_iPaddb
	.globl	_bb_insertIpadEntry
	.globl	_bb_ipadList
	.globl	_bb_ipaddbn
	.globl	_bb_logWin
	.globl	_bb_newBlankIpadBC
	.globl	_bb_newBlankIpadSN
	.globl	_bb_newButton
	.globl	_bb_newIpad
	.globl	_bb_newListBox
	.globl	_bb_newNote
	.globl	_bb_newTextArea
	.globl	_bb_newTextField
	.globl	_bb_noComma
	.globl	_bb_noteWinList
	.globl	_bb_prompt
	.globl	_bb_quickMode
	.globl	_bb_readNote
	.globl	_bb_resetFile
	.globl	_bb_studentList
	.globl	_bb_winList
	.text	
__bb_main:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$28,%esp
	cmpl	$0,_734
	je	_735
	mov	$0,%eax
	add	$28,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_735:
	movl	$1,_734
	call	___bb_blitz_blitz
	call	___bb_drivers_drivers
	call	___bb_appstub_appstub
	call	___bb_audio_audio
	call	___bb_basic_basic
	call	___bb_bmploader_bmploader
	call	___bb_d3d7max2d_d3d7max2d
	call	___bb_d3d9max2d_d3d9max2d
	call	___bb_data_data
	call	___bb_directsoundaudio_directsoundaudio
	call	___bb_dxgraphics_dxgraphics
	call	___bb_eventqueue_eventqueue
	call	___bb_font_font
	call	___bb_freeaudioaudio_freeaudioaudio
	call	___bb_freetypefont_freetypefont
	call	___bb_glgraphics_glgraphics
	call	___bb_glmax2d_glmax2d
	call	___bb_gnet_gnet
	call	___bb_jpgloader_jpgloader
	call	___bb_maxlua_maxlua
	call	___bb_maxutil_maxutil
	call	___bb_oggloader_oggloader
	call	___bb_openalaudio_openalaudio
	call	___bb_pngloader_pngloader
	call	___bb_retro_retro
	call	___bb_tgaloader_tgaloader
	call	___bb_threads_threads
	call	___bb_timer_timer
	call	___bb_wavloader_wavloader
	call	___bb_freejoy_freejoy
	call	___bb_freeprocess_freeprocess
	call	___bb_glew_glew
	call	___bb_win32_win32
	movl	$_10,(%esp)
	call	_bbObjectRegisterType
	movl	$_11,(%esp)
	call	_bbObjectRegisterType
	movl	$_17,(%esp)
	call	_bbObjectRegisterType
	movl	$_bb_Window,(%esp)
	call	_bbObjectRegisterType
	movl	$_bb_Button,(%esp)
	call	_bbObjectRegisterType
	movl	$_bb_TextField,(%esp)
	call	_bbObjectRegisterType
	movl	$_bb_ComboBox,(%esp)
	call	_bbObjectRegisterType
	movl	$_bb_ListBox,(%esp)
	call	_bbObjectRegisterType
	movl	$_bb_iPad,(%esp)
	call	_bbObjectRegisterType
	movl	$_bb_Note,(%esp)
	call	_bbObjectRegisterType
	movl	$_bb_Student,(%esp)
	call	_bbObjectRegisterType
	movl	$_bb_logWin,(%esp)
	call	_bbObjectRegisterType
	movl	$_bb_MainWindow,(%esp)
	call	_bbObjectRegisterType
	movl	$_bb_TextArea,(%esp)
	call	_bbObjectRegisterType
	movl	_687,%eax
	and	$1,%eax
	cmp	$0,%eax
	jne	_688
	movl	$_brl_linkedlist_TList,(%esp)
	call	_bbObjectNew
	incl	4(%eax)
	movl	%eax,_bb_winList
	orl	$1,_687
_688:
	movl	_687,%eax
	and	$2,%eax
	cmp	$0,%eax
	jne	_690
	movl	$_brl_linkedlist_TList,(%esp)
	call	_bbObjectNew
	incl	4(%eax)
	movl	%eax,_bb_ipadList
	orl	$2,_687
_690:
	movl	_687,%eax
	and	$4,%eax
	cmp	$0,%eax
	jne	_692
	movl	$_brl_linkedlist_TList,(%esp)
	call	_bbObjectNew
	incl	4(%eax)
	movl	%eax,_bb_studentList
	orl	$4,_687
_692:
	movl	_687,%eax
	and	$8,%eax
	cmp	$0,%eax
	jne	_694
	movl	$_bb_logWin,(%esp)
	call	_bbObjectNew
	incl	4(%eax)
	movl	%eax,_bb_debugWin
	orl	$8,_687
_694:
	movl	_bb_debugWin,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	movl	$0,4(%esp)
	movl	$_696,(%esp)
	call	_bb_dlog
	movl	_687,%eax
	and	$16,%eax
	cmp	$0,%eax
	jne	_698
	movl	$_21,4(%esp)
	movl	_bb_dataDir,%eax
	movl	%eax,(%esp)
	call	_bbStringConcat
	incl	4(%eax)
	movl	%eax,_bb_ipaddbn
	orl	$16,_687
_698:
	movl	_687,%eax
	and	$32,%eax
	cmp	$0,%eax
	jne	_700
	movl	$1,8(%esp)
	movl	$1,4(%esp)
	movl	_bb_ipaddbn,%eax
	movl	%eax,(%esp)
	call	_brl_filesystem_OpenFile
	incl	4(%eax)
	movl	%eax,_bb_iPaddb
	orl	$32,_687
_700:
	cmpl	$_bbNullObject,_bb_iPaddb
	jne	_701
	movl	_bb_ipaddbn,%eax
	movl	%eax,(%esp)
	call	_brl_filesystem_WriteFile
	movl	$1,8(%esp)
	movl	$1,4(%esp)
	movl	_bb_ipaddbn,%eax
	movl	%eax,(%esp)
	call	_brl_filesystem_OpenFile
	mov	%eax,%ebx
	incl	4(%ebx)
	movl	_bb_iPaddb,%eax
	decl	4(%eax)
	jnz	_705
	movl	%eax,(%esp)
	call	_bbGCFree
_705:
	movl	%ebx,_bb_iPaddb
_701:
	movl	_687,%eax
	and	$64,%eax
	cmp	$0,%eax
	jne	_707
	movl	$_brl_linkedlist_TList,(%esp)
	call	_bbObjectNew
	incl	4(%eax)
	movl	%eax,_bb_noteWinList
	orl	$64,_687
_707:
	xor	%eax,%eax
	cmp	$0,%eax
	je	_708
	movl	$1,12(%esp)
	movl	$1,8(%esp)
	movl	$0,4(%esp)
	movl	$_183,(%esp)
	call	_bb_prompt
	movl	$_18,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	je	_709
	movl	$1,4(%esp)
	movl	$_711,(%esp)
	call	_brl_system_Notify
	call	_bbEnd
_709:
_708:
	movl	$_bb_MainWindow,(%esp)
	call	_bbObjectNew
	mov	%eax,%edi
	mov	%edi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	jmp	_186
_188:
	call	_brl_eventqueue_WaitEvent
	mov	%edi,%eax
	movl	_brl_eventqueue_CurrentEvent,%edx
	movl	%edx,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_715
	mov	$0,%eax
	jmp	_352
_715:
	movl	_bb_debugWin,%eax
	cmpl	$_bbNullObject,8(%eax)
	je	_716
	movl	_bb_debugWin,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*60(%eax)
_716:
	movl	_bb_ipadList,%ebx
	mov	%ebx,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	mov	%eax,%esi
	jmp	_189
_191:
	mov	%esi,%eax
	movl	$_bb_iPad,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	cmp	$_bbNullObject,%eax
	je	_189
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*72(%eax)
_189:
	mov	%esi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_191
_190:
	movl	_bb_noteWinList,%ebx
	mov	%ebx,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	mov	%eax,%esi
	jmp	_192
_194:
	mov	%esi,%eax
	movl	$_bb_Note,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	cmp	$_bbNullObject,%eax
	je	_192
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
_192:
	mov	%esi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_194
_193:
_186:
	mov	$1,%eax
	cmp	$0,%eax
	jne	_188
_187:
	mov	%edi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*60(%eax)
	movl	_bb_debugWin,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*64(%eax)
	call	_bbEnd
	mov	$0,%eax
	jmp	_352
_352:
	add	$28,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_10,(%ebx)
	mov	$0,%eax
	jmp	_355
_355:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_Delete:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
_358:
	mov	$0,%eax
	jmp	_736
_736:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_11,(%ebx)
	mov	$0,%eax
	jmp	_361
_361:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
_364:
	mov	$0,%eax
	jmp	_737
_737:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_17,(%ebx)
	mov	$0,%eax
	jmp	_367
_367:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
_370:
	mov	$0,%eax
	jmp	_738
_738:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Window_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_bb_Window,(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,8(%ebx)
	mov	$_bbEmptyString,%eax
	incl	4(%eax)
	movl	%eax,12(%ebx)
	movl	$_brl_linkedlist_TList,(%esp)
	call	_bbObjectNew
	incl	4(%eax)
	movl	%eax,16(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,20(%ebx)
	mov	$0,%eax
	jmp	_373
_373:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Window_Delete:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
_376:
	movl	20(%ebx),%eax
	decl	4(%eax)
	jnz	_745
	movl	%eax,(%esp)
	call	_bbGCFree
_745:
	movl	16(%ebx),%eax
	decl	4(%eax)
	jnz	_747
	movl	%eax,(%esp)
	call	_bbGCFree
_747:
	movl	12(%ebx),%eax
	decl	4(%eax)
	jnz	_749
	movl	%eax,(%esp)
	call	_bbGCFree
_749:
	movl	8(%ebx),%eax
	decl	4(%eax)
	jnz	_751
	movl	%eax,(%esp)
	call	_bbGCFree
_751:
	mov	$0,%eax
	jmp	_743
_743:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Window__new:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$28,%esp
	movl	8(%ebp),%esi
	movl	16(%ebp),%edi
	movl	20(%ebp),%ebx
	movl	24(%ebp),%ecx
	movl	28(%ebp),%edx
	movl	32(%ebp),%eax
	movl	%eax,24(%esp)
	movl	$_bbNullObject,20(%esp)
	movl	%ecx,16(%esp)
	movl	%ebx,12(%esp)
	movl	%edi,8(%esp)
	movl	12(%ebp),%eax
	movl	%eax,4(%esp)
	movl	%edx,(%esp)
	call	_maxgui_maxgui_CreateWindow
	incl	4(%eax)
	mov	%eax,%ebx
	movl	8(%esi),%eax
	decl	4(%eax)
	jnz	_755
	movl	%eax,(%esp)
	call	_bbGCFree
_755:
	movl	%ebx,8(%esi)
	movl	%esi,4(%esp)
	movl	_bb_winList,%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListAddLast
	movl	8(%esi),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_WindowMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	20(%esi),%eax
	decl	4(%eax)
	jnz	_759
	movl	%eax,(%esp)
	call	_bbGCFree
_759:
	movl	%ebx,20(%esi)
	mov	$0,%eax
	jmp	_385
_385:
	add	$28,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Window__show:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_ShowGadget
	mov	$0,%eax
	jmp	_388
_388:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Window__hide:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_HideGadget
	mov	$0,%eax
	jmp	_391
_391:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Window__update:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	mov	$0,%eax
	jmp	_394
_394:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Button_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_bb_Button,(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,8(%ebx)
	movl	$0,12(%ebx)
	mov	$0,%eax
	jmp	_397
_397:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Button_Delete:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
_400:
	movl	8(%eax),%eax
	decl	4(%eax)
	jnz	_763
	movl	%eax,(%esp)
	call	_bbGCFree
_763:
	mov	$0,%eax
	jmp	_761
_761:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Button__init:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$28,%esp
	movl	8(%ebp),%ebx
	movl	20(%ebp),%esi
	movl	24(%ebp),%edx
	movl	28(%ebp),%eax
	movl	32(%ebp),%edi
	movl	36(%ebp),%ecx
	movl	%ecx,24(%esp)
	movl	8(%edi),%ecx
	movl	%ecx,20(%esp)
	movl	%eax,16(%esp)
	movl	%edx,12(%esp)
	movl	%esi,8(%esp)
	movl	16(%ebp),%eax
	movl	%eax,4(%esp)
	movl	12(%ebp),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_CreateButton
	incl	4(%eax)
	mov	%eax,%esi
	movl	8(%ebx),%eax
	decl	4(%eax)
	jnz	_767
	movl	%eax,(%esp)
	call	_bbGCFree
_767:
	movl	%esi,8(%ebx)
	movl	8(%ebx),%eax
	movl	%eax,4(%esp)
	movl	16(%edi),%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListAddLast
	mov	$0,%eax
	jmp	_410
_410:
	add	$28,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Button__state:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$20,%esp
	movl	8(%ebp),%ebx
	movl	12(%ebx),%eax
	cmp	$0,%eax
	sete	%al
	movzbl	%al,%eax
	movl	%eax,12(%ebx)
	movl	12(%ebx),%eax
	movl	%eax,4(%esp)
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetButtonState
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_ButtonState
	jmp	_413
_413:
	add	$20,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Button__cstate:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
	movl	12(%eax),%eax
	jmp	_416
_416:
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_newButton:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$44,%esp
	movl	28(%ebp),%edi
	movl	32(%ebp),%esi
	movl	$_bb_Button,(%esp)
	call	_bbObjectNew
	mov	%eax,%ebx
	mov	%ebx,%eax
	movl	%esi,28(%esp)
	movl	%edi,24(%esp)
	movl	24(%ebp),%edx
	movl	%edx,20(%esp)
	movl	20(%ebp),%edx
	movl	%edx,16(%esp)
	movl	16(%ebp),%edx
	movl	%edx,12(%esp)
	movl	12(%ebp),%edx
	movl	%edx,8(%esp)
	movl	8(%ebp),%edx
	movl	%edx,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	mov	%ebx,%eax
	jmp	_425
_425:
	add	$44,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_TextField_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_bb_TextField,(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,8(%ebx)
	mov	$0,%eax
	jmp	_428
_428:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_TextField_Delete:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
_431:
	movl	8(%eax),%eax
	decl	4(%eax)
	jnz	_773
	movl	%eax,(%esp)
	call	_bbGCFree
_773:
	mov	$0,%eax
	jmp	_771
_771:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_TextField__init:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$28,%esp
	movl	8(%ebp),%ebx
	movl	16(%ebp),%esi
	movl	20(%ebp),%edx
	movl	24(%ebp),%eax
	movl	28(%ebp),%edi
	movl	32(%ebp),%ecx
	movl	%ecx,20(%esp)
	movl	8(%edi),%ecx
	movl	%ecx,16(%esp)
	movl	%eax,12(%esp)
	movl	%edx,8(%esp)
	movl	%esi,4(%esp)
	movl	12(%ebp),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_CreateTextField
	incl	4(%eax)
	mov	%eax,%esi
	movl	8(%ebx),%eax
	decl	4(%eax)
	jnz	_777
	movl	%eax,(%esp)
	call	_bbGCFree
_777:
	movl	%esi,8(%ebx)
	movl	8(%ebx),%eax
	movl	%eax,4(%esp)
	movl	16(%edi),%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListAddLast
	mov	$0,%eax
	jmp	_440
_440:
	add	$28,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_TextField__getText:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_TextFieldText
	jmp	_443
_443:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_TextField__clear:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
	movl	$_1,4(%esp)
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	mov	$0,%eax
	jmp	_446
_446:
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_newTextField:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$28,%esp
	movl	24(%ebp),%edi
	movl	28(%ebp),%esi
	movl	$_bb_TextField,(%esp)
	call	_bbObjectNew
	mov	%eax,%ebx
	mov	%ebx,%eax
	movl	%esi,24(%esp)
	movl	%edi,20(%esp)
	movl	20(%ebp),%edx
	movl	%edx,16(%esp)
	movl	16(%ebp),%edx
	movl	%edx,12(%esp)
	movl	12(%ebp),%edx
	movl	%edx,8(%esp)
	movl	8(%ebp),%edx
	movl	%edx,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	mov	%ebx,%eax
	jmp	_454
_454:
	add	$28,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_ComboBox_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_bb_ComboBox,(%ebx)
	mov	$0,%eax
	jmp	_457
_457:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_ComboBox_Delete:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
_460:
	mov	$0,%eax
	jmp	_780
_780:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_ListBox_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_bb_ListBox,(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,8(%ebx)
	mov	$0,%eax
	jmp	_463
_463:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_ListBox_Delete:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
_466:
	movl	8(%eax),%eax
	decl	4(%eax)
	jnz	_784
	movl	%eax,(%esp)
	call	_bbGCFree
_784:
	mov	$0,%eax
	jmp	_782
_782:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_ListBox__init:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$28,%esp
	movl	8(%ebp),%ebx
	movl	16(%ebp),%esi
	movl	20(%ebp),%edx
	movl	24(%ebp),%eax
	movl	28(%ebp),%edi
	movl	32(%ebp),%ecx
	movl	%ecx,20(%esp)
	movl	8(%edi),%ecx
	movl	%ecx,16(%esp)
	movl	%eax,12(%esp)
	movl	%edx,8(%esp)
	movl	%esi,4(%esp)
	movl	12(%ebp),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_CreateListBox
	incl	4(%eax)
	mov	%eax,%esi
	movl	8(%ebx),%eax
	decl	4(%eax)
	jnz	_788
	movl	%eax,(%esp)
	call	_bbGCFree
_788:
	movl	%esi,8(%ebx)
	movl	8(%ebx),%eax
	movl	%eax,4(%esp)
	movl	16(%edi),%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListAddLast
	mov	$0,%eax
	jmp	_475
_475:
	add	$28,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_ListBox__add:
	push	%ebp
	mov	%esp,%ebp
	sub	$24,%esp
	movl	8(%ebp),%ecx
	movl	12(%ebp),%edx
	movl	16(%ebp),%eax
	movl	$_bbNullObject,20(%esp)
	movl	$_1,16(%esp)
	movl	$-1,12(%esp)
	movl	%eax,8(%esp)
	movl	%edx,4(%esp)
	movl	8(%ecx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_AddGadgetItem
	mov	$0,%eax
	jmp	_480
_480:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_ListBox__modify:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$36,%esp
	movl	8(%ebp),%ebx
	movl	12(%ebp),%eax
	movl	$_bbNullObject,24(%esp)
	movl	$_1,20(%esp)
	movl	$-1,16(%esp)
	movl	$0,12(%esp)
	movl	%eax,8(%esp)
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SelectedGadgetItem
	movl	%eax,4(%esp)
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_ModifyGadgetItem
	mov	$0,%eax
	jmp	_484
_484:
	add	$36,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_ListBox__remove:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$20,%esp
	movl	8(%ebp),%ebx
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SelectedGadgetItem
	movl	%eax,4(%esp)
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_RemoveGadgetItem
	mov	$0,%eax
	jmp	_487
_487:
	add	$20,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_ListBox__getText:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$20,%esp
	movl	8(%ebp),%ebx
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SelectedGadgetItem
	movl	%eax,4(%esp)
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetItemText
	jmp	_490
_490:
	add	$20,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_newListBox:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$28,%esp
	movl	24(%ebp),%edi
	movl	28(%ebp),%esi
	movl	$_bb_ListBox,(%esp)
	call	_bbObjectNew
	mov	%eax,%ebx
	mov	%ebx,%eax
	movl	%esi,24(%esp)
	movl	%edi,20(%esp)
	movl	20(%ebp),%edx
	movl	%edx,16(%esp)
	movl	16(%ebp),%edx
	movl	%edx,12(%esp)
	movl	12(%ebp),%edx
	movl	%edx,8(%esp)
	movl	8(%ebp),%edx
	movl	%edx,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	mov	%ebx,%eax
	jmp	_498
_498:
	add	$28,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_iPad_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_bb_iPad,(%ebx)
	movl	$_bb_Window,(%esp)
	call	_bbObjectNew
	incl	4(%eax)
	movl	%eax,8(%ebx)
	movl	$0,12(%ebx)
	mov	$_bbEmptyString,%eax
	incl	4(%eax)
	movl	%eax,16(%ebx)
	mov	$_bbEmptyString,%eax
	incl	4(%eax)
	movl	%eax,20(%ebx)
	mov	$_bbEmptyString,%eax
	incl	4(%eax)
	movl	%eax,24(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,28(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,32(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,36(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,40(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,44(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,48(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,52(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,56(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,60(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,64(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,68(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,72(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,76(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,80(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,84(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,88(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,92(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,96(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,100(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,104(%ebx)
	movl	$_brl_linkedlist_TList,(%esp)
	call	_bbObjectNew
	incl	4(%eax)
	movl	%eax,108(%ebx)
	mov	$0,%eax
	jmp	_501
_501:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_iPad_Delete:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
_504:
	movl	108(%ebx),%eax
	decl	4(%eax)
	jnz	_818
	movl	%eax,(%esp)
	call	_bbGCFree
_818:
	movl	104(%ebx),%eax
	decl	4(%eax)
	jnz	_820
	movl	%eax,(%esp)
	call	_bbGCFree
_820:
	movl	100(%ebx),%eax
	decl	4(%eax)
	jnz	_822
	movl	%eax,(%esp)
	call	_bbGCFree
_822:
	movl	96(%ebx),%eax
	decl	4(%eax)
	jnz	_824
	movl	%eax,(%esp)
	call	_bbGCFree
_824:
	movl	92(%ebx),%eax
	decl	4(%eax)
	jnz	_826
	movl	%eax,(%esp)
	call	_bbGCFree
_826:
	movl	88(%ebx),%eax
	decl	4(%eax)
	jnz	_828
	movl	%eax,(%esp)
	call	_bbGCFree
_828:
	movl	84(%ebx),%eax
	decl	4(%eax)
	jnz	_830
	movl	%eax,(%esp)
	call	_bbGCFree
_830:
	movl	80(%ebx),%eax
	decl	4(%eax)
	jnz	_832
	movl	%eax,(%esp)
	call	_bbGCFree
_832:
	movl	76(%ebx),%eax
	decl	4(%eax)
	jnz	_834
	movl	%eax,(%esp)
	call	_bbGCFree
_834:
	movl	72(%ebx),%eax
	decl	4(%eax)
	jnz	_836
	movl	%eax,(%esp)
	call	_bbGCFree
_836:
	movl	68(%ebx),%eax
	decl	4(%eax)
	jnz	_838
	movl	%eax,(%esp)
	call	_bbGCFree
_838:
	movl	64(%ebx),%eax
	decl	4(%eax)
	jnz	_840
	movl	%eax,(%esp)
	call	_bbGCFree
_840:
	movl	60(%ebx),%eax
	decl	4(%eax)
	jnz	_842
	movl	%eax,(%esp)
	call	_bbGCFree
_842:
	movl	56(%ebx),%eax
	decl	4(%eax)
	jnz	_844
	movl	%eax,(%esp)
	call	_bbGCFree
_844:
	movl	52(%ebx),%eax
	decl	4(%eax)
	jnz	_846
	movl	%eax,(%esp)
	call	_bbGCFree
_846:
	movl	48(%ebx),%eax
	decl	4(%eax)
	jnz	_848
	movl	%eax,(%esp)
	call	_bbGCFree
_848:
	movl	44(%ebx),%eax
	decl	4(%eax)
	jnz	_850
	movl	%eax,(%esp)
	call	_bbGCFree
_850:
	movl	40(%ebx),%eax
	decl	4(%eax)
	jnz	_852
	movl	%eax,(%esp)
	call	_bbGCFree
_852:
	movl	36(%ebx),%eax
	decl	4(%eax)
	jnz	_854
	movl	%eax,(%esp)
	call	_bbGCFree
_854:
	movl	32(%ebx),%eax
	decl	4(%eax)
	jnz	_856
	movl	%eax,(%esp)
	call	_bbGCFree
_856:
	movl	28(%ebx),%eax
	decl	4(%eax)
	jnz	_858
	movl	%eax,(%esp)
	call	_bbGCFree
_858:
	movl	24(%ebx),%eax
	decl	4(%eax)
	jnz	_860
	movl	%eax,(%esp)
	call	_bbGCFree
_860:
	movl	20(%ebx),%eax
	decl	4(%eax)
	jnz	_862
	movl	%eax,(%esp)
	call	_bbGCFree
_862:
	movl	16(%ebx),%eax
	decl	4(%eax)
	jnz	_864
	movl	%eax,(%esp)
	call	_bbGCFree
_864:
	movl	8(%ebx),%eax
	decl	4(%eax)
	jnz	_866
	movl	%eax,(%esp)
	call	_bbGCFree
_866:
	mov	$0,%eax
	jmp	_816
_816:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_iPad__init:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	sub	$32,%esp
	movl	8(%ebp),%esi
	movl	8(%esi),%eax
	movl	$21,24(%esp)
	movl	$_22,20(%esp)
	movl	$300,16(%esp)
	movl	$400,12(%esp)
	movl	$0,8(%esp)
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	movl	%esi,4(%esp)
	movl	_bb_ipadList,%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListAddLast
	movl	$0,20(%esp)
	movl	8(%esi),%eax
	movl	%eax,16(%esp)
	movl	$125,12(%esp)
	movl	$375,8(%esp)
	movl	$125,4(%esp)
	movl	$10,(%esp)
	call	_bb_newListBox
	incl	4(%eax)
	mov	%eax,%ebx
	movl	92(%esi),%eax
	decl	4(%eax)
	jnz	_871
	movl	%eax,(%esp)
	call	_bbGCFree
_871:
	movl	%ebx,92(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	8(%esi),%eax
	movl	20(%eax),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_23,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	28(%esi),%eax
	decl	4(%eax)
	jnz	_875
	movl	%eax,(%esp)
	call	_bbGCFree
_875:
	movl	%ebx,28(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	28(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_24,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	40(%esi),%eax
	decl	4(%eax)
	jnz	_879
	movl	%eax,(%esp)
	call	_bbGCFree
_879:
	movl	%ebx,40(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	28(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_25,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	44(%esi),%eax
	decl	4(%eax)
	jnz	_883
	movl	%eax,(%esp)
	call	_bbGCFree
_883:
	movl	%ebx,44(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	8(%esi),%eax
	movl	20(%eax),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_26,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	36(%esi),%eax
	decl	4(%eax)
	jnz	_887
	movl	%eax,(%esp)
	call	_bbGCFree
_887:
	movl	%ebx,36(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	36(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_27,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	56(%esi),%eax
	decl	4(%eax)
	jnz	_891
	movl	%eax,(%esp)
	call	_bbGCFree
_891:
	movl	%ebx,56(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	36(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_1,(%esp)
	call	_maxgui_maxgui_CreateMenu
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	36(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_28,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	48(%esi),%eax
	decl	4(%eax)
	jnz	_895
	movl	%eax,(%esp)
	call	_bbGCFree
_895:
	movl	%ebx,48(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	36(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_29,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	52(%esi),%eax
	decl	4(%eax)
	jnz	_899
	movl	%eax,(%esp)
	call	_bbGCFree
_899:
	movl	%ebx,52(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	36(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_30,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	60(%esi),%eax
	decl	4(%eax)
	jnz	_903
	movl	%eax,(%esp)
	call	_bbGCFree
_903:
	movl	%ebx,60(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	36(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_31,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	64(%esi),%eax
	decl	4(%eax)
	jnz	_907
	movl	%eax,(%esp)
	call	_bbGCFree
_907:
	movl	%ebx,64(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	$_bbNullObject,8(%esp)
	movl	$0,4(%esp)
	movl	$_32,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	76(%esi),%eax
	decl	4(%eax)
	jnz	_911
	movl	%eax,(%esp)
	call	_bbGCFree
_911:
	movl	%ebx,76(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	76(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_23,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	68(%esi),%eax
	decl	4(%eax)
	jnz	_915
	movl	%eax,(%esp)
	call	_bbGCFree
_915:
	movl	%ebx,68(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	76(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_33,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	72(%esi),%eax
	decl	4(%eax)
	jnz	_919
	movl	%eax,(%esp)
	call	_bbGCFree
_919:
	movl	%ebx,72(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	72(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_34,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	80(%esi),%eax
	decl	4(%eax)
	jnz	_923
	movl	%eax,(%esp)
	call	_bbGCFree
_923:
	movl	%ebx,80(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	72(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_26,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	84(%esi),%eax
	decl	4(%eax)
	jnz	_927
	movl	%eax,(%esp)
	call	_bbGCFree
_927:
	movl	%ebx,84(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	72(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_35,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	88(%esi),%eax
	decl	4(%eax)
	jnz	_931
	movl	%eax,(%esp)
	call	_bbGCFree
_931:
	movl	%ebx,88(%esi)
	movl	8(%esi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_UpdateWindowMenu
	mov	$0,%eax
	jmp	_507
_507:
	add	$32,%esp
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_iPad__show:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	mov	$0,%eax
	jmp	_510
_510:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_iPad__build:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$28,%esp
	movl	8(%ebp),%edi
	movl	108(%edi),%ebx
	mov	%ebx,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	mov	%eax,%esi
	jmp	_36
_38:
	mov	%esi,%eax
	movl	$_bbStringClass,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	cmp	$_bbNullObject,%eax
	je	_36
	movl	$_bb_Note,(%esp)
	call	_bbObjectNew
	movl	24(%eax),%eax
	movl	%eax,4(%esp)
	movl	108(%edi),%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListAddLast
_36:
	mov	%esi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_38
_37:
	movl	$0,24(%esp)
	movl	8(%edi),%eax
	movl	8(%eax),%eax
	movl	%eax,20(%esp)
	movl	$15,16(%esp)
	movl	$75,12(%esp)
	movl	$10,8(%esp)
	movl	16(%edi),%eax
	movl	%eax,4(%esp)
	movl	$_39,(%esp)
	call	_bbStringConcat
	movl	$10,4(%esp)
	movl	%eax,(%esp)
	call	_maxgui_maxgui_CreateLabel
	incl	4(%eax)
	mov	%eax,%ebx
	movl	96(%edi),%eax
	decl	4(%eax)
	jnz	_943
	movl	%eax,(%esp)
	call	_bbGCFree
_943:
	movl	%ebx,96(%edi)
	movl	$0,24(%esp)
	movl	8(%edi),%eax
	movl	8(%eax),%eax
	movl	%eax,20(%esp)
	movl	$15,16(%esp)
	movl	$125,12(%esp)
	movl	$25,8(%esp)
	movl	20(%edi),%eax
	movl	%eax,4(%esp)
	movl	$_40,(%esp)
	call	_bbStringConcat
	movl	$10,4(%esp)
	movl	%eax,(%esp)
	call	_maxgui_maxgui_CreateLabel
	incl	4(%eax)
	mov	%eax,%ebx
	movl	100(%edi),%eax
	decl	4(%eax)
	jnz	_947
	movl	%eax,(%esp)
	call	_bbGCFree
_947:
	movl	%ebx,100(%edi)
	movl	$0,24(%esp)
	movl	8(%edi),%eax
	movl	8(%eax),%eax
	movl	%eax,20(%esp)
	movl	$15,16(%esp)
	movl	$150,12(%esp)
	movl	$40,8(%esp)
	movl	24(%edi),%eax
	movl	%eax,4(%esp)
	movl	$_41,(%esp)
	call	_bbStringConcat
	movl	$10,4(%esp)
	movl	%eax,(%esp)
	call	_maxgui_maxgui_CreateLabel
	incl	4(%eax)
	mov	%eax,%ebx
	movl	104(%edi),%eax
	decl	4(%eax)
	jnz	_951
	movl	%eax,(%esp)
	call	_bbGCFree
_951:
	movl	%ebx,104(%edi)
	movl	24(%edi),%ebx
	movl	$_42,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	je	_954
	movl	$_43,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	je	_955
	movl	$_44,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	je	_956
	movl	$_47,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	je	_957
	movl	$_48,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	je	_958
	movl	$_49,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	je	_959
	movl	$_50,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	je	_960
	jmp	_953
_954:
	movl	$_27,4(%esp)
	movl	56(%edi),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	jmp	_953
_955:
	movl	$_27,4(%esp)
	movl	56(%edi),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	jmp	_953
_956:
	movl	$_45,4(%esp)
	movl	56(%edi),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	8(%edi),%eax
	movl	20(%eax),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_46,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	32(%edi),%eax
	decl	4(%eax)
	jnz	_964
	movl	%eax,(%esp)
	call	_bbGCFree
_964:
	movl	%ebx,32(%edi)
	jmp	_953
_957:
	movl	$_45,4(%esp)
	movl	56(%edi),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	jmp	_953
_958:
	movl	$_45,4(%esp)
	movl	56(%edi),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	jmp	_953
_959:
	movl	$_45,4(%esp)
	movl	56(%edi),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	jmp	_953
_960:
	movl	$_45,4(%esp)
	movl	56(%edi),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	jmp	_953
_953:
	movl	8(%edi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_UpdateWindowMenu
	mov	$0,%eax
	jmp	_513
_513:
	add	$28,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_iPad__hide:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$12,%esp
	cmpl	$0,_bb_quickMode
	je	_965
	movl	8(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*64(%eax)
	jmp	_967
_965:
	movl	$0,4(%esp)
	movl	$_51,(%esp)
	call	_brl_system_Confirm
	cmp	$0,%eax
	je	_968
	movl	8(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*64(%eax)
_968:
_967:
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	16(%eax),%esi
	mov	%esi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	mov	%eax,%edi
	jmp	_52
_54:
	mov	%edi,%eax
	movl	$_maxgui_maxgui_TGadget,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	mov	%eax,%ebx
	cmp	$_bbNullObject,%ebx
	je	_52
	movl	%ebx,(%esp)
	call	_maxgui_maxgui_HideGadget
	movl	%ebx,(%esp)
	call	_maxgui_maxgui_FreeGadget
_52:
	mov	%edi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_54
_53:
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_HideGadget
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_FreeGadget
	mov	$_bbNullObject,%ebx
	incl	4(%ebx)
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	decl	4(%eax)
	jnz	_980
	movl	%eax,(%esp)
	call	_bbGCFree
_980:
	movl	8(%ebp),%eax
	movl	%ebx,8(%eax)
	movl	8(%ebp),%eax
	movl	%eax,4(%esp)
	movl	_bb_ipadList,%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListRemove
	mov	$0,%eax
	jmp	_516
_516:
	add	$12,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_iPad__save:
	push	%ebp
	mov	%esp,%ebp
	sub	$16,%esp
	push	%ebx
	push	%esi
	push	%edi
	sub	$12,%esp
	movl	8(%ebp),%eax
	movl	24(%eax),%esi
	movl	8(%ebp),%eax
	movl	20(%eax),%ebx
	movl	$_55,4(%esp)
	movl	8(%ebp),%eax
	movl	16(%eax),%eax
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%ebx,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_55,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_55,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,-8(%ebp)
	movl	8(%ebp),%eax
	movl	108(%eax),%eax
	movl	%eax,-12(%ebp)
	movl	-12(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	movl	%eax,-16(%ebp)
	jmp	_56
_58:
	movl	-16(%ebp),%eax
	movl	$_bb_Note,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	mov	%eax,%ebx
	cmp	$_bbNullObject,%ebx
	je	_56
	movl	32(%ebx),%eax
	movl	%eax,-4(%ebp)
	movl	28(%ebx),%eax
	movl	%eax,(%esp)
	call	_bbStringFromInt
	mov	%eax,%edi
	movl	36(%ebx),%esi
	movl	24(%ebx),%eax
	movl	%eax,4(%esp)
	movl	-8(%ebp),%eax
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_59,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_59,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%edi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_59,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	-4(%ebp),%edx
	movl	%edx,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_59,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_60,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,-8(%ebp)
_56:
	movl	-16(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_58
_57:
	movl	$_61,4(%esp)
	movl	-8(%ebp),%eax
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,-8(%ebp)
	movl	-8(%ebp),%eax
	movl	%eax,4(%esp)
	movl	8(%ebp),%eax
	movl	%eax,(%esp)
	call	_bb_insertIpadEntry
	mov	$0,%eax
	jmp	_519
_519:
	add	$12,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_iPad__addNote:
	push	%ebp
	mov	%esp,%ebp
	sub	$16,%esp
	push	%ebx
	push	%esi
	push	%edi
	sub	$12,%esp
	movl	12(%ebp),%eax
	movl	%eax,(%esp)
	call	_bb_convertFromSSV
	movl	$1,-8(%ebp)
	mov	$_bbNullObject,%edi
	movl	%eax,-12(%ebp)
	movl	-12(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	movl	%eax,-16(%ebp)
	jmp	_62
_64:
	movl	-16(%ebp),%eax
	movl	$_bbStringClass,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	movl	%eax,-4(%ebp)
	cmpl	$_bbNullObject,-4(%ebp)
	je	_62
	cmpl	$1,-8(%ebp)
	jne	_997
	call	_bb_newNote
	mov	%eax,%edi
	movl	%edi,4(%esp)
	movl	8(%ebp),%eax
	movl	108(%eax),%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListAddLast
	movl	-4(%ebp),%eax
	incl	4(%eax)
	mov	%eax,%ebx
	movl	24(%edi),%eax
	decl	4(%eax)
	jnz	_1001
	movl	%eax,(%esp)
	call	_bbGCFree
_1001:
	movl	%ebx,24(%edi)
_997:
	cmpl	$2,-8(%ebp)
	jne	_1002
	movl	-4(%ebp),%eax
	incl	4(%eax)
	mov	%eax,%ebx
	movl	36(%edi),%eax
	decl	4(%eax)
	jnz	_1006
	movl	%eax,(%esp)
	call	_bbGCFree
_1006:
	movl	%ebx,36(%edi)
	movl	8(%ebp),%eax
	movl	92(%eax),%ebx
	movl	$1,8(%esp)
	movl	24(%edi),%esi
	movl	$_14,4(%esp)
	movl	36(%edi),%eax
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*52(%eax)
_1002:
	cmpl	$3,-8(%ebp)
	jne	_1008
	movl	-4(%ebp),%eax
	movl	%eax,(%esp)
	call	_bbStringToInt
	movl	%eax,28(%edi)
_1008:
	cmpl	$4,-8(%ebp)
	jne	_1009
	movl	-4(%ebp),%eax
	incl	4(%eax)
	mov	%eax,%ebx
	movl	32(%edi),%eax
	decl	4(%eax)
	jnz	_1013
	movl	%eax,(%esp)
	call	_bbGCFree
_1013:
	movl	%ebx,32(%edi)
_1009:
	addl	$1,-8(%ebp)
_62:
	movl	-16(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_64
_63:
	mov	%edi,%eax
	jmp	_523
_523:
	add	$12,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_iPad__update:
	push	%ebp
	mov	%esp,%ebp
	sub	$16,%esp
	push	%ebx
	push	%esi
	push	%edi
	sub	$28,%esp
	call	_brl_eventqueue_EventSource
	movl	8(%ebp),%edx
	cmpl	40(%edx),%eax
	je	_1016
	movl	8(%ebp),%edx
	cmpl	44(%edx),%eax
	je	_1017
	movl	8(%ebp),%edx
	cmpl	32(%edx),%eax
	je	_1018
	movl	8(%ebp),%edx
	cmpl	48(%edx),%eax
	je	_1019
	movl	8(%ebp),%edx
	cmpl	52(%edx),%eax
	je	_1020
	movl	8(%ebp),%edx
	cmpl	56(%edx),%eax
	je	_1021
	movl	8(%ebp),%edx
	cmpl	60(%edx),%eax
	je	_1022
	movl	8(%ebp),%edx
	cmpl	64(%edx),%eax
	je	_1023
	movl	8(%ebp),%edx
	movl	92(%edx),%edx
	cmpl	8(%edx),%eax
	je	_1024
	movl	8(%ebp),%edx
	cmpl	68(%edx),%eax
	je	_1025
	movl	8(%ebp),%edx
	cmpl	80(%edx),%eax
	je	_1026
	movl	8(%ebp),%edx
	cmpl	84(%edx),%eax
	je	_1027
	movl	8(%ebp),%edx
	cmpl	88(%edx),%eax
	je	_1028
	movl	8(%ebp),%edx
	movl	8(%edx),%edx
	cmpl	8(%edx),%eax
	je	_1029
	jmp	_1015
_1016:
	cmpl	$0,_bb_quickMode
	je	_1031
	mov	$1,%eax
	jmp	_1032
_1031:
	movl	8(%ebp),%eax
	movl	12(%eax),%eax
	movl	%eax,(%esp)
	call	_bbStringFromInt
	movl	%eax,4(%esp)
	movl	$_65,(%esp)
	call	_bbStringConcat
	movl	$_66,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_19,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_67,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	call	_brl_system_Confirm
_1032:
	cmp	$0,%eax
	je	_1033
	movl	$0,12(%esp)
	movl	$0,8(%esp)
	movl	$4,4(%esp)
	movl	$_68,(%esp)
	call	_bb_prompt
	mov	%eax,%ebx
	movl	$_1,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	je	_1035
	movl	8(%ebp),%esi
	movl	8(%ebp),%eax
	movl	16(%eax),%edi
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_69,(%esp)
	call	_bbStringConcat
	movl	$_70,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%edi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_71,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%ebx,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_72,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%esi,(%esp)
	movl	(%esi),%eax
	calll	*68(%eax)
	incl	4(%ebx)
	movl	8(%ebp),%eax
	movl	16(%eax),%eax
	decl	4(%eax)
	jnz	_1040
	movl	%eax,(%esp)
	call	_bbGCFree
_1040:
	movl	8(%ebp),%eax
	movl	%ebx,16(%eax)
	movl	8(%ebp),%eax
	movl	16(%eax),%eax
	movl	%eax,4(%esp)
	movl	$_39,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	8(%ebp),%eax
	movl	96(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
_1035:
_1033:
	jmp	_1015
_1017:
	cmpl	$0,_bb_quickMode
	je	_1042
	mov	$1,%eax
	jmp	_1043
_1042:
	movl	8(%ebp),%eax
	movl	12(%eax),%eax
	movl	%eax,(%esp)
	call	_bbStringFromInt
	movl	%eax,4(%esp)
	movl	$_73,(%esp)
	call	_bbStringConcat
	movl	$_66,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_19,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_74,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	call	_brl_system_Confirm
_1043:
	cmp	$0,%eax
	je	_1044
	movl	$0,12(%esp)
	movl	$0,8(%esp)
	movl	$12,4(%esp)
	movl	$_75,(%esp)
	call	_bb_prompt
	mov	%eax,%ebx
	movl	$_1,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	je	_1046
	movl	8(%ebp),%esi
	movl	8(%ebp),%eax
	movl	20(%eax),%edi
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_76,(%esp)
	call	_bbStringConcat
	movl	$_77,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%edi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_71,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%ebx,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_72,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%esi,(%esp)
	movl	(%esi),%eax
	calll	*68(%eax)
	incl	4(%ebx)
	movl	8(%ebp),%eax
	movl	20(%eax),%eax
	decl	4(%eax)
	jnz	_1051
	movl	%eax,(%esp)
	call	_bbGCFree
_1051:
	movl	8(%ebp),%eax
	movl	%ebx,20(%eax)
	movl	8(%ebp),%eax
	movl	20(%eax),%eax
	movl	%eax,4(%esp)
	movl	$_40,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	8(%ebp),%eax
	movl	100(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
_1046:
_1044:
	jmp	_1015
_1018:
	movl	8(%ebp),%eax
	cmpl	$_bbNullObject,32(%eax)
	je	_1052
	movl	$0,4(%esp)
	movl	$_78,(%esp)
	call	_bb_dlog
_1052:
	jmp	_1015
_1019:
	movl	8(%ebp),%ebx
	movl	8(%ebp),%eax
	movl	24(%eax),%edi
	call	_brl_system_CurrentTime
	mov	%eax,%esi
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_79,(%esp)
	call	_bbStringConcat
	movl	$_80,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_81,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%edi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_82,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*68(%eax)
	mov	%eax,%ebx
	mov	$_48,%eax
	incl	4(%eax)
	mov	%eax,%esi
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	decl	4(%eax)
	jnz	_1058
	movl	%eax,(%esp)
	call	_bbGCFree
_1058:
	movl	8(%ebp),%eax
	movl	%esi,24(%eax)
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	movl	%eax,4(%esp)
	movl	$_41,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	8(%ebp),%eax
	movl	104(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	mov	%ebx,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	movl	16(%ebx),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_ActivateGadget
	jmp	_1015
_1020:
	movl	$0,12(%esp)
	movl	$1,8(%esp)
	movl	$0,4(%esp)
	movl	$_83,(%esp)
	call	_bb_prompt
	mov	%eax,%esi
	movl	8(%ebp),%ebx
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	movl	%eax,-8(%ebp)
	call	_brl_system_CurrentTime
	mov	%eax,%edi
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_84,(%esp)
	call	_bbStringConcat
	movl	$_85,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%edi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_86,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_87,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	-8(%ebp),%edx
	movl	%edx,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_72,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*68(%eax)
	mov	$_47,%ebx
	incl	4(%ebx)
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	decl	4(%eax)
	jnz	_1065
	movl	%eax,(%esp)
	call	_bbGCFree
_1065:
	movl	8(%ebp),%eax
	movl	%ebx,24(%eax)
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	movl	%eax,4(%esp)
	movl	$_41,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	8(%ebp),%eax
	movl	104(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	jmp	_1015
_1021:
	movl	$_27,4(%esp)
	movl	8(%ebp),%eax
	movl	56(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetText
	movl	%eax,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	jne	_1066
	movl	8(%ebp),%ebx
	movl	8(%ebp),%eax
	movl	24(%eax),%edi
	call	_brl_system_CurrentTime
	mov	%eax,%esi
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_88,(%esp)
	call	_bbStringConcat
	movl	$_89,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_90,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%edi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_72,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*68(%eax)
	mov	$_44,%ebx
	incl	4(%ebx)
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	decl	4(%eax)
	jnz	_1071
	movl	%eax,(%esp)
	call	_bbGCFree
_1071:
	movl	8(%ebp),%eax
	movl	%ebx,24(%eax)
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	movl	%eax,4(%esp)
	movl	$_41,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	8(%ebp),%eax
	movl	104(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	movl	$_45,4(%esp)
	movl	8(%ebp),%eax
	movl	56(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	20(%eax),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_46,(%esp)
	call	_maxgui_maxgui_CreateMenu
	mov	%eax,%ebx
	incl	4(%ebx)
	movl	8(%ebp),%eax
	movl	32(%eax),%eax
	decl	4(%eax)
	jnz	_1075
	movl	%eax,(%esp)
	call	_bbGCFree
_1075:
	movl	8(%ebp),%eax
	movl	%ebx,32(%eax)
	jmp	_1076
_1066:
	movl	8(%ebp),%ebx
	movl	8(%ebp),%eax
	movl	24(%eax),%edi
	call	_brl_system_CurrentTime
	mov	%eax,%esi
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_91,(%esp)
	call	_bbStringConcat
	movl	$_92,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_93,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%edi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_72,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*68(%eax)
	mov	$_43,%ebx
	incl	4(%ebx)
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	decl	4(%eax)
	jnz	_1081
	movl	%eax,(%esp)
	call	_bbGCFree
_1081:
	movl	8(%ebp),%eax
	movl	%ebx,24(%eax)
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	movl	%eax,4(%esp)
	movl	$_41,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	8(%ebp),%eax
	movl	104(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	movl	$_27,4(%esp)
	movl	8(%ebp),%eax
	movl	56(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	movl	8(%ebp),%eax
	movl	32(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_FreeGadget
_1076:
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_UpdateWindowMenu
	jmp	_1015
_1022:
	movl	8(%ebp),%ebx
	movl	8(%ebp),%eax
	movl	24(%eax),%edi
	call	_brl_system_CurrentTime
	mov	%eax,%esi
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_94,(%esp)
	call	_bbStringConcat
	movl	$_95,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_96,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%edi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_72,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*68(%eax)
	mov	$_49,%ebx
	incl	4(%ebx)
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	decl	4(%eax)
	jnz	_1086
	movl	%eax,(%esp)
	call	_bbGCFree
_1086:
	movl	8(%ebp),%eax
	movl	%ebx,24(%eax)
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	movl	%eax,4(%esp)
	movl	$_41,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	8(%ebp),%eax
	movl	104(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	jmp	_1015
_1023:
	movl	$0,12(%esp)
	movl	$1,8(%esp)
	movl	$0,4(%esp)
	movl	$_97,(%esp)
	call	_bb_prompt
	mov	%eax,%esi
	movl	8(%ebp),%ebx
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	movl	%eax,-4(%ebp)
	call	_brl_system_CurrentTime
	mov	%eax,%edi
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_98,(%esp)
	call	_bbStringConcat
	movl	$_99,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%edi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_96,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	-4(%ebp),%edx
	movl	%edx,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_100,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_101,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*68(%eax)
	mov	$_50,%ebx
	incl	4(%ebx)
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	decl	4(%eax)
	jnz	_1092
	movl	%eax,(%esp)
	call	_bbGCFree
_1092:
	movl	8(%ebp),%eax
	movl	%ebx,24(%eax)
	movl	8(%ebp),%eax
	movl	24(%eax),%eax
	movl	%eax,4(%esp)
	movl	$_41,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	8(%ebp),%eax
	movl	104(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	jmp	_1015
_1024:
	call	_brl_eventqueue_EventID
	cmp	$8193,%eax
	je	_1095
	cmp	$8196,%eax
	je	_1096
	jmp	_1094
_1095:
	movl	8(%ebp),%ebx
	movl	8(%ebp),%eax
	movl	92(%eax),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*64(%eax)
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*76(%eax)
	movl	%eax,(%esp)
	call	_bb_readNote
	jmp	_1094
_1096:
	movl	$_bbNullObject,8(%esp)
	movl	8(%ebp),%eax
	movl	76(%eax),%eax
	movl	%eax,4(%esp)
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_PopupWindowMenu
	jmp	_1094
_1094:
	jmp	_1015
_1025:
	call	_maxgui_maxgui_ActiveGadget
	movl	8(%ebp),%edx
	movl	92(%edx),%edx
	cmpl	8(%edx),%eax
	jne	_1099
	movl	8(%ebp),%ebx
	movl	8(%ebp),%eax
	movl	92(%eax),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*64(%eax)
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*76(%eax)
	movl	%eax,(%esp)
	call	_bb_readNote
_1099:
	jmp	_1015
_1026:
	movl	8(%ebp),%ebx
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_102,(%esp)
	call	_bbStringConcat
	movl	$_103,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*68(%eax)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	jmp	_1015
_1027:
	movl	8(%ebp),%ebx
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_102,(%esp)
	call	_bbStringConcat
	movl	$_104,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*68(%eax)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	jmp	_1015
_1028:
	movl	8(%ebp),%ebx
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_102,(%esp)
	call	_bbStringConcat
	movl	$_105,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*68(%eax)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	jmp	_1015
_1029:
	call	_brl_eventqueue_EventID
	cmp	$16387,%eax
	je	_1113
	jmp	_1112
_1113:
	movl	8(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*60(%eax)
	jmp	_1112
_1112:
	jmp	_1015
_1015:
	movl	8(%ebp),%eax
	movl	108(%eax),%eax
	movl	%eax,-12(%ebp)
	movl	-12(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	movl	%eax,-16(%ebp)
	jmp	_106
_108:
	movl	-16(%ebp),%eax
	movl	$_bb_Note,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	mov	%eax,%edi
	cmp	$_bbNullObject,%edi
	je	_106
	cmp	$_bbNullObject,%edi
	je	_1121
	call	_brl_eventqueue_EventSource
	movl	8(%edi),%edx
	cmpl	8(%edx),%eax
	je	_1124
	jmp	_1123
_1124:
	call	_brl_eventqueue_EventID
	cmp	$16387,%eax
	je	_1127
	jmp	_1126
_1127:
	mov	%edi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*60(%eax)
	cmp	$0,%eax
	je	_1129
	movl	12(%edi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	incl	4(%eax)
	mov	%eax,%ebx
	movl	24(%edi),%eax
	decl	4(%eax)
	jnz	_1134
	movl	%eax,(%esp)
	call	_bbGCFree
_1134:
	movl	%ebx,24(%edi)
	movl	16(%edi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	incl	4(%eax)
	mov	%eax,%ebx
	movl	32(%edi),%eax
	decl	4(%eax)
	jnz	_1139
	movl	%eax,(%esp)
	call	_bbGCFree
_1139:
	movl	%ebx,32(%edi)
	movl	8(%ebp),%eax
	movl	92(%eax),%ebx
	movl	24(%edi),%esi
	movl	$_14,4(%esp)
	movl	36(%edi),%eax
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*56(%eax)
_1129:
	mov	%edi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*64(%eax)
	jmp	_1126
_1126:
	jmp	_1123
_1123:
_1121:
_106:
	movl	-16(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_108
_107:
	mov	$0,%eax
	jmp	_526
_526:
	add	$28,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_iPad__noteBySummary:
	push	%ebp
	mov	%esp,%ebp
	sub	$4,%esp
	push	%ebx
	push	%esi
	push	%edi
	sub	$8,%esp
	movl	8(%ebp),%eax
	movl	108(%eax),%edi
	mov	%edi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	movl	%eax,-4(%ebp)
	jmp	_109
_111:
	movl	-4(%ebp),%eax
	movl	$_bb_Note,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	mov	%eax,%ebx
	cmp	$_bbNullObject,%ebx
	je	_109
	movl	24(%ebx),%esi
	movl	$_14,4(%esp)
	movl	36(%ebx),%eax
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	12(%ebp),%edx
	movl	%edx,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	jne	_1148
	mov	%ebx,%eax
	jmp	_530
_1148:
_109:
	movl	-4(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_111
_110:
	mov	$_bbNullObject,%eax
	jmp	_530
_530:
	add	$8,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_insertIpadEntry:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$12,%esp
	movl	$_brl_linkedlist_TList,(%esp)
	call	_bbObjectNew
	mov	%eax,%esi
_114:
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_brl_stream_ReadLine
	movl	%eax,4(%esp)
	movl	%esi,(%esp)
	call	_brl_linkedlist_ListAddLast
_112:
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_brl_stream_Eof
	cmp	$0,%eax
	je	_114
_113:
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_brl_filesystem_CloseFile
	movl	$1,8(%esp)
	movl	$1,4(%esp)
	movl	_bb_ipaddbn,%eax
	movl	%eax,(%esp)
	call	_brl_filesystem_OpenFile
	incl	4(%eax)
	mov	%eax,%ebx
	movl	_bb_iPaddb,%eax
	decl	4(%eax)
	jnz	_1153
	movl	%eax,(%esp)
	call	_bbGCFree
_1153:
	movl	%ebx,_bb_iPaddb
	mov	$1,%ebx
	mov	%esi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	mov	%eax,%edi
	jmp	_115
_117:
	mov	%edi,%eax
	movl	$_bbStringClass,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	mov	%eax,%edx
	cmp	$_bbNullObject,%edx
	je	_115
	movl	8(%ebp),%eax
	cmpl	12(%eax),%ebx
	jne	_1161
	movl	12(%ebp),%eax
	movl	%eax,4(%esp)
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_brl_stream_WriteLine
	jmp	_1162
_1161:
	movl	%edx,4(%esp)
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_brl_stream_WriteLine
_1162:
	add	$1,%ebx
_115:
	mov	%edi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_117
_116:
	movl	8(%ebp),%eax
	cmpl	$-1,12(%eax)
	jne	_1163
	movl	12(%ebp),%eax
	movl	%eax,4(%esp)
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_brl_stream_WriteLine
_1163:
	movl	_bb_ipaddbn,%eax
	movl	%eax,4(%esp)
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_bb_resetFile
	mov	%eax,%ebx
	incl	4(%ebx)
	movl	_bb_iPaddb,%eax
	decl	4(%eax)
	jnz	_1167
	movl	%eax,(%esp)
	call	_bbGCFree
_1167:
	movl	%ebx,_bb_iPaddb
	mov	$0,%eax
	jmp	_534
_534:
	add	$12,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_newIpad:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	$_bb_iPad,(%esp)
	call	_bbObjectNew
	mov	%eax,%ebx
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*48(%eax)
	mov	%ebx,%eax
	jmp	_536
_536:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_newBlankIpadBC:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$12,%esp
	movl	8(%ebp),%esi
	call	_bb_newIpad
	mov	%eax,%ebx
	movl	$-1,12(%ebx)
	mov	%esi,%eax
	incl	4(%eax)
	mov	%eax,%esi
	movl	16(%ebx),%eax
	decl	4(%eax)
	jnz	_1174
	movl	%eax,(%esp)
	call	_bbGCFree
_1174:
	movl	%esi,16(%ebx)
	mov	$_118,%eax
	incl	4(%eax)
	mov	%eax,%esi
	movl	20(%ebx),%eax
	decl	4(%eax)
	jnz	_1178
	movl	%eax,(%esp)
	call	_bbGCFree
_1178:
	movl	%esi,20(%ebx)
	mov	$_42,%eax
	incl	4(%eax)
	mov	%eax,%esi
	movl	24(%ebx),%eax
	decl	4(%eax)
	jnz	_1182
	movl	%eax,(%esp)
	call	_bbGCFree
_1182:
	movl	%esi,24(%ebx)
	call	_brl_system_CurrentTime
	mov	%eax,%edi
	call	_brl_system_CurrentDate
	mov	%eax,%esi
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_119,(%esp)
	call	_bbStringConcat
	movl	$_120,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_121,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%edi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_101,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*68(%eax)
	mov	%ebx,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	mov	$_bbNullObject,%eax
	jmp	_539
_539:
	add	$12,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_newBlankIpadSN:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$12,%esp
	movl	8(%ebp),%edi
	call	_bb_newIpad
	mov	%eax,%ebx
	movl	$-1,12(%ebx)
	mov	$_122,%eax
	incl	4(%eax)
	mov	%eax,%esi
	movl	16(%ebx),%eax
	decl	4(%eax)
	jnz	_1189
	movl	%eax,(%esp)
	call	_bbGCFree
_1189:
	movl	%esi,16(%ebx)
	mov	%edi,%eax
	incl	4(%eax)
	mov	%eax,%esi
	movl	20(%ebx),%eax
	decl	4(%eax)
	jnz	_1193
	movl	%eax,(%esp)
	call	_bbGCFree
_1193:
	movl	%esi,20(%ebx)
	mov	$_42,%eax
	incl	4(%eax)
	mov	%eax,%esi
	movl	24(%ebx),%eax
	decl	4(%eax)
	jnz	_1197
	movl	%eax,(%esp)
	call	_bbGCFree
_1197:
	movl	%esi,24(%ebx)
	call	_brl_system_CurrentTime
	mov	%eax,%edi
	call	_brl_system_CurrentDate
	mov	%eax,%esi
	call	_brl_system_CurrentDate
	movl	%eax,4(%esp)
	movl	$_119,(%esp)
	call	_bbStringConcat
	movl	$_120,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_121,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%edi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$_101,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*68(%eax)
	mov	%ebx,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	mov	$_bbNullObject,%eax
	jmp	_542
_542:
	add	$12,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_findBySN:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	push	%ebx
	push	%esi
	push	%edi
	sub	$20,%esp
	movl	_bb_ipadList,%ebx
	mov	%ebx,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	mov	%eax,%esi
	jmp	_123
_125:
	mov	%esi,%eax
	movl	$_bb_iPad,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	cmp	$_bbNullObject,%eax
	je	_123
	movl	8(%ebp),%edx
	movl	%edx,4(%esp)
	movl	20(%eax),%eax
	movl	%eax,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	jne	_1206
	movl	_bb_ipaddbn,%eax
	movl	%eax,4(%esp)
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_bb_resetFile
	mov	%eax,%ebx
	incl	4(%ebx)
	movl	_bb_iPaddb,%eax
	decl	4(%eax)
	jnz	_1210
	movl	%eax,(%esp)
	call	_bbGCFree
_1210:
	movl	%ebx,_bb_iPaddb
	movl	$0,4(%esp)
	movl	$_126,(%esp)
	call	_brl_system_Notify
	mov	$_bbNullObject,%eax
	jmp	_545
_1206:
_123:
	mov	%esi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_125
_124:
	movl	_bb_iPaddb,%edi
	mov	$1,%esi
_129:
	movl	%edi,(%esp)
	call	_brl_stream_ReadLine
	movl	%eax,(%esp)
	call	_bb_convertFromCSV
	mov	%eax,%ebx
	movl	8(%ebp),%eax
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	call	_brl_linkedlist_ListContains
	cmp	$0,%eax
	je	_1215
	movl	$1,-4(%ebp)
	call	_bb_newIpad
	mov	%eax,%edi
	movl	%esi,12(%edi)
	mov	%ebx,%esi
	mov	%esi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	movl	%eax,-8(%ebp)
	jmp	_130
_132:
	movl	-8(%ebp),%eax
	movl	$_bbStringClass,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	cmp	$_bbNullObject,%eax
	je	_130
	cmpl	$1,-4(%ebp)
	jne	_1224
	incl	4(%eax)
	mov	%eax,%ebx
	movl	16(%edi),%eax
	decl	4(%eax)
	jnz	_1228
	movl	%eax,(%esp)
	call	_bbGCFree
_1228:
	movl	%ebx,16(%edi)
	jmp	_1229
_1224:
	cmpl	$2,-4(%ebp)
	jne	_1230
	incl	4(%eax)
	mov	%eax,%ebx
	movl	20(%edi),%eax
	decl	4(%eax)
	jnz	_1234
	movl	%eax,(%esp)
	call	_bbGCFree
_1234:
	movl	%ebx,20(%edi)
	jmp	_1235
_1230:
	cmpl	$3,-4(%ebp)
	jne	_1236
	incl	4(%eax)
	mov	%eax,%ebx
	movl	24(%edi),%eax
	decl	4(%eax)
	jnz	_1240
	movl	%eax,(%esp)
	call	_bbGCFree
_1240:
	movl	%ebx,24(%edi)
	jmp	_1241
_1236:
	mov	%edi,%edx
	movl	%eax,4(%esp)
	movl	%edx,(%esp)
	movl	(%edx),%eax
	calll	*68(%eax)
_1241:
_1235:
_1229:
	addl	$1,-4(%ebp)
_130:
	movl	-8(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_132
_131:
	movl	_bb_ipaddbn,%eax
	movl	%eax,4(%esp)
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_bb_resetFile
	incl	4(%eax)
	mov	%eax,%ebx
	movl	_bb_iPaddb,%eax
	decl	4(%eax)
	jnz	_1246
	movl	%eax,(%esp)
	call	_bbGCFree
_1246:
	movl	%ebx,_bb_iPaddb
	mov	%edi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	mov	%edi,%eax
	jmp	_545
_1215:
	add	$1,%esi
_127:
	movl	%edi,(%esp)
	call	_brl_stream_Eof
	cmp	$0,%eax
	je	_129
_128:
	cmpl	$0,_bb_quickMode
	je	_1248
	movl	8(%ebp),%eax
	movl	%eax,(%esp)
	call	_bb_newBlankIpadSN
	jmp	_1249
_1248:
	movl	8(%ebp),%eax
	movl	%eax,4(%esp)
	movl	$_133,(%esp)
	call	_bbStringConcat
	movl	$_134,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	call	_brl_system_Confirm
	cmp	$0,%eax
	je	_1250
	movl	8(%ebp),%eax
	movl	%eax,(%esp)
	call	_bb_newBlankIpadSN
_1250:
_1249:
	movl	_bb_ipaddbn,%eax
	movl	%eax,4(%esp)
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_bb_resetFile
	mov	%eax,%ebx
	incl	4(%ebx)
	movl	_bb_iPaddb,%eax
	decl	4(%eax)
	jnz	_1254
	movl	%eax,(%esp)
	call	_bbGCFree
_1254:
	movl	%ebx,_bb_iPaddb
	mov	$_bbNullObject,%eax
	jmp	_545
_545:
	add	$20,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_findByBC:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	push	%ebx
	push	%esi
	push	%edi
	sub	$20,%esp
	movl	_bb_ipadList,%ebx
	mov	%ebx,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	mov	%eax,%esi
	jmp	_135
_137:
	mov	%esi,%eax
	movl	$_bb_iPad,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	cmp	$_bbNullObject,%eax
	je	_135
	movl	8(%ebp),%edx
	movl	%edx,4(%esp)
	movl	16(%eax),%eax
	movl	%eax,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	jne	_1261
	movl	_bb_ipaddbn,%eax
	movl	%eax,4(%esp)
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_bb_resetFile
	mov	%eax,%ebx
	incl	4(%ebx)
	movl	_bb_iPaddb,%eax
	decl	4(%eax)
	jnz	_1265
	movl	%eax,(%esp)
	call	_bbGCFree
_1265:
	movl	%ebx,_bb_iPaddb
	movl	$0,4(%esp)
	movl	$_126,(%esp)
	call	_brl_system_Notify
	mov	$_bbNullObject,%eax
	jmp	_548
_1261:
_135:
	mov	%esi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_137
_136:
	movl	_bb_iPaddb,%edi
	mov	$1,%esi
_140:
	movl	%edi,(%esp)
	call	_brl_stream_ReadLine
	movl	%eax,(%esp)
	call	_bb_convertFromCSV
	mov	%eax,%ebx
	movl	8(%ebp),%eax
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	call	_brl_linkedlist_ListContains
	cmp	$0,%eax
	je	_1270
	movl	$1,-4(%ebp)
	call	_bb_newIpad
	mov	%eax,%edi
	movl	%esi,12(%edi)
	mov	%ebx,%esi
	mov	%esi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	movl	%eax,-8(%ebp)
	jmp	_141
_143:
	movl	-8(%ebp),%eax
	movl	$_bbStringClass,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	cmp	$_bbNullObject,%eax
	je	_141
	cmpl	$1,-4(%ebp)
	jne	_1279
	incl	4(%eax)
	mov	%eax,%ebx
	movl	16(%edi),%eax
	decl	4(%eax)
	jnz	_1283
	movl	%eax,(%esp)
	call	_bbGCFree
_1283:
	movl	%ebx,16(%edi)
	jmp	_1284
_1279:
	cmpl	$2,-4(%ebp)
	jne	_1285
	incl	4(%eax)
	mov	%eax,%ebx
	movl	20(%edi),%eax
	decl	4(%eax)
	jnz	_1289
	movl	%eax,(%esp)
	call	_bbGCFree
_1289:
	movl	%ebx,20(%edi)
	jmp	_1290
_1285:
	cmpl	$3,-4(%ebp)
	jne	_1291
	incl	4(%eax)
	mov	%eax,%ebx
	movl	24(%edi),%eax
	decl	4(%eax)
	jnz	_1295
	movl	%eax,(%esp)
	call	_bbGCFree
_1295:
	movl	%ebx,24(%edi)
	jmp	_1296
_1291:
	mov	%edi,%edx
	movl	%eax,4(%esp)
	movl	%edx,(%esp)
	movl	(%edx),%eax
	calll	*68(%eax)
_1296:
_1290:
_1284:
	addl	$1,-4(%ebp)
_141:
	movl	-8(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_143
_142:
	movl	_bb_ipaddbn,%eax
	movl	%eax,4(%esp)
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_bb_resetFile
	incl	4(%eax)
	mov	%eax,%ebx
	movl	_bb_iPaddb,%eax
	decl	4(%eax)
	jnz	_1301
	movl	%eax,(%esp)
	call	_bbGCFree
_1301:
	movl	%ebx,_bb_iPaddb
	mov	%edi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	mov	%edi,%eax
	jmp	_548
_1270:
	add	$1,%esi
_138:
	movl	%edi,(%esp)
	call	_brl_stream_Eof
	cmp	$0,%eax
	je	_140
_139:
	cmpl	$0,_bb_quickMode
	je	_1303
	movl	8(%ebp),%eax
	movl	%eax,(%esp)
	call	_bb_newBlankIpadBC
	jmp	_1304
_1303:
	movl	8(%ebp),%eax
	movl	%eax,4(%esp)
	movl	$_144,(%esp)
	call	_bbStringConcat
	movl	$_134,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	call	_brl_system_Confirm
	cmp	$0,%eax
	je	_1305
	movl	8(%ebp),%eax
	movl	%eax,(%esp)
	call	_bb_newBlankIpadBC
_1305:
_1304:
	movl	_bb_ipaddbn,%eax
	movl	%eax,4(%esp)
	movl	_bb_iPaddb,%eax
	movl	%eax,(%esp)
	call	_bb_resetFile
	mov	%eax,%ebx
	incl	4(%ebx)
	movl	_bb_iPaddb,%eax
	decl	4(%eax)
	jnz	_1309
	movl	%eax,(%esp)
	call	_bbGCFree
_1309:
	movl	%ebx,_bb_iPaddb
	mov	$_bbNullObject,%eax
	jmp	_548
_548:
	add	$20,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Note_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_bb_Note,(%ebx)
	movl	$_bb_Window,(%esp)
	call	_bbObjectNew
	incl	4(%eax)
	movl	%eax,8(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,12(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,16(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,20(%ebx)
	mov	$_bbEmptyString,%eax
	incl	4(%eax)
	movl	%eax,24(%ebx)
	movl	$0,28(%ebx)
	mov	$_bbEmptyString,%eax
	incl	4(%eax)
	movl	%eax,32(%ebx)
	mov	$_bbEmptyString,%eax
	incl	4(%eax)
	movl	%eax,36(%ebx)
	mov	$0,%eax
	jmp	_551
_551:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Note_Delete:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
_554:
	movl	36(%ebx),%eax
	decl	4(%eax)
	jnz	_1319
	movl	%eax,(%esp)
	call	_bbGCFree
_1319:
	movl	32(%ebx),%eax
	decl	4(%eax)
	jnz	_1321
	movl	%eax,(%esp)
	call	_bbGCFree
_1321:
	movl	24(%ebx),%eax
	decl	4(%eax)
	jnz	_1323
	movl	%eax,(%esp)
	call	_bbGCFree
_1323:
	movl	20(%ebx),%eax
	decl	4(%eax)
	jnz	_1325
	movl	%eax,(%esp)
	call	_bbGCFree
_1325:
	movl	16(%ebx),%eax
	decl	4(%eax)
	jnz	_1327
	movl	%eax,(%esp)
	call	_bbGCFree
_1327:
	movl	12(%ebx),%eax
	decl	4(%eax)
	jnz	_1329
	movl	%eax,(%esp)
	call	_bbGCFree
_1329:
	movl	8(%ebx),%eax
	decl	4(%eax)
	jnz	_1331
	movl	%eax,(%esp)
	call	_bbGCFree
_1331:
	mov	$0,%eax
	jmp	_1317
_1317:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Note__init:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	sub	$32,%esp
	movl	8(%ebp),%ebx
	movl	8(%ebx),%esi
	movl	$21,24(%esp)
	movl	36(%ebx),%eax
	movl	%eax,4(%esp)
	movl	$_145,(%esp)
	call	_bbStringConcat
	movl	%eax,20(%esp)
	movl	$300,16(%esp)
	movl	$400,12(%esp)
	movl	$0,8(%esp)
	movl	$450,4(%esp)
	movl	%esi,(%esp)
	movl	(%esi),%eax
	calll	*48(%eax)
	movl	%ebx,4(%esp)
	movl	_bb_noteWinList,%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListAddLast
	movl	$0,20(%esp)
	movl	8(%ebx),%eax
	movl	%eax,16(%esp)
	movl	$20,12(%esp)
	movl	$150,8(%esp)
	movl	$10,4(%esp)
	movl	$10,(%esp)
	call	_bb_newTextField
	incl	4(%eax)
	mov	%eax,%esi
	movl	12(%ebx),%eax
	decl	4(%eax)
	jnz	_1336
	movl	%eax,(%esp)
	call	_bbGCFree
_1336:
	movl	%esi,12(%ebx)
	movl	$_bbNullObject,8(%esp)
	movl	$_bb_noComma,4(%esp)
	movl	12(%ebx),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetFilter
	movl	$0,20(%esp)
	movl	8(%ebx),%eax
	movl	%eax,16(%esp)
	movl	$215,12(%esp)
	movl	$375,8(%esp)
	movl	$35,4(%esp)
	movl	$10,(%esp)
	call	_bb_newTextArea
	incl	4(%eax)
	mov	%eax,%esi
	movl	16(%ebx),%eax
	decl	4(%eax)
	jnz	_1340
	movl	%eax,(%esp)
	call	_bbGCFree
_1340:
	movl	%esi,16(%ebx)
	movl	$_bbNullObject,8(%esp)
	movl	$_bb_noComma,4(%esp)
	movl	16(%ebx),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetFilter
	mov	%ebx,%eax
	movl	$0,24(%esp)
	movl	8(%ebx),%edx
	movl	8(%edx),%edx
	movl	%edx,20(%esp)
	movl	$20,16(%esp)
	movl	$150,12(%esp)
	movl	$10,8(%esp)
	movl	28(%ebx),%edx
	movl	%edx,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	$200,4(%esp)
	movl	%eax,(%esp)
	call	_maxgui_maxgui_CreateLabel
	incl	4(%eax)
	mov	%eax,%esi
	movl	20(%ebx),%eax
	decl	4(%eax)
	jnz	_1345
	movl	%eax,(%esp)
	call	_bbGCFree
_1345:
	movl	%esi,20(%ebx)
	movl	24(%ebx),%eax
	movl	%eax,4(%esp)
	movl	12(%ebx),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	movl	32(%ebx),%eax
	movl	%eax,4(%esp)
	movl	16(%ebx),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	mov	$0,%eax
	jmp	_557
_557:
	add	$32,%esp
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Note__typeByInt:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	12(%ebp),%eax
	cmp	$0,%eax
	je	_1348
	cmp	$1,%eax
	je	_1349
	mov	$_35,%eax
	jmp	_561
_1348:
	mov	$_34,%eax
	jmp	_561
_1349:
	mov	$_26,%eax
	jmp	_561
_561:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Note__update:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	call	_brl_eventqueue_EventSource
	movl	8(%ebx),%edx
	cmpl	8(%edx),%eax
	je	_1352
	movl	12(%ebx),%edx
	cmpl	8(%edx),%eax
	je	_1353
	jmp	_1351
_1352:
	jmp	_1351
_1353:
	jmp	_1351
_1351:
	mov	$0,%eax
	jmp	_564
_564:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Note__save:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$20,%esp
	mov	$0,%ebx
	cmpl	$0,_bb_quickMode
	je	_1355
	mov	$1,%ebx
	jmp	_1356
_1355:
	movl	$0,4(%esp)
	movl	$_146,(%esp)
	call	_brl_system_Confirm
	cmp	$0,%eax
	je	_1357
	mov	$1,%ebx
_1357:
_1356:
	mov	%ebx,%eax
	jmp	_567
_567:
	add	$20,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Note__hide:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	mov	$0,%eax
	jmp	_570
_570:
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_noComma:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
	cmpl	$515,8(%eax)
	jne	_1359
	movl	16(%eax),%eax
	cmp	$44,%eax
	je	_1362
	cmp	$59,%eax
	je	_1363
	cmp	$13,%eax
	je	_1364
	jmp	_1361
_1362:
	movl	$1,4(%esp)
	movl	$_147,(%esp)
	call	_bb_dlog
	mov	$0,%eax
	jmp	_574
_1363:
	movl	$1,4(%esp)
	movl	$_148,(%esp)
	call	_bb_dlog
	mov	$0,%eax
	jmp	_574
_1364:
	movl	$2,4(%esp)
	movl	$_149,(%esp)
	call	_bb_dlog
	movl	$1,4(%esp)
	movl	$_150,(%esp)
	call	_brl_system_Notify
	mov	$0,%eax
	jmp	_574
_1361:
_1359:
	mov	$1,%eax
	jmp	_574
_574:
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_readNote:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
	movl	8(%eax),%edx
	cmpl	$_bbNullObject,8(%edx)
	je	_1365
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	jmp	_1367
_1365:
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
_1367:
	mov	$0,%eax
	jmp	_577
_577:
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_newNote:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	$_bb_Note,(%esp)
	call	_bbObjectNew
	jmp	_579
_579:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Student_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_bb_Student,(%ebx)
	mov	$0,%eax
	jmp	_582
_582:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_Student_Delete:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
_585:
	mov	$0,%eax
	jmp	_1370
_1370:
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_findByFirst:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	mov	$_bbNullObject,%eax
	jmp	_588
_588:
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_findByLast:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	mov	$_bbNullObject,%eax
	jmp	_591
_591:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_logWin_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_bb_logWin,(%ebx)
	movl	$_bb_Window,(%esp)
	call	_bbObjectNew
	incl	4(%eax)
	movl	%eax,8(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,12(%ebx)
	mov	$0,%eax
	jmp	_594
_594:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_logWin_Delete:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
_597:
	movl	12(%ebx),%eax
	decl	4(%eax)
	jnz	_1375
	movl	%eax,(%esp)
	call	_bbGCFree
_1375:
	movl	8(%ebx),%eax
	decl	4(%eax)
	jnz	_1377
	movl	%eax,(%esp)
	call	_bbGCFree
_1377:
	mov	$0,%eax
	jmp	_1373
_1373:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_logWin__init:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$36,%esp
	movl	8(%ebp),%ebx
	movl	8(%ebx),%eax
	movl	$17,24(%esp)
	movl	$_151,20(%esp)
	movl	$180,16(%esp)
	movl	$800,12(%esp)
	movl	$800,8(%esp)
	movl	$240,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*52(%eax)
	xor	%eax,%eax
	cmp	$0,%eax
	je	_1380
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
_1380:
	mov	$0,%eax
	jmp	_600
_600:
	add	$36,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_logWin__populate:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	sub	$32,%esp
	movl	8(%ebp),%ebx
	movl	$0,20(%esp)
	movl	8(%ebx),%eax
	movl	%eax,16(%esp)
	movl	$140,12(%esp)
	movl	$775,8(%esp)
	movl	$10,4(%esp)
	movl	$10,(%esp)
	call	_bb_newListBox
	incl	4(%eax)
	mov	%eax,%esi
	movl	12(%ebx),%eax
	decl	4(%eax)
	jnz	_1385
	movl	%eax,(%esp)
	call	_bbGCFree
_1385:
	movl	%esi,12(%ebx)
	mov	%ebx,%eax
	movl	$0,8(%esp)
	movl	$_1387,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	mov	$0,%eax
	jmp	_603
_603:
	add	$32,%esp
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_logWin__new:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	sub	$16,%esp
	movl	8(%ebp),%edx
	movl	12(%ebp),%esi
	movl	16(%ebp),%eax
	cmp	$0,%eax
	je	_1390
	cmp	$1,%eax
	je	_1391
	cmp	$2,%eax
	je	_1392
	jmp	_1389
_1390:
	movl	12(%edx),%ebx
	movl	$1,8(%esp)
	movl	$_153,4(%esp)
	call	_brl_system_CurrentTime
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*52(%eax)
	jmp	_1389
_1391:
	movl	12(%edx),%ebx
	movl	$1,8(%esp)
	movl	$_154,4(%esp)
	call	_brl_system_CurrentTime
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*52(%eax)
	jmp	_1389
_1392:
	movl	12(%edx),%ebx
	movl	$1,8(%esp)
	movl	$_155,4(%esp)
	call	_brl_system_CurrentTime
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%eax,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*52(%eax)
	jmp	_1389
_1389:
	mov	$0,%eax
	jmp	_608
_608:
	add	$16,%esp
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_logWin__update:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$20,%esp
	movl	8(%ebp),%ebx
	call	_brl_eventqueue_EventSource
	movl	8(%ebx),%edx
	cmpl	8(%edx),%eax
	jne	_1396
	movl	$0,8(%esp)
	movl	$_156,4(%esp)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*56(%eax)
	call	_brl_eventqueue_EventID
	cmp	$16387,%eax
	je	_1400
	jmp	_1399
_1400:
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*64(%eax)
	jmp	_1399
_1399:
	mov	$0,%eax
	jmp	_611
_1396:
	mov	$0,%eax
	jmp	_611
_611:
	add	$20,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_logWin__end:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$12,%esp
	movl	8(%ebp),%eax
	movl	$0,8(%esp)
	movl	$_157,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	16(%eax),%esi
	mov	%esi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	mov	%eax,%edi
	jmp	_158
_160:
	mov	%edi,%eax
	movl	$_maxgui_maxgui_TGadget,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	mov	%eax,%ebx
	cmp	$_bbNullObject,%ebx
	je	_158
	movl	%ebx,(%esp)
	call	_maxgui_maxgui_HideGadget
	movl	%ebx,(%esp)
	call	_maxgui_maxgui_FreeGadget
_158:
	mov	%edi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_160
_159:
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_HideGadget
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_FreeGadget
	mov	$_bbNullObject,%ebx
	incl	4(%ebx)
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	decl	4(%eax)
	jnz	_1412
	movl	%eax,(%esp)
	call	_bbGCFree
_1412:
	movl	8(%ebp),%eax
	movl	%ebx,8(%eax)
	movl	8(%ebp),%eax
	movl	%eax,4(%esp)
	movl	_bb_winList,%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListRemove
	mov	$0,%eax
	jmp	_614
_614:
	add	$12,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_dlog:
	push	%ebp
	mov	%esp,%ebp
	sub	$24,%esp
	movl	8(%ebp),%ecx
	movl	12(%ebp),%edx
	movl	_bb_debugWin,%eax
	movl	%edx,8(%esp)
	movl	%ecx,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	mov	$0,%eax
	jmp	_618
_618:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_MainWindow_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_bb_MainWindow,(%ebx)
	movl	$_bb_Window,(%esp)
	call	_bbObjectNew
	incl	4(%eax)
	movl	%eax,8(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,12(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,16(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,20(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,24(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,28(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,32(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,36(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,40(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,44(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,48(%ebx)
	mov	$0,%eax
	jmp	_621
_621:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_MainWindow_Delete:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
_624:
	movl	48(%ebx),%eax
	decl	4(%eax)
	jnz	_1427
	movl	%eax,(%esp)
	call	_bbGCFree
_1427:
	movl	44(%ebx),%eax
	decl	4(%eax)
	jnz	_1429
	movl	%eax,(%esp)
	call	_bbGCFree
_1429:
	movl	40(%ebx),%eax
	decl	4(%eax)
	jnz	_1431
	movl	%eax,(%esp)
	call	_bbGCFree
_1431:
	movl	36(%ebx),%eax
	decl	4(%eax)
	jnz	_1433
	movl	%eax,(%esp)
	call	_bbGCFree
_1433:
	movl	32(%ebx),%eax
	decl	4(%eax)
	jnz	_1435
	movl	%eax,(%esp)
	call	_bbGCFree
_1435:
	movl	28(%ebx),%eax
	decl	4(%eax)
	jnz	_1437
	movl	%eax,(%esp)
	call	_bbGCFree
_1437:
	movl	24(%ebx),%eax
	decl	4(%eax)
	jnz	_1439
	movl	%eax,(%esp)
	call	_bbGCFree
_1439:
	movl	20(%ebx),%eax
	decl	4(%eax)
	jnz	_1441
	movl	%eax,(%esp)
	call	_bbGCFree
_1441:
	movl	16(%ebx),%eax
	decl	4(%eax)
	jnz	_1443
	movl	%eax,(%esp)
	call	_bbGCFree
_1443:
	movl	12(%ebx),%eax
	decl	4(%eax)
	jnz	_1445
	movl	%eax,(%esp)
	call	_bbGCFree
_1445:
	movl	8(%ebx),%eax
	decl	4(%eax)
	jnz	_1447
	movl	%eax,(%esp)
	call	_bbGCFree
_1447:
	mov	$0,%eax
	jmp	_1425
_1425:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_MainWindow__init:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$36,%esp
	movl	8(%ebp),%ebx
	movl	8(%ebx),%eax
	movl	$589,24(%esp)
	movl	$_219,20(%esp)
	movl	$300,16(%esp)
	movl	$400,12(%esp)
	movl	$0,8(%esp)
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*52(%eax)
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	mov	$0,%eax
	jmp	_627
_627:
	add	$36,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_MainWindow__populate:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	sub	$32,%esp
	movl	8(%ebp),%esi
	movl	$0,20(%esp)
	movl	8(%esi),%eax
	movl	%eax,16(%esp)
	movl	$20,12(%esp)
	movl	$150,8(%esp)
	movl	$50,4(%esp)
	movl	8(%esi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetWidth
	cdq
	and	$1,%edx
	add	%edx,%eax
	sar	$1,%eax
	sub	$75,%eax
	movl	%eax,(%esp)
	call	_bb_newTextField
	incl	4(%eax)
	mov	%eax,%ebx
	movl	12(%esi),%eax
	decl	4(%eax)
	jnz	_1454
	movl	%eax,(%esp)
	call	_bbGCFree
_1454:
	movl	%ebx,12(%esi)
	movl	$2,24(%esp)
	movl	8(%esi),%eax
	movl	%eax,20(%esp)
	movl	$15,16(%esp)
	movl	$75,12(%esp)
	movl	$85,8(%esp)
	movl	8(%esi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetWidth
	cdq
	and	$1,%edx
	add	%edx,%eax
	sar	$1,%eax
	sub	$75,%eax
	movl	%eax,4(%esp)
	movl	$_24,(%esp)
	call	_bb_newButton
	incl	4(%eax)
	mov	%eax,%ebx
	movl	16(%esi),%eax
	decl	4(%eax)
	jnz	_1458
	movl	%eax,(%esp)
	call	_bbGCFree
_1458:
	movl	%ebx,16(%esi)
	movl	16(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	$2,24(%esp)
	movl	8(%esi),%eax
	movl	%eax,20(%esp)
	movl	$15,16(%esp)
	movl	$75,12(%esp)
	movl	$105,8(%esp)
	movl	8(%esi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetWidth
	cdq
	and	$1,%edx
	add	%edx,%eax
	sar	$1,%eax
	sub	$75,%eax
	movl	%eax,4(%esp)
	movl	$_25,(%esp)
	call	_bb_newButton
	incl	4(%eax)
	mov	%eax,%ebx
	movl	20(%esi),%eax
	decl	4(%eax)
	jnz	_1463
	movl	%eax,(%esp)
	call	_bbGCFree
_1463:
	movl	%ebx,20(%esi)
	movl	$2,24(%esp)
	movl	8(%esi),%eax
	movl	%eax,20(%esp)
	movl	$15,16(%esp)
	movl	$75,12(%esp)
	movl	$85,8(%esp)
	movl	8(%esi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetWidth
	cdq
	and	$1,%edx
	add	%edx,%eax
	sar	$1,%eax
	movl	%eax,4(%esp)
	movl	$_161,(%esp)
	call	_bb_newButton
	incl	4(%eax)
	mov	%eax,%ebx
	movl	24(%esi),%eax
	decl	4(%eax)
	jnz	_1467
	movl	%eax,(%esp)
	call	_bbGCFree
_1467:
	movl	%ebx,24(%esi)
	movl	$2,24(%esp)
	movl	8(%esi),%eax
	movl	%eax,20(%esp)
	movl	$15,16(%esp)
	movl	$75,12(%esp)
	movl	$105,8(%esp)
	movl	8(%esi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetWidth
	cdq
	and	$1,%edx
	add	%edx,%eax
	sar	$1,%eax
	movl	%eax,4(%esp)
	movl	$_162,(%esp)
	call	_bb_newButton
	incl	4(%eax)
	mov	%eax,%ebx
	movl	28(%esi),%eax
	decl	4(%eax)
	jnz	_1471
	movl	%eax,(%esp)
	call	_bbGCFree
_1471:
	movl	%ebx,28(%esi)
	movl	$8,24(%esp)
	movl	8(%esi),%eax
	movl	%eax,20(%esp)
	movl	$40,16(%esp)
	movl	$75,12(%esp)
	movl	$250,8(%esp)
	movl	8(%esi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetWidth
	cdq
	and	$1,%edx
	add	%edx,%eax
	sar	$1,%eax
	sub	$37,%eax
	movl	%eax,4(%esp)
	movl	$_163,(%esp)
	call	_bb_newButton
	incl	4(%eax)
	mov	%eax,%ebx
	movl	32(%esi),%eax
	decl	4(%eax)
	jnz	_1475
	movl	%eax,(%esp)
	call	_bbGCFree
_1475:
	movl	%ebx,32(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	8(%esi),%eax
	movl	20(%eax),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_164,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	36(%esi),%eax
	decl	4(%eax)
	jnz	_1479
	movl	%eax,(%esp)
	call	_bbGCFree
_1479:
	movl	%ebx,36(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	36(%esi),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_165,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	48(%esi),%eax
	decl	4(%eax)
	jnz	_1483
	movl	%eax,(%esp)
	call	_bbGCFree
_1483:
	movl	%ebx,48(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	8(%esi),%eax
	movl	20(%eax),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_166,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	44(%esi),%eax
	decl	4(%eax)
	jnz	_1487
	movl	%eax,(%esp)
	call	_bbGCFree
_1487:
	movl	%ebx,44(%esi)
	movl	$0,16(%esp)
	movl	$0,12(%esp)
	movl	8(%esi),%eax
	movl	20(%eax),%eax
	movl	%eax,8(%esp)
	movl	$0,4(%esp)
	movl	$_167,(%esp)
	call	_maxgui_maxgui_CreateMenu
	incl	4(%eax)
	mov	%eax,%ebx
	movl	40(%esi),%eax
	decl	4(%eax)
	jnz	_1491
	movl	%eax,(%esp)
	call	_bbGCFree
_1491:
	movl	%ebx,40(%esi)
	movl	8(%esi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_UpdateWindowMenu
	movl	12(%esi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_ActivateGadget
	mov	$0,%eax
	jmp	_630
_630:
	add	$32,%esp
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_MainWindow__update:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$12,%esp
	movl	8(%ebp),%esi
	call	_brl_eventqueue_EventSource
	cmpl	48(%esi),%eax
	je	_1494
	cmpl	40(%esi),%eax
	je	_1495
	cmpl	44(%esi),%eax
	je	_1496
	movl	12(%esi),%edx
	cmpl	8(%edx),%eax
	je	_1497
	movl	16(%esi),%edx
	cmpl	8(%edx),%eax
	je	_1498
	movl	20(%esi),%edx
	cmpl	8(%edx),%eax
	je	_1499
	movl	24(%esi),%edx
	cmpl	8(%edx),%eax
	je	_1500
	movl	28(%esi),%edx
	cmpl	8(%edx),%eax
	je	_1501
	movl	32(%esi),%edx
	cmpl	8(%edx),%eax
	je	_1502
	jmp	_1493
_1494:
	cmpl	$0,_bb_quickMode
	je	_1503
	movl	$0,_bb_quickMode
	movl	$_165,4(%esp)
	movl	48(%esi),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	jmp	_1504
_1503:
	movl	$1,_bb_quickMode
	movl	$_168,4(%esp)
	movl	48(%esi),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
_1504:
	movl	8(%esi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_UpdateWindowMenu
	jmp	_1493
_1495:
	movl	$0,4(%esp)
	movl	$_1505,(%esp)
	call	_brl_system_Notify
	mov	$0,%eax
	jmp	_634
_1496:
	movl	$0,4(%esp)
	movl	$_170,(%esp)
	call	_bb_dlog
	mov	$0,%eax
	jmp	_634
_1497:
	call	_brl_eventqueue_EventID
	cmp	$8193,%eax
	je	_1508
	jmp	_1507
_1508:
	movl	12(%esi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetText
	mov	%eax,%ebx
	movl	20(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_1511
	cmpl	$12,8(%ebx)
	jne	_1512
	movl	%ebx,4(%esp)
	movl	$_171,(%esp)
	call	_bbStringConcat
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	call	_bb_dlog
	movl	%ebx,(%esp)
	call	_bb_findBySN
	cmp	$_bbNullObject,%eax
	je	_1514
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1514:
	movl	12(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	mov	$0,%eax
	jmp	_634
_1512:
_1511:
	movl	16(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_1518
	movl	$1,4(%esp)
	movl	%ebx,(%esp)
	call	_brl_retro_Left
	movl	$_208,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	jne	_1520
	cmpl	$5,8(%ebx)
	jne	_1521
	movl	$4,4(%esp)
	movl	%ebx,(%esp)
	call	_brl_retro_Right
	mov	%eax,%ebx
	movl	%ebx,4(%esp)
	movl	$_172,(%esp)
	call	_bbStringConcat
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	call	_bb_dlog
	movl	%ebx,(%esp)
	call	_bb_findByBC
	cmp	$_bbNullObject,%eax
	je	_1523
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1523:
	movl	12(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	mov	$0,%eax
	jmp	_634
_1521:
	jmp	_1526
_1520:
	cmpl	$4,8(%ebx)
	jne	_1527
	movl	%ebx,4(%esp)
	movl	$_172,(%esp)
	call	_bbStringConcat
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	call	_bb_dlog
	movl	%ebx,(%esp)
	call	_bb_findByBC
	cmp	$_bbNullObject,%eax
	je	_1529
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1529:
	movl	12(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	mov	$0,%eax
	jmp	_634
_1527:
_1526:
_1518:
	mov	$0,%eax
	jmp	_634
_1507:
	jmp	_1493
_1498:
	call	_brl_eventqueue_EventID
	cmp	$8193,%eax
	je	_1534
	jmp	_1533
_1534:
	movl	16(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	16(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$1,%eax
	jne	_1537
	movl	20(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$1,%eax
	jne	_1539
	movl	20(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1539:
	movl	24(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_1542
	movl	24(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1542:
	movl	28(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_1545
	movl	28(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1545:
_1537:
	jmp	_1533
_1533:
	jmp	_1493
_1499:
	call	_brl_eventqueue_EventID
	cmp	$8193,%eax
	je	_1549
	jmp	_1548
_1549:
	movl	20(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	20(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$1,%eax
	jne	_1552
	movl	16(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$1,%eax
	jne	_1554
	movl	16(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1554:
	movl	24(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_1557
	movl	24(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1557:
	movl	28(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_1560
	movl	28(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1560:
_1552:
	jmp	_1548
_1548:
	jmp	_1493
_1500:
	call	_brl_eventqueue_EventID
	cmp	$8193,%eax
	je	_1564
	jmp	_1563
_1564:
	movl	24(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	24(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$1,%eax
	jne	_1567
	movl	20(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$1,%eax
	jne	_1569
	movl	20(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1569:
	movl	16(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_1572
	movl	16(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1572:
	movl	28(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_1575
	movl	28(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1575:
_1567:
	jmp	_1563
_1563:
	jmp	_1493
_1501:
	call	_brl_eventqueue_EventID
	cmp	$8193,%eax
	je	_1579
	jmp	_1578
_1579:
	movl	28(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	28(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$1,%eax
	jne	_1582
	movl	20(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$1,%eax
	jne	_1584
	movl	20(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1584:
	movl	24(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_1587
	movl	24(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1587:
	movl	16(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_1590
	movl	16(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
_1590:
_1582:
	jmp	_1578
_1578:
	jmp	_1493
_1502:
	call	_brl_eventqueue_EventID
	cmp	$8193,%eax
	je	_1594
	jmp	_1593
_1594:
	movl	12(%esi),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetText
	mov	%eax,%ebx
	movl	24(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_1597
	movl	%ebx,4(%esp)
	movl	$_173,(%esp)
	call	_bbStringConcat
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	call	_bb_dlog
	movl	12(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	mov	$0,%eax
	jmp	_634
_1597:
	movl	28(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	cmp	$0,%eax
	je	_1600
	movl	%ebx,4(%esp)
	movl	$_174,(%esp)
	call	_bbStringConcat
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	call	_bb_dlog
	movl	12(%esi),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	mov	$0,%eax
	jmp	_634
_1600:
	jmp	_1593
_1593:
	jmp	_1493
_1493:
	call	_brl_eventqueue_EventSource
	movl	8(%esi),%edx
	cmpl	8(%edx),%eax
	jne	_1602
	call	_brl_eventqueue_EventID
	cmp	$16387,%eax
	je	_1605
	jmp	_1604
_1605:
	mov	$1,%eax
	jmp	_634
_1604:
	mov	$0,%eax
	jmp	_634
_1602:
	movl	8(%esi),%eax
	movl	16(%eax),%esi
	mov	%esi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	mov	%eax,%edi
	jmp	_175
_177:
	mov	%edi,%eax
	movl	$_maxgui_maxgui_TGadget,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	mov	%eax,%ebx
	cmp	$_bbNullObject,%ebx
	je	_175
	call	_brl_eventqueue_EventSource
	cmp	%ebx,%eax
	jne	_1612
	call	_brl_eventqueue_EventID
	cmp	$8200,%eax
	je	_1615
	cmp	$8193,%eax
	je	_1616
	call	_brl_eventqueue_EventSource
	mov	%eax,%ebx
	call	_brl_eventqueue_EventID
	movl	%eax,(%esp)
	call	_bbStringFromInt
	mov	%eax,%esi
	movl	%ebx,(%esp)
	movl	(%ebx),%eax
	calll	*24(%eax)
	movl	%eax,4(%esp)
	movl	$_178,(%esp)
	call	_bbStringConcat
	movl	$_179,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	%esi,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	call	_bb_dlog
	jmp	_1614
_1615:
	jmp	_1614
_1616:
	jmp	_1614
_1614:
	mov	$0,%eax
	jmp	_634
_1612:
_175:
	mov	%edi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_177
_176:
	mov	$0,%eax
	jmp	_634
_634:
	add	$12,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_MainWindow__end:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$12,%esp
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	16(%eax),%esi
	mov	%esi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*140(%eax)
	mov	%eax,%edi
	jmp	_180
_182:
	mov	%edi,%eax
	movl	$_maxgui_maxgui_TGadget,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	%eax,(%esp)
	call	_bbObjectDowncast
	mov	%eax,%ebx
	cmp	$_bbNullObject,%ebx
	je	_180
	movl	%ebx,(%esp)
	call	_maxgui_maxgui_HideGadget
	movl	%ebx,(%esp)
	call	_maxgui_maxgui_FreeGadget
_180:
	mov	%edi,%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	cmp	$0,%eax
	jne	_182
_181:
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_HideGadget
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_FreeGadget
	mov	$_bbNullObject,%ebx
	incl	4(%ebx)
	movl	8(%ebp),%eax
	movl	8(%eax),%eax
	decl	4(%eax)
	jnz	_1627
	movl	%eax,(%esp)
	call	_bbGCFree
_1627:
	movl	8(%ebp),%eax
	movl	%ebx,8(%eax)
	movl	8(%ebp),%eax
	movl	%eax,4(%esp)
	movl	_bb_winList,%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListRemove
	mov	$0,%eax
	jmp	_637
_637:
	add	$12,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_prompt:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	push	%ebx
	push	%esi
	push	%edi
	sub	$36,%esp
	movl	8(%ebp),%esi
	movl	12(%ebp),%edi
	movl	20(%ebp),%ebx
	movl	$_bb_Window,(%esp)
	call	_bbObjectNew
	movl	%eax,-4(%ebp)
	movl	-4(%ebp),%eax
	movl	$577,24(%esp)
	movl	%esi,20(%esp)
	movl	$65,16(%esp)
	movl	$215,12(%esp)
	movl	$0,8(%esp)
	movl	$0,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	movl	%ebx,20(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,16(%esp)
	movl	$20,12(%esp)
	movl	$150,8(%esp)
	movl	$10,4(%esp)
	movl	$10,(%esp)
	call	_bb_newTextField
	mov	%eax,%ebx
	movl	$8,24(%esp)
	movl	-4(%ebp),%eax
	movl	%eax,20(%esp)
	movl	$20,16(%esp)
	movl	$40,12(%esp)
	movl	$10,8(%esp)
	movl	$165,4(%esp)
	movl	$_163,(%esp)
	call	_bb_newButton
	movl	%eax,-8(%ebp)
	movl	-4(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*52(%eax)
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_ActivateGadget
	jmp	_195
_197:
	call	_brl_eventqueue_WaitEvent
	call	_brl_eventqueue_EventSource
	cmpl	8(%ebx),%eax
	je	_1635
	movl	-8(%ebp),%edx
	cmpl	8(%edx),%eax
	je	_1636
	movl	-4(%ebp),%edx
	cmpl	8(%edx),%eax
	je	_1637
	jmp	_1634
_1635:
	cmp	$0,%edi
	je	_1638
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetText
	cmpl	%edi,8(%eax)
	jle	_1639
	movl	%edi,4(%esp)
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetText
	movl	%eax,(%esp)
	call	_brl_retro_Left
	movl	%eax,4(%esp)
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
_1639:
_1638:
	jmp	_1634
_1636:
	movl	_bb_quickMode,%eax
	cmp	$0,%eax
	jne	_1641
	movl	16(%ebp),%eax
_1641:
	cmp	$0,%eax
	je	_1643
	mov	$1,%eax
	jmp	_1644
_1643:
	movl	$0,4(%esp)
	movl	$_198,(%esp)
	call	_brl_system_Confirm
_1644:
	cmp	$0,%eax
	je	_1645
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_GadgetText
	mov	%eax,%esi
	movl	-4(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	movl	-8(%ebp),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_FreeGadget
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_FreeGadget
	movl	-4(%ebp),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_FreeGadget
	mov	%esi,%eax
	jmp	_643
_1645:
	jmp	_1634
_1637:
	call	_brl_eventqueue_EventID
	cmp	$16387,%eax
	jne	_1648
	movl	-4(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*56(%eax)
	movl	-8(%ebp),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_FreeGadget
	movl	8(%ebx),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_FreeGadget
	movl	-4(%ebp),%eax
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_FreeGadget
	mov	$_1,%eax
	jmp	_643
_1648:
	jmp	_1634
_1634:
_195:
	mov	$1,%eax
	cmp	$0,%eax
	jne	_197
_196:
	mov	$_bbEmptyString,%eax
	jmp	_643
_643:
	add	$36,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_convertFromCSV:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	push	%ebx
	push	%esi
	push	%edi
	sub	$20,%esp
	movl	8(%ebp),%edi
	movl	$_brl_linkedlist_TList,(%esp)
	call	_bbObjectNew
	movl	%eax,-8(%ebp)
	mov	$_bbEmptyString,%esi
	movl	8(%edi),%eax
	movl	%eax,-4(%ebp)
_201:
	movl	$1,4(%esp)
	movl	%edi,(%esp)
	call	_brl_retro_Left
	mov	%eax,%ebx
	movl	$_55,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	jne	_1654
	movl	$_61,4(%esp)
	movl	%esi,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	jne	_1655
	jmp	_200
_1655:
	movl	%esi,4(%esp)
	movl	-8(%ebp),%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListAddLast
	mov	$_1,%esi
_1654:
	movl	$_55,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	je	_1656
	movl	%ebx,4(%esp)
	movl	%esi,(%esp)
	call	_bbStringConcat
	mov	%eax,%esi
_1656:
	subl	$1,-4(%ebp)
	movl	-4(%ebp),%eax
	movl	%eax,4(%esp)
	movl	%edi,(%esp)
	call	_brl_retro_Right
	mov	%eax,%edi
_199:
	movl	$_1,4(%esp)
	movl	%edi,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	jne	_201
_200:
	movl	-8(%ebp),%eax
	jmp	_646
_646:
	add	$20,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_convertFromSSV:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	push	%ebx
	push	%esi
	push	%edi
	sub	$20,%esp
	movl	8(%ebp),%esi
	movl	$_brl_linkedlist_TList,(%esp)
	call	_bbObjectNew
	movl	%eax,-8(%ebp)
	mov	$_bbEmptyString,%edi
	movl	8(%esi),%eax
	movl	%eax,-4(%ebp)
_204:
	movl	$1,4(%esp)
	movl	%esi,(%esp)
	call	_brl_retro_Left
	mov	%eax,%ebx
	movl	$_59,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	jne	_1661
	movl	%edi,4(%esp)
	movl	-8(%ebp),%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListAddLast
	mov	$_1,%edi
_1661:
	movl	$_59,4(%esp)
	movl	%ebx,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	je	_1662
	movl	%ebx,4(%esp)
	movl	%edi,(%esp)
	call	_bbStringConcat
	mov	%eax,%edi
_1662:
	subl	$1,-4(%ebp)
	movl	-4(%ebp),%eax
	movl	%eax,4(%esp)
	movl	%esi,(%esp)
	call	_brl_retro_Right
	mov	%eax,%esi
_202:
	movl	$_1,4(%esp)
	movl	%esi,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	jne	_204
_203:
	movl	$_205,4(%esp)
	movl	%edi,(%esp)
	call	_bbStringCompare
	cmp	$0,%eax
	jne	_1663
	movl	-8(%ebp),%eax
	jmp	_649
_1663:
	movl	-8(%ebp),%eax
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*24(%eax)
	movl	%eax,4(%esp)
	movl	$_206,(%esp)
	call	_bbStringConcat
	movl	$_207,4(%esp)
	movl	%eax,(%esp)
	call	_bbStringConcat
	movl	$2,4(%esp)
	movl	%eax,(%esp)
	call	_bb_dlog
	movl	-8(%ebp),%eax
	jmp	_649
_649:
	add	$20,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_resetFile:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$20,%esp
	movl	8(%ebp),%eax
	movl	12(%ebp),%ebx
	movl	%eax,(%esp)
	call	_brl_filesystem_CloseFile
	movl	$1,8(%esp)
	movl	$1,4(%esp)
	movl	%ebx,(%esp)
	call	_brl_filesystem_OpenFile
	jmp	_653
_653:
	add	$20,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_TextArea_New:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	sub	$4,%esp
	movl	8(%ebp),%ebx
	movl	%ebx,(%esp)
	call	_bbObjectCtor
	movl	$_bb_TextArea,(%ebx)
	mov	$_bbNullObject,%eax
	incl	4(%eax)
	movl	%eax,8(%ebx)
	mov	$0,%eax
	jmp	_656
_656:
	add	$4,%esp
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_TextArea_Delete:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
_659:
	movl	8(%eax),%eax
	decl	4(%eax)
	jnz	_1668
	movl	%eax,(%esp)
	call	_bbGCFree
_1668:
	mov	$0,%eax
	jmp	_1666
_1666:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_TextArea__init:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$28,%esp
	movl	8(%ebp),%esi
	movl	20(%ebp),%edi
	movl	24(%ebp),%ebx
	movl	32(%ebp),%ecx
	movl	36(%ebp),%edx
	mov	$0,%eax
	cmp	$0,%ecx
	je	_1670
	mov	$2,%eax
_1670:
	cmp	$0,%edx
	je	_1671
	mov	$1,%eax
_1671:
	movl	%eax,20(%esp)
	movl	28(%ebp),%eax
	movl	8(%eax),%eax
	movl	%eax,16(%esp)
	movl	%ebx,12(%esp)
	movl	%edi,8(%esp)
	movl	16(%ebp),%eax
	movl	%eax,4(%esp)
	movl	12(%ebp),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_CreateTextArea
	incl	4(%eax)
	mov	%eax,%ebx
	movl	8(%esi),%eax
	decl	4(%eax)
	jnz	_1675
	movl	%eax,(%esp)
	call	_bbGCFree
_1675:
	movl	%ebx,8(%esi)
	movl	8(%esi),%eax
	movl	%eax,4(%esp)
	movl	28(%ebp),%eax
	movl	16(%eax),%eax
	movl	%eax,(%esp)
	call	_brl_linkedlist_ListAddLast
	mov	$0,%eax
	jmp	_669
_669:
	add	$28,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_TextArea__getText:
	push	%ebp
	mov	%esp,%ebp
	sub	$24,%esp
	movl	8(%ebp),%eax
	movl	$1,12(%esp)
	movl	$-1,8(%esp)
	movl	$0,4(%esp)
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_TextAreaText
	jmp	_672
_672:
	mov	%ebp,%esp
	pop	%ebp
	ret
__bb_TextArea__clear:
	push	%ebp
	mov	%esp,%ebp
	sub	$8,%esp
	movl	8(%ebp),%eax
	movl	$_1,4(%esp)
	movl	8(%eax),%eax
	movl	%eax,(%esp)
	call	_maxgui_maxgui_SetGadgetText
	mov	$0,%eax
	jmp	_675
_675:
	mov	%ebp,%esp
	pop	%ebp
	ret
_bb_newTextArea:
	push	%ebp
	mov	%esp,%ebp
	push	%ebx
	push	%esi
	push	%edi
	sub	$44,%esp
	movl	24(%ebp),%edi
	movl	28(%ebp),%esi
	movl	$_bb_TextArea,(%esp)
	call	_bbObjectNew
	mov	%eax,%ebx
	mov	%ebx,%eax
	movl	$1,28(%esp)
	movl	%esi,24(%esp)
	movl	%edi,20(%esp)
	movl	20(%ebp),%edx
	movl	%edx,16(%esp)
	movl	16(%ebp),%edx
	movl	%edx,12(%esp)
	movl	12(%ebp),%edx
	movl	%edx,8(%esp)
	movl	8(%ebp),%edx
	movl	%edx,4(%esp)
	movl	%eax,(%esp)
	movl	(%eax),%eax
	calll	*48(%eax)
	mov	%ebx,%eax
	jmp	_683
_683:
	add	$44,%esp
	pop	%edi
	pop	%esi
	pop	%ebx
	mov	%ebp,%esp
	pop	%ebp
	ret
	.data	
	.align	4
_734:
	.long	0
_221:
	.asciz	"z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0"
_222:
	.asciz	"New"
_223:
	.asciz	"()i"
_224:
	.asciz	"Delete"
	.align	4
_220:
	.long	2
	.long	_221
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	0
	.align	4
_10:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_220
	.long	8
	.long	__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_New
	.long	__bb_z_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_3_0_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
_226:
	.asciz	"z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea"
_227:
	.asciz	"Name"
_228:
	.asciz	"$"
	.align	4
_229:
	.long	_bbStringClass
	.long	2147483646
	.long	12
	.short	105,80,97,100,32,77,97,110,97,103,101,114
_230:
	.asciz	"MajorVersion"
_231:
	.asciz	"i"
	.align	4
_232:
	.long	_bbStringClass
	.long	2147483646
	.long	1
	.short	48
_233:
	.asciz	"MinorVersion"
_234:
	.asciz	"Revision"
	.align	4
_235:
	.long	_bbStringClass
	.long	2147483646
	.long	1
	.short	49
_236:
	.asciz	"VersionString"
	.align	4
_237:
	.long	_bbStringClass
	.long	2147483646
	.long	5
	.short	48,46,48,46,49
_238:
	.asciz	"AssemblyInfo"
	.align	4
_239:
	.long	_bbStringClass
	.long	2147483646
	.long	18
	.short	105,80,97,100,32,77,97,110,97,103,101,114,32,48,46,48
	.short	46,49
_240:
	.asciz	"Platform"
	.align	4
_241:
	.long	_bbStringClass
	.long	2147483646
	.long	5
	.short	77,97,99,79,115
_242:
	.asciz	"Architecture"
	.align	4
_243:
	.long	_bbStringClass
	.long	2147483646
	.long	3
	.short	120,56,54
_244:
	.asciz	"DebugOn"
	.align	4
_225:
	.long	2
	.long	_226
	.long	1
	.long	_227
	.long	_228
	.long	_229
	.long	1
	.long	_230
	.long	_231
	.long	_232
	.long	1
	.long	_233
	.long	_231
	.long	_232
	.long	1
	.long	_234
	.long	_231
	.long	_235
	.long	1
	.long	_236
	.long	_228
	.long	_237
	.long	1
	.long	_238
	.long	_228
	.long	_239
	.long	1
	.long	_240
	.long	_228
	.long	_241
	.long	1
	.long	_242
	.long	_228
	.long	_243
	.long	1
	.long	_244
	.long	_231
	.long	_232
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	0
	.align	4
_11:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_225
	.long	8
	.long	__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New
	.long	__bb_z_blide_bgff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.align	4
__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Application:
	.long	_bbNullObject
	.align	4
__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Resources:
	.long	_bbNullObject
_246:
	.asciz	"z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea"
	.align	4
_245:
	.long	2
	.long	_246
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	0
	.align	4
_17:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_245
	.long	8
	.long	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_New
	.long	__bb_z_My_ff5a7ccc_bdcc_4dce_aad7_5d9b68493dea_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
_248:
	.asciz	"Window"
_249:
	.asciz	"handle"
_250:
	.asciz	":TGadget"
_251:
	.asciz	"id"
_252:
	.asciz	"gadgetList"
_253:
	.asciz	":TList"
_254:
	.asciz	"menu"
_255:
	.asciz	"_new"
_256:
	.asciz	"(i,i,i,i,$,i)i"
_257:
	.asciz	"_show"
_258:
	.asciz	"_hide"
_259:
	.asciz	"_update"
	.align	4
_247:
	.long	2
	.long	_248
	.long	3
	.long	_249
	.long	_250
	.long	8
	.long	3
	.long	_251
	.long	_228
	.long	12
	.long	3
	.long	_252
	.long	_253
	.long	16
	.long	3
	.long	_254
	.long	_250
	.long	20
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	6
	.long	_255
	.long	_256
	.long	48
	.long	6
	.long	_257
	.long	_223
	.long	52
	.long	6
	.long	_258
	.long	_223
	.long	56
	.long	6
	.long	_259
	.long	_223
	.long	60
	.long	0
	.align	4
_bb_Window:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_247
	.long	24
	.long	__bb_Window_New
	.long	__bb_Window_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	__bb_Window__new
	.long	__bb_Window__show
	.long	__bb_Window__hide
	.long	__bb_Window__update
_261:
	.asciz	"Button"
_262:
	.asciz	"state"
_263:
	.asciz	"_init"
_264:
	.asciz	"($,i,i,i,i,:Window,i)i"
_265:
	.asciz	"_state"
_266:
	.asciz	"_cstate"
	.align	4
_260:
	.long	2
	.long	_261
	.long	3
	.long	_249
	.long	_250
	.long	8
	.long	3
	.long	_262
	.long	_231
	.long	12
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	6
	.long	_263
	.long	_264
	.long	48
	.long	6
	.long	_265
	.long	_223
	.long	52
	.long	6
	.long	_266
	.long	_223
	.long	56
	.long	0
	.align	4
_bb_Button:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_260
	.long	16
	.long	__bb_Button_New
	.long	__bb_Button_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	__bb_Button__init
	.long	__bb_Button__state
	.long	__bb_Button__cstate
_268:
	.asciz	"TextField"
_269:
	.asciz	"(i,i,i,i,:Window,i)i"
_270:
	.asciz	"_getText"
_271:
	.asciz	"()$"
_272:
	.asciz	"_clear"
	.align	4
_267:
	.long	2
	.long	_268
	.long	3
	.long	_249
	.long	_250
	.long	8
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	6
	.long	_263
	.long	_269
	.long	48
	.long	6
	.long	_270
	.long	_271
	.long	52
	.long	6
	.long	_272
	.long	_223
	.long	56
	.long	0
	.align	4
_bb_TextField:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_267
	.long	12
	.long	__bb_TextField_New
	.long	__bb_TextField_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	__bb_TextField__init
	.long	__bb_TextField__getText
	.long	__bb_TextField__clear
_274:
	.asciz	"ComboBox"
	.align	4
_273:
	.long	2
	.long	_274
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	0
	.align	4
_bb_ComboBox:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_273
	.long	8
	.long	__bb_ComboBox_New
	.long	__bb_ComboBox_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
_276:
	.asciz	"ListBox"
_277:
	.asciz	"_add"
_278:
	.asciz	"($,i)i"
_279:
	.asciz	"_modify"
_280:
	.asciz	"($)i"
_281:
	.asciz	"_remove"
	.align	4
_275:
	.long	2
	.long	_276
	.long	3
	.long	_249
	.long	_250
	.long	8
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	6
	.long	_263
	.long	_269
	.long	48
	.long	6
	.long	_277
	.long	_278
	.long	52
	.long	6
	.long	_279
	.long	_280
	.long	56
	.long	6
	.long	_281
	.long	_223
	.long	60
	.long	6
	.long	_270
	.long	_271
	.long	64
	.long	0
	.align	4
_bb_ListBox:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_275
	.long	12
	.long	__bb_ListBox_New
	.long	__bb_ListBox_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	__bb_ListBox__init
	.long	__bb_ListBox__add
	.long	__bb_ListBox__modify
	.long	__bb_ListBox__remove
	.long	__bb_ListBox__getText
_283:
	.asciz	"iPad"
_284:
	.asciz	":Window"
_285:
	.asciz	"pos"
_286:
	.asciz	"bc"
_287:
	.asciz	"sn"
_288:
	.asciz	"iEditMenu"
_289:
	.asciz	"ownerMenu"
_290:
	.asciz	"actionMenu"
_291:
	.asciz	"bcEdit"
_292:
	.asciz	"snEdit"
_293:
	.asciz	"damageAction"
_294:
	.asciz	"repairAction"
_295:
	.asciz	"coutAction"
_296:
	.asciz	"lostAction"
_297:
	.asciz	"stolenAction"
_298:
	.asciz	"nEditMenu"
_299:
	.asciz	"newMenu"
_300:
	.asciz	"contextMenu"
_301:
	.asciz	"infNew"
_302:
	.asciz	"actNew"
_303:
	.asciz	"misNew"
_304:
	.asciz	"noteList"
_305:
	.asciz	":ListBox"
_306:
	.asciz	"bcLabel"
_307:
	.asciz	"snLabel"
_308:
	.asciz	"stLabel"
_309:
	.asciz	"noteTypeList"
_310:
	.asciz	"_build"
_311:
	.asciz	"_save"
_312:
	.asciz	"_addNote"
_313:
	.asciz	"($):Note"
_314:
	.asciz	"_noteBySummary"
	.align	4
_282:
	.long	2
	.long	_283
	.long	3
	.long	_249
	.long	_284
	.long	8
	.long	3
	.long	_285
	.long	_231
	.long	12
	.long	3
	.long	_286
	.long	_228
	.long	16
	.long	3
	.long	_287
	.long	_228
	.long	20
	.long	3
	.long	_262
	.long	_228
	.long	24
	.long	3
	.long	_288
	.long	_250
	.long	28
	.long	3
	.long	_289
	.long	_250
	.long	32
	.long	3
	.long	_290
	.long	_250
	.long	36
	.long	3
	.long	_291
	.long	_250
	.long	40
	.long	3
	.long	_292
	.long	_250
	.long	44
	.long	3
	.long	_293
	.long	_250
	.long	48
	.long	3
	.long	_294
	.long	_250
	.long	52
	.long	3
	.long	_295
	.long	_250
	.long	56
	.long	3
	.long	_296
	.long	_250
	.long	60
	.long	3
	.long	_297
	.long	_250
	.long	64
	.long	3
	.long	_298
	.long	_250
	.long	68
	.long	3
	.long	_299
	.long	_250
	.long	72
	.long	3
	.long	_300
	.long	_250
	.long	76
	.long	3
	.long	_301
	.long	_250
	.long	80
	.long	3
	.long	_302
	.long	_250
	.long	84
	.long	3
	.long	_303
	.long	_250
	.long	88
	.long	3
	.long	_304
	.long	_305
	.long	92
	.long	3
	.long	_306
	.long	_250
	.long	96
	.long	3
	.long	_307
	.long	_250
	.long	100
	.long	3
	.long	_308
	.long	_250
	.long	104
	.long	3
	.long	_309
	.long	_253
	.long	108
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	6
	.long	_263
	.long	_223
	.long	48
	.long	6
	.long	_257
	.long	_223
	.long	52
	.long	6
	.long	_310
	.long	_223
	.long	56
	.long	6
	.long	_258
	.long	_223
	.long	60
	.long	6
	.long	_311
	.long	_223
	.long	64
	.long	6
	.long	_312
	.long	_313
	.long	68
	.long	6
	.long	_259
	.long	_223
	.long	72
	.long	6
	.long	_314
	.long	_313
	.long	76
	.long	0
	.align	4
_bb_iPad:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_282
	.long	112
	.long	__bb_iPad_New
	.long	__bb_iPad_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	__bb_iPad__init
	.long	__bb_iPad__show
	.long	__bb_iPad__build
	.long	__bb_iPad__hide
	.long	__bb_iPad__save
	.long	__bb_iPad__addNote
	.long	__bb_iPad__update
	.long	__bb_iPad__noteBySummary
_316:
	.asciz	"Note"
_317:
	.asciz	"sumField"
_318:
	.asciz	":TextField"
_319:
	.asciz	"detField"
_320:
	.asciz	":TextArea"
_321:
	.asciz	"NTypeLabel"
_322:
	.asciz	"summary"
_323:
	.asciz	"nType"
_324:
	.asciz	"details"
_325:
	.asciz	"date"
_326:
	.asciz	"_typeByInt"
_327:
	.asciz	"(i)$"
	.align	4
_315:
	.long	2
	.long	_316
	.long	3
	.long	_249
	.long	_284
	.long	8
	.long	3
	.long	_317
	.long	_318
	.long	12
	.long	3
	.long	_319
	.long	_320
	.long	16
	.long	3
	.long	_321
	.long	_250
	.long	20
	.long	3
	.long	_322
	.long	_228
	.long	24
	.long	3
	.long	_323
	.long	_231
	.long	28
	.long	3
	.long	_324
	.long	_228
	.long	32
	.long	3
	.long	_325
	.long	_228
	.long	36
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	6
	.long	_263
	.long	_223
	.long	48
	.long	6
	.long	_326
	.long	_327
	.long	52
	.long	6
	.long	_259
	.long	_223
	.long	56
	.long	6
	.long	_311
	.long	_223
	.long	60
	.long	6
	.long	_258
	.long	_223
	.long	64
	.long	0
	.align	4
_bb_Note:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_315
	.long	40
	.long	__bb_Note_New
	.long	__bb_Note_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	__bb_Note__init
	.long	__bb_Note__typeByInt
	.long	__bb_Note__update
	.long	__bb_Note__save
	.long	__bb_Note__hide
_329:
	.asciz	"Student"
	.align	4
_328:
	.long	2
	.long	_329
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	0
	.align	4
_bb_Student:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_328
	.long	8
	.long	__bb_Student_New
	.long	__bb_Student_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
_331:
	.asciz	"logWin"
_332:
	.asciz	"entryList"
_333:
	.asciz	"_populate"
_334:
	.asciz	"_end"
	.align	4
_330:
	.long	2
	.long	_331
	.long	3
	.long	_249
	.long	_284
	.long	8
	.long	3
	.long	_332
	.long	_305
	.long	12
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	6
	.long	_263
	.long	_223
	.long	48
	.long	6
	.long	_333
	.long	_223
	.long	52
	.long	6
	.long	_255
	.long	_278
	.long	56
	.long	6
	.long	_259
	.long	_223
	.long	60
	.long	6
	.long	_334
	.long	_223
	.long	64
	.long	0
	.align	4
_bb_logWin:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_330
	.long	16
	.long	__bb_logWin_New
	.long	__bb_logWin_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	__bb_logWin__init
	.long	__bb_logWin__populate
	.long	__bb_logWin__new
	.long	__bb_logWin__update
	.long	__bb_logWin__end
_336:
	.asciz	"MainWindow"
_337:
	.asciz	"snTF"
_338:
	.asciz	"bcBox"
_339:
	.asciz	":Button"
_340:
	.asciz	"snBox"
_341:
	.asciz	"sfnBox"
_342:
	.asciz	"slnBox"
_343:
	.asciz	"goBut"
_344:
	.asciz	"modeMenu"
_345:
	.asciz	"aboutMenu"
_346:
	.asciz	"helpMenu"
_347:
	.asciz	"qmFile"
_348:
	.asciz	"(:TEvent)i"
	.align	4
_335:
	.long	2
	.long	_336
	.long	3
	.long	_249
	.long	_284
	.long	8
	.long	3
	.long	_337
	.long	_318
	.long	12
	.long	3
	.long	_338
	.long	_339
	.long	16
	.long	3
	.long	_340
	.long	_339
	.long	20
	.long	3
	.long	_341
	.long	_339
	.long	24
	.long	3
	.long	_342
	.long	_339
	.long	28
	.long	3
	.long	_343
	.long	_339
	.long	32
	.long	3
	.long	_344
	.long	_250
	.long	36
	.long	3
	.long	_345
	.long	_250
	.long	40
	.long	3
	.long	_346
	.long	_250
	.long	44
	.long	3
	.long	_347
	.long	_250
	.long	48
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	6
	.long	_263
	.long	_223
	.long	48
	.long	6
	.long	_333
	.long	_223
	.long	52
	.long	6
	.long	_259
	.long	_348
	.long	56
	.long	6
	.long	_334
	.long	_223
	.long	60
	.long	0
	.align	4
_bb_MainWindow:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_335
	.long	52
	.long	__bb_MainWindow_New
	.long	__bb_MainWindow_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	__bb_MainWindow__init
	.long	__bb_MainWindow__populate
	.long	__bb_MainWindow__update
	.long	__bb_MainWindow__end
_350:
	.asciz	"TextArea"
_351:
	.asciz	"(i,i,i,i,:Window,i,i)i"
	.align	4
_349:
	.long	2
	.long	_350
	.long	3
	.long	_249
	.long	_250
	.long	8
	.long	6
	.long	_222
	.long	_223
	.long	16
	.long	6
	.long	_224
	.long	_223
	.long	20
	.long	6
	.long	_263
	.long	_351
	.long	48
	.long	6
	.long	_270
	.long	_271
	.long	52
	.long	6
	.long	_272
	.long	_223
	.long	56
	.long	0
	.align	4
_bb_TextArea:
	.long	_bbObjectClass
	.long	_bbObjectFree
	.long	_349
	.long	12
	.long	__bb_TextArea_New
	.long	__bb_TextArea_Delete
	.long	_bbObjectToString
	.long	_bbObjectCompare
	.long	_bbObjectSendMessage
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	_bbObjectReserved
	.long	__bb_TextArea__init
	.long	__bb_TextArea__getText
	.long	__bb_TextArea__clear
	.align	4
_685:
	.long	_bbNullObject
	.align	4
_687:
	.long	0
	.align	4
_bb_winList:
	.long	_bbNullObject
	.align	4
_bb_ipadList:
	.long	_bbNullObject
	.align	4
_bb_studentList:
	.long	_bbNullObject
	.align	4
_bb_quickMode:
	.long	0
	.align	4
_bb_debugWin:
	.long	_bbNullObject
	.align	4
_1:
	.long	_bbStringClass
	.long	2147483647
	.long	0
	.align	4
_bb_dataDir:
	.long	_1
	.align	4
_696:
	.long	_bbStringClass
	.long	2147483647
	.long	24
	.short	89,111,117,32,97,114,101,32,114,117,110,110,105,110,103,32
	.short	111,110,32,77,97,99,79,115
	.align	4
_21:
	.long	_bbStringClass
	.long	2147483647
	.long	10
	.short	105,112,97,100,100,98,46,99,115,118
	.align	4
_bb_ipaddbn:
	.long	_bbEmptyString
	.align	4
_bb_iPaddb:
	.long	_bbNullObject
	.align	4
_bb_noteWinList:
	.long	_bbNullObject
	.align	4
_183:
	.long	_bbStringClass
	.long	2147483647
	.long	10
	.short	80,97,115,115,119,111,114,100,58,32
	.align	4
_18:
	.long	_bbStringClass
	.long	2147483647
	.long	8
	.short	52,51,50,49,120,103,51,116
	.align	4
_711:
	.long	_bbStringClass
	.long	2147483647
	.long	97
	.short	89,111,117,32,109,117,115,116,32,101,110,116,101,114,32,116
	.short	104,101,32,99,111,114,114,101,99,116,32,112,97,115,115,119
	.short	111,114,100,32,102,111,114,32,105,80,97,100,32,77,97,110
	.short	97,103,101,114,32,48,46,48,46,49,32,116,111,32,119,111
	.short	114,107,46,32,80,108,101,97,115,101,32,97,115,107,32,73
	.short	84,32,102,111,114,32,97,115,115,105,115,116,97,110,99,101
	.short	46
	.align	4
_22:
	.long	_bbStringClass
	.long	2147483647
	.long	12
	.short	83,116,117,100,101,110,116,32,105,80,97,100
	.align	4
_23:
	.long	_bbStringClass
	.long	2147483647
	.long	4
	.short	69,100,105,116
	.align	4
_24:
	.long	_bbStringClass
	.long	2147483647
	.long	7
	.short	66,97,114,99,111,100,101
	.align	4
_25:
	.long	_bbStringClass
	.long	2147483647
	.long	6
	.short	83,101,114,105,97,108
	.align	4
_26:
	.long	_bbStringClass
	.long	2147483647
	.long	6
	.short	65,99,116,105,111,110
	.align	4
_27:
	.long	_bbStringClass
	.long	2147483647
	.long	9
	.short	67,104,101,99,107,32,79,117,116
	.align	4
_28:
	.long	_bbStringClass
	.long	2147483647
	.long	13
	.short	82,101,112,111,114,116,32,68,97,109,97,103,101
	.align	4
_29:
	.long	_bbStringClass
	.long	2147483647
	.long	15
	.short	83,101,110,100,32,102,111,114,32,82,101,112,97,105,114
	.align	4
_30:
	.long	_bbStringClass
	.long	2147483647
	.long	12
	.short	77,97,114,107,32,97,115,32,76,111,115,116
	.align	4
_31:
	.long	_bbStringClass
	.long	2147483647
	.long	14
	.short	77,97,114,107,32,97,115,32,83,116,111,108,101,110
	.align	4
_32:
	.long	_bbStringClass
	.long	2147483647
	.long	11
	.short	99,111,110,116,101,120,116,77,101,110,117
	.align	4
_33:
	.long	_bbStringClass
	.long	2147483647
	.long	3
	.short	78,101,119
	.align	4
_34:
	.long	_bbStringClass
	.long	2147483647
	.long	11
	.short	73,110,102,111,114,109,97,116,105,111,110
	.align	4
_35:
	.long	_bbStringClass
	.long	2147483647
	.long	5
	.short	79,116,104,101,114
	.align	4
_39:
	.long	_bbStringClass
	.long	2147483647
	.long	9
	.short	66,97,114,99,111,100,101,58,32
	.align	4
_40:
	.long	_bbStringClass
	.long	2147483647
	.long	8
	.short	83,101,114,105,97,108,58,32
	.align	4
_41:
	.long	_bbStringClass
	.long	2147483647
	.long	7
	.short	83,116,97,116,101,58,32
	.align	4
_42:
	.long	_bbStringClass
	.long	2147483647
	.long	10
	.short	82,101,103,105,115,116,101,114,101,100
	.align	4
_43:
	.long	_bbStringClass
	.long	2147483647
	.long	10
	.short	67,104,101,99,107,101,100,32,73,110
	.align	4
_44:
	.long	_bbStringClass
	.long	2147483647
	.long	11
	.short	67,104,101,99,107,101,100,32,79,117,116
	.align	4
_47:
	.long	_bbStringClass
	.long	2147483647
	.long	15
	.short	83,101,110,116,32,102,111,114,32,82,101,112,97,105,114
	.align	4
_48:
	.long	_bbStringClass
	.long	2147483647
	.long	7
	.short	68,97,109,97,103,101,100
	.align	4
_49:
	.long	_bbStringClass
	.long	2147483647
	.long	4
	.short	76,111,115,116
	.align	4
_50:
	.long	_bbStringClass
	.long	2147483647
	.long	6
	.short	83,116,111,108,101,110
	.align	4
_45:
	.long	_bbStringClass
	.long	2147483647
	.long	8
	.short	67,104,101,99,107,32,73,110
	.align	4
_46:
	.long	_bbStringClass
	.long	2147483647
	.long	5
	.short	79,119,110,101,114
	.align	4
_51:
	.long	_bbStringClass
	.long	2147483647
	.long	58
	.short	87,111,117,108,100,32,121,111,117,32,108,105,107,101,32,116
	.short	111,32,119,114,105,116,101,32,121,111,117,114,32,99,104,97
	.short	110,103,101,115,32,98,97,99,107,32,116,111,32,116,104,101
	.short	32,100,97,116,97,98,97,115,101,63
	.align	4
_55:
	.long	_bbStringClass
	.long	2147483647
	.long	1
	.short	44
	.align	4
_59:
	.long	_bbStringClass
	.long	2147483647
	.long	1
	.short	59
	.align	4
_60:
	.long	_bbStringClass
	.long	2147483647
	.long	4
	.short	101,111,110,44
	.align	4
_61:
	.long	_bbStringClass
	.long	2147483647
	.long	3
	.short	101,111,101
	.align	4
_14:
	.long	_bbStringClass
	.long	2147483647
	.long	1
	.short	32
	.align	4
_65:
	.long	_bbStringClass
	.long	2147483647
	.long	58
	.short	65,114,101,32,121,111,117,32,115,117,114,101,32,116,104,97
	.short	116,32,121,111,117,32,119,97,110,116,32,116,111,32,99,104
	.short	97,110,103,101,32,116,104,101,32,66,97,114,99,111,100,101
	.short	32,102,111,114,32,105,80,97,100,32
	.align	4
_66:
	.long	_bbStringClass
	.long	2147483647
	.long	1
	.short	63
	.align	4
_19:
	.long	_bbStringClass
	.long	2147483647
	.long	147
	.short	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.short	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.short	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.short	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.short	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.short	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.short	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.short	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.short	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.short	32,32,32
	.align	4
_67:
	.long	_bbStringClass
	.long	2147483647
	.long	188
	.short	84,104,105,115,32,119,105,108,108,32,99,104,97,110,103,101
	.short	32,116,104,101,32,73,68,32,105,110,32,116,104,101,32,100
	.short	97,116,97,98,97,115,101,32,97,110,100,32,121,111,117,32
	.short	119,105,108,108,32,110,111,32,108,111,110,103,101,114,32,102
	.short	105,110,100,32,116,104,101,32,105,80,97,100,32,119,105,116
	.short	104,32,116,104,101,32,111,108,100,32,66,97,114,99,111,100
	.short	101,46,32,65,108,115,111,32,105,102,32,121,111,117,32,112
	.short	114,111,118,105,100,101,32,97,110,32,97,108,114,101,97,100
	.short	121,32,117,115,101,100,32,98,97,114,99,111,100,101,32,116
	.short	104,105,115,32,101,110,116,114,121,32,119,105,108,108,32,111
	.short	118,101,114,119,114,105,116,101,32,116,104,101,32,112,114,101
	.short	118,105,111,117,115,32,101,110,116,114,121,46
	.align	4
_68:
	.long	_bbStringClass
	.long	2147483647
	.long	24
	.short	80,108,101,97,115,101,32,101,110,116,101,114,32,110,101,119
	.short	32,66,97,114,99,111,100,101
	.align	4
_69:
	.long	_bbStringClass
	.long	2147483647
	.long	17
	.short	67,104,97,110,103,101,100,32,66,97,114,99,111,100,101,59
	.short	91
	.align	4
_70:
	.long	_bbStringClass
	.long	2147483647
	.long	34
	.short	93,59,49,59,84,104,101,32,98,97,114,99,111,100,101,32
	.short	119,97,115,32,99,104,97,110,103,101,100,32,102,114,111,109
	.short	32,91
	.align	4
_71:
	.long	_bbStringClass
	.long	2147483647
	.long	6
	.short	93,32,116,111,32,91
	.align	4
_72:
	.long	_bbStringClass
	.long	2147483647
	.long	5
	.short	93,59,101,111,110
	.align	4
_73:
	.long	_bbStringClass
	.long	2147483647
	.long	57
	.short	65,114,101,32,121,111,117,32,115,117,114,101,32,116,104,97
	.short	116,32,121,111,117,32,119,97,110,116,32,116,111,32,99,104
	.short	97,110,103,101,32,116,104,101,32,83,101,114,105,97,108,32
	.short	102,111,114,32,105,80,97,100,32
	.align	4
_74:
	.long	_bbStringClass
	.long	2147483647
	.long	204
	.short	84,104,105,115,32,105,115,32,100,97,110,103,101,114,111,117
	.short	115,32,97,110,100,32,119,105,108,108,32,99,104,97,110,103
	.short	101,32,116,104,101,32,73,68,32,105,110,32,116,104,101,32
	.short	100,97,116,97,98,97,115,101,32,97,110,100,32,121,111,117
	.short	32,119,105,108,108,32,110,111,32,108,111,110,103,101,114,32
	.short	102,105,110,100,32,116,104,101,32,105,80,97,100,32,119,105
	.short	116,104,32,116,104,101,32,111,108,100,32,115,101,114,105,97
	.short	108,46,32,65,108,115,111,32,105,102,32,121,111,117,32,112
	.short	114,111,118,105,100,101,32,97,110,32,97,108,114,101,97,100
	.short	121,32,117,115,101,100,32,98,97,114,99,111,100,101,32,116
	.short	104,105,115,32,101,110,116,114,121,32,119,105,108,108,32,111
	.short	118,101,114,119,114,105,116,101,32,116,104,101,32,112,114,101
	.short	118,105,111,117,115,32,101,110,116,114,121,46
	.align	4
_75:
	.long	_bbStringClass
	.long	2147483647
	.long	23
	.short	80,108,101,97,115,101,32,101,110,116,101,114,32,110,101,119
	.short	32,83,101,114,105,97,108
	.align	4
_76:
	.long	_bbStringClass
	.long	2147483647
	.long	16
	.short	67,104,97,110,103,101,100,32,83,101,114,105,97,108,59,91
	.align	4
_77:
	.long	_bbStringClass
	.long	2147483647
	.long	33
	.short	93,59,49,59,84,104,101,32,115,101,114,105,97,108,32,119
	.short	97,115,32,99,104,97,110,103,101,100,32,102,114,111,109,32
	.short	91
	.align	4
_78:
	.long	_bbStringClass
	.long	2147483647
	.long	22
	.short	83,72,79,87,32,79,87,78,69,82,32,65,67,84,73,79
	.short	78,32,72,69,82,69
	.align	4
_79:
	.long	_bbStringClass
	.long	2147483647
	.long	18
	.short	105,80,97,100,32,119,97,115,32,68,97,109,97,103,101,100
	.short	59,91
	.align	4
_80:
	.long	_bbStringClass
	.long	2147483647
	.long	43
	.short	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	.short	32,114,101,112,111,114,116,101,100,32,100,97,109,97,103,101
	.short	100,32,116,111,32,117,115,32,97,116,32
	.align	4
_81:
	.long	_bbStringClass
	.long	2147483647
	.long	80
	.short	46,32,84,104,101,32,111,119,110,101,114,32,114,101,115,112
	.short	111,110,115,105,98,108,101,32,119,97,115,32,91,73,78,83
	.short	69,82,84,32,83,84,85,68,69,78,84,32,78,65,77,69
	.short	32,72,69,82,69,93,46,32,84,104,101,32,112,114,101,118
	.short	105,111,117,115,32,115,116,97,116,101,32,119,97,115,32,91
	.align	4
_82:
	.long	_bbStringClass
	.long	2147483647
	.long	61
	.short	93,46,32,80,108,101,97,115,101,32,97,100,100,32,97,110
	.short	121,32,101,120,116,114,97,32,110,111,116,101,115,32,100,101
	.short	115,99,114,105,98,105,110,103,32,116,104,101,32,100,97,109
	.short	97,103,101,32,104,101,114,101,46,59,101,111,110
	.align	4
_83:
	.long	_bbStringClass
	.long	2147483647
	.long	15
	.short	65,112,112,108,101,32,80,79,32,78,117,109,98,101,114
	.align	4
_84:
	.long	_bbStringClass
	.long	2147483647
	.long	17
	.short	83,101,110,116,32,102,111,114,32,82,101,112,97,105,114,59
	.short	91
	.align	4
_85:
	.long	_bbStringClass
	.long	2147483647
	.long	36
	.short	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	.short	32,115,101,110,116,32,102,111,114,32,114,101,112,97,105,114
	.short	32,97,116,32
	.align	4
_86:
	.long	_bbStringClass
	.long	2147483647
	.long	18
	.short	32,119,105,116,104,32,80,46,79,46,32,110,117,109,98,101
	.short	114,32
	.align	4
_87:
	.long	_bbStringClass
	.long	2147483647
	.long	26
	.short	46,32,84,104,101,32,112,114,101,118,105,111,117,115,32,115
	.short	116,97,116,101,32,119,97,115,32,91
	.align	4
_88:
	.long	_bbStringClass
	.long	2147483647
	.long	13
	.short	67,104,101,99,107,101,100,32,79,117,116,59,91
	.align	4
_89:
	.long	_bbStringClass
	.long	2147483647
	.long	32
	.short	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	.short	32,99,104,101,99,107,101,100,32,111,117,116,32,97,116,32
	.align	4
_90:
	.long	_bbStringClass
	.long	2147483647
	.long	56
	.short	32,116,111,32,91,73,78,83,69,82,84,32,83,84,85,68
	.short	69,78,84,32,78,65,77,69,32,72,69,82,69,93,46,32
	.short	84,104,101,32,112,114,101,118,105,111,117,115,32,115,116,97
	.short	116,101,32,119,97,115,32,91
	.align	4
_91:
	.long	_bbStringClass
	.long	2147483647
	.long	12
	.short	67,104,101,99,107,101,100,32,73,110,59,91
	.align	4
_92:
	.long	_bbStringClass
	.long	2147483647
	.long	31
	.short	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	.short	32,99,104,101,99,107,101,100,32,105,110,32,97,116,32
	.align	4
_93:
	.long	_bbStringClass
	.long	2147483647
	.long	58
	.short	32,102,114,111,109,32,91,73,78,83,69,82,84,32,83,84
	.short	85,68,69,78,84,32,78,65,77,69,32,72,69,82,69,93
	.short	46,32,84,104,101,32,112,114,101,118,105,111,117,115,32,115
	.short	116,97,116,101,32,119,97,115,32,91
	.align	4
_94:
	.long	_bbStringClass
	.long	2147483647
	.long	15
	.short	105,80,97,100,32,119,97,115,32,76,111,115,116,59,91
	.align	4
_95:
	.long	_bbStringClass
	.long	2147483647
	.long	40
	.short	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	.short	32,114,101,112,111,114,116,101,100,32,108,111,115,116,32,116
	.short	111,32,117,115,32,97,116,32
	.align	4
_96:
	.long	_bbStringClass
	.long	2147483647
	.long	66
	.short	32,98,121,32,116,104,101,32,111,119,110,101,114,32,91,73
	.short	78,83,69,82,84,32,83,84,85,68,69,78,84,32,78,65
	.short	77,69,32,72,69,82,69,93,46,32,84,104,101,32,112,114
	.short	101,118,105,111,117,115,32,115,116,97,116,101,32,119,97,115
	.short	32,91
	.align	4
_97:
	.long	_bbStringClass
	.long	2147483647
	.long	30
	.short	80,111,108,105,99,101,32,82,101,112,111,114,116,32,82,101
	.short	102,101,114,101,110,99,101,32,78,117,109,98,101,114
	.align	4
_98:
	.long	_bbStringClass
	.long	2147483647
	.long	17
	.short	105,80,97,100,32,119,97,115,32,83,116,111,108,101,110,59
	.short	91
	.align	4
_99:
	.long	_bbStringClass
	.long	2147483647
	.long	42
	.short	93,59,49,59,84,104,101,32,105,80,97,100,32,119,97,115
	.short	32,114,101,112,111,114,116,101,100,32,115,116,111,108,101,110
	.short	32,116,111,32,117,115,32,97,116,32
	.align	4
_100:
	.long	_bbStringClass
	.long	2147483647
	.long	41
	.short	93,46,32,84,104,101,32,112,111,108,105,99,101,32,114,101
	.short	112,111,114,116,32,114,101,102,101,114,101,110,99,101,32,110
	.short	117,109,98,101,114,32,105,115,32
	.align	4
_101:
	.long	_bbStringClass
	.long	2147483647
	.long	4
	.short	59,101,111,110
	.align	4
_102:
	.long	_bbStringClass
	.long	2147483647
	.long	10
	.short	78,101,119,32,78,111,116,101,59,91
	.align	4
_103:
	.long	_bbStringClass
	.long	2147483647
	.long	19
	.short	93,59,48,59,78,101,119,32,68,101,116,97,105,108,115,59
	.short	101,111,110
	.align	4
_104:
	.long	_bbStringClass
	.long	2147483647
	.long	19
	.short	93,59,49,59,78,101,119,32,68,101,116,97,105,108,115,59
	.short	101,111,110
	.align	4
_105:
	.long	_bbStringClass
	.long	2147483647
	.long	19
	.short	93,59,50,59,78,101,119,32,68,101,116,97,105,108,115,59
	.short	101,111,110
	.align	4
_118:
	.long	_bbStringClass
	.long	2147483647
	.long	11
	.short	78,101,101,100,32,83,101,114,105,97,108
	.align	4
_119:
	.long	_bbStringClass
	.long	2147483647
	.long	17
	.short	82,101,103,105,115,116,101,114,101,100,32,105,80,97,100,59
	.short	91
	.align	4
_120:
	.long	_bbStringClass
	.long	2147483647
	.long	23
	.short	93,59,49,59,82,101,103,105,115,116,101,114,101,100,32,105
	.short	80,97,100,32,111,110,32
	.align	4
_121:
	.long	_bbStringClass
	.long	2147483647
	.long	4
	.short	32,97,116,32
	.align	4
_122:
	.long	_bbStringClass
	.long	2147483647
	.long	4
	.short	45,45,45,45
	.align	4
_126:
	.long	_bbStringClass
	.long	2147483647
	.long	72
	.short	84,104,105,115,32,105,80,97,100,32,105,115,32,99,117,114
	.short	114,101,110,116,108,121,32,98,101,105,110,103,32,97,99,99
	.short	101,115,115,101,100,32,101,108,115,101,119,104,101,114,101,44
	.short	32,112,108,101,97,115,101,32,116,114,121,32,97,103,97,105
	.short	110,32,108,97,116,101,114,46
	.align	4
_133:
	.long	_bbStringClass
	.long	2147483647
	.long	51
	.short	84,104,101,114,101,32,105,115,32,110,111,32,101,110,116,114
	.short	121,32,105,110,32,39,105,112,97,100,100,98,46,99,115,118
	.short	39,32,119,105,116,104,32,116,104,101,32,83,101,114,105,97
	.short	108,32,91
	.align	4
_134:
	.long	_bbStringClass
	.long	2147483647
	.long	9
	.short	93,32,67,114,101,97,116,101,63
	.align	4
_144:
	.long	_bbStringClass
	.long	2147483647
	.long	52
	.short	84,104,101,114,101,32,105,115,32,110,111,32,101,110,116,114
	.short	121,32,105,110,32,39,105,112,97,100,100,98,46,99,115,118
	.short	39,32,119,105,116,104,32,116,104,101,32,66,97,114,99,111
	.short	100,101,32,91
	.align	4
_145:
	.long	_bbStringClass
	.long	2147483647
	.long	10
	.short	105,80,97,100,32,78,111,116,101,32
	.align	4
_146:
	.long	_bbStringClass
	.long	2147483647
	.long	33
	.short	87,111,117,108,100,32,121,111,117,32,108,105,107,101,32,116
	.short	111,32,115,97,118,101,32,116,104,105,115,32,110,111,116,101
	.short	63
	.align	4
_147:
	.long	_bbStringClass
	.long	2147483647
	.long	27
	.short	89,111,117,32,99,97,110,110,111,116,32,117,115,101,32,99
	.short	111,109,109,97,115,32,104,101,114,101,46
	.align	4
_148:
	.long	_bbStringClass
	.long	2147483647
	.long	31
	.short	89,111,117,32,99,97,110,110,111,116,32,117,115,101,32,115
	.short	101,109,105,99,111,108,111,110,115,32,104,101,114,101,46
	.align	4
_149:
	.long	_bbStringClass
	.long	2147483647
	.long	81
	.short	84,104,101,32,114,101,116,117,114,110,32,107,101,121,32,119
	.short	97,115,32,117,115,101,100,32,105,110,32,97,32,110,111,116
	.short	101,46,32,84,104,105,115,32,109,97,121,32,104,97,118,101
	.short	32,99,111,114,114,117,112,116,101,100,32,116,104,101,32,100
	.short	97,116,97,32,102,111,114,32,116,104,101,32,105,80,97,100
	.short	46
	.align	4
_150:
	.long	_bbStringClass
	.long	2147483647
	.long	235
	.short	89,111,117,32,99,97,110,110,111,116,32,117,115,101,32,116
	.short	104,101,32,69,110,116,101,114,32,75,101,121,32,104,101,114
	.short	101,46,32,84,104,101,32,119,97,121,32,111,117,114,32,100
	.short	97,116,97,98,97,115,101,32,105,115,32,115,116,114,117,99
	.short	116,117,114,101,100,32,105,116,32,119,105,108,108,32,99,111
	.short	114,114,117,112,116,32,97,108,108,32,116,104,101,32,100,97
	.short	116,97,32,97,110,100,32,101,114,97,115,101,32,97,108,108
	.short	32,110,111,116,101,115,32,102,111,114,32,116,104,105,115,32
	.short	105,80,97,100,46,32,80,108,101,97,115,101,32,98,97,99
	.short	107,115,112,97,99,101,32,116,111,32,99,111,110,116,105,110
	.short	117,101,32,111,110,32,116,104,101,32,108,105,110,101,32,121
	.short	111,117,32,119,101,114,101,32,119,114,105,116,105,110,103,32
	.short	111,110,32,97,110,100,32,100,111,32,110,111,116,32,117,115
	.short	101,32,116,104,101,32,101,110,116,101,114,32,107,101,121,32
	.short	104,101,114,101,32,97,103,97,105,110,46
	.align	4
_151:
	.long	_bbStringClass
	.long	2147483647
	.long	9
	.short	68,101,98,117,103,32,76,111,103
	.align	4
_1387:
	.long	_bbStringClass
	.long	2147483647
	.long	26
	.short	83,116,97,114,116,101,100,32,105,80,97,100,32,77,97,110
	.short	97,103,101,114,32,48,46,48,46,49
	.align	4
_153:
	.long	_bbStringClass
	.long	2147483647
	.long	9
	.short	58,32,91,73,78,70,79,93,32
	.align	4
_154:
	.long	_bbStringClass
	.long	2147483647
	.long	12
	.short	58,32,91,87,65,82,78,73,78,71,93,32
	.align	4
_155:
	.long	_bbStringClass
	.long	2147483647
	.long	10
	.short	58,32,91,69,82,82,79,82,93,32
	.align	4
_156:
	.long	_bbStringClass
	.long	2147483647
	.long	25
	.short	108,111,103,87,105,110,32,116,114,105,103,103,101,114,101,100
	.short	32,97,110,32,101,118,101,110,116
	.align	4
_157:
	.long	_bbStringClass
	.long	2147483647
	.long	19
	.short	83,101,110,100,105,110,103,32,107,105,108,108,32,115,105,103
	.short	110,97,108
	.align	4
_219:
	.long	_bbStringClass
	.long	2147483647
	.long	18
	.short	105,80,97,100,32,77,97,110,97,103,101,114,32,48,46,48
	.short	46,49
	.align	4
_161:
	.long	_bbStringClass
	.long	2147483647
	.long	5
	.short	70,105,114,115,116
	.align	4
_162:
	.long	_bbStringClass
	.long	2147483647
	.long	4
	.short	76,97,115,116
	.align	4
_163:
	.long	_bbStringClass
	.long	2147483647
	.long	2
	.short	71,111
	.align	4
_164:
	.long	_bbStringClass
	.long	2147483647
	.long	4
	.short	77,111,100,101
	.align	4
_165:
	.long	_bbStringClass
	.long	2147483647
	.long	20
	.short	81,117,105,99,107,32,77,111,100,101,32,91,116,117,114,110
	.short	32,111,110,93
	.align	4
_166:
	.long	_bbStringClass
	.long	2147483647
	.long	4
	.short	72,101,108,112
	.align	4
_167:
	.long	_bbStringClass
	.long	2147483647
	.long	5
	.short	65,98,111,117,116
	.align	4
_168:
	.long	_bbStringClass
	.long	2147483647
	.long	21
	.short	81,117,105,99,107,32,77,111,100,101,32,91,116,117,114,110
	.short	32,111,102,102,93
	.align	4
_1505:
	.long	_bbStringClass
	.long	2147483647
	.long	145
	.short	105,80,97,100,32,77,97,110,97,103,101,114,32,48,46,48
	.short	46,49,32,119,97,115,32,112,114,111,103,114,97,109,109,101
	.short	100,32,98,121,32,67,111,114,101,121,32,39,82,121,97,110
	.short	39,32,68,101,97,110,32,102,111,114,32,84,97,108,108,97
	.short	115,115,101,101,32,67,105,116,121,32,83,99,104,111,111,108
	.short	115,32,102,111,114,32,117,115,101,32,111,102,32,109,97,110
	.short	97,103,105,110,103,32,116,104,101,32,115,116,117,100,101,110
	.short	116,32,105,80,97,100,115,32,97,116,32,84,97,108,108,97
	.short	115,115,101,101,32,72,105,103,104,32,83,99,104,111,111,108
	.short	46
	.align	4
_170:
	.long	_bbStringClass
	.long	2147483647
	.long	25
	.short	72,69,76,80,77,69,78,85,32,65,67,84,73,79,78,32
	.short	71,79,69,83,32,72,69,82,69
	.align	4
_171:
	.long	_bbStringClass
	.long	2147483647
	.long	32
	.short	83,101,97,114,99,104,105,110,103,32,102,111,114,32,101,110
	.short	116,114,121,32,119,105,116,104,32,115,101,114,105,97,108,32
	.align	4
_208:
	.long	_bbStringClass
	.long	2147483647
	.long	1
	.short	48
	.align	4
_172:
	.long	_bbStringClass
	.long	2147483647
	.long	32
	.short	83,101,97,114,99,104,105,110,32,102,111,114,32,101,110,116
	.short	114,121,32,119,105,116,104,32,66,97,114,99,111,100,101,32
	.align	4
_173:
	.long	_bbStringClass
	.long	2147483647
	.long	36
	.short	83,101,97,114,99,104,105,110,103,32,102,111,114,32,101,110
	.short	116,114,121,32,119,105,116,104,32,70,105,114,115,116,32,78
	.short	97,109,101,32
	.align	4
_174:
	.long	_bbStringClass
	.long	2147483647
	.long	35
	.short	83,101,97,114,99,104,105,110,103,32,102,111,114,32,101,110
	.short	116,114,121,32,119,105,116,104,32,76,97,115,116,32,78,97
	.short	109,101,32
	.align	4
_178:
	.long	_bbStringClass
	.long	2147483647
	.long	35
	.short	80,76,69,65,83,69,32,83,69,84,32,85,80,32,69,86
	.short	69,78,84,32,72,65,78,68,76,69,32,70,79,82,32,83
	.short	82,67,58
	.align	4
_179:
	.long	_bbStringClass
	.long	2147483647
	.long	4
	.short	32,73,68,58
	.align	4
_198:
	.long	_bbStringClass
	.long	2147483647
	.long	77
	.short	65,114,101,32,121,111,117,32,115,117,114,101,63,32,82,101
	.short	109,101,109,98,101,114,32,116,104,97,116,32,116,104,101,114
	.short	101,32,105,115,32,110,111,32,119,97,121,32,116,111,32,117
	.short	110,100,111,32,119,104,97,116,32,121,111,117,32,97,114,101
	.short	32,97,98,111,117,116,32,116,111,32,100,111,46
	.align	4
_205:
	.long	_bbStringClass
	.long	2147483647
	.long	3
	.short	101,111,110
	.align	4
_206:
	.long	_bbStringClass
	.long	2147483647
	.long	26
	.short	84,104,101,32,110,111,116,101,32,100,97,116,97,32,105,115
	.short	32,99,111,114,114,117,112,116,32,91
	.align	4
_207:
	.long	_bbStringClass
	.long	2147483647
	.long	1
	.short	93
