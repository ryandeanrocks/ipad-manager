'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type MainWindow
	Field handle:Window = New Window
	
	'GUI Gadgets
	Field snTF:TextField, bcBox:Button, snBox:Button, sfnBox:Button, slnBox:Button, goBut:Button
	
	Field modeMenu:TGadget, aboutMenu:TGadget, helpMenu:TGadget
	
	Field qmFile:TGadget
	
	Method _init()
		handle._new(0, 0, 400, 300, ProgramInfo, winStyleDefault)
		
		_populate()
		handle._show()
	End Method
	Method _populate()
		snTF = newTextField((GadgetWidth(handle.handle) / 2) - (150 / 2), 50, 150, 20, handle)
		bcBox = newButton("Barcode", (GadgetWidth(handle.handle) / 2) - (75), 85, 75, 15, handle, BUTTON_CHECKBOX)
		bcBox._state()
		snBox = newButton("Serial", (GadgetWidth(handle.handle) / 2) - (75), 105, 75, 15, handle, BUTTON_CHECKBOX)
		sfnBox = newButton("First", (GadgetWidth(handle.handle) / 2), 85, 75, 15, handle, BUTTON_CHECKBOX)
		slnBox = newButton("Last", (GadgetWidth(handle.handle) / 2), 105, 75, 15, handle, BUTTON_CHECKBOX)
		goBut = newButton("Go", (GadgetWidth(handle.handle) / 2) - (75 / 2), 150, 75, 40, handle)
		
		'menus
		modeMenu = CreateMenu("Mode", 0, handle.menu)
		qmFile = CreateMenu("Quick Mode [turn on]", 0, modeMenu)
		helpMenu = CreateMenu("Help", 0, handle.menu)
		aboutMenu = CreateMenu("About", 0, handle.menu)
		UpdateWindowMenu(handle.handle)
		ActivateGadget(snTF.handle)
	End Method
		
	Method _update(event:TEvent)
		Select EventSource()
			Case qmFile
				If quickMode
					quickMode = False
					SetGadgetText qmFile, "Quick Mode [turn on]"
				Else
					quickMode = True
					SetGadgetText qmFile, "Quick Mode [turn off]"
				EndIf
				
				UpdateWindowMenu(handle.handle)
			Case aboutMenu
				Notify ProgramInfo + " was programmed by Corey 'Ryan' Dean for Tallassee City Schools for use of managing the student iPads at Tallassee High School."
				Return
			Case helpMenu
				dlog "HELPMENU ACTION GOES HERE"
				Return
			Case snTF.handle
				Select EventID()
					Case EVENT_GADGETACTION
						Local line:String = GadgetText(snTF.handle)
						If snBox._cstate()
							If Len(line) = 12
								dlog "Searching for entry with serial " + line
								Local ni:iPad = findBySN(line)
								If ni <> Null
									ni._show()
								EndIf
								snTF._clear()
								Return
							EndIf
						End If
						If bcBox._cstate()
							Local tline:String = Left(line, 1)
							If tline = 0
								If Len(line) = 5
									line = Right(line, 4)
									dlog "Searchin for entry with Barcode " + line
									Local ni:iPad = findByBC(line)
									If ni <> Null
										ni._show()
									EndIf
									snTF._clear()
									Return
								EndIf
							Else If Len(line) = 4
								dlog "Searchin for entry with Barcode " + line
								Local ni:iPad = findByBC(line)
								If ni <> Null
									ni._show()
								EndIf
								snTF._clear()
								Return
							EndIf
							
						End If
						Return
					
				End Select
			Case goBut.handle
				Select EventID()
					Case EVENT_GADGETACTION
						Local line:String = GadgetText(snTF.handle)
						If sfnBox._cstate()
							dlog "Searching for entry with First Name " + line
							Local tmpStudent:Student = newBlankStudent(line, "[blank]")'findByFirst(line)
							snTF._clear()
							Return
						End If
						If slnBox._cstate()
							dlog "Searching for entry with Last Name " + line
							Local tmpStudent:Student = newBlankStudent("[blank]",line)'findByLast(line)
							snTF._clear()
							Return
						End If
				End Select
			Case bcBox.handle
				Select EventID()
					Case EVENT_GADGETACTION
						bcBox._state()
						If bcBox._cstate() = 1
							If snBox._cstate() = 1
								snBox._state()
							End If
							If sfnBox._cstate()
								sfnBox._state()
							End If
							If slnBox._cstate()
								slnBox._state()
							End If
						End If
				End Select
			Case snBox.handle
				Select EventID()
					Case EVENT_GADGETACTION
						snBox._state()
						If snBox._cstate() = 1
							If bcBox._cstate() = 1
								bcBox._state()
							End If
							If sfnBox._cstate()
								sfnBox._state()
							End If
							If slnBox._cstate()
								slnBox._state()
							End If
						End If
				End Select
			Case sfnBox.handle
				Select EventID()
					Case EVENT_GADGETACTION
						sfnBox._state()
						If sfnBox._cstate() = 1
							If snBox._cstate() = 1
								snBox._state()
							End If
							If bcBox._cstate()
								bcBox._state()
							End If
							If slnBox._cstate()
								slnBox._state()
							End If
						End If
				End Select
			Case slnBox.handle
				Select EventID()
					Case EVENT_GADGETACTION
						slnBox._state()
						If slnBox._cstate() = 1
							If snBox._cstate() = 1
								snBox._state()
							End If
							If sfnBox._cstate()
								sfnBox._state()
							End If
							If bcBox._cstate()
								bcBox._state()
							End If
						End If
				End Select
			
			
		End Select
		If EventSource() = handle.handle
			Select EventID()
				Case EVENT_WINDOWCLOSE
					Return 1
			End Select
			Return
		End If
		For Local es:TGadget = EachIn handle.gadgetList
			If EventSource() = es
				Select EventID()
					Case EVENT_GADGETLOSTFOCUS
					Case 8193
					Default
						dlog "PLEASE SET UP EVENT HANDLE FOR SRC:" + EventSource().ToString() + " ID:" + EventID()
				End Select
				
				Return
			End If
		Next
		Return
	End Method
	Method _end()
		For Local g:TGadget = EachIn handle.gadgetList
			HideGadget(g)
			FreeGadget(g)
		Next
		HideGadget(handle.handle)
		FreeGadget(handle.handle)
		handle = Null
		ListRemove(winList, Self)
		Return
	End Method
End Type
