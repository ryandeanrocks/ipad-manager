'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem

Global studdbn:String = dataDir + "studdb.csv"
Global studdb:TStream = OpenFile(studdbn)
If studdb = Null
	WriteFile(studdbn)
	studdb = OpenFile(studdbn)
End If


Type Student
	Field handle:Window = New Window
	Field pos:Int
	
	Field first:String, last:String, balance:Int, ipadPos:Int, grade:Int
	
	Field noteList:ListBox, fnLabel:TGadget, balLabel:TGadget, grLabel:TGadget
	
	Field noteTypeList:TList = New TList
	
	Field sipad:Ipad
	
	Field fnEdit:TGadget, lnEdit:TGadget, gEdit:TGadget
	Field ipadAction:TGadget, chargerFee:TGadget, cableFee:TGadget, caseFee:TGadget, extraFee:TGadget, lostIpad:TGadget, stolenIpad:TGadget, damagedIpad:TGadget, iPadMenu:TGadget
	
	
	Method _init()
		handle._new(10, 400, 400, 300, "Student Profile", WINDOW_TOOL + WINDOW_TITLEBAR + WINDOW_MENU)
		ListAddLast(studentList, Self)
		noteList = newListBox(10, 75, 375, 170, handle)
		
		Local editMenu:TGadget = CreateMenu("Edit", 0, handle.menu)
			fnEdit = CreateMenu("First Name", 0, editMenu)
			lnEdit = CreateMenu("Last Name", 0, editMenu)
			gEdit = CreateMenu("Grade", 0, editMenu)
		Local actionMenu:TGadget = CreateMenu("Action", 0, handle.menu)
			ipadAction = CreateMenu("Assign iPad", 0, actionMenu)
			CreateMenu("", 0, actionMenu)
			Local feeAction:TGadget = CreateMenu("Charger Fee", 0, actionMenu)
				chargerFee = CreateMenu("Wall Charger [$" + chargerPrice + "]", 0, feeAction)
				cableFee = CreateMenu("USB Cable [$" + cablePrice + "]", 0, feeAction)
				caseFee = CreateMenu("Case [$" + casePrice + "]", 0, feeAction)
				extraFee = CreateMenu("Extra Fee", 0, feeAction)
			lostIpad = CreateMenu("Lost iPad", 0, actionMenu)
			stolenIpad = CreateMenu("Stolen iPad", 0, actionMenu)
			damagedIpad = CreateMenu("Damaged iPad", 0, actionMenu)
		iPadMenu = CreateMenu("iPad Profile", 0, handle.menu)
			
		
		UpdateWindowMenu(handle.handle)
	End Method
	
	Method _show()
		handle._show()
	End Method
	
	Method _hide()
		If quickMode
			_save()
		ElseIf Confirm("Would you like to write your changes back to the database?")
			_save()
		End If
		handle._hide()
		For Local i:TGadget = EachIn handle.gadgetList
			HideGadget(i)
			FreeGadget(i)
		Next
		HideGadget(handle.handle)
		FreeGadget(handle.handle)
		handle = Null
		ListRemove(studentList, Self)
	End Method
	
	Method _build()
		For Local i:String = EachIn noteTypeList
			Local nNote:Note = New Note
			ListAddLast(noteTypeList, nNote.summary)
		Next
		
		fnLabel = CreateLabel("Name: " + first + " " + last, 10, 10, 150, 20, handle.handle)
		balLabel = CreateLabel("Balance Owed: $" + balance, 200, 10, 150, 20, handle.handle)
		grLabel = CreateLabel("Grade: " + grade, 10, 35, 150, 20, handle.handle)
		
		If siPad = Null
			
		Else
			SetGadgetText(ipadAction, "Return iPad")
		EndIf
		UpdateWindowMenu(handle.handle)
	End Method
	
	Method _save()
		dlog "This is where you would save"
	End Method
	
	Method _addNote:Note(command:String)
		Local lineArray:TList = convertFromSSV(command)
		Local i:Int = 1
		Local nNote:Note
		For Local n:String = EachIn lineArray
			If i = 1
				nNote = newNote()
				ListAddLast(noteTypeList, nNote)
				nNote.summary = n
			EndIf
			If i = 2
				nNote.date = n
				noteList._add(nNote.date + " " + nNote.summary, GADGETITEM_DEFAULT)
			EndIf
			If i = 3
				nNote.nType = Int n
			EndIf
			If i = 4
				nNote.details = n
			EndIf
			i = i + 1
		Next
		
		
		Return nNote
	End Method
	
	Method _update()
		Select EventSource()
			Case ipadAction
				If siPad = Null
					siPad = findByBC(prompt("Please enter Barcode:", 4))
					If sipad.state = "Checked In"
						sipad._addNote("Checked Out;[" + CurrentDate() + "];1;The iPad was checked out at " + CurrentTime() + " to [INSERT STUDENT NAME HERE]. The previous state was [" + sipad.state + "];eon")
						sipad.state = "Checked Out"
						SetGadgetText(sipad.stLabel, "State: " + sipad.state)
						SetGadgetText(sipad.coutAction, "Check In")
						sipad.ownerMenu = CreateMenu("Owner", 0, sipad.handle.menu)
					
						SetGadgetText(ipadAction, "Return iPad")
						UpdateWindowMenu(sipad.handle.handle)
						UpdateWindowMenu(handle.handle)
					Else
						Notify ("This iPad is not checked in to the library.")
					EndIf
				Else
					dlog "Release iPad"
				EndIf
			Case gEdit
				Local conf:Int
				If quickMode
					conf = 1
				Else
					conf = Confirm("Are you sure that you want to change this student's grade? Grades are automatically incremented every year.")
				End If
				If conf
					Local ng:String = prompt("New Grade:", 1)
					If ng <> ""
						_addNote("Changed Student Grade Level;[" + CurrentDate() + "];1;This student's grade level was changed from [" + grade + "] to [" + ng + "].;eon")
						grade = int ng
						SetGadgetText(grLabel, "Grade: " + grade)
					End If
				End If
			Case lnEdit
				Local conf:Int
				If quickMode
					conf = 1
				Else
					conf = Confirm("Are you sure that you want to change the last name? You will not find thsi student by their old name.")
				End If
				If conf
					Local nln:String = prompt("New last name:")
					If nln <> ""
						_addNote("Changed Last Name;[" + CurrentDate() + "];1;This student's last name was changed from [" + last + "] to [" + nln + "].;eon")
						last = nln
						SetGadgetText(fnLabel, "Name: " + first + " " + last)
					End If
				End If
			Case fnEdit
				Local conf:Int
				If quickMode
					conf = 1
				Else
					conf = Confirm("Are you sure that you want to change the first name? You will not find this student by their old name.")
				EndIf
				If conf
					Local nfn:String = prompt("New first name:")
					If nfn <> ""
						_addNote("Changed First Name;[" + CurrentDate() + "];1;This student's first name was changed from [" + first + "] to [" + nfn + "].;eon")
						first = nfn
						SetGadgetText(fnLabel, "Name: " + first + " " + last)
					End If
				EndIf
			Case noteList.handle
				Select EventID()
					Case EVENT_GADGETACTION
						readNote(_noteBySummary(noteList._getText()))
					Case EVENT_GADGETMENU
						'PopupWindowMenu(handle.handle, contextMenu)
				End Select
			Case handle.handle
				Select EventID()
					Case EVENT_WINDOWCLOSE
						_hide()
				End Select
		End Select
		
		For Local i:Note = EachIn noteTypeList
			If i <> Null
				Select EventSource()
					Case i.handle.handle
						Select EventID()
							Case EVENT_WINDOWCLOSE
								If i._save()
									i.summary = i.sumField._getText()
									i.details = i.detField._getText()
									noteList._modify(i.date + " " + i.summary)
								End If
								i._hide()
						End Select
				End Select
			EndIf
		Next
	End Method
	
	Method _noteBySummary:Note(sum:String)
		For Local i:Note = EachIn noteTypeList
			If i.date + " " + i.summary = sum
				Return i
			End If
		Next
	End Method
	
End Type

Function newStudent:Student()
	Local ns:Student = New Student
	ns._init()
	Return ns
End Function

Function newBlankStudent:Student(fn:String, ls:String)
	Local ns:Student = newStudent()
	ns.pos = -1
	ns.first = fn
	ns.last = ls
	ns.balance = 0.00
	ns.grade = 9
	ns._addNote("Registered Student;[" + CurrentDate() + "];1;Registered student into system on " + CurrentDate() + " at " + CurrentTime() + ";eon")
	ns._build()
	Return ns
End Function

Function findByFirst:Student(fn:String)
	Local tmpStudent:Student = newStudent()
	
	Return tmpStudent
End Function

Function findByLast:Student(ln:String)
	dlog "Find by last name " + ln
End Function