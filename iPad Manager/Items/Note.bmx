'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Global noteWinList:TList = New TList
Type Note
	Field handle:Window = New Window
	
	Field sumField:TextField, detField:TextArea, NTypeLabel:TGadget
	
	Field summary:String, nType:Int, details:String, date:String
	
	Method _init()
		handle._new(450, 0, 400, 300, "iPad Note " + date, WINDOW_TOOL + WINDOW_TITLEBAR + WINDOW_MENU)
		ListAddLast(noteWinList, Self)
		
		sumField = newTextField(10, 10, 150, 20, handle)
		SetGadgetFilter(sumField.handle, noComma)
		detField = newTextArea(10, 35, 375, 300 - 35 - 50, handle)
		SetGadgetFilter(detField.handle, noComma)
		NTypeLabel = CreateLabel(_typeByInt(nType), 200, 10, 150, 20, handle.handle)
		SetGadgetText(sumField.handle, summary)
		SetGadgetText(detField.handle, details)
	End Method
	
	Method _typeByInt:String(nType:Int)
		Select nType
			Case 0
				Return "Information"
			Case 1
				Return "Action"
			Default
				Return "Other"
		End Select
	End Method
	
	Method _update()
		Select EventSource()
			Case handle.handle
				
			Case sumField.handle
		End Select
	End Method
	
	Method _save()
		Local choice:Int = 0
		If quickMode
			choice = 1
		ElseIf Confirm("Would you like to save this note?")
			choice = 1
		EndIf
		Return choice
	End Method
	
	Method _hide()
		handle._hide()
	End Method
End Type

Function noComma(event:TEvent, context:Object)
	If event.id = EVENT_KEYCHAR
		Select Event.data
			Case 44
				dlog "You cannot use commas here.", 1
				Return 0
			Case 59
				dlog "You cannot use semicolons here.", 1
				Return 0
			Case 13
				dlog "The return key was used in a note. This may have corrupted the data for the iPad.", 2
				Notify("You cannot use the Enter Key here. The way our database is structured it will corrupt all the data and erase all notes for this iPad. Please backspace to continue on the line you were writing on and do not use the enter key here again.", True)
				Return 0
		End Select
	End If
	Return 1
End Function


Function readNote(tNote:Note)
	If tNote.handle.handle <> Null
		tNote.handle._show()
	Else
		tNote._init()
	EndIf
End Function

Function newNote:Note()
	Local tNote:Note = New Note
	Return tNote
End Function
