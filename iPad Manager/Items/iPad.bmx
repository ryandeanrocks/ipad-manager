'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem


Global ipaddbn:String = dataDir + "ipaddb.csv"
Global iPaddb:TStream = OpenFile(ipaddbn)
If iPaddb = Null
	WriteFile(ipaddbn)
	iPaddb = OpenFile(ipaddbn)
End If


Type iPad
	Field handle:Window = New Window
	
	Field pos:Int
	
	Field bc:String, sn:String, state:String, ownerPos:Int
	
	Field iEditMenu:TGadget, ownerMenu:TGadget, actionMenu:TGadget
	
	Field bcEdit:TGadget, snEdit:TGadget, damageAction:TGadget, repairAction:TGadget, coutAction:TGadget, lostAction:TGadget, stolenAction:TGadget
	
	Field nEditMenu:TGadget, newMenu:TGadget, contextMenu:TGadget
	
	Field infNew:TGadget, actNew:TGadget, misNew:TGadget
	
	Field noteList:ListBox, bcLabel:TGadget, snLabel:TGadget, stLabel:TGadget
	
	Field noteTypeList:TList = New TList
	
	
	Method _init()
		
		handle._new(0, 0, 400, 300, "Student iPad", WINDOW_TOOL + WINDOW_TITLEBAR + WINDOW_MENU)
		ListAddLast(ipadList, Self)
		noteList = newListBox(10, 75, 375, 170, handle)
		
		iEditMenu = CreateMenu("Edit", 0, handle.menu)
			bcEdit = CreateMenu("Barcode", 0, iEditMenu)
			snEdit = CreateMenu("Serial", 0, iEditMenu)
		actionMenu = CreateMenu("Action", 0, handle.menu)
			coutAction = CreateMenu("Check Out", 0, actionMenu)
			CreateMenu("", 0, actionMenu)
			damageAction = CreateMenu("Report Damage", 0, actionMenu)
			repairAction = CreateMenu("Send for Repair", 0, actionMenu)
			lostAction = CreateMenu("Mark as Lost", 0, actionMenu)
			stolenAction = CreateMenu("Mark as Stolen", 0, actionMenu)
			
		
		contextMenu = CreateMenu("contextMenu", 0, Null)
			nEditMenu = CreateMenu("Edit", 0, contextMenu)
			newMenu = CreateMenu("New", 0, contextMenu)
		infNew = CreateMenu("Information", 0, newMenu)
			actNew = CreateMenu("Action", 0, newMenu)
			misNew = CreateMenu("Other", 0, newMenu)
			
		'All possible states Registered, Checked In, Checked Out, Damaged, Lost, Stolen, Sent for Repair
		
		UpdateWindowMenu(handle.handle)
	End Method
	
	Method _show()
		handle._show()
	End Method
	
	Method _build()
		For Local i:String = EachIn noteTypeList
			Local nNote:Note = New Note
			ListAddLast(noteTypeList, nNote.summary)
		Next
		bcLabel = CreateLabel("Barcode: " + bc, 10, 10, 75, 15, handle.handle)
		snLabel = CreateLabel("Serial: " + sn, 10, 25, 125, 15, handle.handle)
		stLabel = CreateLabel("State: " + state, 10, 40, 150, 15, handle.handle)
		
		Select state
			Case "Registered"
				SetGadgetText(coutAction, "Check Out")
			Case "Checked In"
				SetGadgetText(coutAction, "Check Out")
			Case "Checked Out"
				SetGadgetText(coutAction, "Check In")
				ownerMenu = CreateMenu("Owner", 0, handle.menu)
			Case "Sent for Repair"
				SetGadgetText(coutAction, "Check In")
				'FreeGadget(repairAction)
			Case "Damaged"
				SetGadgetText(coutAction, "Check In")
			Case "Lost"
				SetGadgetText(coutAction, "Check In")
				'FreeGadget(lostAction)
			Case "Stolen"
				SetGadgetText(coutAction, "Check In")
			'	FreeGadget(stolenAction)
		End Select
		
		UpdateWindowMenu(handle.handle)
	End Method
	
	Method _hide()
		If quickMode
			_save()
		ElseIf Confirm("Would you like to write your changes back to the database?")
			_save()
		End If
		handle._hide()
		For Local i:TGadget = EachIn handle.gadgetList
			HideGadget(i)
			FreeGadget(i)
		Next
		HideGadget(handle.handle)
		FreeGadget(handle.handle)
		handle = Null
		ListRemove(ipadList, Self)
	End Method
	
	Method _save()
		Local line:String = bc + "," + sn + "," + state + ","
		For Local i:Note = EachIn noteTypeList
			line = line + i.summary + ";" + i.date + ";" + i.nType + ";" + i.details + ";" + "eon,"
		Next
		line = line + "eoe"
		insertIpadEntry(Self, line)
	End Method
	Method _addNote:Note(command:String)
		Local lineArray:TList = convertFromSSV(command)
		Local i:Int = 1
		Local nNote:Note
		For Local n:String = EachIn lineArray
			If i = 1
				nNote = newNote()
				ListAddLast(noteTypeList, nNote)
				nNote.summary = n
			EndIf
			If i = 2
				nNote.date = n
				noteList._add(nNote.date + " " + nNote.summary, GADGETITEM_DEFAULT)
			EndIf
			If i = 3
				nNote.nType = Int n
			EndIf
			If i = 4
				nNote.details = n
			EndIf
			i = i + 1
		Next
		
		
		Return nNote
	End Method
	Method _update()
		Select EventSource()
			Case bcEdit
				Local conf:Int
				If quickMode
					conf = 1
				Else
					conf = Confirm("Are you sure that you want to change the Barcode for iPad " + pos + "?" + nbsp + "This will change the ID in the database and you will no longer find the iPad with the old Barcode. Also if you provide an already used barcode this entry will overwrite the previous entry.")
				EndIf
				If conf
					Local tbc:String = prompt("Please enter new Barcode", 4)
					If tbc <> ""
						_addNote("Changed Barcode;[" + CurrentDate() + "];1;The barcode was changed from [" + bc + "] to [" + tbc + "];eon")
						bc = tbc
						SetGadgetText(bcLabel, "Barcode: " + bc)
					End If
				End If
			Case snEdit
				Local conf:Int
				If quickMode
					conf = 1
				Else
					conf = Confirm("Are you sure that you want to change the Serial for iPad " + pos + "?" + nbsp + "This is dangerous and will change the ID in the database and you will no longer find the iPad with the old serial. Also if you provide an already used barcode this entry will overwrite the previous entry.")
				End If
				If conf
					Local tsn:String = prompt("Please enter new Serial", 12)
					If tsn <> ""
						_addNote("Changed Serial;[" + CurrentDate() + "];1;The serial was changed from [" + sn + "] to [" + tsn + "];eon")
						sn = tsn
						SetGadgetText(snLabel, "Serial: " + sn)
					End If
				EndIf
			Case ownerMenu
				If ownerMenu <> Null
					dlog "SHOW OWNER ACTION HERE"
				EndIf
			Case damageAction
				Local nNote:Note = _addNote("iPad was Damaged;[" + CurrentDate() + "];1;The iPad was reported damaged to us at " + CurrentTime() + ". The owner responsible was [INSERT STUDENT NAME HERE]. The previous state was [" + state + "]. Please add any extra notes describing the damage here.;eon")
				state = "Damaged"
				SetGadgetText(stLabel, "State: " + state)
				nNote._init()
				ActivateGadget(nNote.detField.handle)
			Case repairAction
				Local po:String = prompt("Apple PO Number", 0, 1)
				_addNote("Sent for Repair;[" + CurrentDate() + "];1;The iPad was sent for repair at " + CurrentTime() + " with P.O. number " + po + ". The previous state was [" + state + "];eon")
				state = "Sent for Repair"
				SetGadgetText(stLabel, "State: " + state)
			Case coutAction
				If GadgetText(coutAction) = "Check Out"
					_addNote("Checked Out;[" + CurrentDate() + "];1;The iPad was checked out at " + CurrentTime() + " to [INSERT STUDENT NAME HERE]. The previous state was [" + state + "];eon")
					state = "Checked Out"
					SetGadgetText(stLabel, "State: " + state)
					SetGadgetText(coutAction, "Check In")
					ownerMenu = CreateMenu("Owner", 0, handle.menu)
				Else
					_addNote("Checked In;[" + CurrentDate() + "];1;The iPad was checked in at " + CurrentTime() + " from [INSERT STUDENT NAME HERE]. The previous state was [" + state + "];eon")
					state = "Checked In"
					SetGadgetText(stLabel, "State: " + state)
					SetGadgetText(coutAction, "Check Out")
					FreeGadget(ownerMenu)
				EndIf
				UpdateWindowMenu(handle.handle)
			Case lostAction
				_addNote("iPad was Lost;[" + CurrentDate() + "];1;The iPad was reported lost to us at " + CurrentTime() + " by the owner [INSERT STUDENT NAME HERE]. The previous state was [" + state + "];eon")
				state = "Lost"
				SetGadgetText(stLabel, "State: " + state)
			Case stolenAction
				Local pr:String = prompt("Police Report Reference Number", 0, 1)
				_addNote("iPad was Stolen;[" + CurrentDate() + "];1;The iPad was reported stolen to us at " + CurrentTime() + " by the owner [INSERT STUDENT NAME HERE]. The previous state was [" + state + "]. The police report reference number is " + pr + ";eon")
				state = "Stolen"
				SetGadgetText(stLabel, "State: " + state)
		
			Case noteList.handle
				Select EventID()
					Case EVENT_GADGETACTION
						readNote(_noteBySummary(noteList._getText()))
					Case EVENT_GADGETMENU
						PopupWindowMenu(handle.handle, contextMenu)
				End Select
				
			Case nEditMenu
				If ActiveGadget() = noteList.handle
					readNote(_noteBySummary(noteList._getText()))
				EndIf
			Case infNew
				Local nNote:Note = _addNote("New Note;[" + CurrentDate() + "];0;New Details;eon")
				nNote._init()
			Case actNew
				Local nNote:Note = _addNote("New Note;[" + CurrentDate() + "];1;New Details;eon")
				nNote._init()
			Case misNew
				Local nNote:Note = _addNote("New Note;[" + CurrentDate() + "];2;New Details;eon")
				nNote._init()
			Case handle.handle
				Select EventID()
					Case EVENT_WINDOWCLOSE
						_hide()
				End Select
		End Select
		For Local i:Note = EachIn noteTypeList
			If i <> Null
				Select EventSource()
					Case i.handle.handle
						Select EventID()
							Case EVENT_WINDOWCLOSE
								If i._save()
									i.summary = i.sumField._getText()
									i.details = i.detField._getText()
									noteList._modify(i.date + " " + i.summary)
								End If
								i._hide()
						End Select
				End Select
			EndIf
		Next
	End Method
	Method _noteBySummary:Note(sum:String)
		For Local i:Note = EachIn noteTypeList
			If i.date + " " + i.summary = sum
				Return i
			End If
		Next
	End Method
End Type

Function insertIpadEntry(nIpad:iPad, line:String)
	Local entryList:TList = New TList
	Repeat
		ListAddLast(entryList, ReadLine(iPaddb))
	Until Eof(iPaddb)
	CloseFile(iPaddb)
	iPaddb = OpenFile(iPaddbn)
	Local b:Int = 1
	For Local i:String = EachIn entryList
		If b = nIpad.pos
			WriteLine iPaddb, line
		Else
			WriteLine iPaddb, i
		EndIf
		b = b + 1
	Next
	If nIpad.pos = -1
		WriteLine iPaddb, line
	End If
	iPaddb = resetFile(iPaddb, ipaddbn)
End Function


Function newIpad:iPad()
	Local ni:iPad = New iPad
	ni._init()
	Return ni
End Function

Function newBlankIpadBC:iPad(bc:String)
	Local ni:iPad = newIpad()
	ni.pos = -1
	ni.bc = bc
	ni.sn = "Need Serial"
	ni.state = "Registered"
	ni._addNote("Registered iPad;[" + CurrentDate() + "];1;Registered iPad on " + CurrentDate() + " at " + CurrentTime() + ";eon")
	ni._build()
End Function

Function newBlankIpadSN:iPad(sn:String)
	Local ni:iPad = newIpad()
	ni.pos = -1
	ni.bc = "----"
	ni.sn = sn
	ni.state = "Registered"
	ni._addNote("Registered iPad;[" + CurrentDate() + "];1;Registered iPad on " + CurrentDate() + " at " + CurrentTime() + ";eon")
	ni._build()
End Function


Function findBySN:iPad(sn:String)
	For Local t:iPad = EachIn ipadList
		If t.sn = sn
			iPaddb = resetFile(iPaddb, ipaddbn)
			Notify "This iPad is currently being accessed elsewhere, please try again later."
			Return Null
		End If
	Next
	Local file:TStream = ipadDB
	Local line:String
	Local lineArray:TList
	Local pos:Int = 1
	Repeat
		line = ReadLine(file)
		lineArray = convertFromCSV(line)
		If ListContains(lineArray, sn)
			Local l:Int = 1
			Local ni:iPad = newIpad()
			ni.pos = pos
			For Local i:String = EachIn lineArray
				If l = 1
					ni.bc = i
				ElseIf l = 2
					ni.sn = i
				ElseIf l = 3
					ni.state = i
				Else
					ni._addNote(i)
				End If
				l = l + 1
			Next
			iPaddb = resetFile(iPaddb, ipaddbn)
			ni._build()
			Return ni
		End If
		pos = pos + 1
	Until Eof(file)
	If quickMode
		newBlankIpadSN(sn)
	ElseIf Confirm ("There is no entry in 'ipaddb.csv' with the Serial [" + sn + "] Create?")
		newBlankIpadSN(sn)
	EndIf
	iPaddb = resetFile(iPaddb, ipaddbn)
	Return Null
	
End Function

Function findByBC:iPad(bc:String)
	For Local t:iPad = EachIn ipadList
		If t.bc = bc
			iPaddb = resetFile(iPaddb, ipaddbn)
			Notify "This iPad is currently being accessed elsewhere, please try again later."
			Return Null
		End If
	Next
	Local file:TStream = ipadDB
	Local line:String
	Local lineArray:TList
	Local pos:Int = 1
	Repeat
		line = ReadLine(file)
		lineArray = convertFromCSV(line)
		If ListContains(lineArray, bc)
			Local l:Int = 1
			Local ni:iPad = newIpad()
			ni.pos = pos
			For Local i:String = EachIn lineArray
				If l = 1
					ni.bc = i
				ElseIf l = 2
					ni.sn = i
				ElseIf l = 3
					ni.state = i
				Else
					ni._addNote(i)
				End If
				l = l + 1
			Next
			iPaddb = resetFile(iPaddb, ipaddbn)
			ni._build()
			Return ni
		End If
		pos = pos + 1
	Until Eof(file)
	If quickMode
		newBlankIpadBC(bc)
	ElseIf Confirm ("There is no entry in 'ipaddb.csv' with the Barcode [" + bc + "] Create?")
		newBlankIpadBC(bc)
	EndIf
	iPaddb = resetFile(iPaddb, ipaddbn)
	Return Null
	
End Function