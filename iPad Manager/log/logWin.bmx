'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type logWin
	Field handle:Window = New Window
	
	Field entryList:ListBox
	
	Method _init()
		handle._new(240, 800, 800, 180, "Debug Log", WINDOW_TOOL + WINDOW_TITLEBAR)
		_populate()
		If dm = False
			handle._hide()
		EndIf
	End Method
	Method _populate()
		entryList = newListBox(10, 10, 775, 140, handle)
		_new("Started " + ProgramInfo, 0)
	End Method
	Method _new(line:String, style)
		Select style
			Case 0
				entryList._add(CurrentTime() + ": [INFO] " + line, GADGETITEM_DEFAULT)
		    Case 1
				entryList._add(CurrentTime() + ": [WARNING] " + line, GADGETITEM_DEFAULT)
			Case 2
				entryList._add(CurrentTime() + ": [ERROR] " + line, GADGETITEM_DEFAULT)
		End Select
		
	End Method
	Method _update()
		If EventSource() = handle.handle
			_new("logWin triggered an event", 0)
			Select EventID()
				Case EVENT_WINDOWCLOSE
					_end()
			End Select
			Return
		End If
	End Method
	Method _end()
		_new("Sending kill signal", 0)
		
		For Local g:TGadget = EachIn handle.gadgetList
			HideGadget(g)
			FreeGadget(g)
		Next
		HideGadget(handle.handle)
		FreeGadget(handle.handle)
		handle = Null
		ListRemove(winList, Self)
		Return
	End Method
End Type

Function dlog(line:String, style = 0)
	debugWin._new(line, style)
End Function
