'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type ListBox
	Field handle:TGadget
	Method _init(x:Int, y:Int, w:Int, h:Int, win:Window, style:Int)
		handle = CreateListBox(x, y, w, h, win.handle, style)
		ListAddLast(win.gadgetList, handle)
	End Method
	Method _add(item:String, df = 0)
		AddGadgetItem(handle, item, df)
	End Method
	Method _modify(item:String)
		ModifyGadgetItem(handle, SelectedGadgetItem(handle), item)
	End Method
	Method _remove()
		RemoveGadgetItem(handle, SelectedGadgetItem(handle))
	End Method
	Method _getText:String()
		Return GadgetItemText(handle, SelectedGadgetItem(handle))
	End Method
End Type

Function newListBox:ListBox(x:Int, y:Int, w:Int, h:Int, win:Window, style:Int = 0)
	Local retLB:ListBox = New ListBox
	retLB._init(x, y, w, h, win, style)
	Return retLB
End Function
