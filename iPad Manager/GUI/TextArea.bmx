'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type TextArea
	Field handle:TGadget
	Method _init(x:Int, y:Int, w:Int, h:Int, win:Window, readOnly = False, wordWrap = True)
		Local style:Int
		If readOnly
			style = TEXTAREA_READONLY
		End If
		If wordWrap
			style = TEXTAREA_WORDWRAP
		End If
		handle = CreateTextArea(x, y, w, h, win.handle, style)
		ListAddLast(win.gadgetList, handle)
	End Method
	Method _getText:String()
		Return TextAreaText(handle)
	End Method
	Method _clear()
		SetGadgetText(handle, "")
	End Method
End Type

Function newTextArea:TextArea(x:Int, y:Int, w:Int, h:Int, win:Window, mask:Int = False)
	Local retTF:TextArea = New TextArea
	retTF._init(x, y, w, h, win, mask)
	Return retTF
End Function