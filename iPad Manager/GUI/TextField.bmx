'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type TextField
	Field handle:TGadget
	Method _init(x:Int, y:Int, w:Int, h:Int, win:Window, mask:Int = False)
		handle = CreateTextField(x, y, w, h, win.handle, mask)
		ListAddLast(win.gadgetList, handle)
	End Method
	Method _getText:String()
		Return TextFieldText(handle)
	End Method
	Method _clear()
		SetGadgetText(handle, "")
	End Method
End Type

Function newTextField:TextField(x:Int, y:Int, w:Int, h:Int, win:Window, mask:Int = False)
	Local retTF:TextField = New TextField
	retTF._init(x, y, w, h, win, mask)
	Return retTF
End Function

