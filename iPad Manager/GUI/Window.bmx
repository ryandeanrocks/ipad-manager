'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type Window
	Field handle:TGadget
	Field id:String
	Field gadgetList:TList = New TList
	Field menu:TGadget
	
	Method _new(x:Int, y:Int, w:Int, h:Int, title:String, style:Int)
		handle = CreateWindow(title, x, y, w, h, Null, style)
		ListAddLast(winList, Self)
		menu = WindowMenu(handle)
	End Method
	Method _show()
		ShowGadget(handle)
	End Method
	Method _hide()
		HideGadget(handle)
	End Method
	Method _update()
		
	End Method
End Type
