'This BMX file was edited with BLIde ( http://www.blide.org )
Rem
	bbdoc:Undocumented type
End Rem
Type Button
	Field handle:TGadget
	Field state:Int
	Method _init(label:String, x:Int, y:Int, w:Int, h:Int, win:Window, style:Int = BUTTON_PUSH)
		handle = CreateButton(label, x, y, w, h, win.handle, style)
		ListAddLast(win.gadgetList, handle)
	End Method
	Method _state()
		state = Not state
		SetButtonState(handle, state)
		Return ButtonState(handle)
	End Method
	Method _cstate()
		Return state
	End Method
End Type

Function newButton:Button(label:String, x:Int, y:Int, w:Int, h:Int, win:Window, style:Int = BUTTON_PUSH)
	Local retButton:Button = New Button
	retButton._init(label, x, y, w, h, win, style)
	Return retButton
End Function
